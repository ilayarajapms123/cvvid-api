<?php

/**
 * This is the model class for table "cv16_jobs".
 *
 * The followings are the available columns in table 'cv16_jobs':
 * @property string $id
 * @property string $owner_id
 * @property string $owner_type
 * @property string $user_id
 * @property string $industry_id
 * @property string $company_name
 * @property string $title
 * @property string $salary_type
 * @property string $salary_min
 * @property string $salary_max
 * @property string $end_date
 * @property string $skill_category_id
 * @property string $location
 * @property string $location_lat
 * @property string $location_lng
 * @property string $type
 * @property string $description
 * @property string $additional
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $closed_date
 * @property string $successful_applicant_id
 * @property string $num_views
 *
 * The followings are the available model relations:
 * @property JobApplications[] $jobApplications
 * @property JobSkill[] $jobSkills
 * @property JobViews[] $jobViews
 * @property Industries $industry
 * @property SkillCategories $skillCategory
 * @property Users $successfulApplicant
 * @property Users $user
 */
class Jobs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_jobs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('owner_id', 'required','message'=>'The employer field is required.'),
			array('title, description', 'required','message'=>'The {attribute} field is required.'),
			array('owner_id, user_id, industry_id, salary_min, salary_max, skill_category_id, successful_applicant_id, num_views', 'length', 'max'=>10),
			array('owner_type, company_name, title, salary_type, location, location_lat, location_lng, type, status', 'length', 'max'=>255),
			array('end_date, description, additional, created_at, updated_at, closed_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, owner_id, owner_type, user_id, industry_id, company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description, additional, created_at, updated_at, status, closed_date, successful_applicant_id, num_views', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jobApplications' => array(self::HAS_MANY, 'JobApplications', 'job_id'),
			'jobSkills' => array(self::HAS_MANY, 'JobSkill', 'job_id'),
			'jobViews' => array(self::HAS_MANY, 'JobViews', 'job_id'),
			'industry' => array(self::BELONGS_TO, 'Industries', 'industry_id'),
			'skillCategory' => array(self::BELONGS_TO, 'SkillCategories', 'skill_category_id'),
			'successfulApplicant' => array(self::BELONGS_TO, 'Users', 'successful_applicant_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'employer' => array(self::BELONGS_TO, 'Employers', 'owner_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'owner_id' => 'Owner',
			'owner_type' => 'Owner Type',
			'user_id' => 'User',
			'industry_id' => 'Industry',
			'company_name' => 'Company Name',
			'title' => 'Title',
			'salary_type' => 'Salary Type',
			'salary_min' => 'Salary Min',
			'salary_max' => 'Salary Max',
			'end_date' => 'End Date',
			'skill_category_id' => 'Skill Category',
			'location' => 'Location',
			'location_lat' => 'Location Lat',
			'location_lng' => 'Location Lng',
			'type' => 'Type',
			'description' => 'Description',
			'additional' => 'Additional',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'status' => 'Status',
			'closed_date' => 'Closed Date',
			'successful_applicant_id' => 'Successful Applicant',
			'num_views' => 'Num Views',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('owner_id',$this->owner_id,true);
		$criteria->compare('owner_type',$this->owner_type,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('industry_id',$this->industry_id,true);
		$criteria->compare('company_name',$this->company_name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('salary_type',$this->salary_type,true);
		$criteria->compare('salary_min',$this->salary_min,true);
		$criteria->compare('salary_max',$this->salary_max,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('skill_category_id',$this->skill_category_id,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('location_lat',$this->location_lat,true);
		$criteria->compare('location_lng',$this->location_lng,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('additional',$this->additional,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('closed_date',$this->closed_date,true);
		$criteria->compare('successful_applicant_id',$this->successful_applicant_id,true);
		$criteria->compare('num_views',$this->num_views,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchByEmployer($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('owner_id',$this->owner_id,true);
		$criteria->compare('owner_type',$this->owner_type,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('industry_id',$this->industry_id,true);
		$criteria->compare('company_name',$this->company_name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('salary_type',$this->salary_type,true);
		$criteria->compare('salary_min',$this->salary_min,true);
		$criteria->compare('salary_max',$this->salary_max,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('skill_category_id',$this->skill_category_id,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('location_lat',$this->location_lat,true);
		$criteria->compare('location_lng',$this->location_lng,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('additional',$this->additional,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('closed_date',$this->closed_date,true);
		$criteria->compare('successful_applicant_id',$this->successful_applicant_id,true);
		$criteria->compare('num_views',$this->num_views,true);
                
                $criteria->addCondition('owner_id='.$id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Jobs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
