<?php

class Membership {

    /**
     * @var array
     */
    public static $plans = [];

    /**
     * @return bool
     */
    public static function freeTrailEnabled()
    {
        return true;
    }
    
    public static function plan($name, $id)
    {
        static::$plans[] = $plan = new Plan($name, $id);

        return $plan;
    }
    
    public static function onTrail($trail)
    {
        $now = new DateTime();
        
        $diff =  date_diff(date_create($now->format('Y-m-d')), date_create($trail));
        
        if($diff->format("%R%a") < 0){
            return "";
        } elseif($trail==""){
            return "";
        }else{
            return TRUE;
        }
        
    }
}