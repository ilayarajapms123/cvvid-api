<?php

/**
 * This is the model class for table "cv16_tutor_groups".
 *
 * The followings are the available columns in table 'cv16_tutor_groups':
 * @property string $id
 * @property string $institution_id
 * @property string $teacher_id
 * @property string $name
 * @property string $location
 * @property string $location_lat
 * @property string $location_lng
 * @property string $graduation_date
 * @property integer $account_limit
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * The followings are the available model relations:
 * @property Institutions $institution
 * @property Users $teacher
 * @property TutorGroupsStudents[] $tutorGroupsStudents
 */
class TutorGroups extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_tutor_groups';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('institution_id, name', 'required'),
			array('account_limit', 'numerical', 'integerOnly'=>true),
			array('institution_id, teacher_id', 'length', 'max'=>10),
			array('name, location, location_lat, location_lng', 'length', 'max'=>255),
			array('graduation_date, created_at, updated_at, deleted_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, institution_id, teacher_id, name, location, location_lat, location_lng, graduation_date, account_limit, created_at, updated_at, deleted_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'institution' => array(self::BELONGS_TO, 'Institutions', 'institution_id'),
			'teacher' => array(self::BELONGS_TO, 'Users', 'teacher_id'),
			'tutorGroupsStudents' => array(self::HAS_MANY, 'TutorGroupsStudents', 'tutor_group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'institution_id' => 'Institution',
			'teacher_id' => 'Teacher',
			'name' => 'Name',
			'location' => 'Location',
			'location_lat' => 'Location Lat',
			'location_lng' => 'Location Lng',
			'graduation_date' => 'Graduation Date',
			'account_limit' => 'Account Limit',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('institution_id',$this->institution_id,true);
		$criteria->compare('teacher_id',$this->teacher_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('location_lat',$this->location_lat,true);
		$criteria->compare('location_lng',$this->location_lng,true);
		$criteria->compare('graduation_date',$this->graduation_date,true);
		$criteria->compare('account_limit',$this->account_limit);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);

		$criteria->addCondition('deleted_at is null');
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchByName($namearray)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('institution_id',$this->institution_id,true);
	    $criteria->compare('teacher_id',$this->teacher_id,true);
	    $criteria->compare('name',$this->name,true);
	    $criteria->compare('location',$this->location,true);
	    $criteria->compare('location_lat',$this->location_lat,true);
	    $criteria->compare('location_lng',$this->location_lng,true);
	    $criteria->compare('graduation_date',$this->graduation_date,true);
	    $criteria->compare('account_limit',$this->account_limit);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    $criteria->compare('deleted_at',$this->deleted_at,true);
	    
	    $criteria->addInCondition('teacher_id', $namearray,'AND');
	    $criteria->addCondition('deleted_at is null');
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,
	    ));
	}
	
	
	public function searchTutorsByInstitution($id)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('institution_id',$this->institution_id,true);
	    $criteria->compare('teacher_id',$this->teacher_id,true);
	    $criteria->compare('name',$this->name,true);
	    $criteria->compare('location',$this->location,true);
	    $criteria->compare('location_lat',$this->location_lat,true);
	    $criteria->compare('location_lng',$this->location_lng,true);
	    $criteria->compare('graduation_date',$this->graduation_date,true);
	    $criteria->compare('account_limit',$this->account_limit);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    $criteria->compare('deleted_at',$this->deleted_at,true);
	    
	    $criteria->addCondition('institution_id ='.$id.' and deleted_at is null');
	    
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,'pagination'=>false
	    ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TutorGroups the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getTeacher($id)
	{
	    $usermodel = Users::model()->findByPk($id);
	    return $usermodel->forenames." ".$usermodel->surname;
	}
	
	public function getCount($id)
	{
	    $tgmodel = TutorGroups::model()->findByPk($id);
	    $instmodel = Institutions::model()->findByPk($tgmodel->institution_id);
	    $tgs_cnt = TutorGroupsStudents::model()->countByAttributes(array('tutor_group_id'=>$id));
	    return $tgs_cnt." / ".$tgmodel->account_limit;
	}
}
