<?php

/**
 * This is the model class for table "cv16_tutor_groups_students".
 *
 * The followings are the available columns in table 'cv16_tutor_groups_students':
 * @property string $id
 * @property string $institution_id
 * @property string $tutor_group_id
 * @property string $student_id
 * @property string $graduation_date
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property Institutions $institution
 * @property Users $student
 * @property TutorGroups $tutorGroup
 */
class TutorGroupsStudents extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_tutor_groups_students';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('institution_id, tutor_group_id, student_id', 'required'),
			array('institution_id, tutor_group_id, student_id', 'length', 'max'=>10),
			array('graduation_date, created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, institution_id, tutor_group_id, student_id, graduation_date, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'institution' => array(self::BELONGS_TO, 'Institutions', 'institution_id'),
			'student' => array(self::BELONGS_TO, 'Users', 'student_id'),
			'tutorGroup' => array(self::BELONGS_TO, 'TutorGroups', 'tutor_group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'institution_id' => 'Institution',
			'tutor_group_id' => 'Tutor Group',
			'student_id' => 'Student',
			'graduation_date' => 'Graduation Date',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('institution_id',$this->institution_id,true);
		$criteria->compare('tutor_group_id',$this->tutor_group_id,true);
		$criteria->compare('student_id',$this->student_id,true);
		$criteria->compare('graduation_date',$this->graduation_date,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function searchByName($namearray)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('institution_id',$this->institution_id,true);
	    $criteria->compare('tutor_group_id',$this->tutor_group_id,true);
	    $criteria->compare('student_id',$this->student_id,true);
	    $criteria->compare('graduation_date',$this->graduation_date,true);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    
	    $criteria->addInCondition('student_id', $namearray,'AND');
	    
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,
	    ));
	}
	
	public function searchByInstitution($id)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('institution_id',$this->institution_id,true);
	    $criteria->compare('tutor_group_id',$this->tutor_group_id,true);
	    $criteria->compare('student_id',$this->student_id,true);
	    $criteria->compare('graduation_date',$this->graduation_date,true);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    
	    $criteria->addCondition('institution_id ='.$id);
	    
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,
	    ));
	}
	
	public function searchStudents($id)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('institution_id',$this->institution_id,true);
	    $criteria->compare('tutor_group_id',$this->tutor_group_id,true);
	    $criteria->compare('student_id',$this->student_id,true);
	    $criteria->compare('graduation_date',$this->graduation_date,true);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    
	    $criteria->addCondition('tutor_group_id ='.$id);
	    
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,'pagination'=>false
	    ));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TutorGroupsStudents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getTeacher($id)
	{
	    $usermodel = Users::model()->findByPk($id);
            if($usermodel != null)
	        return $usermodel->forenames." ".$usermodel->surname;
            else 
                return "";
	}
	
	public function getCount($id)
	{
	    $tgmodel = TutorGroups::model()->findByPk($id);
	    $tgs_cnt = TutorGroupsStudents::model()->countByAttributes(array('tutor_group_id'=>$id));
	    return $tgs_cnt." / ".$tgmodel->account_limit;
	}
	public function actiongetTeacher($id)
	{
	    $teachermodel = Users::model()->findByPk($id);
	    return $teachermodel->forenames." ".$teachermodel->surname;
	}
	public function getInsid()
	{
	    return Yii::app()->user->getState('instid');
	}
}
