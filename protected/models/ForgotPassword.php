<?php
/**
 * ForgotPassword class.
 * ForgotPassword is the data structure for keeping
 * user forgot password form data. It is used by the 'login' action of 'SiteController'.
 */
class ForgotPassword extends CFormModel
{
	public $email;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// password are required
			array(' email', 'required')
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'email'=>'Email',
		);
	}

}
