<?php

/**
 * This is the model class for table "cv16_job_applications".
 *
 * The followings are the available columns in table 'cv16_job_applications':
 * @property string $id
 * @property string $job_id
 * @property string $user_id
 * @property string $message
 * @property integer $status
 * @property string $interview_date
 * @property string $accepted_date
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * The followings are the available model relations:
 * @property Jobs $job
 * @property Users $user
 */
class JobApplications extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_job_applications';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('job_id, user_id', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('job_id, user_id', 'length', 'max'=>10),
			array('message, interview_date, accepted_date, created_at, updated_at, deleted_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, job_id, user_id, message, status, interview_date, accepted_date, created_at, updated_at, deleted_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'job' => array(self::BELONGS_TO, 'Jobs', 'job_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'job_id' => 'Job',
			'user_id' => 'User',
			'message' => 'Message',
			'status' => 'Status',
			'interview_date' => 'Interview Date',
			'accepted_date' => 'Accepted Date',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('job_id',$this->job_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('interview_date',$this->interview_date,true);
		$criteria->compare('accepted_date',$this->accepted_date,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function searchByID($id)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('job_id',$this->job_id,true);
	    $criteria->compare('user_id',$this->user_id,true);
	    $criteria->compare('message',$this->message,true);
	    $criteria->compare('status',$this->status);
	    $criteria->compare('interview_date',$this->interview_date,true);
	    $criteria->compare('accepted_date',$this->accepted_date,true);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    $criteria->compare('deleted_at',$this->deleted_at,true);
	    
	    $criteria->addCondition('user_id='.$id);
	    
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,
	    ));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JobApplications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getCompany($id)
	{
	    $empmodel = Employers::model()->findByPk($id);
	    return $empmodel->name;
	}
        public function getAppCount($id){
           return JobApplications::model()->countByAttributes(array('status'=> 0, 'job_id'=>$id));
        }
        public function getShortCount($id){
           return JobApplications::model()->countByAttributes(array('status'=> 1, 'job_id'=>$id));
        }
        public function getInterCount($id){
           return JobApplications::model()->countByAttributes(array('status'=> 2, 'job_id'=>$id));
        }
}
