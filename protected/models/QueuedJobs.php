<?php

/**
 * This is the model class for table "cv16_queued_jobs".
 *
 * The followings are the available columns in table 'cv16_queued_jobs':
 * @property string $id
 * @property string $queue
 * @property string $payload
 * @property integer $attempts
 * @property integer $reserved
 * @property string $reserved_at
 * @property string $available_at
 * @property string $created_at
 */
class QueuedJobs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_queued_jobs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('queue, payload, attempts, reserved, available_at, created_at', 'required'),
			array('attempts, reserved', 'numerical', 'integerOnly'=>true),
			array('queue', 'length', 'max'=>255),
			array('reserved_at, available_at, created_at', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, queue, payload, attempts, reserved, reserved_at, available_at, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'queue' => 'Queue',
			'payload' => 'Payload',
			'attempts' => 'Attempts',
			'reserved' => 'Reserved',
			'reserved_at' => 'Reserved At',
			'available_at' => 'Available At',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('queue',$this->queue,true);
		$criteria->compare('payload',$this->payload,true);
		$criteria->compare('attempts',$this->attempts);
		$criteria->compare('reserved',$this->reserved);
		$criteria->compare('reserved_at',$this->reserved_at,true);
		$criteria->compare('available_at',$this->available_at,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QueuedJobs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
