<?php

/**
 * This is the model class for table "cv16_users".
 *
 * The followings are the available columns in table 'cv16_users':
 * @property string $id
 * @property string $type
 * @property string $forenames
 * @property string $surname
 * @property string $email
 * @property string $password
 * @property string $dob
 * @property string $status
 * @property integer $is_premium
 * @property string $tel
 * @property string $mobile
 * @property string $current_job
 * @property string $location
 * @property string $location_lat
 * @property string $location_lng
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $stripe_active
 * @property string $stripe_id
 * @property string $stripe_subscription
 * @property string $stripe_plan
 * @property string $last_four
 * @property string $card_expiry
 * @property string $card_expiry_sent
 * @property string $trial_ends_at
 * @property string $subscription_ends_at
 *
 * The followings are the available model relations:
 * @property Activities[] $activities
 * @property EmployerUsers[] $employerUsers
 * @property Employers[] $employers
 * @property InstitutionUsers[] $institutionUsers
 * @property JobApplications[] $jobApplications
 * @property Jobs[] $jobs
 * @property Jobs[] $jobs1
 * @property TutorGroups[] $tutorGroups
 * @property TutorGroupsStudents[] $tutorGroupsStudents
 * @property UserDocuments[] $userDocuments
 * @property UserExperiences[] $userExperiences
 * @property UserHobbies[] $userHobbies
 * @property UserLanguages[] $userLanguages
 * @property UserQualifications[] $userQualifications
 */
class Users extends CActiveRecord {

    //public $password;
    public $confirmpassword;
    public $captcha;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cv16_users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('forenames, surname, email', 'required', 'message' => 'The {attribute} field is required.'),
            array('password', 'required', 'message' => 'The {attribute} field is required.','on'=>'insert'),
            array('email', 'unique', 'attributeName' => 'email', 'message' => 'This Email is already in use', 'on' => 'insert'),
            array('email', 'match', 'pattern' => '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD'),
            array('is_premium, stripe_active', 'numerical', 'integerOnly' => true),
            array('captcha', 'required', 'message' => 'The captcha form field is required', 'on' => 'captcha'),
            array('type, forenames, surname, email, tel,  current_job, location, location_lat, location_lng, stripe_id, stripe_subscription', 'length', 'max' => 255),
            array('mobile', 'numerical', 'integerOnly' => true),
            array('password, confirmpassword', 'length', 'min' => 6),
           // array('confirmpassword', 'compare', 'compareAttribute'=>'password' , 'on'=>'confirm'),
          // array('confirmpassword', 'required', 'message' => 'The password confirmation does not match.','on'=>'confirmpassword'),
            array('status, remember_token, stripe_plan', 'length', 'max' => 100),
            array('last_four', 'length', 'max' => 4),
            array('dob, created_at, updated_at, deleted_at,email, card_expiry, card_expiry_sent, trial_ends_at, subscription_ends_at', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, type, forenames, surname, email, password, dob, status, is_premium, tel, mobile, current_job, location, location_lat, location_lng, remember_token, created_at, updated_at, deleted_at, stripe_active, stripe_id, stripe_subscription, stripe_plan, last_four, card_expiry, card_expiry_sent, trial_ends_at, subscription_ends_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'activities' => array(self::HAS_MANY, 'Activities', 'user_id'),
            'employerUsers' => array(self::HAS_MANY, 'EmployerUsers', 'user_id'),
            'employers' => array(self::HAS_MANY, 'Employers', 'user_id'),
            'institutionUsers' => array(self::HAS_MANY, 'InstitutionUsers', 'user_id'),
            'jobApplications' => array(self::HAS_MANY, 'JobApplications', 'user_id'),
            'jobs' => array(self::HAS_MANY, 'Jobs', 'successful_applicant_id'),
            'jobs1' => array(self::HAS_MANY, 'Jobs', 'user_id'),
            'tutorGroups' => array(self::HAS_MANY, 'TutorGroups', 'teacher_id'),
            'tutorGroupsStudents' => array(self::HAS_MANY, 'TutorGroupsStudents', 'student_id'),
            'userDocuments' => array(self::HAS_MANY, 'UserDocuments', 'user_id'),
            'userExperiences' => array(self::HAS_MANY, 'UserExperiences', 'user_id'),
            'userHobbies' => array(self::HAS_MANY, 'UserHobbies', 'user_id'),
            'userLanguages' => array(self::HAS_MANY, 'UserLanguages', 'user_id'),
            'userQualifications' => array(self::HAS_MANY, 'UserQualifications', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'forenames' => 'Forenames',
            'surname' => 'Surname',
            'email' => 'Email',
            'password' => 'Password',
            'dob' => 'Dob',
            'status' => 'Status',
            'is_premium' => 'Is Premium',
            'tel' => 'Telephone',
            'mobile' => 'Mobile',
            'current_job' => 'Current Job',
            'location' => 'Location',
            'location_lat' => 'Location Lat',
            'location_lng' => 'Location Lng',
            'remember_token' => 'Remember Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'stripe_active' => 'Stripe Active',
            'stripe_id' => 'Stripe',
            'stripe_subscription' => 'Stripe Subscription',
            'stripe_plan' => 'Stripe Plan',
            'last_four' => 'Last Four',
            'card_expiry' => 'Card Expiry',
            'card_expiry_sent' => 'Card Expiry Sent',
            'trial_ends_at' => 'Trial Ends At',
            'subscription_ends_at' => 'Subscription Ends At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($trial, $paid, $basic) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('forenames', $this->forenames, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('dob', $this->dob, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('is_premium', $this->is_premium);
        $criteria->compare('tel', $this->tel, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('current_job', $this->current_job, true);
        $criteria->compare('location', $this->location, true);
        $criteria->compare('location_lat', $this->location_lat, true);
        $criteria->compare('location_lng', $this->location_lng, true);
        $criteria->compare('remember_token', $this->remember_token, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('deleted_at', $this->deleted_at, true);
        $criteria->compare('stripe_active', $this->stripe_active);
        $criteria->compare('stripe_id', $this->stripe_id, true);
        $criteria->compare('stripe_subscription', $this->stripe_subscription, true);
        $criteria->compare('stripe_plan', $this->stripe_plan, true);
        $criteria->compare('last_four', $this->last_four, true);
        $criteria->compare('card_expiry', $this->card_expiry, true);
        $criteria->compare('card_expiry_sent', $this->card_expiry_sent, true);
        $criteria->compare('trial_ends_at', $this->trial_ends_at, true);
        $criteria->compare('subscription_ends_at', $this->subscription_ends_at, true);


        if (Yii::app()->user->getState('role') == 'admin') {
            $criteria->condition = "type = 'candidate' AND deleted_at is null";

            if ($trial == 1)
                $criteria->addCondition('trial_ends_at is not null');
            else if ($paid == 1)
                $criteria->addCondition('stripe_id > 0');
            else if ($basic == 1)
                $criteria->addCondition('trial_ends_at is null and stripe_id is null');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchByName($namearray) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('forenames', $this->forenames, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('dob', $this->dob, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('is_premium', $this->is_premium);
        $criteria->compare('tel', $this->tel, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('current_job', $this->current_job, true);
        $criteria->compare('location', $this->location, true);
        $criteria->compare('location_lat', $this->location_lat, true);
        $criteria->compare('location_lng', $this->location_lng, true);
        $criteria->compare('remember_token', $this->remember_token, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('deleted_at', $this->deleted_at, true);
        $criteria->compare('stripe_active', $this->stripe_active);
        $criteria->compare('stripe_id', $this->stripe_id, true);
        $criteria->compare('stripe_subscription', $this->stripe_subscription, true);
        $criteria->compare('stripe_plan', $this->stripe_plan, true);
        $criteria->compare('last_four', $this->last_four, true);
        $criteria->compare('card_expiry', $this->card_expiry, true);
        $criteria->compare('card_expiry_sent', $this->card_expiry_sent, true);
        $criteria->compare('trial_ends_at', $this->trial_ends_at, true);
        $criteria->compare('subscription_ends_at', $this->subscription_ends_at, true);

        $criteria->addInCondition('id', $namearray, 'AND');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmins() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('forenames', $this->forenames, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('dob', $this->dob, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('is_premium', $this->is_premium);
        $criteria->compare('tel', $this->tel, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('current_job', $this->current_job, true);
        $criteria->compare('location', $this->location, true);
        $criteria->compare('location_lat', $this->location_lat, true);
        $criteria->compare('location_lng', $this->location_lng, true);
        $criteria->compare('remember_token', $this->remember_token, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('deleted_at', $this->deleted_at, true);
        $criteria->compare('stripe_active', $this->stripe_active);
        $criteria->compare('stripe_id', $this->stripe_id, true);
        $criteria->compare('stripe_subscription', $this->stripe_subscription, true);
        $criteria->compare('stripe_plan', $this->stripe_plan, true);
        $criteria->compare('last_four', $this->last_four, true);
        $criteria->compare('card_expiry', $this->card_expiry, true);
        $criteria->compare('card_expiry_sent', $this->card_expiry_sent, true);
        $criteria->compare('trial_ends_at', $this->trial_ends_at, true);
        $criteria->compare('subscription_ends_at', $this->subscription_ends_at, true);

        $criteria->addCondition("type = 'admin' and deleted_at is null");

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
