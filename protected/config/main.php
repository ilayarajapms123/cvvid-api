<?php
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'CVVID',
	'theme'=>'Dev',
	// preloading 'log' component
	'preload'=>array('log'),
		'aliases' => array(
				//	'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'), // change this if necessary
				'booster' => realpath(__DIR__ . '/../extensions/booster'),
		),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		 
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'admin',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
			'bootstrap'=>array(
					'class'=>'bootstrap.components.Bootstrap',
			),
		'user'=>array(
			//'loginUrl'=>array('admin'),
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			 'showScriptName'=>false,
    		// 'caseSensitive'=>false,   
			'rules'=>array(
				//'<controller:\w+>/' => '<controller>/login',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
		
			),
		),
		

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),
			
		'ePdf' => array(
					'class'         => 'ext.yii-pdf.EYiiPdf',
					'params'        => array(
							'mpdf'     => array(
									'librarySourcePath' => 'application.extensions.vendors.mpdf.*',
									'constants'         => array(
											'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
									),
									'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
									/*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
									 'mode'              => '', //  This parameter specifies the mode of the new document.
											'format'            => 'A4', // format A4, A5, ...
											'default_font_size' => 0, // Sets the default document font size in points (pt)
											'default_font'      => '', // Sets the default font-family for the new document.
											'mgl'               => 15, // margin_left. Sets the page margins for the new document.
											'mgr'               => 15, // margin_right
											'mgt'               => 16, // margin_top
											'mgb'               => 16, // margin_bottom
											'mgh'               => 9, // margin_header
											'mgf'               => 9, // margin_footer
											'orientation'       => 'P', // landscape or portrait orientation
									)*/
							),
							'HTML2PDF' => array(
									'librarySourcePath' => 'application.extensions.vendors.html2pdf.*',
									'classFile'         => 'Html2Pdf.php', // For adding to Yii::$classMap
									/*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
									 'orientation' => 'P', // landscape or portrait orientation
											'format'      => 'A4', // format A4, A5, ...
											'language'    => 'en', // language: fr, en, it ...
											'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
											'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
											'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
									)*/
							)
					),
			),
			
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
	     'PUBLIC_Key' => 'pk_test_BgbyUBWQhgPsWwHHk42LacXL',
	     'SECRET_Key' => 'sk_test_tX9tPyKw9rFuNvP6XNgAl1cQ',
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);


?>