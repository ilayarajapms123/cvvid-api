<?php 
use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;

class InstitutionsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $institutionid = 0;
	public $edit = 0;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
// 			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
		
		);
	}

	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Institutions;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Institutions']))
		{
			$model->attributes=$_POST['Institutions'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Institutions']))
		{
			$model->attributes=$_POST['Institutions'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Institutions');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Institutions('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Institutions']))
			$model->attributes=$_GET['Institutions'];

		$fullname = "";
		$namesearch = 0;
		$userarr = array();
		if(isset($_GET['fullname']))
		{
		    $namesearch = 1;
		    $fullname = $_GET['fullname'];
		    $cri = new CDbCriteria();
		    $cri->alias = 'i';		    
		    $cri->join = "inner join cv16_users u on u.id = i.admin_id";
		    $cri->condition = "concat_ws(' ',u.forenames,u.surname) like '%".$fullname."%'";
		    $searchusers = Institutions::model()->findAll($cri);
		    $userarr = array();
		    foreach ($searchusers as $users)
		        $userarr[] = $users['admin_id'];
		        
		}
		
		$this->render('admin',array(
		    'model'=>$model,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Institutions the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Institutions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Institutions $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='institutions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actioneditDetails($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $this->render('editpage',array(
	        'model'=>$model,
	    ));
	}
	
	public function actionsaveSubscription($id)
	{
	    
	    $price = $_POST['Plans']['price'];
	    $interval = $_POST['interval'];
	    $expiry = $_POST['active_to'];
	    
	    $expiry_date = date_format(new DateTime($expiry),"Y-m-d H:i:s");
	    $now = new DateTime();
	    
	    $instmodel = Institutions::model()->findByPk($id);
	    $planmodel = new Plans();
	    $planmodel->owner_type = "Institution";
	    $planmodel->owner_id = $id;
	    $planmodel->name = $instmodel->name;
	    $planmodel->stripe_plan = strtolower(str_replace(' ', '_', $instmodel->name));
	    $planmodel->quantity = $instmodel->num_pupils;
	    $planmodel->price = $price;
	    $planmodel->interval = $interval;
	    $planmodel->interval_count = 1;
	    $planmodel->active_to = $expiry_date;
	    $planmodel->created_at = $now->format('Y-m-d H:i:s');
	    if($planmodel->save())
	        Institutions::model()->updateAll(array('stripe_active'=>1),'id =:id',array(':id'=>$id));
	    $this->render('admin',array(
	        'model'=>Institutions::model()
	    ));
	}
	
	public function actioneditSubscriptionDetails($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $this->render('editsubscriptionpage',array(
	        'model'=>$model,
	    ));
	}
	
	public function actioncreateStudent($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    
	    $profilemodel = new Profiles();
	    $usermodel = new Users();
	    $addressmodel = new Addresses();
	    
	    $this->render('studentForm',array(
	        'model'=>$model,'profilemodel'=>$profilemodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel
	    ));
	}
	
	public function actioncreateInstitution()
	{
	    $model = Institutions::model();
	    $this->render('createinstitution',array(
	        'model'=>$model,'usermodel'=>Users::model(),'planmodel'=>Plans::model()
	    ));
	}
	
	public function actionsaveInstitution()
	{
	    $now = new DateTime();
	    $usermodel = new Users();
	    $usermodel->attributes = $_POST['Users'];
	    $usermodel->type = 'institution_admin';
	    $usermodel->status = $_POST['status'];
	    $usermodel->created_at = $now->format('Y-m-d H:i:s');
	    $model = Institutions::model();
	    if($usermodel->save())
	    {	    
    	    
    	    $model->attributes = $_POST['Institutions'];
    	    $model->admin_id = $usermodel->id;
    	    $model->status = $_POST['status'];
    	    $model->email = $usermodel->email;
    	    $model->created_at = $now->format('Y-m-d H:i:s');
    	    if($model->save())
    	    {
    	        $iusermodel = new InstitutionUsers();
    	        $iusermodel->institution_id = $model->id;
    	        $iusermodel->user_id = $usermodel->id;
    	        $iusermodel->role = "admin";
    	        $iusermodel->created_at = $now->format('Y-m-d H:i:s');
    	        if($iusermodel->save())
    	        {
        	        if($_POST['Plans']['active_to'] != "" && $_POST['Plans']['active_to'] != "")
        	        {
        	            $expiry_date = date_format(new DateTime($_POST['Plans']['active_to']),"Y-m-d H:i:s");
        	            
        	            $planmodel = new Plans();
        	            $planmodel->attributes = $_POST['Plans'];
        	            $planmodel->active_to = $expiry_date;
        	            $planmodel->owner_type = "Institution";
        	            $planmodel->owner_id = $model->id;
        	            $planmodel->name = $model->name;
        	            $planmodel->stripe_plan = strtolower(str_replace(' ', '_', $model->name));
        	            $planmodel->quantity = $model->num_pupils;
        	            $planmodel->created_at = $now->format('Y-m-d H:i:s');
        	           if($planmodel->save())
        	           {
        	               Institutions::model()->updateAll(array('stripe_active'=>1),'id =:id',array(':id'=>$model->id));
        	           }
        	        }  	        
        	            	        
        	        $pmodel = new Profiles();
        	        $pmodel->owner_id = $model->id;
        	        $pmodel->owner_type = "Institution";
        	        $pmodel->photo_id = $_POST['photo_id'];
        	        $pmodel->type = "institution";
        	        $pmodel->slug = strtolower(str_replace(' ', '-', $model->name));
        	        $pmodel->created_at = $now->format('Y-m-d H:i:s');
        	        $pmodel->save();
        	        
    	            
    	        }
    	        
    	    }
    	    $imodel = Institutions::model()->findByPk($model->id);
    	    $this->render(($model->stripe_active == 1 ? 'editsubscriptionpage' : 'editpage'),array(
    	        'model'=>$imodel,
    	    ));
    	   
	    }
	   
	    $this->render('createinstitution',array(
	        'model'=>$model,'usermodel'=>Users::model(),'planmodel'=>Plans::model()
	    ));	    
	    
	}
	
	public function actionupdateInstitution($id)
	{    	    
    	    $now = new DateTime();
	    
    	    $model = Institutions::model()->findByPk($id);
	        $model->attributes = $_POST['Institutions'];
	        $model->status = $_POST['status'];
	        $model->email = $_POST['Users']['email'];
	        $model->updated_at = $now->format('Y-m-d H:i:s');
	        if($model->save())
	        {
	            $usermodel = Users::model()->findByPk($model->admin_id);
	            $pwd = $usermodel->password;
	            $usermodel->attributes = $_POST['Users'];
	            if($_POST['Users']['passowrd'] == "")
	                $usermodel->password = $pwd;
	            $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	            if($usermodel->save())
	            {
                    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>'institution'));
                    Profiles::model()->updateAll(array('photo_id'=>$_POST['phot_id']),'id =:id',array(':id'=>$pmodel->id));
	            }
	                
	       
	            $this->redirect($this->createUrl('institutions/editSubscriptionDetails',array('id'=>$id)));
// 	        $this->render('editsubscriptionpage',array(
// 	            'model'=>$model,
// 	        ));
	    }
	    
	    
	}
	
	public function actionupdateCurrentAdmin($id)
	{
	    $now = new DateTime();
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $pwd = $usermodel->password;
	    $usermodel->attributes = $_POST['Users'];
	    $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	    if($_POST['Users']['password'] == "")
	        $usermodel->password = $pwd;
	    $usermodel->save();	  
	    
	    $this->redirect($this->createUrl('institutions/editSubscriptionDetails',array('id'=>$id)));
// 	    $this->render('editsubscriptionpage',array(
// 	        'model'=>$model,
// 	    ));
	}
	
	public function actionaddNewAdmin($id)
	{
	    $now = new DateTime();
	    $model = Institutions::model()->findByPk($id);
	    $olduser = Users::model()->findByPk($model->admin_id);
	    
	    Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$model->admin_id));
	    
	    $newusermodel = new Users();
	    $newusermodel->attributes = $_POST['Users'];
	    $newusermodel->type = $olduser->type;
	    $newusermodel->created_at = $now->format('Y-m-d H:i:s')                                                                                       ;
	    $newusermodel->status = "active";
	    $newusermodel->stripe_active = $olduser->stripe_active;
	    if($newusermodel->save())
	        Institutions::model()->updateAll(array('admin_id'=>$newusermodel->id),'id =:id',array(':id'=>$id));
	    
	    $this->redirect($this->createUrl('institutions/editSubscriptionDetails',array('id'=>$id)));
// 	    $this->render('editsubscriptionpage',array(
// 	        'model'=>$model,
// 	    ));
	}
	
	function resize_image($image,$image_type,$fullpath,$destpath,$width,$height)
	{
	    $size_arr = getimagesize($fullpath);
	    
	    list($width_orig, $height_orig, $img_type) = $size_arr;
	    $ratio_orig = $width_orig/$height_orig;
	    
	    $tempimg = imagecreatetruecolor($width, $height);
	    imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	    
	    if($image_type == "image/jpg" || $image_type == "image/jpeg")
	        imagejpeg($tempimg, $destpath);
	        else if( $image_type == "image/png" )
	            imagepng($tempimg, $destpath);
	            else if( $image_type == "image/gif" )
	                imagegif($tempimg, $destpath);
	}
	
	public function actioncreateTeacher($id)
	{
	    $instmodel = InstitutionUsers::model()->findByPk($id);
	    $usermodel = new Users();
	    $this->render('teacherForm',array(
	        'model'=>$instmodel,'usermodel'=>$usermodel
	    ));
	}
	public function actionsaveTeacher($id)
	{
	    $now = new DateTime();
	    $usermodel = new Users();
	    $usermodel->attributes = $_POST['Users'];
	    $usermodel->type = "teacher";
	    $usermodel->created_at = $now->format('Y-m-d H:i:s');
	    $usermodel->status = "status";
	    if($usermodel->save())
	    {
	        $iusermodel = new InstitutionUsers();
	        $iusermodel->institution_id = $id;
	        $iusermodel->user_id = $usermodel->id;
	        $iusermodel->role = "teacher";
	        $iusermodel->created_at = $now->format('Y-m-d H:i:s');
	        $iusermodel->save();
	    }
	    
	    $this->redirect($this->createUrl('institutions/editSubscriptionDetails',array('id'=>$id)));
	}
	public function actioneditTeacher($id)
	{
	    $iusermodel = InstitutionUsers::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($iusermodel->user_id);
	    $model = Institutions::model()->findByPk($iusermodel->institution_id);
	    $this->render('teacherForm',array(
	        'model'=>$model,'iusermodel'=>$iusermodel,'usermodel'=>$usermodel
	    ));
	}
	public function actionupdateTeacher($id)
	{
	    if($_POST['user_id'] > 0)
	    {
    	    $now = new DateTime();
    	    $usermodel = Users::model()->findByPk($_POST['user_id']);
    	    $usermodel->attributes = $_POST['Users'];
    	    $usermodel->updated_at = $now->format('Y-m-d H:i:s');
    	    $usermodel->save();
	    }
	    
	    $this->redirect($this->createUrl('institutions/editSubscriptionDetails',array('id'=>$id)));
	}
	public function actioncreateTutor($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = new Users();
	    $tutormodel = new TutorGroups();
	    $this->render('createtutor',array(
	        'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tutormodel
	    ));
	}
	public function actionsaveTutor($id)
	{	    
	    $now = new DateTime();
	    $tgmodel = new TutorGroups();
	    $tgmodel->attributes = $_POST['TutorGroups'];
	    if($_POST['TutorGroups']['teacher_id'] <= 0)
	    {
	        $tmodel = new Users();
	        $tmodel->attributes = $_POST['Users'];
	        $tmodel->type = "teacher";
	        $tmodel->status = "active";
	        $tmodel->created_at = $now->format('Y-m-d H:i:s');
	        if($tmodel->save())
	            $tgmodel->teacher_id = $tmodel->id;
	    }
	   
	        $grad_date = null;
	        if($_POST['graduation_date'] != "")
	            $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
	        $tgmodel->institution_id = $id;
	        $tgmodel->graduation_date= $grad_date;
	        $tgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $tgmodel->account_limit = $_POST['allocated_students'];
	        if($tgmodel->save())
	        {
	            $forenames_arr = $_POST['forenames'];
	            array_pop($forenames_arr);
	            $surname_arr = $_POST['surname'];
	            array_pop($surname_arr);
	            $password_arr = $_POST['password'];
	            array_pop($password_arr);
	           // $confirmpwd_arr = $_POST['confirmpassword'];
	            
	            
	            for($i = 0; $i < $_POST['allocated_students']; $i++)
	            {	                
	                $umodel = new Users();
	                $umodel->forenames = $forenames_arr[$i];
	                $umodel->surname = $surname_arr[$i];
	                $umodel->password = $password_arr[$i];
	                $umodel->email = strtolower($forenames_arr[$i])."@test.com";
	                $umodel->type = "candidate";
	                $umodel->status = "active";
	                $umodel->created_at = $now->format('Y-m-d H:i:s');
	                if($umodel->save())
	                {
	                    $pmodel = new Profiles();
	                    $pmodel->owner_id = $umodel->id;
	                    $pmodel->owner_type = "Candidate";
	                    $pmodel->type = "candidate";
	                    $pmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $pmodel->save();
	                    
    	                $tgsmodel = new TutorGroupsStudents();
    	                $tgsmodel->institution_id = $id;
    	                $tgsmodel->tutor_group_id = $tgmodel->id;
    	                $tgsmodel->student_id = $umodel->id;
    	                $tgsmodel->graduation_date = $grad_date;
    	                $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
    	                $tgsmodel->save();
	                }
	               
	            }
	        }
	    
	        $tutormodel = TutorGroups::model()->findByPk($tgmodel->id);
	        $model = Institutions::model()->findByPk($id);
	        $usermodel = Users::model()->findByPk($tgmodel->teacher_id);
	        
	        $this->render('edittutor',array(
	            'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tutormodel
	        ));
	      
	}
	public function actioneditTutor($id)
	{
	    $tutormodel = TutorGroups::model()->findByPk($id);
	    $model = Institutions::model()->findByPk($tutormodel->institution_id);
	    $usermodel = new Users();
	    
	    $this->render('edittutor',array(
	        'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tutormodel
	    ));
	    
	}
	public function actionupdateTutor($id)
	{
	    
	    $tgmodel = TutorGroups::model()->findByPk($id);
	    $model = Institutions::model()->findByPk($tgmodel->institution_id);
	    $usermodel = new Users();
	  
	    $now = new DateTime();
	    $tgmodel->attributes = $_POST['TutorGroups'];
	    
	    if($_POST['TutorGroups']['teacher_id'] <= 0)
	    {
	        $tmodel = new Users();
	        $tmodel->attributes = $_POST['Users'];
	        $tmodel->type = "teacher";
	        $tmodel->status = "active";
	        $tmodel->created_at = $now->format('Y-m-d H:i:s');
	        
	        if($tmodel->save())
	            $tgmodel->teacher_id = $tmodel->id;
	    }
	    
	    $grad_date = null;
	    if($_POST['graduation_date'] != "")
	        $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
	    
	        $tgmodel->graduation_date= $grad_date;
	        $tgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        $tgmodel->account_limit = $_POST['allocated_students'];
	        $tgmodel->save();      
	        	        
	        $this->render('edittutor',array(
	            'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tgmodel
	        ));
	        
	}
	
	public function actiongetInstituteDetails()
	{
	    $model = Institutions::model()->findByPk($_POST['id']);
	    
	    $umodel = Users::model()->findByPk($model->admin_id);
	    $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id));
	    $cnt = $student_cnt.' / '.$model->num_pupils;
	    
	    $iarr = array();
	    $iarr['name'] = $model->name;
	    $iarr['admin'] = $umodel->forenames." ".$umodel->surname;
	    $iarr['student_count'] = $cnt;
	    echo json_encode($iarr);
	}
	
	public function actiongetTutorDetails()
	{
	    $model = TutorGroups::model()->findByPk($_POST['id']);
	    $imodel = Institutions::model()->findByPk($model->institution_id);
	    $umodel = Users::model()->findByPk($model->teacher_id);
	    
	    $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$imodel->id));
	    $cnt = $student_cnt.' / '.$model->account_limit;
	    
	    $iarr = array();
	    $iarr['name'] = $model->name;
	    $iarr['teacher'] = $umodel->forenames." ".$umodel->surname;
	    $iarr['student_count'] = $cnt;
	    echo json_encode($iarr);
	}
	
	public function actiongetTutors()
	{
	    $tgmodel = TutorGroups::model()->findAllByAttributes(array('institution_id'=>$_POST['id'],'deleted_at'=>null));
	    $tutor_arr = CHtml::listData($tgmodel, 'id', 'name');
	    //$tutor_arr = array('0'=>'') + $tutor_arr;
	    echo CHtml::tag('option',
	        array('value'=>''),'',true);
	    foreach($tutor_arr as $value=>$name)
	    {
	        echo CHtml::tag('option',
	            array('value'=>$value),CHtml::encode($name),true);
	    }
	}
	public function actionsaveStudent($id)
	{
	    $now = new DateTime();
	    $dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
	    $pwderr = "";
	    $slugerr = "";
	    $promodel = Profiles::model()->countByAttributes(array('slug'=>$_POST["Profiles"]['slug'],'deleted_at'=>null));
	    
	    $tgsmodel = new TutorGroupsStudents();
	    $model = new Institutions();
	    $tgmodel = new TutorGroups();
	    $usermodel = new Users();
	    $profilemodel = new Profiles();	    
	    $addressmodel = new Addresses();
	    
	    if($_POST["Users"]["password"] != "" && ($_POST["Users"]["password"] != $_POST["confirmpassword"]))
	        $pwderr =  "Password not matched";
        else if($promodel > 0)
            $slugerr = 1;
	    else
	    {
    	    $usermodel = new Users();
    	    $usermodel->attributes = $_POST['Users'];
    	    
    	    $usermodel->type = "candidate";
    	    $usermodel->status = "active";
    	    $usermodel->dob = $dobformat;
    	    $usermodel->created_at = $now->format('Y-m-d H:i:s');
    	    if($usermodel->save())
    	    {	        
    	        if($_POST['tutor_group_id'] > 0)
    	        {
    	            $tgsmodel = new TutorGroupsStudents();
    	            $tgsmodel->institution_id = $id;
    	            $tgsmodel->tutor_group_id = $_POST['tutor_group_id'];
    	            $tgsmodel->student_id = $usermodel->id;
    	            $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
    	            $tgsmodel->save();
    	        }
    	        if($_POST['Addresses']['postcode'] != "")
    	        {
        	         $addressmodel = new Addresses();   
        	         $addressmodel->attributes = $_POST['Addresses'];
        	         $addressmodel->model_id = $usermodel->id;
        	         $addressmodel->model_type = "Candidate";
        	         $addressmodel->created_at = $now->format('Y-m-d H:i:s');
        	         $addressmodel->save();
    	        }
    	        $pmodel = new Profiles();
    	        $pmodel->attributes = $_POST['Profiles'];
    	        $pmodel->owner_id = $usermodel->id;
    	        $pmodel->type = "candidate";
    	        $pmodel->owner_type = "Candidate";
    	        $pmodel->photo_id = $_POST['photo_id'];
    	        $pmodel->created_at = $now->format('Y-m-d H:i:s');
    	        $pmodel->save();
    	        
    	        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>'candidate'));
    	        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$usermodel->id));    	        	       
    	    }
	    }
	        
	    $this->render('studentEditForm',array(
	        'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tgmodel,'studentmodel'=>$tgsmodel,
	        'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,'pwderr'=>$pwderr,'slugerr'=>$slugerr
	    ));
	}
	
	public function actioneditStudent($id)
	{
	   $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	   $model = Institutions::model()->findByPk($tgsmodel->institution_id);
	   $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
	   $usermodel = Users::model()->findByPk($tgsmodel->student_id);
// 	   echo "fdg==".$usermodel->id;die();
	   $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>'candidate'));
	   $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$usermodel->id));
	   if($addressmodel == null )
	       $addressmodel = new Addresses();
	   $this->render('studentEditForm',array(
	       'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tgmodel,'tgsmodel'=>$tgsmodel,
	       'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel
	   ));
	}
	
	public function actionupdateStudent($id)
	{
	    
	    $now = new DateTime();
	    $dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($tgsmodel->student_id);
	    $pwd = $usermodel->password;
	    
	    $model = new Institutions();
	    $tgmodel = new TutorGroups();
	    $usermodel = new Users();
	    $profilemodel = new Profiles();
	    $addressmodel = new Addresses();
	    
	    if($_POST["Users"]["password"] != "" && ($_POST["Users"]["password"] != $_POST["confirmpassword"]))
	        $pwderr =  "Password not matched";
        else
        {
    	    $usermodel->attributes = $_POST['Users'];
    	    
    	    if($_POST['Users']['password'] == "")
    	        $usermodel->password = $pwd;
    	        $usermodel->dob = $dobformat;
    	    $usermodel->updated_at = $now->format('Y-m-d H:i:s');
    	   
    	    if($usermodel->save())
    	    {	        
    	        
                $amodel = Addresses::model()->findByAttributes(array('model_id'=>$usermodel->id));
                if($amodel == null)
                {
                    $addressmodel = new Addresses();
                    $addressmodel->attributes = $_POST['Addresses'];            
                    $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                    $addressmodel->model_id = $usermodel->id;
                    $addressmodel->model_type = "Candidate";
                }
                else {
                    $addressmodel = Addresses::model()->findByPk($amodel->id);
                    $addressmodel->attributes = $_POST['Addresses'];            
                    $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                }
                         
                $addressmodel->save();
                
                $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
    	        $pmodel->attributes = $_POST['Profiles'];
    	        $pmodel->photo_id = $_POST['photo_id'];
    	        $pmodel->updated_at = $now->format('Y-m-d H:i:s');
    	        $pmodel->save();
    	        
    	        
    	     }
    	     
        }
        
        $model = Institutions::model()->findByPk($tgsmodel->institution_id);
        $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>'candidate'));
        $addressmodel = Addresses::model()->findByPk($addressmodel->id);
        
        $this->render('studentEditForm',array(
            'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tgmodel,'tgsmodel'=>$tgsmodel,
            'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,'pwderr'=>$pwderr,'slugerr'=>$slugerr
        ));
	}
	
	public function actiondeleteStudent($id)
	{
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	    $userid = $tgsmodel->student_id;
	    
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid,'type'=>'candidate'));
	    
	    Addresses::model()->deleteAllByAttributes(array('model_id'=>$userid));
	    if($profilemodel->photo_id > 0)
	       Media::model()->deleteAllByAttributes(array('media_id'=>$profilemodel->photo_id));
	    Profiles::model()->deleteAllByAttributes(array('owner_id'=>$userid,'type'=>'candidate'));
	    Users::model()->deleteAllByAttributes(array('id'=>$userid));
	    TutorGroupsStudents::model()->deleteAllByAttributes(array('id'=>$id));
	    	
	   // $this->redirect($this->createUrl('institutions/editSubscriptionDetails',array('id'=>$instid)));
	}
	
	public function actionProfile($id)
	{
		  $this->layout = "profile";
	    $this->institutionid = $id;
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>'institution'));
	    $this->render('profile',array(
	        'model'=>$model,'usermodel'=>$usermodel,'profilemodel'=>$profilemodel
	    ));
	}
	public function actionEdit($id)
	{
		  $this->layout = "profile";
	    $this->edit = 1;
	    $this->institutionid = $id;
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>'institution'));
	    $this->render('editprofile',array(
	        'model'=>$model,'usermodel'=>$usermodel,'profilemodel'=>$profilemodel
	    ));
	}
	public function actionsaveInsBackground($id)
	{
    	 $body = $_POST['content'];    	
    	 Institutions::model()->updateAll(array('body'=>$body),'id =:id',array(':id'=>$id));    	 
    	 $model = Institutions::model()->findByPk($id);
    	 echo $this->renderPartial('instbackground',array('model'=>$model));
	}
	public function actionchangeVisiblity($id)
	{
	    $status = $_POST['status'];
	    $model = Institutions::model()->findByPk($id);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>'institution'));
	    $profilemodel = Profiles::model()->findByPk($pmodel->id);
	    $profilemodel->visibility = ($status == "Public" ? 0 : 1);
	    $profilemodel->save();
	}
	public function actionchangeStatus($id)
	{
	    $status = $_POST['status'];
	   
	    $model = Institutions::model()->findByPk($id);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>'institution'));
	    $pub = (strtolower($status) == 'active' ? 0 : 1);
	    
	    Profiles::model()->updateAll(array('published'=>$pub),'id =:id',array(':id'=>$pmodel->id));
	}
	
	public function actionuserAccountInfo($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $profile = Profiles::model()->findByAttributes(array('owner_id'=>$id));
	    $pmodel = Profiles::model()->findByPk($profile['id']);
	    $this->render('accountinfo',array('model'=>$model,'pmodel'=>$pmodel,'usermodel'=>$usermodel));
	}
	
	public function actionupdateAccountInfo($id)
	{
	    $now = new DateTime();
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$id));
	    $profilemodel = Profiles::model()->findByPk($pmodel->id);
	    $model->attributes=$_POST['Institutions'];
	    $model->updated_at = $now->format('Y-m-d H:i:s');
	    if($model->save())
	    {	        
	        $usermodel->attributes = $_POST['Users'];
	        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	        if($usermodel->save())
	        {
    	        $profilemodel->updated_at = $model->created_at;
    	        $profilemodel->slug = $_POST['Profiles']['slug'];
    	        $profilemodel->save();
	        }
	    }
	    
	    $this->render('accountinfo',array('model'=>$model,'pmodel'=>$pmodel,'usermodel'=>$usermodel));
	}
	public function actionsaveCoverPhoto()
	{
	    $profileid = $_POST['profileid'];
	    $pmodel = Profiles::model()->findByPk($profileid);
	    $now = new DateTime();
	    if($pmodel->cover_id > 0)
	    {
	        $mediamodel = Media::model()->findByPk($pmodel->cover_id);
	        $mediamodel->updated_at = $now->format('Y-m-d H:i:s');
	    }
	    else
	    {
	        $mediamodel = new Media();
	        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
	        $mediamodel->collection_name = "sample";
	        $mediamodel->model_id = $profileid;
	    }
	    $filename = $_FILES['cover']['name'];
	    $image_type = $_FILES['cover']['type'];
	    $file = $_FILES['cover']['tmp_name'];
	    $name = pathinfo( $_FILES['cover']['name'], PATHINFO_FILENAME);
	    
	    $mediamodel->name = $name;
	    $mediamodel->file_name = $filename;
	    $mediamodel->size = $_FILES['cover']['size'];
	    
	    if($mediamodel->save())
	    {
	        $mediafolder  = "images/media/".$mediamodel->id."/conversions";
	        
	        if (!file_exists($mediafolder)) {
	            mkdir($mediafolder, 0777, true);
	        }
	        
	        $imgprefix  = "images/media/".$mediamodel->id;
	        $fullpath = "images/media/".$mediamodel->id."/".$filename;
	        
	        Profiles::model()->updateAll(array('cover_id'=>$mediamodel->id),'id =:id',array(':id'=>$profileid));
	        
	        if(move_uploaded_file($_FILES['cover']['tmp_name'], $fullpath))
	        {
	            //create cover
	            $coverwidth  = 1000;
	            $coverheight = 400;
	            $coverdestpath = $imgprefix."/conversions/cover.jpg";
	            
	            //create icon
	            $iconwidth  = 40;
	            $iconheight = 40;
	            $icondestpath = $imgprefix."/conversions/icon.jpg";
	            
	            //create joblisting
	            $jlwidth  = 150;
	            $jlheight = 85;
	            $jldestpath = $imgprefix."/conversions/joblisting.jpg";
	            
	            //create search
	            $searchwidth  = 167;
	            $searchheight = 167;
	            $searchdestpath = $imgprefix."/conversions/search.jpg";
	            
	            //create thumb
	            $thumbwidth  = 126;
	            $thumbheight = 126;
	            $thumbdestpath = $imgprefix."/conversions/thumb.jpg";
	            
	            if( $image_type == "image/jpg" || $image_type == "image/jpeg") {
	                
	                $image = imagecreatefromjpeg($fullpath);
	                
	                $this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
	                $this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
	                $this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
	                $this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
	                $this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
	                
	            }
	            elseif( $image_type == "image/gif" )  {
	                
	                $image = imagecreatefromgif($fullpath);
	                
	                $this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
	                $this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
	                $this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
	                $this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
	                $this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
	                
	            }
	            elseif( $image_type == "image/png" ) {
	                
	                $image = imagecreatefrompng($fullpath);
	                
	                $this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
	                $this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
	                $this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
	                $this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
	                $this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
	            }
	            
	        }
	        
	        echo Yii::app()->baseUrl."/".$fullpath;
	    }
	    	    
	}
	
	public function actionOverview()
	{
	    $model=new Institutions('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['Institutions']))
	    {
	        $model->attributes=$_GET['Institutions'];
	    }
        $this->render('overview',array(
            'model'=>$model
        ));
	}
	public function actionTeachers()
	{
	    $model=new InstitutionUsers('search');
	    $insmodel = new Institutions();
	    $model->unsetAttributes();  // clear any default values
	    $namesearch = 0;
	    $fullname = '';
	    $userarr == array();
	    if(isset($_GET['InstitutionUsers']))
	    {
	        $model->attributes = $_GET['InstitutionUsers'];
	    }
	    if(isset($_GET['Institutions']))
	    {
	        $model->institution_id = $_GET['Institutions']['id'];
	        $insmodel->id = $model->institution_id;
	    }
	    if(isset($_GET['fullname']) && $_GET['fullname'] != "")
	    {
	        $namesearch = 1;
	        $fullname = $_GET['fullname'];
	        $cri = new CDbCriteria();
	        $cri->alias = "iu";
	        $cri->join = "inner join cv16_users u on u.id = iu.user_id";
	        $cri->condition = "concat_ws(' ',u.forenames,u.surname) like '%".$fullname."%'";
	        $searchusers = InstitutionUsers::model()->findAll($cri);	 
	        $userarr = array();
	        foreach ($searchusers as $users)
	           $userarr[] = $users['user_id'];        
	        
	    }
	    
        $this->render('teachers',array(
            'model'=>$model,'insmodel'=>$insmodel,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
        ));
	}
	public function actionTutorGroups()
	{
	    $model=new TutorGroups('search');
	    $model->unsetAttributes();  // clear any default values
	    $insmodel = new Institutions();
	    $namesearch = 0;
	    $fullname = '';
	    $userarr == array();
	    if(isset($_GET['TutorGroups']))
	        $model->attributes=$_GET['TutorGroups'];
	        
        if(isset($_GET['Institutions']))
        {
            $model->institution_id = $_GET['Institutions']['id'];
            $insmodel->id = $model->institution_id;
        }
        if(isset($_GET['fullname']) && $_GET['fullname'] != "")
        {
            $namesearch = 1;
            $fullname = $_GET['fullname'];
            $cri = new CDbCriteria();
            $cri->alias = "tg";
            $cri->join = "inner join cv16_users u on u.id = tg.teacher_id";
            $cri->condition = "concat_ws(' ',u.forenames,u.surname) like '%".$fullname."%'";
            $searchusers = TutorGroups::model()->findAll($cri);
            $userarr = array();
            foreach ($searchusers as $users)
                $userarr[] = $users['teacher_id'];
                
        }
        
        $this->render('tutorgroups',array(
            'model'=>$model,'insmodel'=>$insmodel,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
        ));
	}
	public function actionStudents()
	{
	    $model=new TutorGroupsStudents('search');
	    $model->unsetAttributes();  // clear any default values
	    
	    $insmodel = new Institutions();
	    $namesearch = 0;
	    $fullname = '';
	    $userarr == array();
	    
	    if(isset($_GET['TutorGroupsStudents']))
	        $model->attributes=$_GET['TutorGroupsStudents'];
	    
        if(isset($_GET['Institutions']))
        {
            $model->institution_id = $_GET['Institutions']['id'];
            $insmodel->id = $model->institution_id;
        }
        if(isset($_GET['fullname']) && $_GET['fullname'] != "" )
        {
            $namesearch = 1;
            $fullname = $_GET['fullname'];
            $cri = new CDbCriteria();
            $cri->alias = "tgs";
            $cri->join = "inner join cv16_users u on u.id = tgs.student_id";
            $cri->condition = "concat_ws(' ',u.forenames,u.surname) like '%".$fullname."%'";
            $searchusers = TutorGroupsStudents::model()->findAll($cri);
            $userarr = array();
            foreach ($searchusers as $users)
                $userarr[] = $users['student_id'];
                
        }
	        
        $this->render('students',array(
            'model'=>$model,'insmodel'=>$insmodel,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
        ));
	}
	public function actionaddTeacher()
	{
	    $usermodel = new Users();	    
	    $this->render('insteacher',array(
	        'model'=>new InstitutionUsers(),'usermodel'=>new Users(),'insmodel'=>new Institutions()
	    ));
	}

	public function actionsaveInsTeacher(){
	    
	    $now = new DateTime();
	    $usermodel = new Users();
	    $usermodel->attributes = $_POST['Users'];
	    $usermodel->type = "teacher";
	    $usermodel->created_at = $now->format('Y-m-d H:i:s');
	    if($usermodel->save())
	    {
	        $iumodel = new InstitutionUsers();
	        $iumodel->attributes = $_POST['InstitutionUsers'];
	        $iumodel->user_id = $usermodel->id;
	        $iumodel->role = $usermodel->type;
	        $iumodel->created_at = $now->format('Y-m-d H:i:s');
	        $iumodel->save();
	    }
	    
	    $this->render('teachers',array(
	        'model'=>new InstitutionUsers()
	    ));
	}
	public function actioneditInsTeacher($id)
	{
	    $iusermodel = InstitutionUsers::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($iusermodel->user_id);
	    $insmodel = Institutions::model()->findByPk($iusermodel->institution_id);
	    $this->render('insteacher',array(
	        'model'=>$iusermodel,'usermodel'=>$usermodel,'insmodel'=>$insmodel
	    ));
	}
	
	public function actionupdateInsTeacher($id){
	    
	    $now = new DateTime();
	    $iumodel = InstitutionUsers::model()->findByPk($id);
	    $iumodel->attributes = $_POST['InstitutionUsers'];
	    $iumodel->updated_at = $now->format('Y-m-d H:i:s');
	    if($iumodel->save())
	    {	        
	        $usermodel = Users::model()->findByPk($iumodel->user_id);
	        $pwd = $usermodel->password;
	        $usermodel->attributes = $_POST['Users'];
	        if(trim($_POST['Users']['password']) == "")
	            $usermodel->password = $pwd;
	            
	        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	        $usermodel->save();
	    }
	    	    
	    $this->redirect($this->createUrl('institutions/teachers'));
	}
	public function actioncreateTutorGroup()
	{
	            $model = new Institutions();	            
	            $usermodel = new Users();
	            $tutormodel = new TutorGroups();
	            $this->render('createtutorgroup',array(
	                'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tutormodel
	            ));
	}
	public function actionsaveTutorGroup()
	{	   
	            $id = $_POST['institution_id'];
	            $now = new DateTime();
	            $tgmodel = new TutorGroups();
	           
    	            $tgmodel->attributes = $_POST['TutorGroups'];
    	            if($_POST['TutorGroups']['teacher_id'] <= 0)
    	            {
    	                $tmodel = new Users();
    	                $tmodel->attributes = $_POST['Users'];
    	                $tmodel->type = "teacher";
    	                $tmodel->status = "active";
    	                $tmodel->created_at = $now->format('Y-m-d H:i:s');
    	                if($tmodel->save())
    	                    $tgmodel->teacher_id = $tmodel->id;
    	            }
    	            
    	            $grad_date = null;
    	            if($_POST['graduation_date'] != "")
    	                $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
    	            
    	                $tgmodel->institution_id = $id;
    	                $tgmodel->graduation_date= $grad_date;
    	                $tgmodel->created_at = $now->format('Y-m-d H:i:s');
    	                $tgmodel->account_limit = $_POST['allocated_students'];
    	                if($tgmodel->save())
    	                {
    	                    $forenames_arr = $_POST['forenames'];
    	                    array_pop($forenames_arr);
    	                    $surname_arr = $_POST['surname'];
    	                    array_pop($surname_arr);
    	                    $password_arr = $_POST['password'];
    	                    array_pop($password_arr);
    	                    // $confirmpwd_arr = $_POST['confirmpassword'];
    	                    
    	                    
    	                    for($i = 0; $i < $_POST['allocated_students']; $i++)
    	                    {
    	                        $umodel = new Users();
    	                        $umodel->forenames = $forenames_arr[$i];
    	                        $umodel->surname = $surname_arr[$i];
    	                        $umodel->password = $password_arr[$i];
    	                        $umodel->email = strtolower($forenames_arr[$i])."@test.com";
    	                        $umodel->type = "candidate";
    	                        $umodel->status = "active";
    	                        $umodel->created_at = $now->format('Y-m-d H:i:s');
    	                        if($umodel->save())
    	                        {
    	                            $pmodel = new Profiles();
    	                            $pmodel->owner_id = $umodel->id;
    	                            $pmodel->owner_type = "Candidate";
    	                            $pmodel->type = "candidate";
    	                            $pmodel->created_at = $now->format('Y-m-d H:i:s');
    	                            $pmodel->save();
    	                            
    	                            $tgsmodel = new TutorGroupsStudents();
    	                            $tgsmodel->institution_id = $id;
    	                            $tgsmodel->tutor_group_id = $tgmodel->id;
    	                            $tgsmodel->student_id = $umodel->id;
    	                            $tgsmodel->graduation_date = $grad_date;
    	                            $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
    	                            $tgsmodel->save();
    	                        }
    	                        
    	                    }
    	                }
    	            
                    $this->redirect($this->createUrl('institutions/TutorGroups'));
	               
	}
	public function actioneditTutorGroup($id)
	{
	    $tutormodel = TutorGroups::model()->findByPk($id);
	    $model = Institutions::model()->findByPk($tutormodel->institution_id);
	    $usermodel = Users::model()->findByPk($tutormodel->teacher_id);
	    $this->render('edittutorgroup',array(
	        'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tutormodel,'institution_id'=>$tutormodel->institution_id
	    ));
	}
	public function actionupdateTutorGroup()
	{
	    $id = $_POST['institution_id'];
	    $now = new DateTime();
	    $tgmodel = new TutorGroups();
	    $tgmodel->attributes = $_POST['TutorGroups'];
	    if($_POST['TutorGroups']['teacher_id'] <= 0)
	    {
	        $tmodel = new Users();
	        $tmodel->attributes = $_POST['Users'];
	        $tmodel->type = "teacher";
	        $tmodel->status = "active";
	        $tmodel->created_at = $now->format('Y-m-d H:i:s');
	        if($tmodel->save())
	            $tgmodel->teacher_id = $tmodel->id;
	    }
	    
	    $grad_date = null;
	    if($_POST['graduation_date'] != "")
	        $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
	        
	        $tgmodel->institution_id = $id;
	        $tgmodel->graduation_date= $grad_date;
	        $tgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $tgmodel->account_limit = $_POST['allocated_students'];
	        if($tgmodel->save())
	        {
	            $forenames_arr = $_POST['forenames'];
	            array_pop($forenames_arr);
	            $surname_arr = $_POST['surname'];
	            array_pop($surname_arr);
	            $password_arr = $_POST['password'];
	            array_pop($password_arr);
	            // $confirmpwd_arr = $_POST['confirmpassword'];
	            
	            
	            for($i = 0; $i < $_POST['allocated_students']; $i++)
	            {
	                $umodel = new Users();
	                $umodel->forenames = $forenames_arr[$i];
	                $umodel->surname = $surname_arr[$i];
	                $umodel->password = $password_arr[$i];
	                $umodel->email = strtolower($forenames_arr[$i])."@test.com";
	                $umodel->type = "candidate";
	                $umodel->status = "active";
	                $umodel->created_at = $now->format('Y-m-d H:i:s');
	                if($umodel->save())
	                {
	                    $pmodel = new Profiles();
	                    $pmodel->owner_id = $umodel->id;
	                    $pmodel->owner_type = "Candidate";
	                    $pmodel->type = "candidate";
	                    $pmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $pmodel->save();
	                    
	                    $tgsmodel = new TutorGroupsStudents();
	                    $tgsmodel->institution_id = $id;
	                    $tgsmodel->tutor_group_id = $tgmodel->id;
	                    $tgsmodel->student_id = $umodel->id;
	                    $tgsmodel->graduation_date = $grad_date;
	                    $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $tgsmodel->save();
	                }
	                
	            }
	        }
	        
	        $this->redirect($this->createUrl('institutions/TutorGroups'));
	        
	}
	
	public function actiongetNumPupils()
	{
	    $model = Institutions::model()->findByPk($_POST['id']);
	    $tgs_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id));
	    echo $model->num_pupils - $tgs_cnt;
	}
	
	public function actionexportTutorsCSV()
	{
	    Yii::import('ext.ECSVExport');
	    $criteria = new CDbCriteria();
	    $criteria->alias = "iu";
	    $criteria->select = "CONCAT_WS(' ', u.forenames, u.surname) as names,u.email,i.name,u.created_at";
	    $criteria->join = "inner join cv16_users u on u.id = iu.user_id
                           inner join cv16_institutions i on i.id = iu.institution_id";
	    $criteria->condition = "iu.role = 'teacher'";
	    	    
	    $dataProvider = new CActiveDataProvider('InstitutionUsers', array(
	        'criteria'=>$criteria
	    ));
	    
	    $csv = new ECSVExport($dataProvider);
	    $content = $csv->toCSV();
	    Yii::app()->getRequest()->sendFile('export.csv', $content, "text/csv", false);
	}
	
	public function actioncreateInstitutionAdmin()
	{
	    $usermodel = new Users();
	    $this->render('insadmin',array(
	        'model'=>new InstitutionUsers(),'usermodel'=>new Users(),'insmodel'=>new Institutions()
	    ));
	}
	public function actionexportStudentsCSV()
	{
	    Yii::import('ext.ECSVExport');
	    $criteria = new CDbCriteria();
	    $criteria->alias = "tgs";
	    $criteria->select = "CONCAT_WS(' ', u.forenames, u.surname) as names,u.email,i.name as school,tgs.graduation_date,u.created_at";
	    $criteria->join = "inner join cv16_users u on u.id = tgs.student_id
                           inner join cv16_institutions i on i.id = tgs.institution_id";
	    
	    $dataProvider = new CActiveDataProvider('TutorGroupsStudents', array(
	        'criteria'=>$criteria
	    ));
	    
	    $csv = new ECSVExport($dataProvider);
	    $content = $csv->toCSV();
	    Yii::app()->getRequest()->sendFile('export.csv', $content, "text/csv", false);
	}
	public function actioneditInsStudent($id)
	{
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	    $model = Users::model()->findByPk($tgsmodel->student_id);
	    
	    $this->render('editstudent',array(
	        'model'=>$model,'tgsmodel'=>$tgsmodel
	    ));
	}
	public function actionupdateStudentDetails($id)
	{	    
	    $now = new DateTime();
	    $usermodel = Users::model()->findByPk($id);
	    $pwd = $usermodel->password;
	    $usermodel->attributes = $_POST["Users"];
	    if($_POST["Users"]["passowrd"] == "")
	        $usermodel->password = $pwd;
	        $usermodel->type = "candidate";
	        $dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
	        $usermodel->dob = $dobformat;
	        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	        if($usermodel->save())
	        {
	            $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$id));
	            if($addressmodel == null)
	            {
	                $addressmodel = new Addresses();
	                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	            }
	            else
	                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
	                
	                $addressmodel->attributes = $_POST["Addresses"];
	                $addressmodel->model_id = $id;
	                if($addressmodel->save())
	                {
	                    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>$usermodel->type));
	                    $profilemodel = Profiles::model()->findByPk($pmodel->id);
	                    $profilemodel->slug = $_POST["Profiles"]['slug'];
	                    $profilemodel->photo_id = $_POST['photo_id'];
	                    $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
	                    $profilemodel->save();
	                }
	                $this->render('editstudent',array('model'=>$usermodel));
	        }	        
	        
	}
	public function actionaccountSubscription($id = 0)
	{
	    if($id > 0)
	       $model = Institutions::model()->findByPk($id);
	    else 
	       $model = new Institutions();
	    
	    $this->render('subscription',array(
	        'model'=>$model
	    ));
	}
	public function actionPayment($id = 0)
	{
	    require Yii::app()->basePath . '/extensions/stripe/init.php';
	    Stripe::setApiKey(Yii::app()->params['stripeKey']);
	    
	    $amount = 2 * 100;
	    
	    
	    $token = Token::create(
	        array(
	            "card" => array(
	                "name" => "test123",
	                "number" => "5200828282828210",
	                "exp_month" => "02",
	                "exp_year" => "2025",
	                "cvc" => "1234"
	            )
	          )
	        );
	    
	    $plan = Plan::create(array(
	        "amount" => 5000,
	        "interval" => "month",
	        "name" => "test plang",
	        "currency" => "usd",
	        "id" => "test_plan"
	    ));
	    
	    $customer = Customer::create(array(
	        'source'   => $token,
	        'email'    => "testcust@gmail.com",
	        'plan'     => "test_plan",
	        
	        // 	            'account_balance' => $setupFee,
	    // 	            'description' => "Charge with one time setup fee"
	    ));
	    // Create the charge on Stripe's servers - this will charge the user's card
	    
	        $charge = Charge::create(array(
	            
	            "amount" => $amount , // amount in cents, again
	            "currency" => "usd",
	            "customer" => $customer->id,
	            "description" => "Ex charge"
	        ));
	    
	        $subscription = Subscription::create(array(
	            "customer" => $customer->id,
	            "items" => array(
    	                array(
        	                    "plan" => "test_plan",
        	                ),
    	            )
	        ));
	  
	        echo var_dump($subscription);
// 	        $invoice = InvoiceItem::create(array(
// 	            "customer" => "cus_Bx9pdar7CC5jft",
// 	            "amount" => 2500,
// 	            "currency" => "usd",
// 	            "description" => "One-time setup fee")
// 	            );
	        
// 	        $subscription = Subscription::create(array(
// 	            "customer" => "cus_Bx9pQ0le0wrA9x",
// 	            "items" => array(
// 	                array(
// 	                    "plan" => "gold",
// 	                ),
// 	            )
// 	        ));
	        
	        
// 	    Yii::import('ext.stripe.*');
// 	    $pay = new SpecifierPay(Yii::app()->params['stripeKey']);
// 	    $cardArray = array ("number"=>"5200828282828210", "exp_month"=>"02", "exp_year"=>"2121", "cvc"=>"1234", "name"=>"testname",
// 	        "address_line1"=>"test addr 1", "address_line2"=>"test addr 2", "address_city"=>"test addr city", "address_zip"=>"9034d", "address_country"=>"UK");
// 	    $asd = $pay->insertClient('mytestcomp','testcomp@gmail.com',$cardArray);
	       
	}
}
