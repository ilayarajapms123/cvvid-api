<?php

class EmployersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        public $employerid = 0;
        public $edit = 0;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
        public function actiondeleteEmployer($id)
        {
               
            $now = new DateTime();
            
            $empmodel =  Employers::model()->findByPk($id);
            
            $empmodel->deleted_at = $now->format('Y-m-d H:i:s');
            $empmodel->save();
            
            $usermodel = Users::model()->findByPk($empmodel->user_id);
            $usermodel->deleted_at = $now->format('Y-m-d H:i:s');
            $usermodel->save();
 
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>'employer'));
            $profilemodel->deleted_at = $now->format('Y-m-d H:i:s');
            $profilemodel->save();
            
            $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$id,'model_type'=>'employer'));
            $addressmodel->deleted_at = $now->format('Y-m-d H:i:s');
            $addressmodel->save();
                       
            $this->redirect($this->createUrl('employers/admin'));
        }
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Employers;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Employers']))
		{
			$now = new DateTime();
			$model->attributes=$_POST['Employers'];
			$model->save();
		}

		$this->render('create',array(
			'model'=>$model,
			
		));
	}
        
        	/**
	 * Manages all models.
	 */
	public function actionsearchAdmin()
	{
	    $model=new Employers('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['Employers']))
	    {
	        $model->attributes=$_GET['Employers'];	        
	    } 
	    
            $this->render('admin',array(
                'model'=>$model,'issearch'=>1));
            }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioncreateEmployerDetails()
	{
         
		$model=new Employers;
		$empusermodel=new EmployerUsers;
		$usermodel=new Users;
		$profilemodel=new Profiles;
		$mediamodel=new Media;
		$addressmodel=new Addresses;
		$pwderr = "";
		$slugerr = "";
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                $now = new DateTime();

                if (isset($_POST['Employers'])) {
                    
                  
                    $promodel = Profiles::model()->countByAttributes(array('slug'=>$_POST["Profiles"]['slug'],'deleted_at'=>null));
                    
                    if($_POST["Users"]["password"] != "" && ($_POST["Users"]["password"] != $_POST["confirmpassword"]))
                        $pwderr =  "Password not matched";
                    else if($promodel > 0)
                        $slugerr = 1;
                    else
                    {
    //                    insert user details
                        $usermodel->attributes = $_POST["Users"];
                        $usermodel->created_at = $now->format('Y-m-d H:i:s');
                        $usermodel->type = 'employer';
                        $usermodel->location_lat = $_POST['Addresses']['latitude'];
                        $usermodel->location_lng = $_POST['Addresses']['longitude'];
                        $usermodel->tel = $_POST['Employers']['tel'];
        
                        if ($usermodel->save()) {  
       //                    insert employer details
                        $model->attributes = $_POST['Employers'];
                        $model->email = $usermodel->email;
                        $model->created_at = $now->format('Y-m-d H:i:s');
                        $model->user_id = $usermodel->id;
                        $model->body = $_POST['body'];
                        if ($model->save()) {
                                //employer user update
                                $empusermodel->employer_id = $model->id;
                                $empusermodel->user_id = $usermodel->id;
                                $empusermodel->role = Yii::app()->user->getState('role');
                                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                                $empusermodel->save();
                                //address update
                                $addressmodel->attributes = $_POST["Addresses"];
                                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                                $addressmodel->model_type = 'employer';
                                $addressmodel->model_id = $model->id;
                                if ($addressmodel->save()) {
                                    $profilemodel->attributes = $_POST["Profiles"];
                                    $profilemodel->owner_id = $model->id;
                                    $profilemodel->owner_type = 'employer';
                                    $profilemodel->type = 'employer';
                                    $profilemodel->slug = $_POST["Profiles"]['slug'];
                                    $profilemodel->photo_id = $_POST['photo_id'];
                                    $profilemodel->save();
                                    $model->unsetAttributes();
                                    $this->redirect(array('admin'));
                                  
                              }
                            }
                          }
                        }
                    }
                    
                    $this->render('create',array(
                                'model'=>$model,'pwderr'=>$pwderr,'slugerr'=>$slugerr
                     ));
                }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioncreateEmployerUsers() {

            $empusermodel = new EmployerUsers;
            $usermodel = new Users;

            $now = new DateTime();

    //      insert user details
            $usermodel->attributes = $_POST["Users"];
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->type = 'employer';
            $usermodel->location_lat = $_POST['Addresses']['latitude'];
            $usermodel->location_lng = $_POST['Addresses']['longitude'];

            if ($usermodel->save()) {
                //employer user update
                $empusermodel->employer_id = $_POST['employer_id'];
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = Yii::app()->user->getState('role');
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                $empusermodel->save();
            }

            $model = Employers::model()->findByPk($_POST['employer_id']);
	    $this->render('users',array('model'=>$model));
            
        }
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioncreateUsers($id) {

            $empusermodel = new EmployerUsers;
            $usermodel = new Users;
            $addressmodel = new Addresses;

            $now = new DateTime();

    //      insert user details
            $usermodel->attributes = $_POST["Users"];
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->type = 'employer';
            $usermodel->location_lat = $_POST['Addresses']['latitude'];
            $usermodel->location_lng = $_POST['Addresses']['longitude'];

            if ($usermodel->save()) {
                
                //address update
                $addressmodel->attributes = $_POST["Addresses"];
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                $addressmodel->model_type = 'employer';
                $addressmodel->model_id = $id;
                $addressmodel->save();
                
                //employer user update
                $empusermodel->employer_id = $id;
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = Yii::app()->user->getState('role');
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                $empusermodel->save();
            }

            $model = Employers::model()->findByPk($id);
	    $this->render('employerUsers',array('model'=>$model));
            
        }
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionemployerUsersEdit($id) {

            $empusermodel = EmployerUsers::model()->findByPk($id);
            $usermodel = Users::model()->findByPk($empusermodel->user_id);
            $addressmodel = Addresses::model()->findByPk($empusermodel->employer_id);

            $now = new DateTime();

    //      insert user details
            $usermodel->attributes = $_POST["Users"];
            $usermodel->updated_at = $now->format('Y-m-d H:i:s');

            if ($usermodel->save()) {
                
                //address update
                $addressmodel->attributes = $_POST["Addresses"];
                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                $addressmodel->save();
                
                $empusermodel->updated_at = $now->format('Y-m-d H:i:s');
                $empusermodel->save();
            }

            $model = Employers::model()->findByPk($empusermodel->employer_id);
	    $this->render('employerUsers',array('model'=>$model));
            
        }
        
        public function actionsaveBody()
        {
            $employerid = $_POST['employerid'];
            $body = $_POST['body'];
                        
            $model = Employers::model()->findByPk($employerid);
            
            $model->body = $body;
            $model->save();
            
        }
        
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioneditUsers($id) {

            $usermodel = Users::model()->findByPk($id);

            $now = new DateTime();

    //      insert user details
            $usermodel->attributes = $_POST["Users"];
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->type = 'employer';

            if ($usermodel->save()) {
                //employer user update
                $empusermodel->employer_id = $_POST['employer_id'];
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = Yii::app()->user->getState('role');
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                $empusermodel->save();
            }

            $model = Employers::model()->findByPk($_POST['employer_id']);
	    $this->render('users',array('model'=>$model));
            
        }

       /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Employers']))
		{
			$model->attributes=$_POST['Employers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actiondeleteEmployers()
        {
            $ids = implode(',', $_POST['ids']);
            
            $userids = explode(",",$ids);  
            
            $usr_arr = array();
            foreach ($userids as $id){
               $uid =  Employers::model()->findByPk($id);
               $usr_arr[] = $uid->user_id;
            }
            
            $user_ids = implode(',', $usr_arr);
            
            $now = new DateTime();

            $cri = new CDbCriteria();
            $cri->condition = "id in (".$ids.")";
            Employers::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')), $cri);
            
            $cri1 = new CDbCriteria();
            $cri1->condition = "id in (".$user_ids.")";
            Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')), $cri1);

            $cri2 = new CDbCriteria();
            $cri2->condition = "owner_id in (".$ids.")";
            Profiles::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')), $cri2);
            
            $cri3 = new CDbCriteria();
            $cri3->condition = "model_id in (".$ids.")";
            Addresses::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')), $cri3);
            
        }
        
        
        public function actionupdatePublish()
        {
            $employerid =$_POST['employerid'];
            $status =$_POST['status'];
            
            $publish = 1;
            if($status == 1){
                $publish = 0;
            }
            
            $model = Employers::model()->findByPk($employerid);
            
            $model->published = $publish;
            $model->save();
            
        }
        
        public function actionchangeEmployerVisiblity()
        {
            $employerid =$_POST['employerid'];
            $status =$_POST['status'];
            
            $visiblity = 1;
            if($status == 1){
                $visiblity = 0;
            }
            
            $promodel = Profiles::model()->findByAttributes(array('owner_id'=>$employerid,'type'=>'employer'));
            
            $promodel->visibility = $visiblity;
            $promodel->save();
            
        }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Employers');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Employers('search');
		$empmodel=new Employers();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Employers']))
			$model->attributes=$_GET['Employers'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
       
        
        public function actionEmployer($id)
        {
            $this->layout = "profile";
            $this->employerid = $id;
            $model = Employers::model()->findByPk($id);
            $this->render('employer',array(
                            'model'=>$model,
            ));
        }
        public function actionemployeruserCreate($id)
        {
            $model = Employers::model()->findByPk($id);
            $this->render('_employerusercreate',array(
                            'model'=>$model,
            ));
        }
        
        public function actionEdit($id)
        {
            $this->layout = "profile";
            $this->employerid = $id;
            $this->edit = 1;
            $model = Employers::model()->findByPk($id);
            $this->render('editemployer',array(
                            'model'=>$model,
            ));
        }
        
        public function actioneditDetails($id)
        {
            $model = Employers::model()->findByPk($id);
            $usermodel = Users::model()->findByPk($model->user_id);
            $this->render('editDetails',array(
                            'model'=>$model,'usermodel'=>$usermodel
            ));
        }
        
         /**
	 * Manages Employers Edit.
	 */
        function actionemployerEdit($id)
	{
	    $model = Employers::model()->findByPk($id);
	    $this->render('employerEdit',array('model'=>$model));
	}
         /**
	 * Manages Employers Edit.
	 */
        function actionUsersEdit($id)
	{
	    $model = EmployerUsers::model()->findByPk($id);
	    $this->render('_employeruseredit',array('empusermodel'=>$model));
	}
        
        function actionUsers($id)
	{
	    $model = Employers::model()->findByPk($id);
	    $this->render('users',array('model'=>$model));
	}
        
        function actionuserCreate($id)
	{
	    $model = Employers::model()->findByPk($id);
	    $this->render('usercreate',array('model'=>$model));
	}
        
        function actionuserEdit($id)
	{
	    $empusermodel = Employers::model()->findByPk($id);
	    $this->render('useredit',array('empusermodel'=>$empusermodel));
	}
        
        
        public function actionusersActivate()
        {
            $ids = implode(',', $_POST['ids']);

            $cri = new CDbCriteria();
            $cri->condition = "id in (".$ids.")";
            Users::model()->updateAll(array('status'=>'active'), $cri);
            
        }
        
        public function actionusersBlock()
        {
            $ids = implode(',', $_POST['ids']);
            
            $cri = new CDbCriteria();
            $cri->condition = "id in (".$ids.")";
            Users::model()->updateAll(array('status'=>'blocked'), $cri);
            
        }
        
        function actiongetProfilePhoto()
	{
	    echo $_FILES['profile']['tmp_name'];
	}
	function actionupdateProfileDetails($id) {
            
            $now = new DateTime();
            $empmodel = Employers::model()->findByPk($id);

            $empmodel->attributes = $_POST["Employers"];
            $empmodel->updated_at = $now->format('Y-m-d H:i:s');
            $empmodel->save();
            
            $this->employerid = $id;
            $this->edit = 1;
            $this->render('editemployer',array(
                            'model'=>$empmodel,
            ));
        
        }
        
	function actionupdateEmployerDetails($id) {
        $now = new DateTime();
        $empmodel = Employers::model()->findByPk($id);
        $usermodel = Users::model()->findByPk($empmodel->user_id);
        
        $empmodel->attributes = $_POST["Employers"];
        $empmodel->updated_at = $now->format('Y-m-d H:i:s');
        $empmodel->body = $_POST['body'];
//            employers update
        if ($empmodel->save()) {
//                address update
            $addressmodel = Addresses::model()->findByAttributes(array('model_id' => $id));
            if ($addressmodel == null) {
                $addressmodel = new Addresses();
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            } else
                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');

            $addressmodel->attributes = $_POST["Addresses"];
            $addressmodel->model_id = $id;
            if ($addressmodel->save()) {
                $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $id, 'type' => $usermodel->type));
                $profilemodel = Profiles::model()->findByPk($pmodel->id);
                $profilemodel->slug = $_POST["Profiles"]['slug'];
                $profilemodel->photo_id = $_POST['photo_id'];
                $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
                $profilemodel->save();
            }
        }
         $users = Users::model()->findByPk($empmodel->user_id);
        $this->render('employerEdit', array('model' => $empmodel,'usermodel'=>$users));
    }
    
    function resize_image($image,$image_type,$fullpath,$destpath,$width,$height)
    {		
                $size_arr = getimagesize($fullpath);

                list($width_orig, $height_orig, $img_type) = $size_arr;
        $ratio_orig = $width_orig/$height_orig;  

        $tempimg = imagecreatetruecolor($width, $height);
        imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        if($image_type == "image/jpg" || $image_type == "image/jpeg") 
                imagejpeg($tempimg, $destpath);	    		
        else if( $image_type == "image/png")  
                imagepng($tempimg, $destpath);
        else if( $image_type == "image/gif" )
                imagegif($tempimg, $destpath);
    }

    /**updateEmployerDetails
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Employers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Employers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Employers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='employers-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
}
