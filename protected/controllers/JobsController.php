<?php

class JobsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
        /**
	 * Manages all models.
	 */
	public function actionsearchAdmin()
	{
	    $model=new Jobs('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['Jobs']))
	    {
	        $model->attributes=$_GET['Jobs'];	        
	    } 
	    
        $this->render('admin',array(
            'model'=>$model,'issearch'=>1));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Jobs;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jobs']))
		{
			$model->attributes=$_POST['Jobs'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioncreateJobs()
	{
            $model=new Jobs;
            
            $now = new DateTime();
            
            if(isset($_POST['Jobs']))
            {
                //  insert jobs details
                    $model->attributes = $_POST["Jobs"];
                    $model->owner_id = $_POST["employer"];
                    $model->owner_type = 'employer';
                    $model->user_id = Yii::app()->user->getState('userid');
                    $model->industry_id = $_POST["industry_id"];
                    $endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
	            $model->end_date = $endformat;
	            $model->skill_category_id = $_POST["skill_category_id"];
	            $model->type = $_POST["type"];
	            $model->description = $_POST["description"];
	            $model->additional = $_POST["additional"];
	            $model->location_lat = $_POST["location_lat"];
	            $model->location_lng = $_POST["location_lng"];
                    $model->created_at = $now->format('Y-m-d H:i:s');
                    if($model->save())
                    {
                        
                        $skills = $_POST['skills'];

                        foreach ($skills as $skills_val){
                            $jobskillmodel=new JobSkill;
                            $jobskillmodel->job_id = $model->id;
                            $jobskillmodel->skill_id = $skills_val;
                            $jobskillmodel->save();
                        }
                        $this->redirect(array('admin'));
                       
                    }else{
                        //echo var_dump($model->errors);
                       $this->render('create',array(
			'model'=>$model,
		));
                    }
            }
                
        }
        
        public function actiondeleteJobs(){
            
            $ids = implode(',', $_POST['ids']);
                                  
            $now = new DateTime();
            
            $cri = new CDbCriteria();
            $cri->condition = "id in (".$ids.")";
            Jobs::model()->deleteAll($cri);
            
            $cri1 = new CDbCriteria();
            $cri1->condition = "job_id in (".$ids.")";
            JobSkill::model()->deleteAll($cri1);
            
           
        }


        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionjobsUpdate($id)
	{
            $now = new DateTime();
            
            $model = Jobs::model()->findByPk($id);
            $usermodel = Users::model()->findByPk($empmodel->user_id);
            
            if(isset($_POST['Jobs']))
            {
                //  insert jobs details
                    $model->attributes = $_POST["Jobs"];
                    $model->industry_id = $_POST["industry_id"];
                    $endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
	            $model->end_date = $endformat;
	            $model->skill_category_id = $_POST["skill_category_id"];
	            $model->type = $_POST["type"];
	            $model->description = $_POST["description"];
	            $model->additional = $_POST["additional"];
	            $model->location_lat = $_POST["location_lat"];
	            $model->location_lng = $_POST["location_lng"];
                    $model->updated_at = $now->format('Y-m-d H:i:s');
                    if($model->save())
                    {
                        
                        $skills = $_POST['skills'];
                        
                        $jkillmodel= JobSkill::model()->deleteAllByAttributes(array('job_id'=>$model->id));
                        
                        foreach ($skills as $skills_val){
                            $jobskillmodel=new JobSkill;
                            $jobskillmodel->job_id = $model->id;
                            $jobskillmodel->skill_id = $skills_val;
                            $jobskillmodel->save();
                        }
                        $this->redirect(array('admin'));
                       
                    }
            }
                
        }
        
        
         /**
	 * Manages Employers Edit.
	 */
        function actionupdateJobs($id)
	{
	    $model = Jobs::model()->findByPk($id);
	    $employermodel = Employers::model()->findByPk($model->owner_id);
	    $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
       	    $this->render('jobsEdit',array('model'=>$model,'employermodel'=>$employermodel,'jobskillmodel'=>$jobskillmodel));
	}
        
        function actionjobsView($id)
	{
	    $model = Jobs::model()->findByPk($id);
	    $employermodel = Employers::model()->findByPk($model->owner_id);
	    $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
       	    $this->render('jobsDetails',array('model'=>$model,'employermodel'=>$employermodel,'jobskillmodel'=>$jobskillmodel));
	}
        
        function actionApplications($id)
	{
	    $model = Jobs::model()->findByPk($id);
	    $jobappmodel = JobApplications::model()->findAllByAttributes(array('job_id'=>$model->id));
       	    $this->render('_jobsapplication',array('model'=>$model,'jobappmodel'=>$jobappmodel));
	}
        
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jobs']))
		{
			$model->attributes=$_POST['Jobs'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Jobs');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Jobs('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Jobs']))
			$model->attributes=$_GET['Jobs'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
         public function actionloadSkills()
         {
    		$model = Skills::model()->findAllByAttributes(array('skill_category_id'=>$_POST['id'])); 
                
                foreach ($model as $mcat) {
                echo "<option  value='$mcat[id]'>$mcat[name]</option>";
                }

             //   echo $this->renderPartial('loadSkills',array('model'=>$docmodel));
    			
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Jobs the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Jobs::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Jobs $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='jobs-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
}
