<?php

class SlideshowsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
		);
	}

	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Slideshows;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Slideshows']))
		{
		    $now = new DateTime();
			$model->attributes=$_POST['Slideshows'];
			$model->user_id = Yii::app()->user->getState('userid');
			$model->status = "published";
			$model->created_at = $now->format('Y-m-d H:i:s');
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Slideshows']))
		{
		    $now = new DateTime();
			$model->attributes=$_POST['Slideshows'];
			$model->background_id = $_POST['photo_id'];
			$model->updated_at = $now->format('Y-m-d H:i:s');			
			if($model->save())
			{			   
				$this->redirect(array('admin'));
			}
		}

		$this->render('edit',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Slideshows');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Slideshows('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Slideshows']))
			$model->attributes=$_GET['Slideshows'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Slideshows the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Slideshows::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Slideshows $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='slideshows-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionEdit($id)
	{
	    $slidemodel = Slideshows::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($slidemodel->user_id);
	    $this->render('edit',array(
	        'model'=>$slidemodel,'usermodel'=>$usermodel
	    ));
	}
	public function actioncreateSlide($id)
	{
	    $slidemodel = Slideshows::model()->findByPk($id);
	    $ssmodel = new SlideshowSlides();
	    
	    $this->render('createslide',array(
	        'model'=>$ssmodel,'ssmodel'=>$slidemodel
	    ));
	}
	public function actionsaveSlide($id)
	{
	    $now = new DateTime();
	    $slidemodel = Slideshows::model()->findByPk($id);
	    $ssmodel = new SlideshowSlides();
	    $ssmodel->attributes = $_POST['SlideshowSlides'];
	    $ssmodel->slideshow_id = $id;
	    $ssmodel->media_id = $_POST['photo_id'];
	    $ssmodel->created_at = $now->format('Y-m-d H:i:s');
	    $ssmodel->user_id = Yii::app()->user->getState('userid');
	    if($ssmodel->save())
	    {
    	    $this->redirect(array('edit','id'=>$slidemodel->id));
	    }
	    
	    $this->render('edit',array(
	        'model'=>$slidemodel,'usermodel'=>$usermodel
	    ));
	}
	public function actionEditSlide($id)
	{
	    $slidemodel = SlideshowSlides::model()->findByPk($id);
	    $slideshowmodel = Slideshows::model()->findByPk($slidemodel->slideshow_id);
	    $this->render('editslide',array(
	        'model'=>$slidemodel,'slideshowmodel'=>$slideshowmodel
	    ));
	}
	public function actionupdateSlide($id)
	{
	    $now = new DateTime();
	    $slidemodel = SlideshowSlides::model()->findByPk($id);
	    $slideshowmodel = Slideshows::model()->findByPk($slidemodel->slideshow_id);
	   
	    $slidemodel->attributes = $_POST['SlideshowSlides'];
	    $slidemodel->updated_at = $now->format('Y-m-d H:i:s');
	    $slidemodel->media_id = $_POST['photo_id'];
	    $slidemodel->user_id = Yii::app()->user->getState('userid');
	    if($slidemodel->save())
	    {	       
	        $this->redirect(array('edit','id'=>$slidemodel->slideshow_id));
	    }
	    
	    $this->render('edit',array(
	        'model'=>$slidemodel
	    ));
	}
	function resize_image($image,$image_type,$fullpath,$destpath,$width,$height)
	{
	    $size_arr = getimagesize($fullpath);
	    
	    list($width_orig, $height_orig, $img_type) = $size_arr;
	    $ratio_orig = $width_orig/$height_orig;
	    
	    $tempimg = imagecreatetruecolor($width, $height);
	    imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	    
	    if($image_type == "image/jpg" || $image_type == "image/jpeg")
	        imagejpeg($tempimg, $destpath);
	        else if( $image_type == "image/png" )
	            imagepng($tempimg, $destpath);
	            else if( $image_type == "image/gif" )
	                imagegif($tempimg, $destpath);
	}
}
