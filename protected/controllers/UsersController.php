<?php
use Spipu\Html2Pdf\Tag\Address;

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
    public $userid = 0;
    public $edit = 0;
    public $dashboard = 0;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
		);
	}

	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Users;
		$profilemodel=new Profiles;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$now = new DateTime();
			$model->attributes=$_POST['Users'];
			$model->type = 'candidate';
			$model->created_at = $now->format('Y-m-d H:i:s');
			if($model->save())
			{
				$profilemodel->owner_id = $model->id;
				$profilemodel->created_at = $model->created_at;
				$profilemodel->type = $model->type;
				$profilemodel->slug = lcfirst($model->forenames);
				if($profilemodel->save())
					$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'pmodel'=>$profilemodel
		));
	}
	public function actioncreateCandidate($trial,$paid,$basic)
	{
	    $model=new Users();
	    $profilemodel= new Profiles();
	    $addressmodel= new Addresses();
	    
	    // Uncomment the following line if AJAX validation is needed
	    // $this->performAjaxValidation($model);
	    
	    if(isset($_POST['Users']))
	    {
	        $promodel = Profiles::model()->countByAttributes(array('slug'=>$_POST["Profiles"]['slug'],'deleted_at'=>null));
            if($_POST["Users"]["password"] != "" && ($_POST["Users"]["password"] != $_POST["confirmpassword"]))
                $pwderr =  "Password not matched";
            else if($promodel > 0)
                $slugerr = 1;
            else
            {
    	        $now = new DateTime();
    	        $model->attributes=$_POST['Users'];
    	        $dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
    	        $model->dob = $dobformat;
    	        $model->type = 'candidate';
    	        $model->created_at = $now->format('Y-m-d H:i:s');
    	        
    	        if($trial == 1)        // 1 year
    	        {
    	            $now->modify("+1 year");
    	            $model->trial_ends_at = $now->format('Y-m-d H:i:s');
    	        }
    
    	        if($model->save())
    	        {
	            $profilemodel->owner_type = "Candidate";
	            $profilemodel->owner_id = $model->id;
	            $profilemodel->photo_id = $_POST['photo_id'];
	            $profilemodel->created_at = $model->created_at;
	            $profilemodel->type = $model->type;
	            $profilemodel->slug = lcfirst($model->forenames);
	            if($profilemodel->save())
	            {
	                if($_POST['Addresses']['postcode'] != "")
	                {
    	                $addressmodel->attributes = $_POST['Addresses'];
    	                $addressmodel->model_id = $model->id;
    	                $addressmodel->model_type = "Candidate";
    	                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
    	                $addressmodel->save();
	                }
	                
	                $this->redirect($this->createUrl('users/admin',array('trial'=>$trial,'paid'=>$paid,'basic'=>$basic)));
	            }
	        }
	      }
	    }
	    
	    $this->render('createcandidate',array(
	        'model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,
	        'trial'=>$trial,'paid'=>$paid,'basic'=>$basic,'pwderr'=>$pwderr,'slugerr'=>$slugerr
	    ));
	  }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$now = new DateTime();
			$model->attributes=$_POST['Users'];
			$model->updated_at = $now->format('Y-m-d H:i:s');
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//$this->loadModel($id)->delete();

		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		$now = new DateTime();
		$model->deleted_at = $now->format('Y-m-d H:i:s');
		if($model->save())
		{
		    $pmodel = new Profiles(); 
		    $now = new DateTime();
		    $pmodel->deleted_at = $now->format('Y-m-d H:i:s');
		    $pmodel->save();
		}
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Users');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($trial = 0,$paid = 0,$basic = 0)
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		
		$namesearch = 0;
		$fullname = '';
		$userarr = array();
		
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];
		if(isset($_GET['fullname']) && $_GET['fullname']!= "")	
		{
		    $namesearch = 1;
		    $fullname = $_GET['fullname'];
		    $cri = new CDbCriteria();
		    $cri->condition = "concat_ws(' ',forenames,surname) like '%".$fullname."%' and type = 'candidate' and deleted_at is null";
		    
		    if($trial == 1)
		        $cri->addCondition('trial_ends_at is not null');
		        else if($paid == 1)
		            $cri->addCondition('stripe_id > 0');
		            else if($basic == 1)
		                $cri->addCondition('trial_ends_at is null and stripe_id is null');
		            
		    $searchusers = Users::model()->findAll($cri);
		    $userarr = array();
		    foreach ($searchusers as $users)
		        $userarr[] = $users['id'];
		}
		
		$this->render('admin',array(
		    'model'=>$model,'trial'=>$trial,'paid'=>$paid,'basic'=>$basic,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionsearchAdmin($trial = 0,$paid = 0,$basic = 0)
	{
	    $model=new Users('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['Users']))
	    {
	        $model->attributes=$_GET['Users'];	        
	    } 
	    
        $this->render('admin',array(
            'model'=>$model,'issearch'=>1,'trial'=>$trial,'paid'=>$paid,'basic'=>$basic
        ));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
    public function actionProfile($id)
    {
        $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$id));
        $pid = $pmodel->id;
        $this->layout = "profile";
        $now = new DateTime();
        $type = ucfirst(Yii::app()->user->getState('role'));
        $modelid = Yii::app()->user->getState('userid');
        $cri = new CDbCriteria();
        $cri->condition = "profile_id =".$pid." and model_type like '%".$type."%' and model_id=".$modelid;
       
        $profileviews = ProfileViews::model()->find($cri);
        if($profileviews == null)
        {
            $pvmodel = new ProfileViews();
            $pvmodel->profile_id = $pid;
            $pvmodel->model_type = ucfirst(Yii::app()->user->getState('role'));
            $pvmodel->model_id = Yii::app()->user->getState('userid');
            $pvmodel->view_count = 1;
            $pvmodel->created_at = $now->format('Y-m-d H:i:s');
            $pvmodel->save();
        }
        else 
        {
//          ProfileViews::model()->updateCounters(array('view_count'=>1),
//                          "profile_id =:profile_id and model_type like '%:model_type%' and model_id =:model_id",array(':profile_id'=>$id,':model_type'=>$type,':model_id'=>$modelid));
            
            $countcri = new CDbCriteria();
            $countcri->select = 'max(view_count) as view_count';
            $countcri->condition = "profile_id =".$pid." and model_type like '%".$type."%' and model_id=".$modelid;
            $proview = ProfileViews::model()->find($countcri);
            $viewcount = $proview['view_count'] + 1;
                        
            $cri = new CDbCriteria();
            $cri->condition = "profile_id =".$pid." and model_type like '%".$type."%' and model_id=".$modelid;
            ProfileViews::model()->updateAll(array('updated_at'=>$now->format('Y-m-d H:i:s'),'view_count'=>$viewcount),$cri);
            
            //"profile_id =:profile_id and model_type like '%:model_type%' and model_id =:model_id",array(':profile_id'=>$id,':model_type'=>$type,':model_id'=>$modelid));
        }
    	$this->userid = $id;
    	$model = Users::model()->findByPk($id);
    	$this->render('profile',array(
    			'model'=>$model,
    	));
    }
    public function actionEdit($id)
    {
        $this->layout = "profile";
    	$this->userid = $id;
    	$this->edit = 1;
    	$model = Users::model()->findByPk($id);
    	$this->render('editprofile',array(
    			'model'=>$model,
    	));
    }
    public function actionsaveUserDoc()
    {
            $usermodel = Users::model()->findByPk($_POST['user_id']);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
        
    		$now = new DateTime();
    	
	    	$mediamodel = new Media();
	    	$mediamodel->model_type = "Candidate";
	    	$mediamodel->model_id = $_POST['user_id'];
	    	$mediamodel->collection_name = "documents";
	    	$mediamodel->name = $_FILES['doc_file']['name'];
	    	$mediamodel->file_name = $_FILES['doc_file']['name'];
	    	$mediamodel->size = $_FILES['doc_file']['size'];
	    	$mediamodel->disk = "documents";
	    	$mediamodel->created_at = $now->format('Y-m-d H:i:s');
	    	if($mediamodel->save())
	    	{
		    	$model = new UserDocuments();
		    	$model->user_id = $_POST['user_id'];
		    	$model->media_id = $mediamodel->id;
		    	$model->name = $_POST['doc_name'];
		    	$model->category_id = $_POST['category_id'];
		    	$model->published = $_POST['published'];
		    	$model->created_at = $now->format('Y-m-d H:i:s');
		    	if($model->save())
		    	{
    		    	$pdmodel = new ProfileDocuments();    		    	
    		    	$pdmodel->profile_id = $profilemodel->id;
    		    	$pdmodel->user_document_id = $model->id;
    		    	$pdmodel->created_at = $now->format('Y-m-d H:i:s');
    		    	$pdmodel->save();
		    	}
		    	$docmodel = Users::model()->findByPk($model->user_id);
		    	echo $this->renderPartial('userDocuments',array('model'=>$docmodel));
	    	}
    }
    
 	public function actionupdateUserDoc()
    {
    		$now = new DateTime();    	
    		$model = UserDocuments::model()->findByPk($_POST['id']); 
    		
    		$usermodel = Users::model()->findByPk($model->user_id);
    		$profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
    		
	    	$mediamodel = Media::model()->findByPk($_POST['media_id']);
	    	$mediamodel->name = $_FILES['doc_file']['name'];
	    	$mediamodel->file_name = $_FILES['doc_file']['name'];
	    	$mediamodel->size = $_FILES['doc_file']['size'];
	    	$mediamodel->updated_at = $now->format('Y-m-d H:i:s');
	    	if($mediamodel->save())
	    	{
		    	$model->name = $_POST['name'];
		    	$model->category_id = $_POST['category_id'];
		    	$model->published = $_POST['published'];
		    	$model->updated_at = $now->format('Y-m-d H:i:s');
		    	if($model->save())
		    	{
		    	    $pdmodel = ProfileDocuments::model()->findByAttributes(array('profile_id'=>$profilemodel->id,'user_document_id'=>$model->id));		    	   
		    	    $pdmodel->updated_at = $now->format('Y-m-d H:i:s');
		    	    $pdmodel->save();
		    	}
		    	
		    	$docmodel = Users::model()->findByPk($model->user_id);
		    	echo $this->renderPartial('userDocuments',array('model'=>$docmodel));
	    	}
    }
    public function actiondeleteUserDoc()
    {
    	$model = UserDocuments::model()->findByPk($_POST['id']);
    	$userid = $model->user_id;
    	
    	$docmodel = Users::model()->findByPk($userid);
    	
    	$cri = new CDbCriteria();
    	$cri->condition = "model_id =".$model->media_id;
    	Media::model()->deleteAll($cri);
    	
    	$cri1 = new CDbCriteria();
    	$cri1->condition = "user_document_id =".$_POST['id'];
    	ProfileDocuments::model()->deleteAll($cri1);
    	
    	$cri2 = new CDbCriteria();
    	$cri2->condition = "id =".$_POST['id'];
    	UserDocuments::model()->deleteAll($cri2);
    	
    	
    	echo $this->renderPartial('userDocuments',array('model'=>$docmodel));    	
    }
    
    public function actionsaveUserExp()
    {
            $usermodel = Users::model()->findByPk($_POST['user_id']);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
        
    		$now = new DateTime();
    		$current = null;
    		$start_month = $_POST['start_month'];
    		$start_year = $_POST['start_year'];
    		$start_date = $_POST['start_year']."-".$_POST['start_month']."-01";
    		$end_month = $_POST['end_month'];
    		$end_year = $_POST['end_year'];
    		$end_date = $_POST['end_year']."-".$_POST['end_month']."-01";
    		
    		if(isset($_POST['is_current']))
    			$end_date = null;
    		
    		$model = new UserExperiences();
    		$model->user_id = $_POST['user_id'];
    		$model->start_date = $start_date;
    		$model->end_date = $end_date;
    		$model->description = $_POST['description'];
    		$model->position = $_POST['position'];
    		$model->company_name = $_POST['company_name'];
    		$model->location = $_POST['location'];    		
    		$model->created_at = $now->format('Y-m-d H:i:s');
    		if($model->save())
    		{
    		    $pemodel = new ProfileExperiences();
    		    $pemodel->profile_id = $profilemodel->id;
    		    $pemodel->user_experience_id = $model->id;
    		    $pemodel->created_at = $now->format('Y-m-d H:i:s');
    		    $pemodel->save();
    		}
    		
    		$expmodel = Users::model()->findByPk($model->user_id);
    		echo $this->renderPartial('userExp',array('model'=>$expmodel));
    	
    }
    
    public function actionupdateUserExp()
    {
        $model = UserExperiences::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
        
	    	$now = new DateTime();	    	
	    	$current = null;
	    	$start_month = $_POST['start_month'];
	    	$start_year = $_POST['start_year'];
	    	$start_date = $_POST['start_year']."-".$_POST['start_month']."-01";
	    	$end_month = $_POST['end_month'];
	    	$end_year = $_POST['end_year'];
	    	$end_date = $_POST['end_year']."-".$_POST['end_month']."-01";
	    	
	    	if(isset($_POST['is_current']))
    		  $end_date = null;
    		
    	
    		$model->start_date = $start_date;
    		$model->end_date = $end_date;
    		$model->description = $_POST['description'];
    		$model->position = $_POST['position'];
    		$model->company_name = $_POST['company_name'];
    		$model->location = $_POST['location'];    		
    		$model->updated_at = $now->format('Y-m-d H:i:s');
    		if($model->save())
    		{
    		    $pemodel = ProfileExperiences::model()->findByAttributes(array('profile_id'=>$profilemodel->id,'user_experience_id'=>$model->id));
    		    $pemodel->updated_at = $now->format('Y-m-d H:i:s');
    		    $pemodel->save();
    		}
    		
    		$expmodel = Users::model()->findByPk($model->user_id);
    		echo $this->renderPartial('userExp',array('model'=>$expmodel));
    	
    }
    public function actiondeleteUserExp()
    {
    	$model = UserExperiences::model()->findByPk($_POST['id']);
    	$userid = $model->user_id;
    	
    	$cri = new CDbCriteria();
    	$cri->condition = "user_experience_id =".$_POST['id'];
    	ProfileExperiences::model()->deleteAll($cri);
    	
    	$cri1 = new CDbCriteria();
    	$cri1->condition = "id =".$_POST['id'];
    	UserExperiences::model()->deleteAll($cri1);
    	 
    	$expmodel = Users::model()->findByPk($userid);
    	echo $this->renderPartial('userExp',array('model'=>$expmodel));
    	
    }
    
    public function actionsaveUserEducation()
    {
            $usermodel = Users::model()->findByPk($_POST['user_id']);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
        
	    	$now = new DateTime();
	    	$completion_month = $_POST['completion_month'];
	    	$completion_year = $_POST['completion_year'];
	    	$completion_date = $_POST['completion_year']."-".$_POST['completion_month']."-01";
    
    		$model = new UserQualifications();
    		$model->user_id = $_POST['user_id'];
    		$model->completion_date = $completion_date;
    		$model->institution = $_POST['institution'];
    		$model->location = $_POST['location'];
    		$model->field = $_POST['field'];
    		$model->level = $_POST['level'];
    		$model->grade = $_POST['grade'];
    		$model->description = $_POST['description'];
    		$model->created_at = $now->format('Y-m-d H:i:s');
    		if($model->save())
    		{
    		    $pqmodel = new ProfileQualifications();
    		    $pqmodel->profile_id = $profilemodel->id;
    		    $pqmodel->user_qualification_id = $model->id;
    		    $pqmodel->created_at = $now->format('Y-m-d H:i:s');
    		    $pqmodel->save();
    		}
    
    		$edumodel = Users::model()->findByPk($model->user_id);
    		echo $this->renderPartial('userEducation',array('model'=>$edumodel));
    		 
    }
    
    public function actionupdateUserEducation()
    {
        $model = UserQualifications::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
        
    		$now = new DateTime();
    
    		$completion_month = $_POST['completion_month'];
	    	$completion_year = $_POST['completion_year'];
	    	$completion_date = $_POST['completion_year']."-".$_POST['completion_month']."-01";
    
    		
    		$model->completion_date = $completion_date;
    		$model->institution = $_POST['institution'];
    		$model->location = $_POST['location'];
    		$model->field = $_POST['field'];
    		$model->level = $_POST['level'];
    		$model->grade = $_POST['grade'];
    		$model->description = $_POST['description'];
    		$model->updated_at = $now->format('Y-m-d H:i:s');
    		if($model->save())
    		{
    		    $pemodel = ProfileQualifications::model()->findByAttributes(array('profile_id'=>$profilemodel->id,'user_qualification_id'=>$model->id));
    		    $pemodel->updated_at = $now->format('Y-m-d H:i:s');
    		    $pemodel->save();
    		}
    
    		$edumodel = Users::model()->findByPk($model->user_id);
    		echo $this->renderPartial('userEducation',array('model'=>$edumodel));
    		 
    }
    public function actiondeleteUserEducation()
    {
    	$model = UserQualifications::model()->findByPk($_POST['id']);
    	$userid = $model->user_id;
    	 
    	$cri = new CDbCriteria();
    	$cri->condition = "user_qualification_id =".$_POST['id'];
    	ProfileQualifications::model()->deleteAll($cri);
    	
    	$cri1 = new CDbCriteria();
    	$cri1->condition = "id =".$_POST['id'];
    	$model = UserQualifications::model()->deleteAll($cri1);
    
    	$expmodel = Users::model()->findByPk($userid);
    	echo $this->renderPartial('userEducation',array('model'=>$expmodel));
    	 
    }
    
    public function actionsaveUserHobbies()
    {
        $usermodel = Users::model()->findByPk($_POST['user_id']);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
    	$now = new DateTime();
    	
    	$model = new UserHobbies();
    	$model->user_id = $_POST['user_id'];
    	$model->activity = $_POST['activity'];
    	$model->description = $_POST['description'];
    	$model->created_at = $now->format('Y-m-d H:i:s');
    	if($model->save())
    	{
    	    $pqmodel = new ProfileHobbies();
    	    $pqmodel->profile_id = $profilemodel->id;
    	    $pqmodel->user_hobby_id = $model->id;
    	    $pqmodel->created_at = $now->format('Y-m-d H:i:s');
    	    $pqmodel->save();
    	}
    
    	$edumodel = Users::model()->findByPk($model->user_id);
    	echo $this->renderPartial('userHobbies',array('model'=>$edumodel));
    	 
    }
    
    public function actionupdateUserHobbies()
    {
        $model =  UserHobbies::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
        
    	$now = new DateTime();    
    	
    	$model->activity = $_POST['activity'];
    	$model->description = $_POST['description'];
    	$model->updated_at = $now->format('Y-m-d H:i:s');
    	if($model->save())
    	{
    	    $pemodel = ProfileHobbies::model()->findByAttributes(array('profile_id'=>$profilemodel->id,'user_hobby_id'=>$model->id));
    	    $pemodel->updated_at = $now->format('Y-m-d H:i:s');
    	    $pemodel->save();
    	}
    
    	$edumodel = Users::model()->findByPk($model->user_id);
    	echo $this->renderPartial('userHobbies',array('model'=>$edumodel));
    	 
    }
    public function actiondeleteUserHobbies()
    {
    	$model = UserHobbies::model()->findByPk($_POST['id']);
    	$userid = $model->user_id;
    
    	$cri = new CDbCriteria();
    	$cri->condition = "user_hobby_id =".$_POST['id'];
    	ProfileHobbies::model()->deleteAll($cri);
    	
    	$cri1 = new CDbCriteria();
    	$cri1->condition = "id =".$_POST['id'];
    	$model = UserHobbies::model()->deleteAll($cri1);
    
    	$expmodel = Users::model()->findByPk($userid);
    	echo $this->renderPartial('userHobbies',array('model'=>$expmodel));
    
    }
    public function actionsaveUserLanguages()
    {
        $usermodel = Users::model()->findByPk($_POST['user_id']);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
    	$now = new DateTime();
    	 
    	$model = new UserLanguages();
    	$model->user_id = $_POST['user_id'];
    	$model->name = $_POST['name'];
    	$model->proficiency = $_POST['proficiency'];
    	$model->created_at = $now->format('Y-m-d H:i:s');
    	if($model->save())
    	{
    	    $pqmodel = new ProfileLanguages();
    	    $pqmodel->profile_id = $profilemodel->id;
    	    $pqmodel->user_language_id = $model->id;
    	    $pqmodel->created_at = $now->format('Y-m-d H:i:s');
    	    $pqmodel->save();
    	}
    
    	$langmodel = Users::model()->findByPk($model->user_id);
    	echo $this->renderPartial('userLanguage',array('model'=>$langmodel));
    
    }
    
    public function actionupdateUserLanguages()
    {
        $model = UserLanguages::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
        
    	$now = new DateTime();    	
    	
    	$model->name = $_POST['name'];
    	$model->proficiency = $_POST['proficiency'];
    	$model->updated_at = $now->format('Y-m-d H:i:s');
    	if($model->save())
    	{
    	    $pemodel = ProfileLanguages::model()->findByAttributes(array('profile_id'=>$profilemodel->id,'user_language_id'=>$model->id));
    	    $pemodel->updated_at = $now->format('Y-m-d H:i:s');
    	    $pemodel->save();
    	}
    
    	$langmodel = Users::model()->findByPk($model->user_id);
    	echo $this->renderPartial('userLanguage',array('model'=>$langmodel));
    
    }
    public function actiondeleteUserLanguages()
    {
    	$model = UserLanguages::model()->findByPk($_POST['id']);
    	$userid = $model->user_id;
    
    	$cri = new CDbCriteria();
    	$cri->condition = "user_language_id =".$_POST['id'];
    	ProfileLanguages::model()->deleteAll($cri);
    	
    	$cri1 = new CDbCriteria();
    	$cri1->condition = "id =".$_POST['id'];
    	$model = UserLanguages::model()->deleteAll($cri1);
    
    	$langmodel = Users::model()->findByPk($userid);
    	echo $this->renderPartial('userLanguage',array('model'=>$langmodel));
    
    }
    public function actionresume($id)
    {
    	$usermodel = Users::model()->findByPk($id);
    	
    	$mPDF1 = Yii::app()->ePdf->mpdf();    	
    	$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');    	
    	$stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
    	$mPDF1->WriteHTML($stylesheet, 1);    	
    	$mPDF1->WriteHTML($this->renderPartial('resume',array('model'=>$usermodel), true));    
    	$mPDF1->Output();
    	
       // $this->render('resume',array('model'=>$usermodel));
    }
    public function actionviewBCard($id)
    {
    	$usermodel = Users::model()->findByPk($id);
    	 
    	$mPDF1 = Yii::app()->ePdf->mpdf();
    	$mPDF1 = Yii::app()->ePdf->mpdf('', 'A7');    	
//     	$stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
//     	$mPDF1->WriteHTML($stylesheet, 1);
    	$mPDF1->AddPage('L');
    	$mPDF1->SetDisplayMode('fullwidth');
    	$mPDF1->WriteHTML($this->renderPartial('businesscard',array('model'=>$usermodel), true));
    	$mPDF1->Output(); 
    	 
    	// $this->render('businesscard',array('model'=>$usermodel));
    }
    
    public function actiondownloadBCard($id)
    {
    	$usermodel = Users::model()->findByPk($id);
    
    	$mPDF1 = Yii::app()->ePdf->mpdf();
    	$mPDF1 = Yii::app()->ePdf->mpdf('', 'A7');
    	//     	$stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
    	//     	$mPDF1->WriteHTML($stylesheet, 1);
    	$mPDF1->AddPage('L');
    	$mPDF1->SetDisplayMode('fullwidth');
    	$mPDF1->WriteHTML($this->renderPartial('businesscard',array('model'=>$usermodel), true));
    	$mPDF1->Output('BusinessCard.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD );
    
    	// $this->render('businesscard',array('model'=>$usermodel));
    }
    
    public function actionchangeUserSkills()
    {
    	$skillid = $_POST['skill_id'];
    	$userid = $_POST['user_id'];
    	$action = $_POST['action'];
    	
    	$profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid));
    	$profileid = $profilemodel['id'];
    	if($action == "add")
    	{
    		$psmodel = new ProfileSkills();
    		$psmodel->profile_id = $profileid;
    		$psmodel->skill_id = $skillid;
    		$psmodel->save();
    	}
    	else 
    	{
    		ProfileSkills::model()->deleteAllByAttributes(array('profile_id'=>$profileid,'skill_id'=>$skillid));
    	}
    	
    	$pmodel = Users::model()->findByPk($userid);
    	
    	$profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$pmodel->id,'type'=>$pmodel->type));
    	echo $this->renderPartial('userCareer',array('model'=>$pmodel,'profilemodel'=>$profilemodel));
    }
    
    public function actionrenderUserSkills()
    {
    	$userid = $_POST['user_id'];
    	$pmodel = Users::model()->findByPk($userid);
    	 
    	$profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$pmodel->id,'type'=>$pmodel->type));
    	echo $this->renderPartial('userCareer',array('model'=>$pmodel,'profilemodel'=>$profilemodel));
    }
    
    public function actionuserAccountInfo($id)
    {
    	$model = Users::model()->findByPk($id);
    	$profile = Profiles::model()->findByAttributes(array('owner_id'=>$id));
    	$pmodel = Profiles::model()->findByPk($profile['id']);
    	$this->render('accountinfo',array('model'=>$model,'pmodel'=>$pmodel));
    }
    
    public function actionupdateAccountInfo($id)
    {
    	$now = new DateTime();
    	$model = Users::model()->findByPk($id);
    	$pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$id));
    	$profilemodel = Profiles::model()->findByPk($pmodel->id);
    	$model->attributes=$_POST['Users'];
    	$model->type = 'candidate';
    	$model->updated_at = $now->format('Y-m-d H:i:s');
    	if($model->save())
    	{
    		$profilemodel->updated_at = $model->created_at;
    		$profilemodel->type = $model->type;
    		$profilemodel->slug = $_POST['Profiles']['slug'];
    		$profilemodel->visibility = $_POST['status'];
    		$profilemodel->save();
    	}
    	
    	$this->render('accountinfo',array('model'=>$model,'pmodel'=>$pmodel));
    }
    public function actionchangeUserVisiblity()
    {
    	$status = $_POST['status'];
    	$model = Users::model()->findByPk($_POST['userid']);
    	$pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id));
    	$profilemodel = Profiles::model()->findByPk($pmodel->id);
    	$profilemodel->visibility = ($status == "Public" ? 0 : 1);
    	$profilemodel->save();
    }
    public function actionchangeUserStatus()
    {
    	$status = $_POST['status'];
    	$model = Users::model()->findByPk($_POST['userid']);    	
    	$pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));
    	$profilemodel = Profiles::model()->findByPk($pmodel->id);
    	$profilemodel->published = (strtolower($status) == 'active' ? 0 : 1);
    	$profilemodel->save();
    }
    
    public function actionsaveCoverPhoto()
    {
    	$profileid = $_POST['profileid'];
    	$pmodel = Profiles::model()->findByPk($profileid);
    	$now = new DateTime();
    	if($pmodel->cover_id > 0)
    	{
    	    $mediamodel = Media::model()->findByPk($pmodel->cover_id);
    	    $mediamodel->updated_at = $now->format('Y-m-d H:i:s');
    	}
    	else 
    	{
    	    $mediamodel = new Media();
    	    $mediamodel->created_at = $now->format('Y-m-d H:i:s');
    	    $mediamodel->collection_name = "sample";
    	    $mediamodel->model_id = $profileid;
    	}
    		$filename = $_FILES['cover']['name'];
    		$image_type = $_FILES['cover']['type'];
    		$file = $_FILES['cover']['tmp_name'];
    		$name = pathinfo( $_FILES['cover']['name'], PATHINFO_FILENAME);
    			    	
	    	$mediamodel->name = $name;
	    	$mediamodel->file_name = $filename;
	    	$mediamodel->size = $_FILES['cover']['size'];
	    	
	    	if($mediamodel->save())
	    	{	    	    
	    	    $mediafolder  = "images/media/".$mediamodel->id."/conversions";
	    	    
	    	    if (!file_exists($mediafolder)) {
	    	        mkdir($mediafolder, 0777, true);
	    	    }
	    	    
	    	    $imgprefix  = "images/media/".$mediamodel->id;
	    	    $fullpath = "images/media/".$mediamodel->id."/".$filename;
	    	    
	    	    Profiles::model()->updateAll(array('cover_id'=>$mediamodel->id),'id =:id',array(':id'=>$profileid));
	    	    
	    		    	if(move_uploaded_file($_FILES['cover']['tmp_name'], $fullpath)) 
	    		    	{
		    		    		//create cover
		    		    		$coverwidth  = 1000;
		    		    		$coverheight = 400;
		    		    		$coverdestpath = $imgprefix."/conversions/cover.jpg";
		    		    		
		    		    		//create icon
		    		    		$iconwidth  = 40;
		    		    		$iconheight = 40;
		    		    		$icondestpath = $imgprefix."/conversions/icon.jpg";
		    		    		
		    		    		//create joblisting
		    		    		$jlwidth  = 150;
		    		    		$jlheight = 85;
		    		    		$jldestpath = $imgprefix."/conversions/joblisting.jpg";
		    		    		
		    		    		//create search
		    		    		$searchwidth  = 167;
		    		    		$searchheight = 167;
		    		    		$searchdestpath = $imgprefix."/conversions/search.jpg";
		    		    		
		    		    		//create thumb
		    		    		$thumbwidth  = 126;
		    		    		$thumbheight = 126;
		    		    		$thumbdestpath = $imgprefix."/conversions/thumb.jpg";
		    		    		
    		    		    	if( $image_type == "image/jpg" || $image_type == "image/jpeg") {    		    		    		
					    			
					    			$image = imagecreatefromjpeg($fullpath);
					    			
					    			$this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
					    			$this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
					    			$this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
					    			$this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
					    			$this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
					    			
					    		}
						    	elseif( $image_type == "image/gif" )  {						    		
						    		
					    			$image = imagecreatefromgif($fullpath);
					    			
					    			$this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
					    			$this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
					    			$this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
					    			$this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
					    			$this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
					    			
						    	}
						    	elseif( $image_type == "image/png" ) {
						    		
						    		$image = imagecreatefrompng($fullpath);
					    			
					    			$this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
					    			$this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
					    			$this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
					    			$this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
					    			$this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
						    	}
						    		
	    		    	}	   
	    		    	
	    		    	echo Yii::app()->baseUrl."/".$fullpath;
	    	}
    	   	
    	  
    }
    
	function resize_image($image,$image_type,$fullpath,$destpath,$width,$height)
	{		
			$size_arr = getimagesize($fullpath);
		
	 		list($width_orig, $height_orig, $img_type) = $size_arr;
	    	$ratio_orig = $width_orig/$height_orig;  
	    			
	    	$tempimg = imagecreatetruecolor($width, $height);
	    	imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	    	
	    	if($image_type == "image/jpg" || $image_type == "image/jpeg") 
	    		imagejpeg($tempimg, $destpath);	    		
	    	else if( $image_type == "image/png" )  
	    		imagepng($tempimg, $destpath);
	    	else if( $image_type == "image/gif" )
	    		imagegif($tempimg, $destpath);
	}
	function actionprofileEdit($id)
	{
	    $model = Users::model()->findByPk($id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));
	    $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$model->id));
	    $this->render('profileEdit',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel));
	}
	
	function actiongetProfilePhoto()
	{
	    echo $_FILES['profile']['tmp_name'];
	}
	function actionupdateProfileDetails($id)
	{
	    $now = new DateTime();
	    $usermodel = Users::model()->findByPk($id);
	    $pwd = $usermodel->password;
	    $usermodel->attributes = $_POST["Users"];
	    $pwderr = "";
	    $slugerr = "";
	    $promodel = Profiles::model()->countByAttributes(array('slug'=>$_POST["Profiles"]['slug'],'deleted_at'=>null));
	    
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>$usermodel->type));
	    $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$usermodel->id));
	    
	    if($_POST["Users"]["password"] != "" && ($_POST["Users"]["password"] != $_POST["confirmpassword"]))
	        $pwderr =  "Password not matched";
	    else if($promodel > 0)
	        $slugerr = 1;
	    else 
	    {
	        if($_POST["Users"]["password"] == "")
	            $usermodel->password = $pwd;
	            
	            $usermodel->type = "candidate";
	            $dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
	            $usermodel->dob = $dobformat;
	            $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	            if($usermodel->save())
	            {
	                $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>$usermodel->type));
	                $profilemodel = Profiles::model()->findByPk($pmodel->id);
	                $profilemodel->slug = $_POST["Profiles"]['slug'];
	                $profilemodel->photo_id =  $_POST['photo_id'];
	                $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
	                if($profilemodel->save())
	                {
	                    $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$id));
	                    if($addressmodel == null)
	                    {
	                        $addressmodel = new Addresses();
	                        $addressmodel->attributes = $_POST["Addresses"];
	                        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                    }
	                    else
	                    {
	                        $addressmodel->attributes = $_POST["Addresses"];
	                        $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
	                    }
	                    
	                    $addressmodel->model_id = $id;
	                    $addressmodel->save();
	                }
	                
	            }
	    }
	    $this->render('profileEdit',array('model'=>$usermodel,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,'pwderr'=>$pwderr,'slugerr'=>$slugerr));
	}
	
	public function actionaddProfileVideo()
	{	   
	    $now = new DateTime();
	    $userid = $_POST['userid'];
	    $name = $_POST['video_name'];
	    $size = $_FILES['video']['size'];
	    
	    
	    $usermodel = Users::model()->findByPk($userid);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid,'type'=>$usermodel->type));
       
	    require_once Yii::app()->basePath . '/extensions/vimeo/autoload.php';
	    
	    $client_id = "4b7c96cce1b91f014fda20977a566fb3f2fad55b";
	    $client_secret = "0YCMBrjyjvYcg1NNnADG8NcqUz4tlefTZwWgo710izgAg61shhbA40cBNUPSpESkBvk/isCpKSVzbhw4R9Lo3RU7T7CyO9k8a9LkyGZfwA4e08fO7v8AHmAcIpwWDkk6";
	    $lib = new \Vimeo\Vimeo($client_id, $client_secret);
	    
	    $token = $lib->clientCredentials(array('private,upload'));
	    
	    $lib->setToken('7d94930eef527f3e6c8efda9e3142660');
	    
	   $response = $lib->upload($_FILES['video']['tmp_name'], false);
// 	    $response = $lib->replace('/videos/241658003', $_FILES['video']['tmp_name'], false);
	    if($response)
	    {
	        
	       $response1 = $lib->request($response, array('name' => $name ), 'PATCH');
	        
	        $duration = $response1['body']['duration'];
	        $url = $response1['body']['link'];
	        $videoid = substr($url, 0, strpos($url, "/videos/"));
	        
	        $str = $response;
	        $del = "/videos/";
	        $pos = strpos($str, $del);
	        $videoid = substr($str, $pos+strlen($del), strlen($str)-1);
	       
	        $videomodel = new Videos();
	        $videomodel->model_id = $userid;
	        $videomodel->video_id = $videoid;
	        $videomodel->name = $name;
	        $videomodel->duration = $duration;
	        $videomodel->model_type = $usermodel->type;
	        $videomodel->category_id = 1;
	        $videomodel->is_processed = 1;
	        $videomodel->state = 1;
	        $videomodel->status = "active";
	        $videomodel->created_at = $now->format('Y-m-d H:i:s');
	        if($videomodel->save())
	        {	            
	            $pvideomodel = new ProfileVideos(); 
	            $pvideomodel->profile_id = $pmodel->id;
	            $pvideomodel->video_id = $videomodel->id;
	            $pvideomodel->created_at = $now->format('Y-m-d H:i:s');
	            $pvideomodel->state = 1;
	            if($pvideomodel->save())
	            {	             
	                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$videoid.php"));
	                $imgpath = $hash[0]['thumbnail_medium']; 
	                $del = "http://i.vimeocdn.com/video/";
	                $pos = strpos($imgpath, $del);
	                $img_name = substr($imgpath, $pos+strlen($del), strlen($imgpath)-1);
	                
	                $mediamodel = new Media();
	                $mediamodel->model_id = $videomodel->id;
	                $mediamodel->disk = "media";
	                $mediamodel->model_type = $usermodel->type;
	                $mediamodel->collection_name = "video";
	                $mediamodel->name = $img_name;
	                $mediamodel->file_name = $img_name;
	                $mediamodel->created_at = $now->format('Y-m-d H:i:s');
	                if($mediamodel->save())
	                {
	                    $filename = $img_name;
	                    $mediafolder  = "images/media/".$mediamodel->id."/conversions";
	                    
	                    $imgprefix  = "images/media/".$mediamodel->id;
	                    $fullpath = "images/media/".$mediamodel->id."/".$img_name;
	                    
	                    if (!file_exists($mediafolder)) {
	                        mkdir($mediafolder, 0777, true);
	                    }
	                     
	                    if (copy($imgpath, $fullpath))
    	                    {
    	                        
    	                        //create joblisting
    	                        $jlwidth  = 350;
    	                        $jlheight = 190;
    	                        $jldestpath = $imgprefix."/conversions/profile.jpg";
    	                        
    	                        //create search
    	                        $searchwidth  = 167;
    	                        $searchheight = 167;
    	                        $searchdestpath = $imgprefix."/conversions/search.jpg";
    	                        
    	                        //create thumb
    	                        $thumbwidth  = 126;
    	                        $thumbheight = 126;
    	                        $thumbdestpath = $imgprefix."/conversions/thumb.jpg";
    	                        
    	                        if( $image_type == "image/jpg" || $image_type == "image/jpeg") {
    	                            
    	                            $image = imagecreatefromjpeg($fullpath);
    	                            
    	                            $this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
    	                            $this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
    	                            $this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
    	                            $this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
    	                            $this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
    	                            
    	                        }
    	                        elseif( $image_type == "image/gif" )  {
    	                            
    	                            $image = imagecreatefromgif($fullpath);
    	                            
    	                            $this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
    	                            $this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
    	                            $this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
    	                            $this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
    	                            $this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
    	                            
    	                        }
    	                        elseif( $image_type == "image/png" ) {
    	                            
    	                            $image = imagecreatefrompng($fullpath);
    	                            
    	                            $this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
    	                            $this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
    	                            $this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
    	                            $this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
    	                            $this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
    	                        }
    	                        
    	                     }	
	                }
	            }
	            
	            
	        }
	        else 
	        {
	            echo var_dump($videomodel->errors);
	            die();
	        }
	     }
           	    
	     echo $this->renderPartial('userVideos',array('model'=>$usermodel,'profilemodel'=>$pmodel));
	}
	
	public function actionupdateProfileVideoName()
	{
	    $videoid = $_POST['id'];
	    $videoname = $_POST['name'];
	    
	    $now = new DateTime();
	    $vmodel = Videos::model()->findByPk($videoid);	   
	    $vmodel->name = $videoname;
	    $vmodel->updated_at = $now->format('Y-m-d H:i:s');
	    $vmodel->save();
	    
	    $userid = $vmodel->model_id;
	    $usermodel = Users::model()->findByPk($userid);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid,'type'=>$usermodel->type));
	    
	    echo $this->renderPartial('userVideos',array('model'=>$usermodel,'profilemodel'=>$pmodel));
	}
	
	public function actionprofileVideoDefault()
	{
	    
	    $videoid = $_POST['id'];
	    $vmodel = Videos::model()->findByPk($videoid);
	    $userid = $vmodel->model_id;
	    $usermodel = Users::model()->findByPk($userid);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid,'type'=>$usermodel->type));
	    
	    ProfileVideos::model()->updateAll(array('is_default'=> 0),'profile_id =:profile_id',array(':profile_id'=>$pmodel->id));
	    
	    ProfileVideos::model()->updateAll(array('is_default'=> 1),'video_id =:video_id',array(':video_id'=>$videoid));

	    echo $this->renderPartial('userVideos',array('model'=>$usermodel,'profilemodel'=>$pmodel));
	}
	
	public function actionprofileVideoDelete()
	{
	    
	    $videoid = $_POST['id'];
	    $vmodel = Videos::model()->findByPk($videoid);
	    $userid = $vmodel->model_id;
	    
	    $vmodel = Videos::model()->deleteByPk($videoid);
	    
	    $usermodel = Users::model()->findByPk($userid);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid,'type'=>$usermodel->type));
	    
	    ProfileVideos::model()->deleteAllByAttributes(array('video_id'=>$videoid));	    
	    
	    echo $this->renderPartial('userVideos',array('model'=>$usermodel,'profilemodel'=>$pmodel));
	}
	
	public function actionprofileVideoPublish()
	{
	    
	    $videoid = $_POST['id'];
	    $publishstatus = $_POST['status'];
	    
	    if(trim($publishstatus) == "Published")
	        $state = 0;
	    else 
	        $state = 1;
	    
	    ProfileVideos::model()->updateAll(array('state'=> $state),'video_id =:video_id',array(':video_id'=>$videoid));
	    
	    $vmodel = Videos::model()->findByPk($videoid);
	    $userid = $vmodel->model_id;
	    
	    $usermodel = Users::model()->findByPk($userid);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid,'type'=>$usermodel->type));
	    
	    echo $this->renderPartial('userVideos',array('model'=>$usermodel,'profilemodel'=>$pmodel));
	}
	
	public function actiondeleteUsers()
	{
	    $ids = implode(',', $_POST['ids']);
	    $now = new DateTime();
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "id in (".$ids.")";
	    Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')), $cri);
	    
	    $cri1 = new CDbCriteria();
	    $cri->condition = "owner_id in (".$ids.")";
// 	    $cri1->addInCondition('owner_id', array($ids));
	    Profiles::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')), $cri1);	  
	}
		
	public function actionexportCSV($trial,$paid,$basic)
	{
	    Yii::import('ext.ECSVExport');
	    $criteria = new CDbCriteria();
	    $criteria->select = "CONCAT_WS(' ', forenames, surname) as name,email,created_at";
	    if(Yii::app()->user->getState('role') == 'admin')
	    {
	        $criteria->condition = "type like 'candidate' AND deleted_at is null";
	    }
	    if($trial == 1)
	        $criteria->addCondition('trial_ends_at is not null');
	        else if($paid == 1)
	            $criteria->addCondition('stripe_id > 0');
	            else if($basic == 1)
	                $criteria->addCondition('trial_ends_at is null and stripe_id is null');
	            
	    // CActiveDataProvider
	    $dataProvider = new CActiveDataProvider('Users', array(
	        'criteria'=>$criteria,
	    ));
	    
	    $csv = new ECSVExport($dataProvider);
	    $content = $csv->toCSV();
	    Yii::app()->getRequest()->sendFile('export.csv', $content, "text/csv", false);
	}
	
	public function actiondashboard()
	{
	    $this->dashboard = 1;
	    $model = new Users();
	    $this->render('dashboard',array(
	        'model'=>$model
	    ));
	}
	
	public function actionactivateUsers()
	{
	    $ids = implode(',', $_POST['ids']);
	    
	    $now = new DateTime();
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "id in (".$ids.")";
	    Users::model()->updateAll(array('status'=>'active'), $cri);
	    
	}
	
	public function actionblockUsers()
	{
	    $ids = implode(',', $_POST['ids']);
	    
	    $now = new DateTime();
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "id in (".$ids.")";
	    Users::model()->updateAll(array('status'=>'blocked'), $cri);
	    
	}
	public function actionfavourites($id)
	{
	    $profilemodel = Profiles::model()->findByPk($id);
	    $model = Users::model()->findByPk($profilemodel->owner_id);
	    $this->render('favourites',array(
	        'model'=>$model,'profilemodel'=>$profilemodel
	    ));
	}
	
	public function actioncreateAdmin()
	{
	    $model = new Users();
	    $addressmodel = new Addresses();
	    $this->render('admindetails',array(
	        'model'=>$model,'addressmodel'=>$addressmodel
	    ));
	}
	
	public function actionmanageUsers()
	{
	    $model = Users::model();
	    
	    $namesearch = 0;
	    $fullname = '';
	    $userarr = array();
	    if(isset($_GET['Users']))
	        $model->attributes=$_GET['Users'];
	    
	    if(isset($_GET['fullname']) && $_GET['fullname']!= "")
	    {
	        $namesearch = 1;
	        $fullname = $_GET['fullname'];
	        $cri = new CDbCriteria();
	        $cri->condition = "concat_ws(' ',forenames,surname) like '%".$fullname."%' and type = 'admin' and deleted_at is null";
	        
            $searchusers = Users::model()->findAll($cri);
            foreach ($searchusers as $users)
                $userarr[] = $users['id'];
	    }
	    
	    $this->render('manageusers',array(
	        'model'=>$model,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
	    ));
	}
	
	public function actioneditManageUser($id)
	{
	    $model = Users::model()->findByPk($id);
	    $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$model->id));
	    if($addressmodel == null)
	        $addressmodel = new Addresses(); 
	    
	    $this->render('admindetails',array(
	        'model'=>$model,'addressmodel'=>$addressmodel
	    ));
	}
	public function actionsaveAdmin()
	{
	    $now = new DateTime();
	    $model = new Users();
	    $model->attributes = $_POST['Users'];
	    
	    $model->type = "admin";
	    $dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
	    $model->dob = $dobformat;
	    $model->created_at = $now->format('Y-m-d H:i:s');
	    if($model->save())
	    {
	        if($_POST['Addresses']['address'] != "" && $_POST['Addresses']['postcode'] != "")
	        {
    	        $addressmodel = new Addresses();
    	        $addressmodel->attributes = $_POST['Addresses'];
    	        $addressmodel->model_id = $model->id;
    	        $addressmodel->model_type = "Admin";
    	        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
    	        $addressmodel->save();
	        }
	        $this->redirect($this->createUrl('users/manageUsers'));
	    }
	    
	    $this->render('admindetails',array(
	        'model'=>$model,
	        'addressmodel'=>$addressmodel
	    ));
	}
	public function actionupdateAdmin($id)
	{
	    $now = new DateTime();
	    $model = Users::model()->findByPk($id);
	    $pwd = $model->password;
	    $model->attributes = $_POST['Users'];
	    if($_POST['Users']['password'] == "")
	        $model->password = $pwd;
	    
	    $dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
	    $model->dob = $dobformat;
	    $model->created_at = $now->format('Y-m-d H:i:s');
	    if($model->save())
	    {
	        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$id));
	        if($addressmodel == null)
	        {
	            $addressmodel = new Addresses(); 
	            $addressmodel->attributes = $_POST['Addresses'];
	            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	        }
	        else
	        {
	            $addressmodel->attributes = $_POST['Addresses'];
	            $addressmodel->updated_at = $now->format('Y-m-d H:i:s');	        
	        }
	        
	        $addressmodel->model_id = $model->id;
	        $addressmodel->model_type = "Admin";
	        $addressmodel->save();
	        
	        $this->redirect($this->createUrl('users/manageUsers'));
	    }
	    
	    $this->render('admindetails',array(
	        'model'=>$model,
	        'addressmodel'=>$addressmodel
	    ));
	}
	
	public function actiondeleteAdmin()
	{
	    $ids = implode(',', $_POST['ids']);
	    $now = new DateTime();
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "id in (".$ids.")";
	    Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')), $cri);
	}
	
	public function actionsendMessage()
	{
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $_POST['subject'];
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if($conmodel->save())
        {
            $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = Yii::app()->user->getState('userid');
            $conmsgmodel->name = $usermodel->forenames." ".$usermodel->surname;
            $conmsgmodel->message = $_POST['message'];
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if($conmsgmodel->save())
            {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
                $conmemmodel_sender->name = $usermodel->forenames." ".$usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if($conmemmodel_sender->save())
                {
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $_POST['user_id'];
                    $conmemmodel_rec->name = $usermodel->forenames." ".$usermodel->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if($conmemmodel_rec->save())
                    {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = Yii::app()->user->getState('userid');
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: ".$_POST['subject'];
                        $actmodel_sender->message = $_POST['message'];
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if($actmodel_sender->save())
                        {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $_POST['user_id'];
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: ".$_POST['subject'];
                            $actmodel_rec->message = $_POST['message'];
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                    
                }
                

            }
        }
	} 
	
	public function actioncheckSlug()
	{
	    $pmodel = Profiles::model()->countByAttributes(array('slug'=>$_POST['slug'],'deleted_at'=>null));
	    if($pmodel > 0)
	      echo -1;
	    else 
	      echo 1;
	}
}
