<?php

class SkillCategoriesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SkillCategories;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SkillCategories']))
		{
			$model->attributes=$_POST['SkillCategories'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
        
        
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioncreateSkillCategories() {

            $model = new SkillCategories;

            $now = new DateTime();

    //      insert user details
            $model->attributes = $_POST["SkillCategories"];
            $model->created_at = $now->format('Y-m-d H:i:s');

            $model->save();
            
            $model->unsetAttributes();  // clear any default values

           $this->render('admin',array(
                    'model'=>$model,
            ));
            
        }
        
        function actionskillCategoriesEdit($id)
	{
	    $model = SkillCategories::model()->findByPk($id);
	    $this->render('edit',array('model'=>$model));
	}
        
        
        function actionupdateSkillCategories($id) {
            
            $now = new DateTime();
            $model = SkillCategories::model()->findByPk($id);

            $model->attributes = $_POST["SkillCategories"];
            $model->updated_at = $now->format('Y-m-d H:i:s');
            $model->save();
            
            $model->unsetAttributes();
            $this->render('admin',array(
                            'model'=>$model,
            ));
        
        }
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
        public function actiondeleteSkillCategories()
        {
            $ids = implode(',', $_POST['ids']);
            $now = new DateTime();

            $cri = new CDbCriteria();
            $cri->condition = "id in (".$ids.")";
            SkillCategories::model()->deleteAll($cri);

        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SkillCategories']))
		{
			$model->attributes=$_POST['SkillCategories'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SkillCategories');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SkillCategories('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SkillCategories']))
			$model->attributes=$_GET['SkillCategories'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        
        	/**
	 * Manages all models.
	 */
	public function actionsearchAdmin()
	{
	    $model=new SkillCategories('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['SkillCategories']))
	    {
	        $model->attributes=$_GET['SkillCategories'];	        
	    } 
	    
            $this->render('admin',array(
                'model'=>$model,'issearch'=>1));
            }



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SkillCategories the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SkillCategories::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SkillCategories $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='skill-categories-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
        
        /**
        * Reorder items based on drag and drop
        *
        * @param Request $request
        */
       public function actionreorder()
       {
           $orders = $_POST['orders'];
           if(!count($orders)) return;
           foreach ($orders as $index => $element) {
                $category_id = (int)str_replace('category-', '', $element);
                $model = SkillCategories::model()->findByPk($category_id);
                $model->ordering = $index;
                $model->save();
           }

       }
}
