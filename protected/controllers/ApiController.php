<?php

use Stripe\Charge;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Subscription;

header("Access-Control-Allow-Origin: *");

//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}"); 
class ApiController extends Controller {

// Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */
    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';

    /**
     * @return array action filters
     */
    public function filters() {
        return array();
    }

//        Login api


    private function _sendResponse($status = 200, $body = array(), $content_type = 'text/html') {
// set the status
// and the content type
        header('Content-type: ' . $content_type);

// this should be templated in a real-world solution
        $arr = array();
        $arr['status'] = $status;
        $arr['message'] = $this->_getStatusCodeMessage($status);
        $arr['data'] = $body;
        echo json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        Yii::app()->end();
    }

    private function _sendResponsewithMessage($status = 200, $message = '', $content_type = 'text/html') {
// set the status
// and the content type
        header('Content-type: ' . $content_type);

// this should be templated in a real-world solution
        $arr = array();
        $arr['status'] = $status;
        $arr['message'] = $message;
        echo json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status) {
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Invalid Authorization Credentials',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Email already Exists',
            503 => 'Invalid Email Error',
            504 => 'Invalid Password Error',
            505 => 'Passenger Details Missing',
            506 => 'Car Model Not Found',
            507 => 'No Results Found',
            508 => 'Upload failed',
            509 => 'Updated failed'
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }




    public function notifyforgetpassword($email) {
        $body = "change password ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'cvvid.com';
        $mail->Username = 'info@mangalaisai.com';
        $mail->Password = 'gz~MFZUsb*qS';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('info@mangalaisai.com', 'mangalaisai');
        $mail->Subject = 'Your password details for cvvid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($email);
        $mail->AddReplyTo('cvvid@gmail.com', 'Reply to cvvid');
        $mail->SetFrom('info@mangalaisai.com', 'Cvvid');
        $mail->Send();
    }


    private function _checkAuth() {
// 		list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':' , base64_decode(substr($_SERVER['REDIRECT_REDIRECT_HTTP_AUTHORIZATION'], 6)));
// 			if (!isset($_SERVER['PHP_AUTH_USER']) && !isset($_SERVER['PHP_AUTH_PW'])) {
// 				return false;
// 			} 
// 			else {
        if ($_SERVER['PHP_AUTH_USER'] == 'cvvid' && $_SERVER['PHP_AUTH_PW'] == 'cvvid') {
            return true;
        } else
            return false;
//}    
// Check if we have the USERNAME and PASSWORD HTTP headers set?
// if(!(isset($_SERVER['HTTP_X_USERNAME']) and isset($_SERVER['HTTP_X_PASSWORD']))) {
// 	// Error: Unauthorized
// 	$this->_sendResponse(401);
// }
// $username = $_SERVER['HTTP_X_USERNAME'];
// $password = $_SERVER['HTTP_X_PASSWORD'];
// // Find the user
// $user = Member::model()->find('LOWER(username)=?',array(strtolower($username)));
// if($user===null) {
// 	// Error: Unauthorized
// 	$this->_sendResponse(401, 'Error: User Name is invalid');
// } else if(!$user->validatePassword($password)) {
// 	// Error: Unauthorized
// 	$this->_sendResponse(401, 'Error: User Password is invalid');
// }
    }

    public function getValidatedSlug($slug) {
        $cri = new CDbCriteria();
        $cri->condition = "slug ='" . $slug . "'";
        $pmodel = Profiles::model()->findAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "slug like '" . $slug . "-%'";
        $pmodel1 = Profiles::model()->findAll($cri1);

        if (count($pmodel1) == 0 && count($pmodel) == 1) {
            return strtolower($slug) . "-1";
        } else if (count($pmodel1) > 0) {
            return (strtolower($slug) . "-" . (count($pmodel1) + 1));
        } else if (count($pmodel) == 0)
            return strtolower($slug);
        else
            return (strtolower($slug) . "-" . rand());
    }

    /**
     * Returns the json or xml encoded array
     *
     * @param mixed $model
     * @param mixed $array Data to be encoded
     * @access private
     * @return void
     */
    private function _getObjectEncoded($model, $array) {
        if (isset($_GET['format']))
            $this->format = $_GET['format'];

        if ($this->format == 'json') {
            return
            ;
        } elseif ($this->format == 'xml') {
            $result = '<?xml version="1.0">';
            $result .= "\n<$model>\n";
            foreach ($array as $key => $value)
                $result .= "    <$key>" . utf8_encode($value) . "</$key>\n";
            $result .= '</' . $model . '>';
            return $result;
        } else {
            return;
        }
    }

    
    // Start Vinoth API //

    
    /*login*/
    public function actionlogin() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $email = $_POST['email'];
        $password = $_POST['password'];
        $cri = new CDbCriteria();
        $cri->condition = "email = '" . $email . "'";
        $model = Users::model();
        $usrarr = $model->find($cri);
        $cri1 = new CDbCriteria();
        $cri1->alias = 'i';
        $cri1->select = "i.name,i.id as institution_id,u.id as user_id,u.forenames,u.type,u.surname,u.email,u.stripe_active,u.current_job,u.location";
        $cri1->join = "left outer join cv16_users u on u.id = i.admin_id";
        $cri1->condition = "i.admin_id = '" . $usrarr['id'] . "'";
        if ($usrarr['type'] == 'institution_admin')
            $result = Institutions::model()->getCommandBuilder()->createFindCommand(Institutions::model()->tableSchema, $cri1)->queryAll();
        else if ($usrarr['type'] == 'candidate')
            $result = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (count($result) <= 0) {
            $this->_sendResponse(503, array());
        } else if (!password_verify($password, $usrarr['password'])) {
            $this->_sendResponse(504, array());
        } else
            $this->_sendResponse(200, $result);
    }
    
    public function actionforgetpassword() {
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $email = $_POST['email'];
        $usersmodel = Users::model()->findByAttributes(array('email' => $email));
        if (count($usersmodel) > 0) {
            $usersmodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
            $usersmodel->save();
          //  $this->notifyforgetpassword($email);
            $this->_sendResponse(200, sprintf("Password changed  Successfully!", ""));
        } else {
            $this->_sendResponse(503,array());
        }
    }
    
   
    
    
    public function actioncreatecandidate() {
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($var == 'ispremium') {
                //  $this->_sendResponse(400, array());
            } else {
                $this->_sendResponse(500, array());
            }
        }
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        // Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            $usermodel->type = 'candidate';
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
                $addressmodel->model_id = $usermodel->id;
                $addressmodel->model_type = "Candidate";
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($addressmodel->save()) {
                    $profilemodel->owner_type = "Candidate";
                    $profilemodel->owner_id = $usermodel->id;
                    $profilemodel->slug = $usermodel->forenames.''.$usermodel->surname;
                    $profilemodel->created_at = $usermodel->created_at;
                    $profilemodel->type = $usermodel->type;
                if ($profilemodel->save()) {
                    $cri = new CDbCriteria();
                    $cri->select = 'id,forenames,surname,email,mobile';
                    $cri->condition = "id = " . $usermodel->id;
                    $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                    $this->_sendResponse(200, $users);
                }
            }
        } else {
              echo var_dump($usermodel->getErrors());
            $this->_sendResponse(500, array());
        }
    }
    
    public function actionfetchuserdetails() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
     
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->alias = 'us';
        $cri->select ='us.id, us.type, us.forenames, us.surname, us.email, us.password, us.dob, us.status, us.is_premium, us.tel, us.mobile, us.current_job, us.location, us.location_lat,'
                . ' us.location_lng, us.remember_token, us.created_at, us.updated_at, us.deleted_at, us.stripe_active, us.stripe_id, us.stripe_subscription, us.stripe_plan,'
                . ' us.last_four, us.card_expiry, us.card_expiry_sent, us.trial_ends_at, us.subscription_ends_at,hob.activity,hob.description,lan.name,lan.proficiency,ad.address,ad.town,ad.postcode,ad.county,ad.country';
        $cri->join = 'left outer join cv16_addresses  ad on ad.model_id = us.id left outer join cv16_user_qualifications qua on qua.user_id = us.id left outer join cv16_user_hobbies hob on hob.user_id = us.id left outer join cv16_user_languages lan on lan.user_id = us.id left outer join cv16_profiles pro on pro.owner_id = us.id';
        $cri->condition = 'us.id = '.$id;
        $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
        $this->_sendResponse(200, $users);
    }
    
    
    public function actionupdatecandidate() {
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id));
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } elseif ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $profile = new Profiles();
            $profile->type = 'candidate';
            $profile->owner_type = 'Candidate';
            $profile->owner_id = $id;
            $profile->industry_id = $profilemodel['industry_id'];
            $profile->photo_id = $profilemodel['photo_id'];
            $profile->cover_id = $profilemodel['cover_id'];
            $profile->visibility = $profilemodel['visibility'];
            $profile->published = $profilemodel['published'];
            $profile->hired = $profilemodel['hired'];
            $profile->updated_at = $now->format('Y-m-d H:i:s');
            if ($profilemodel->save()) {
                $this->_sendResponse(200, array());
            }
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* cover photo update */
    public function actionupdatecoverphoto() {
         if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
         if (!isset($_POST['mediaid']) || !isset($_POST['profileid']))
            $this->_sendResponse(500, array());

        $mediaid = $_POST['mediaid'];
        $profileid = $_POST['profileid'];

        $model = Profiles::model()->findByPk($profileid);
        $model->cover_id = $mediaid;
        if ($model->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,cover_id';
                $cri->condition = "id = " .$profileid;
                $model = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $model);
        } else {
            $this->_sendResponse(400, array());
        }
    }
    /* cover photo update */
    
    
    /*supporting documentation */
    /*create supporting documentation*/
    public function actioncreateuserdocumentation() {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();

        $userid = $id;
        $filename = $_FILES['doc_file']['name'];
        $size = $_FILES['doc_file']['size'];
        $name = pathinfo($_FILES['doc_file']['name'], PATHINFO_FILENAME);
        $docname = $_POST['doc_name'];
        $category_id = $_POST['category_id'];
        $published = $_POST['published'];

        $now = new DateTime();

        $mediamodel = new Media();
        $mediamodel->model_type = "Candidate";
        $mediamodel->model_id = $userid;
        $mediamodel->collection_name = "documents";
        $mediamodel->name = $name;
        $mediamodel->file_name = $filename;
        $mediamodel->size = $size;
        $mediamodel->disk = "documents";
        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
        if ($mediamodel->save()) {
            $model = new UserDocuments();
            $model->user_id = $userid;
            $model->media_id = $mediamodel->id;
            $model->name = $docname;
            $model->category_id = $category_id;
            $model->published = $published;
            $model->created_at = $now->format('Y-m-d H:i:s');
            if ($model->save()) {
                $pdmodel = new ProfileDocuments();
                $pdmodel->profile_id = $profilemodel->id;
                $pdmodel->user_document_id = $model->id;
                $pdmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($pdmodel->save()) {
                    $userdocu = UserDocuments::model()->findByPk($model->id);
                    $this->_sendResponse(200, $userdocu);
                } else {
                    $this->_sendResponse(500, array());
                }
            }
        }
    }
     
    /*create*/
    
    /*edit supporting documentation*/
    public function actionupdateuserdocumentation() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        
        $docmodel = UserDocuments::model()->findByPk($id);
        $userid = $docmodel->user_id;
        $usermodel = Users::model()->findByPk($userid);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        

        $now = new DateTime();

        $userid = $usermodel->id;
        $filename = $_FILES['doc_file']['name'];
        $size = $_FILES['doc_file']['size'];
        $name = pathinfo($_FILES['doc_file']['name'], PATHINFO_FILENAME);
        $docname = $_POST['doc_name'];
        $category_id = $_POST['category_id'];
        $published = $_POST['published'];

        $now = new DateTime();

        $mediamodel = new Media();
        $mediamodel->model_type = "Candidate";
        $mediamodel->model_id = $userid;
        $mediamodel->collection_name = "documents";
        $mediamodel->name = $name;
        $mediamodel->file_name = $filename;
        $mediamodel->size = $size;
        $mediamodel->disk = "documents";
        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
        if ($mediamodel->save()) {
            $model =  UserDocuments::model()->findByAttributes(array('id'=>$id));
            $model->user_id = $userid;
            $model->media_id = $mediamodel->id;
            $model->name = $docname;
            $model->category_id = $category_id;
            $model->published = $published;
            $model->created_at = $now->format('Y-m-d H:i:s');
            if ($model->save()) {
                $pdmodel =  ProfileDocuments::model()->findByAttributes(array('profile_id'=>$profilemodel->id));
                $pdmodel->profile_id = $profilemodel->id;
                $pdmodel->user_document_id = $model->id;
                $pdmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($pdmodel->save()) {
                    $this->_sendResponsewithMessage(200, sprintf("Successfully Documentation updated", ""));
                } else {
                    $this->_sendResponse(500, array());
                }
            }
        }
    }
    /*edit supporting documentation*/
    
    /*get supporting documentation*/
    public function actionprofiledocumentation() {
       if (!isset($_GET['id']) || $_GET['id'] == NULL)
            $this->_sendResponse(404, array());
     
        $id = $_GET['id'];        
        $model = UserDocuments::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->alias = "ud";
        $cri->select = "ud.name,ud.id,m.file_name as filename,mc.name as category";
        $cri->join = "left outer join cv16_profile_documents pd on pd.user_document_id = ud.id "
                . "left outer join cv16_media m on m.id = ud.media_id "
                . " left outer join cv16_media_categories mc on mc.id = ud.category_id";
        $cri->condition = "pd.user_document_id =" . $id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }
    
     public function actiongetprofiledocumentation() {
        $docid = isset($_GET['id']) ? $_GET['id'] : null;
        $docmodel = UserDocuments::model()->findByPk($docid);
        if ($docid != $docmodel->id)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = new UserDocuments();
        $cri = new CDbCriteria();
        $cri->alias = "ud";
        $cri->select = "ud.name,ud.id,m.file_name as filename,mc.name as category";
        $cri->join = "inner join cv16_profile_documents pd on pd.user_document_id = ud.id"
                . " inner join cv16_media m on m.id = ud.media_id "
                . " inner join cv16_media_categories mc on mc.id = ud.category_id";
        $cri->condition = "pd.user_document_id =" . $id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }
    /*get supporting documentation*/
    
    
        /*get all profile documentation*/
    public function actiongetallprofiledocumentation() {
        $model = UserDocuments::model();
        $cri = new CDbCriteria();
        $cri->alias = "ud";
        $cri->select = "ud.name as name,ud.id,mc.name as category";
        $cri->join = "inner join cv16_media m on m.id = ud.media_id "
                  . " inner join cv16_media_categories mc on mc.id = ud.category_id";
       // $cri->condition = "ud.user_id =" . $id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }
    /*get all profile documentation*/
    
      /*delete profile documentations*/
    public function actiondeleteprofiledocumentation() {
        $docid = isset($_GET['id']) ? $_GET['id'] : null;
        $docmodel = UserDocuments::model()->findByPk($docid);
        if ($docid != $docmodel->id)
            $this->_sendResponse(404, array());
        $cri = new CDbCriteria();
        
        $cri->condition = "id =" . $_GET['id'];
        UserDocuments::model()->deleteAll($cri);
        
        $cri->condition = "user_document_id =" . $_GET['id'];
        ProfileDocuments::model()->deleteAll($cri);
      //  UserDocuments::model()->deleteAll(array('condition' => "id = ':id'",'params' => array(':id' => $id),  ));
     //   ProfileDocuments::model()->deleteAll(array( 'condition' => "user_document_id = ':id'",'params' => array(':id' => $id),  ));
        $this->_sendResponse(200, "Delete Successfully");
    }
          /*delete profile documentations*/

    

    /*update profile picture*/    
    public function actionupdateprofilepicture() {
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        if (!isset($_POST['mediaid']) || !isset($_POST['profileid']))
            $this->_sendResponse(500, array());
        $mediaid = $_POST['mediaid'];
        $profileid = $_POST['profileid'];
        $model = Profiles::model()->findByPk($profileid);
        $model->photo_id = $mediaid;
        if ($model->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Profile picture updated successfully", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }
     /*update profile picture*/ 
    /*supporting documentation */
    
    public function actiongetcarrerexperience() {
         if (!isset($_GET['id']) || $_GET['id'] == NULL)
            $this->_sendResponse(404, array());
          $id = $_GET['id'];
          $skillsmodel = Skills::model()->findByAttributes(array('skill_category_id'=>$_GET['id']));
          $cri = new CDbCriteria();
          $cri->alias ='s'; 
          $cri->select ='s.skill_category_id,s.id,s.name as Name,sc.name'; 
          $cri->join = 'left outer join cv16_skill_categories sc on sc.id = s.skill_category_id';
          $cri->condition = 's.skill_category_id = '.$_GET['id'];
          $skillsmodel = $skillsmodel->getCommandBuilder()->createFindCommand($skillsmodel->tableSchema, $cri)->queryAll();
        if (!empty($skillsmodel)) {
            $this->_sendResponse(200, $skillsmodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
    
    public function actionvideoupload() {

        if (!isset($_GET['profile_id']) || $_GET['profile_id'] == 0)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        // $target_path = Yii::app()->getBaseUrl(true); // Target path where file is to be stored
        // $target_path = $target_path . basename($_FILES['video']['name']);

        try {
//            //throw exception if can't move the file
//            if (!move_uploaded_file($_FILES['video']['tmp_name'], $target_path)) {
//               // throw new Exception('Could not move file');
//            }

            $now = new DateTime();
            $userid = $_GET['profile_id'];
            $name = $_POST['video_name'];
            $size = $_FILES['video']['size'];
            $image_type = $_FILES['video']['type'];
            $filename = $_FILES['video']['name'];

            $usermodel = Users::model()->findByPk($userid);
            $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

            $vname = pathinfo($name, PATHINFO_FILENAME);

            $mediamodel = new Media();
            $mediamodel->disk = "media";
            $mediamodel->model_type = $usermodel->type;
            $mediamodel->collection_name = "video";
            $mediamodel->name = $vname;
            $mediamodel->file_name = $vname . ".jpg";
            $mediamodel->created_at = $now->format('Y-m-d H:i:s');
            $mediamodel->size = 0; //(int)$duration;
            if ($mediamodel->save()) {
                $mediafolder = "images/media/" . $mediamodel->id;

                if (!file_exists($mediafolder)) {
                    mkdir($mediafolder, 0777, true);
                }

                $fullpath = "images/media/" . $mediamodel->id . "/" . $name;

                if (move_uploaded_file($_FILES['video']['tmp_name'], $fullpath)) {
                    $videomodel = new Videos();
                    $videomodel->model_id = $userid;
                    $videomodel->video_id = $fullpath;
                    $videomodel->name = $vname;
                    $videomodel->duration = 0;
                    $videomodel->model_type = $usermodel->type;
                    $videomodel->category_id = 1;
                    $videomodel->is_processed = 1;
                    $videomodel->state = 1;
                    $videomodel->status = "active";
                    $videomodel->created_at = $now->format('Y-m-d H:i:s');
                    if ($videomodel->save()) {

                        $pvideomodel = new ProfileVideos();
                        $pvideomodel->profile_id = $pmodel->id;
                        $pvideomodel->video_id = $videomodel->id;
                        $pvideomodel->created_at = $now->format('Y-m-d H:i:s');
                        $pvideomodel->state = 1;
                        $pvideomodel->save();
                        
                        $sec = 0;

                        $cmd = "ffmpeg -i $fullpath 2>&1";
                        $ffmpeg = shell_exec($cmd);

                        $search = "/Duration: (.*?)\./";
                        preg_match($search, $ffmpeg, $matches);

                        if (isset($matches[1])) {
                            $data['duration'] = $matches[1];
                            $time_sec = explode(':', $data['duration']);
                            $sec = ($time_sec['0'] * 3600) + ($time_sec['1'] * 60) + $time_sec['2'];
                        }

                        Videos::model()->updateByPk($videomodel->id, array('duration' => $sec));
                        Media::model()->updateByPk($mediamodel->id, array('model_id' => $videomodel->id, 'size' => $sec));

                        $iname = $vname . ".jpg";
                        $imgfullpath = "images/media/" . $mediamodel->id . "/" . $iname;
                        $imgprefix = "images/media/" . $mediamodel->id;


                        $process = exec("ffmpeg -i $fullpath -ss 00:00:2 -vframes 1 $imgfullpath");

                        $jlwidth = 350;
                        $jlheight = 190;
                        $jldestpath = $imgprefix . "/conversions/profile.jpg";

                        //create search
                        $searchwidth = 167;
                        $searchheight = 167;
                        $searchdestpath = $imgprefix . "/conversions/search.jpg";

                        //create thumb
                        $thumbwidth = 126;
                        $thumbheight = 126;
                        $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
                        $image_type == "image/jpg";

                        $image = imagecreatefromjpeg($imgfullpath);

                        $this->resize_image($image, $image_type, $imgfullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                        $this->resize_image($image, $image_type, $imgfullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                        $this->resize_image($image, $image_type, $imgfullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                    
                         echo "The video has been uploaded";
                        
                        }
                }
            }
        } catch (Exception $e) {
            die('File did not upload: ' . $e->getMessage());
        }
    }
    
    
    public function actionbusinesscard() {
          if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->select = 'forenames,surname,email';
        $cri->condition = 'id='.$id;
        $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
        $this->_sendResponse(200, $users);
    }
    
    
  
    
        
    public function actioncreateemployer() {
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users;
        $empmodel = new Employers;
        $empusermodel = new EmployerUsers;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        
      
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($empmodel->hasAttribute($var)) {
                $empmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($empusermodel->hasAttribute($var)) {
                $empusermodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($var == 'ispremium') {
                
            } else {
                $this->_sendResponse(500, array());
            }
        }
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->type = 'employer';
        if ((isset($_POST['location']))) {
            $lonlat = $this->getLatLong($_POST['location']);
        }
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        $usermodel->location_lat = $lonlat['location_lat'];
        $usermodel->location_lng = $lonlat['location_lng'];
        $usermodel->tel = $_POST['tel'];
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
            $empmodel->name = $_POST['name'];
            $empmodel->user_id = $usermodel->id;
            $empmodel->email = $usermodel->email;
            $empmodel->tel = $usermodel->mobile;
            $empmodel->location = $usermodel->location;
            $empmodel->website = $_POST['slug'];
            $empmodel->location_lat = $usermodel->location_lat;
            $empmodel->location_lng = $usermodel->location_lng;
            $empmodel->created_at = $now->format('Y-m-d H:i:s');
            if (isset($_POST['ispremium']) && $_POST['ispremium'])
                $empmodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            if ($empmodel->save()) {
                $empusermodel->employer_id = $empmodel->id;
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = 'admin';
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
            if ($empusermodel->save()) {
                    // if($_POST['industry_id'] > 0)
                    $profilemodel->industry_id = $_POST['industry_id'];
                    $profilemodel->type = 'employer';
                    $profilemodel->owner_id = $usermodel->id;
                    $profilemodel->owner_type = 'employer';
                    $profilemodel->slug = $usermodel->forenames.''.$usermodel->surname;
             if ($profilemodel->save()) {
                        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                        $addressmodel->model_type = 'employer';
                        $addressmodel->model_id = $usermodel->id;
            if ($addressmodel->save()) {
                            $cri = new CDbCriteria();
                            $cri->select = 'id,forenames,surname,email,mobile';
                            $cri->condition = "id = " . $usermodel->id;
                            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                            $this->_sendResponse(200, $users);
                        }
                    } else {
                        echo var_dump($profilemodel->getErrors());
                    }
                } else {
                    echo var_dump($empusermodel->getErrors());
                }
            } else {
                echo var_dump($empmodel->getErrors());
            }
        } else {
            echo ($usermodel->getErrors());
            $this->_sendResponse(500, array());
        }
    }

    public function actionfetchemployerprofiledetails() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
     
         $id = $_GET['id'];
         $employermodel = EmployerUsers::model()->findByAttributes(array('user_id'=>$id));
         $cri = new CDbCriteria();
         $cri->alias = 'emp';
         $cri->select = 'us.forenames,us.surname,us.email,us.current_job,us.location,ad.county,ad.country';
         $cri->join = 'left outer join cv16_users us on us.id = emp.user_id '
                 . 'left outer join cv16_addresses ad on ad.model_id = us.id '
                 . 'left outer join cv16_profiles prof on prof.owner_id = emp.user_id';
         $cri->condition = 'emp.user_id ='.$id;
         $users = $employermodel->getCommandBuilder()->createFindCommand($employermodel->tableSchema, $cri)->queryAll();
         $this->_sendResponse(200, $users);
    }
    
    public function actionupdateemployer() {
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $id = $_GET['id'];

        $usermodel = Users::model()->findByPk($id);
        $empusermodel = EmployerUsers::model()->findByAttributes(array('user_id' => $id));
        $employermodel = Employers::model()->findByAttributes(array('user_id' => $id));
        $userid = $employermodel->user_id;
        $addressmodel = Addresses::model()->findByAttributes(array('model_id' => $usermodel->id, 'model_type' => 'employer'));
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid));
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($employermodel->hasAttribute($var)) {
                $employermodel->$var = $value;
            } elseif ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            }elseif($profilemodel->hasAttribute($var)){
                $profilemodel->$var = $value;
            } 
            else {
                $this->_sendResponse(400, array());
            }
        }
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $empusermodel = EmployerUsers::model()->findByAttributes(array('user_id' => $id));
            $employermodel->name = $usermodel->forenames;
            $employermodel->email = $usermodel->email;
            $employermodel->location = $usermodel->location;
        if ($employermodel->save()) {
                $addressmodel = Addresses::model()->findByAttributes(array('model_id' => $usermodel->id, 'model_type' => 'employer'));
                $addressmodel->location = $addressmodel['location'];
        if ($addressmodel->save()) {
                     $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid));
                      $profilemodel->slug = $usermodel->forenames.''.$usermodel->surname;
       if($profilemodel->save()){
                    $cri = new CDbCriteria();
                    $cri->select = 'id';
                    $cri->condition = "id = " . $id;
                    $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                    $this->_sendResponse(200, $users);
                }else {
                    echo var_dump($profilemodel->getErrors());
                }
                } else {
                    echo var_dump($addressmodel->getErrors());
                }
            } else {
                echo var_dump($employermodel->getErrors());
            }
        } else {
            echo var_dump($usermodel->getErrors());
        }
    }
    
    
    
    public function actionaddnewjob() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
        $employer = Employers::model()->findByPk($id);
        $userid = $employer->user_id;
        
        $jobskillmodel = new JobSkill();
        $jobmodel = new Jobs();
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($jobmodel->hasAttribute($var)) {
                $jobmodel->$var = $value;
            } elseif ($var == 'skills') {
                $skills = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $jobmodel->owner_id = $id;
        $jobmodel->owner_type = 'employer';
        $jobmodel->user_id = $userid;
        $d = str_replace('/', '-', $_POST["end_date"]);
        $endformat = date('Y-m-d', strtotime($d));
        $jobmodel->end_date = $endformat;
        $jobmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($jobmodel->save())
            {
                    foreach ($skills as $value)
                    {
                        $jobskillmodels = new JobSkill();
                        $jobskillmodels->job_id = $jobmodel->id;
                        $jobskillmodels->skill_id = $value['skill_id'];
                        $jobskillmodels->vacancies = $value['vacancies'];
                    }
                if ($jobskillmodels->save()) 
                {
                            $cri = new CDbCriteria();
                            $cri->select = 'id';
                            $cri->condition = 'id=' . $jobmodel->id;
                            $users = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri)->queryAll();
                       if(!empty($users)){
                            $this->_sendResponse(200,$users);
                       }else{
                           $this->_sendResponse(200,array());
                       } 
               } else{
                   echo var_dump($jobskillmodels->getErrors());
               }
           }
           else{
               echo var_dump($jobmodel->getErrors());
           }         
    }

    public function actioncreateindustry() {
         if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $industrymodel = new Industries();
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if($industrymodel->hasAttribute($var)){
                $industrymodel->$var = $value;
            } else {
                $this->_sendResponse(400,array());
            }
        }
        $industrymodel->created_at = $now->format('Y-m-d H:i:s');
        if($industrymodel->save()){
            $cri = new CDbCriteria();
            $cri->select = 'id,name';
            $cri->condition = 'id='.$industrymodel->id;
            $industry = $industrymodel->getCommandBuilder()->createFindCommand($industrymodel->tableSchema, $cri)->queryAll();
             if (!empty($industry)) {
            $this->_sendResponse(200, $industry);
        } }else {
             $this->_sendResponse(500, array());
        }
    }
    
    
    public function actiongetindustry() {
        $industrymodel = Industries::model();
        $cri = new CDbCriteria();
        $cri->select = 'id,name';
        $industry = $industrymodel->getCommandBuilder()->createFindCommand($industrymodel->tableSchema, $cri)->queryAll();
        if (!empty($industry)) {
            $this->_sendResponse(200, $industry);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
    
    
    public function actioncreateskillcategory() {
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $skillcategory = new SkillCategories();
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($skillcategory->hasAttribute($var)) {
                $skillcategory->$var = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $skillcategory->created_at = $now->format('Y-m-d H:i:s');
        if ($skillcategory->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id,name';
            $cri->condition = 'id=' . $skillcategory->id;
            $skillcategory = $skillcategory->getCommandBuilder()->createFindCommand($skillcategory->tableSchema, $cri)->queryAll();
            if (!empty($skillcategory)) {
                $this->_sendResponse(200, $skillcategory);
            }
        } else {
            $this->_sendResponse(500, array());
        }
    }

    
    public function actionNewcareerskill() {
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $skillsmodel = new Skills();
        $now = new DateTime();
        $skillsmodel->skill_category_id = $_POST['skill_category_id'];
        $skillsmodel->name = $_POST['name'];
        $skillsmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($skillsmodel->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id,name,skill_category_id';
            $cri->condition = 'id=' . $skillsmodel->id;
            $skills = $skillsmodel->getCommandBuilder()->createFindCommand($skillsmodel->tableSchema, $cri)->queryAll();
            if (!empty($skills)) {
                $this->_sendResponse(200, $skills);
            }
        } else {
            $this->_sendResponse(500, array());
        }
    }
    
    public function actiongetcareerskill() {
        if(!isset($_GET['id']) || $_GET['id'] == 0 )
            $this->_sendResponse (400,array());
     //   $skillcategorymodel = SkillCategories::model()->findByPk($id);
        $id  =$_GET['id'];
        $skillsmodel = Skills::model()->findByAttributes(array('skill_category_id'=>$id));
        $cri = new CDbCriteria();
        $cri->alias = 'sk';
        $cri->select = 'sk.id,sk.name,sk.skill_category_id';
        $cri->join = 'left outer join cv16_skill_categories skc on skc.id = sk.skill_category_id';
        $cri->condition = 'sk.skill_category_id=' . $id;
        $skills = $skillsmodel->getCommandBuilder()->createFindCommand($skillsmodel->tableSchema, $cri)->queryAll();
        if (!empty($skills)) {
                $this->_sendResponse(200, $skills);
            } else {
                $this->_sendResponse(500, array());
            }
    }    
    /*employer->addjob->getparticularid->to show all employer */
    public function actiongetjob() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $jobsmodel = Jobs::model()->findByAttributes(array('owner_id' => $id));
        $cri = new CDbCriteria();
        $cri->select = 'id, owner_id, owner_type, user_id, industry_id, company_name,'
                . ' title, salary_type, salary_min, salary_max, end_date, skill_category_id, '
                . 'location, location_lat, location_lng, type, description, additional,'
                . ' created_at, updated_at, status, closed_date, successful_applicant_id, num_views';
        $cri->condition = 'owner_id=' . $id;
        $jobs = $jobsmodel->getCommandBuilder()->createFindCommand($jobsmodel->tableSchema, $cri)->queryAll();
        if (!empty($jobs)) {
            $this->_sendResponse(200, $jobs);
        } else {
            $this->_sendResponse(500, array());
        }
    }
    
    
    
    
    public function actionfetchjobemployer() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $jobsmodel = Jobs::model()->findByPk($id);
        $cri = new CDbCriteria();
         $cri->select = 'id, owner_id, owner_type, user_id, industry_id, company_name,'
                . ' title, salary_type, salary_min, salary_max, end_date, skill_category_id, '
                . 'location, location_lat, location_lng, type, description, additional,'
                . ' created_at, updated_at, status, closed_date, successful_applicant_id, num_views';
        $cri->condition = 'id='.$id;
        $jobs = $jobsmodel->getCommandBuilder()->createFindCommand($jobsmodel->tableSchema, $cri)->queryAll();
        if (!empty($jobs)) {
            $this->_sendResponse(200, $jobs);
        } else {
            $this->_sendResponse(500, array());
        }
    }
    
    /*public or private*/
    public function actionemployerupdateprivatepublic() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $employer = Employers::model()->findByPk($id);
        $ownerid = $employer->user_id;
        $profile = Profiles::model()->findByAttributes(array('owner_id' => $ownerid, 'owner_type' => 'employer'));
        if ($profile->visibility == 0) {
            $profile->visibility = '1';
        } else {
            $profile->visibility = '0';
        }
        if ($profile->save()) {
            $this->_sendResponse(200, array());
        } else {
            $this->_sendResponse(500, array());
        }
    }
    
    /*active or inactive*/
    public function actionemployerchangestatus() {
         if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $employer = Employers::model()->findByPk($id);
        $ownerid = $employer->user_id;
        $usermodel = Users::model()->findByAttributes(array('id'=>$ownerid ,'type'=>'employer'));
        if($usermodel->status == 'active'){
            $usermodel->status = 'inactive';
        }else {
            $usermodel->status = 'active';
        }
        if($usermodel->save()){
            $this->_sendResponse(200,array());
        } else {
            $this->_sendResponse(500,array());
        }
    }
    
    /*Employer View button*/
    public function actionemployerview() {
          if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $count = 0;
        $viewemployer = array();
        
        $employer = Employers::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->alias = 'emp';
        $cri->select = 'emp.name,emp.email,emp.location,emp.tel,prof.slug';
        $cri->join = 'inner join cv16_users user on user.id = emp.user_id  '
                . 'inner join cv16_profiles prof on prof.owner_id = user.id';
        $cri->condition = 'emp.id='.$id;
        $employer = $employer->getCommandBuilder()->createFindCommand($employer->tableSchema, $cri)->queryAll();
        
        $job = Jobs::model()->findByAttributes(array('owner_id'=>$id));
        $cri = new CDbCriteria();
        $cri->select = 'company_name,title,salary_type,salary_min,salary_max,end_date,location,salary_type';
        $cri->condition = 'owner_id='.$id;
        $job = $job->getCommandBuilder()->createFindCommand($job->tableSchema, $cri)->queryAll();
        
        
        $count1 = count($employer);
        for ($i = 0; $i < $count1; $i++) {
            $viewemployer[$i] = $employer[$i];
        }
       
        $count2 = count($job);
        $count2 = $count2 + $count1;
        $j = 0;
        for ($i = $count1; $i < $count2; $i++) {
            $viewemployer[$i] = $job[$j];
            $j++;
        }
        
        if (!empty($viewemployer)) {
            $this->_sendResponse(200, $viewemployer);
        } else {
            $this->_sendResponse(507, array());
        }
    }
     /*Employer View button*/
    
    /*User*/
    /*Add User Create*/
    public function actionadduser() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $empolyer = Employers::model()->findByPk($id);
        $employerid = $empolyer->id;

        $usermodel = new Users();
        $empusermodel = new EmployerUsers;
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } elseif ($empusermodel->hasAttribute($var)) {
                $empusermodel->$var = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->type = 'employer';
        if ($usermodel->save()) {
            $empusermodel->employer_id = $employerid;
            $empusermodel->user_id = $usermodel->id;
            $empusermodel->role = 'employer';
            $empusermodel->created_at = $now->format('Y-m-d H:i:s');
            if ($empusermodel->save()) {
                 $cri = new CDbCriteria();
                $cri->select='id, type, forenames, surname, email, password, dob, status, is_premium, tel, mobile, current_job, location, location_lat, location_lng';
                $cri->condition='id ='.$usermodel->id;
                $user = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $user);
            } else {
                $this->_sendResponse(507, array());
            }
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /*Add User Create*/
    /*get all*/
    public function actiongetalluser() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];
        $count = 0;
        $viewemployeuser = array();

        $emodel = Employers::model()->findByPk($id);
        $empid = $emodel->id;

        $empuser = EmployerUsers::model()->findByAttributes(array("employer_id" => $empid, "role" => 'employer'));
        $cri = new CDbCriteria();
        $cri->alias = 'empuser';
        $cri->select = 'user.id,user.forenames, user.surname, user.email,user.created_at';
        $cri->join = 'inner join cv16_users user on user.id = empuser.user_id';
        $cri->condition = "empuser.employer_id ='" . $id . "' and empuser.role ='employer' ";
        $empuser = $empuser->getCommandBuilder()->createFindCommand($empuser->tableSchema, $cri)->queryAll();

        $count1 = count($empuser);
        for ($i = 0; $i < $count1; $i++) {
            $viewemployeuser[$i] = $empuser[$i];
        }
        if (!empty($viewemployeuser)) {
            $this->_sendResponse(200, $viewemployeuser);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /*get all*/
    
    /*fetch user*/
    public function actionfetchuser() {
          if(!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse (400,array());
        
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $cri  = new CDbCriteria();
        $cri->select = 'forenames, surname, email';
        $cri->condition = 'id='.$id;
        $user = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
         if (!empty($user)) {
            $this->_sendResponse(200, $user);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    /*fetch user*/
    /*update user*/
    public function actionupdateuser() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $_POST = $put_vars;
        }

        $usermodel = Users::model()->findByPk($id);
        $usermodel->forenames = $_POST['forenames'];
        $usermodel->surname = $_POST['surname'];
        $usermodel->email = $_POST['email'];
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->confirmpassword = password_hash($_POST['confirmpassword'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email';
            $cri->condition = 'id='.$id;
            $user = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $user);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /*update user*/
    
    /*delete user*/
    public function actiondeleteuser() {
         if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $employeruser = EmployerUsers::model()->findByAttributes(array('user_id'=>$id));
        $cri = new CDbCriteria();
        $cri->condition = 'user_id='.$id;
        EmployerUsers::model()->deleteAll($cri);
        
        $usermodel = Users::model()->findByPk($id);
        $now = new DateTime();
        $usermodel->deleted_at = $now->format('Y-m-d H:i:s');
        $usermodel->save();
        $this->_sendResponse(200, $usermodel);
    }
    /*delete user*/
    
    /*User*/
    
    /*Employer inbox*/
    public function actionsendmessage() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $_POST['subject'];
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk($id);
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = $usermodel->id;
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $_POST['message'];
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = $usermodel->id;
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($_POST['user_id']);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $_POST['user_id'];
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = $usermodel->id;
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
                        $actmodel_sender->message = $_POST['message'];
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $_POST['user_id'];
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
                            $actmodel_rec->message = $_POST['message'];
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            if ($actmodel_rec->save()) {
                                $this->_sendResponse(200, sprintf("Message Inserted Successfully"));
                            } else {
                                $this->_sendResponse(507, array());
                            }
                        }
                    }
                }
            }
        } else {
            $this->_sendResponse(507, array());
        }
    }
     /*Employer inbox*/
    
    /*Employer Office*/
    /*add office location*/
    public function actionaddofficelocation() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $employer = Employers::model()->findByPk($id);
        $empusrid = $employer->id;

        $user = Users::model()->findByAttributes(array('id' => $empusrid));
        $address = new Addresses();
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($address->hasAttribute($var)) {
                $address->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        if ((isset($_POST['postcode']))) {
            $lonlat = $this->getLatLong($_POST['postcode']);
        }
        $address->model_id = $empusrid;
        $address->model_type = 'employer';
        $address->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        $address->latitude = $lonlat['latitide'];
        $address->longitude = $lonlat['longitute'];
        $address->created_at = $now->format('Y-m-d H:i:s');
        if ($address->save()) {
            $employer->location = $address->location;
            $employer->location_lat = $address->latitude;
            $employer->location_lng = $address->longitude;
            if ($employer->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id, model_id, model_type, street_number, address, town, postcode, county, country, location, latitude, longitude, created_at';
                $cri->condition = 'model_id=' . $id;
                $address = $address->getCommandBuilder()->createFindCommand($address->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $address);
            } else {
                $this->_sendResponse(507, array());
            }
        }
    }
    
      function getLatLong($lonlat) {
        $formattedAddr = str_replace(' ', '+', $lonlat);
        $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false');
        $result = json_decode($geocodeFromAddr, TRUE);
        $lat = $result['results'][0]['geometry']['location']['lat'];
        $lng = $result['results'][0]['geometry']['location']['lng'];
        $data['longitute'] = $lat;
        $data['latitide'] = $lng;         
        return $data;
    }
    /*add office location*/
    
    /*getall office */
    public function actiongetallofficeaddress() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $employer = Employers::model()->findByPk($id);
        $userid = $employer->user_id;
        $address = Addresses::model()->findByAttributes(array('model_id' => $id, 'model_type' => 'employer'));
        $cri = new CDbCriteria();
        $cri->select = 'id, model_id, model_type, street_number, address, town, postcode, county, country, location, latitude, longitude, created_at';
        $cri->condition = 'model_id=' . $id;
        $address = $address->getCommandBuilder()->createFindCommand($address->tableSchema, $cri)->queryAll();
        if (!empty($address)) {
            $this->_sendResponse(200, $address);
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /*get office */
    
    /*fetch office*/
    public function actionfetchoffice() {
         if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $address = Addresses::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->select = 'id, model_id, model_type, street_number, address, town, postcode, county, country, location, latitude, longitude, created_at';
        $cri->condition = 'id=' . $id;
        $address = $address->getCommandBuilder()->createFindCommand($address->tableSchema, $cri)->queryAll();
        if (!empty($address)) {
            $this->_sendResponse(200, $address);
        } else {
            $this->_sendResponse(500, array());
        }
    }
    /*fetch office*/
    
    /* delete office */
    public function actiondeleteoffice() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $address = Addresses::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = 'id=' . $id;
        Addresses::model()->deleteAll($cri);
        $address = $address->getCommandBuilder()->createFindCommand($address->tableSchema, $cri)->queryAll();
        $this->_sendResponse(200, sprintf("Deleted successfully"));
       
    }
    /* delete office */
    /*edit office*/
     public function actionupdateofficelocation() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }else {
            $_POST = $put_vars;
        }

        $employer = Employers::model()->findByPk($id);
        $empusrid = $employer->id;

        $user = Users::model()->findByAttributes(array('id' => $empusrid));
        $address = Addresses::model()->findByAttributes(array('model_id'=>$empusrid));
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($address->hasAttribute($var)) {
                $address->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        if ((isset($_POST['postcode']))) {
            $lonlat = $this->getLatLong($_POST['postcode']);
        }
        $address->model_id = $empusrid;
        $address->model_type = 'employer';
        $address->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        $address->latitude = $lonlat['latitide'];
        $address->longitude = $lonlat['longitute'];
        $address->updated_at = $now->format('Y-m-d H:i:s');
        if ($address->save()) {
            $employer->location = $address->location;
            $employer->location_lat = $address->latitude;
            $employer->location_lng = $address->longitude;
            if ($employer->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id, model_id, model_type, street_number, address, town, postcode, county, country, location, latitude, longitude, created_at';
                $cri->condition = 'model_id=' . $id;
                $address = $address->getCommandBuilder()->createFindCommand($address->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $address);
            } else {
                $this->_sendResponse(507, array());
            }
        }
    }
    /*edit office*/
    
    public function actioneditbody() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $_POST = $put_vars;
        }
        $now = new DateTime();
        $employer = Employers::model()->findByPk($id);
        $employer->body = $_POST['body'];
        $employer->updated_at = $now->format('Y-m-d H:i:s');
        if ($employer->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id, body';
            $cri->condition = 'id=' . $id;
            $employer = $employer->getCommandBuilder()->createFindCommand($employer->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $employer);
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /*Employer Office*/
    
    
   public function actiongetmessage() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $count = 0;
        $viewmessage = array();
        $convmodel = Conversations::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->alias = 'con';
        $cri->select = 'con.subject,cm.message';
        $cri->join = 'inner join cv16_conversation_messages cm on cm.conversation_id = con.id ';
             //   . 'inner join cv16_conversation_members conmem on conmem.conversation_id = con.id';
        $cri->condition = "con.id=" . $id;       
        $convmodel = $convmodel->getCommandBuilder()->createFindCommand($convmodel->tableSchema, $cri)->queryAll();      
        if (!empty($convmodel)) {
            $this->_sendResponse(200, $convmodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
     /*save message*/
    public function actionsavemessage() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        $usermodel = Users::model()->findByPk($id);
        $conmsgmodel = new ConversationMessages();
        $conmsgmodel->conversation_id = $_POST['cid'];
        $conmsgmodel->user_id = $usermodel->id;
        $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
        $conmsgmodel->message = $_POST['message'];
        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmsgmodel->save()) {
            $conmemmodel_sender = new ConversationMembers();
            $conmemmodel_sender->conversation_id = $_POST['cid'];
            $conmemmodel_sender->user_id = $usermodel->id;
            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmemmodel_sender->save()) {
                $this->_sendResponse(200, sprintf("Successfully inserted",""));
            } else {
                $this->_sendResponse(507, array());
            }
        }
    }

    /*save message*/
    
    
    
     public function actionupdateapplicationstatus() {
       if(!isset($_GET['id'])||$_GET['id'] == 0)
           $this->_sendResponse (400,array());
       $id = $_GET['id'];
       if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $jobappmodel = JobApplications::model()->findByPk($id);
        $value = $_POST['value'];
        $status = 0;
        if ($value == 'pending') {
            $status = 0;
        } else if ($value == 'shortlist') {
            $status = 1;
        } else if ($value == 'invite') {
            $status = 2;
        } else if ($value == 'interview') {
            $status = 3;
        } else if ($value == 'reject') {
            $status = -1;
        }
        $jobappmodel->status = $status;
        if ($jobappmodel->save())
            $this->_sendResponse(200, sprintf("Successfully updated", ""));
        else
            $this->_sendResponse(500, array());
    }
    
    /*employer search candidate*/
       public function actionsearchcandidates() {
           if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $skillcategid = $_POST['skill_category'];        
        $skillarr = isset($_POST['skills']) ? $_POST['skills'] : array();   
        if ((isset($_POST['location']))) {
            $lonlat = $this->getLatLong($_POST['location']);
        }       
        $lat = $lonlat['latitude'];
        $long = $lonlat['longitude'];
        $radius = isset($_POST['radius']) ? $_POST['radius'] : 0;
        $skillids = implode(',', $skillarr);
        $pmodel = Profiles::model();
        $cri = new CDbCriteria();
        $cri->alias = "p";
        $cri->distinct = true;      
        $cri->join = "inner join cv16_users u on u.id = p.owner_id "
                . "left outer join cv16_profile_skills ps on ps.profile_id = p.id "
                . "left outer join cv16_skills s on s.id = ps.skill_id "
                . "left outer join cv16_skill_categories sc on sc.id = s.skill_category_id";       
        if ($skillcategid > 0)
            $cri->addCondition("s.skill_category_id =" . $skillcategid);
        if ($skillids != "")
            $cri->addCondition('ps.skill_id in (' . $skillids . ')', 'AND');
        if ($lat != "") {
            if ($radius > 0) {
                $cri->select = "(6371 * acos( cos( radians(" . $lat . ") ) * cos( radians(" . $lat . ") ) *
                                cos(radians(" . $long . ") - radians(" . $long . ")) + sin(radians(" . $lat . ")) *
                                sin(radians(" . $lat . ")) )) as distance";
             
                $cri->having = "distance < " . $radius;
            } else {
                $cri->addCondition("u.location like '%" . $location . "%'");
//              $cri->addCondition("u.location_lat =".$lat." and u.location_lng=".$long);
            }
        }
        $cri->addCondition("p.visibility = 1");
        $cri->group = "p.id";             
        $pmodel = $pmodel->getCommandBuilder()->createFindCommand($pmodel->tableSchema, $cri)->queryAll();
        
        if (!empty($pmodel)) {
            $this->_sendResponse(200, $pmodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }
     
    
    
    
    /*employer search candidate*/
    
    
    
     public function actionreccreateemployer() {
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users;
        $empmodel = new Employers;
        $empusermodel = new EmployerUsers;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        $agencymodel = new AgencyEmployers();

        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($empmodel->hasAttribute($var)) {
                $empmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($empusermodel->hasAttribute($var)) {
                $empusermodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($agencymodel->hasAttribute($var)) {
                $agencymodel->$var = $value;
            } else if ($var == 'ispremium') {
                
            } else {
                $this->_sendResponse(500, array());
            }
        }
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->type = 'employer';
        if ((isset($_POST['location']))) {
            $lonlat = $this->getLatLong($_POST['location']);
        }
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        $usermodel->location_lat = $lonlat['location_lat'];
        $usermodel->location_lng = $lonlat['location_lng'];
        $usermodel->tel = $_POST['tel'];
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
            $empmodel->name = $_POST['name'];
            $empmodel->user_id = $usermodel->id;
            $empmodel->email = $usermodel->email;
            $empmodel->tel = $usermodel->mobile;
            $empmodel->location = $usermodel->location;
            $empmodel->website = $_POST['slug'];
            $empmodel->location_lat = $usermodel->location_lat;
            $empmodel->location_lng = $usermodel->location_lng;
            $empmodel->created_at = $now->format('Y-m-d H:i:s');
            if (isset($_POST['ispremium']) && $_POST['ispremium'])
                $empmodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            if ($empmodel->save()) {
                $empusermodel->employer_id = $empmodel->id;
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = 'admin';
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                if ($empusermodel->save()) {
                    // if($_POST['industry_id'] > 0)
                    $profilemodel->industry_id = $_POST['industry_id'];
                    $profilemodel->type = 'employer';
                    $profilemodel->owner_id = $usermodel->id;
                    $profilemodel->owner_type = 'employer';
                    $profilemodel->slug = $usermodel->forenames . '' . $usermodel->surname;
                    if ($profilemodel->save()) {
                        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                        $addressmodel->model_type = 'employer';
                        $addressmodel->model_id = $usermodel->id;
                        if ($addressmodel->save()) {
                            $agencymodel->employer_id = $empmodel->id;
                            $agencymodel->created_at = $now->format('Y-m-d H:i:s');
                            if ($agencymodel->save()) {
                                $cri = new CDbCriteria();
                                $cri->select = 'id,forenames,surname,email,mobile';
                                $cri->condition = "id = " . $usermodel->id;
                                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                                $this->_sendResponse(200, $users);
                            }
                        }
                    } else {
                        echo var_dump($profilemodel->getErrors());
                    }
                } else {
                    echo var_dump($empusermodel->getErrors());
                }
            } else {
                echo var_dump($empmodel->getErrors());
            }
        } else {
            echo ($usermodel->getErrors());
            $this->_sendResponse(500, array());
        }
    }
    
    
    
    ///   /    get recruitment staff
public function actiongetrecemployer() {
    if (!isset($_GET['id']) || $_GET['id'] == 0)
        $this->_sendResponse(404, array());

    $model = Agency::model()->findByPk($_GET['id']);
    $cri = new CDbCriteria();
    $cri->alias ='i';
    $cri->select ="s.id  as staff_id,u.id as employer_id,u.name, u.website, u.email ";

    $cri->join = "left outer join cv16_agency_employers s on s.agency_id = i.id
                  left outer join cv16_employers u on u.id = s.employer_id";

    $cri->condition="i.id =".$_GET['id'];
    // echo var_dump($cri);
    // die();

    $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

    if (empty($users)) {
        $this->_sendResponse(404, array());
    } else {
        $this->_sendResponse(200, $users);
    }
}



/* delete rec candidate */

 public function actiondeleterecemployer() {
    if (!isset($_GET['id']) || $_GET['id'] == 0)
        $this->_sendResponse(404, array());
    $id = $_GET['id'];
    $deletedraftmsg = AgencyEmployers::model();
    $cri = new CDbCriteria();
    $cri->condition = "employer_id = " . $_GET['id'];
    AgencyEmployers::model()->deleteAll($cri);
       $employer = Employers::model()->findByAttributes(array('id'=>$id));
       $userid = $employer->user_id;
    $cri = new CDbCriteria();
    $cri->condition = "id = " . $_GET['id'];
    Users::model()->deleteAll($cri);
    $deletedraftmsg = Users::model()->findByAttributes(array('id'=>$userid));
    $cri = new CDbCriteria();
    $cri->condition = "id = " . $_GET['id'];
    Users::model()->deleteAll($cri);
    $this->_sendResponse(200, "Deleted Successfully");
}


    public function actioncreatevacancy() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $agency = Agency::model()->findByPk($id);
        $userid = $agency->user_id;
        $jobskillmodel = new JobSkill();
        $jobmodel = new Jobs();
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($jobmodel->hasAttribute($var)) {
                $jobmodel->$var = $value;
            } elseif ($var == 'skills') {
                $skills = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $jobmodel->owner_id = $id;
        $jobmodel->owner_type = 'agency';
        $jobmodel->user_id = $userid;
        $d = str_replace('/', '-', $_POST["end_date"]);
        $endformat = date('Y-m-d', strtotime($d));
        $jobmodel->end_date = $endformat;
        $jobmodel->created_at = $now->format('Y-m-d H:i:s');
        if (isset($_POST['location'])) {
            $lonlat = $this->getLatLong($_POST['location']);
        }
        $jobmodel->location_lat = $lonlat['location_lat'];
        $jobmodel->location_lng = $lonlat['location_lng'];
        if ($jobmodel->save()) {

            foreach ($skills as $value) {
                $jobskillmodels = new JobSkill();
                $jobskillmodels->job_id = $jobmodel->id;
                $jobskillmodels->skill_id = $value['skill_id'];
                $jobskillmodels->vacancies = $value['vacancies'];
                $jobskill = $jobskillmodels->save();
            }
            if ($jobskill) {
                $cri = new CDbCriteria();
                $cri->select = 'id, owner_id, owner_type, user_id, industry_id, company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description, additional';
                $cri->condition = 'id=' . $jobmodel->id;
                $users = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri)->queryAll();
                if (!empty($users)) {
                    $this->_sendResponse(200, $users);
                } else {
                    $this->_sendResponse(200, array());
                }
            } else {
                echo var_dump($jobskillmodels->getErrors());
            }
        } else {
            echo var_dump($jobmodel->getErrors());
        }
    }
    
    /*delete vacancy recruit*/
    public function actiondeletevacancy() {
          if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        
        $jobs = Jobs::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = 'id='.$id;
        Jobs::model()->deleteAll($cri);
        
        $jobsskill = JobSkill::model()->findByAttributes(array('job_id'=>$id));
        $cri = new CDbCriteria();
        $cri->condition = 'job_id='.$id;
        JobSkill::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }
    /*delete vacancy recruit*/
    
    public function actiongetallvacancy() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $agency = Agency::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->alias = 'agency';
        $cri->select = 'jobs.title, jobs.salary_type,jobs.vacancies';
        $cri->join = 'inner join cv16_jobs jobs on agency.id = jobs.owner_id ';                
        $cri->condition = 'agency.id=' . $id;
        $agency = $agency->getCommandBuilder()->createFindCommand($agency->tableSchema, $cri)->queryAll();
        if (!empty($agency)) {
            $this->_sendResponse(200, $agency);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
    
    public function actionoldfetchvacancy() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];

        $jobs = Jobs::model()->findByPk($id);

        $cri = new CDbCriteria();
        $cri->alias = 'j';
        $cri->select = 's.name,sc.name,j.industry_id, j.company_name, j.title, j.salary_type, j.salary_min, j.salary_max, j.end_date, j.skill_category_id, j.location, j.location_lat, j.location_lng';
        $cri->join = 'left outer join cv16_job_skill js on js.job_id = j.id '
                . 'left outer join cv16_skills s on s.id = js.skill_id '
                . 'left outer join cv16_skill_categories sc on sc.id = s.skill_category_id';
        $cri->condition = 'j.id=' . $id;
        $jobs = $jobs->getCommandBuilder()->createFindCommand($jobs->tableSchema, $cri)->queryAll();
        if (!empty($jobs)) {
            $this->_sendResponse(200, $jobs);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
    
    
    
    
    public function actionupdatevacancy() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $_POST = $put_vars;
        }
        $agency = Agency::model()->findByPk($id);
        $userid = $agency->user_id;
        
       
        $jobmodel = Jobs::model()->findByAttributes(array('owner_id'=>$id));
      
        $skills  = array();
        $now = new DateTime();
       
        foreach ($_POST as $var => $value) {
            if ($jobmodel->hasAttribute($var)) {
                $jobmodel->$var = $value;
            } elseif ($var == 'skills') {
                $skills = array_merge($skills,$value);
            } else {
                $this->_sendResponse(400, array());
            }
            
        }
        $jobmodel->owner_id = $id;
        $jobmodel->owner_type = 'agency';
        $jobmodel->user_id = $userid;
        $d = str_replace('/', '-', $_POST["end_date"]);
        $endformat = date('Y-m-d', strtotime($d));
        $jobmodel->end_date = $endformat;
        $jobmodel->created_at = $now->format('Y-m-d H:i:s');
        if (isset($_POST['location'])) {
            $lonlat = $this->getLatLong($_POST['location']);
        }
        $jobmodel->location_lat = $lonlat['location_lat'];
        $jobmodel->location_lng = $lonlat['location_lng'];
        if ($jobmodel->save()) {
                $jobskillmodels = JobSkill::model()->deleteAllByAttributes(array('job_id'=>$jobmodel->id));  
            foreach ($skills as $value=>$key) {                
                $jobskillmodels = new JobSkill();            
                $jobskillmodels->job_id = $jobmodel->id;
                $jobskillmodels->skill_id = $key['skill_id'];
                $jobskillmodels->vacancies = $key['vacancies']; 
                $jobskillmodels->save();
            }        
                $cri = new CDbCriteria();
                $cri->select = 'id, owner_id, owner_type, user_id, industry_id, company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description, additional';
                $cri->condition = 'id=' . $jobmodel->id;
                $users = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri)->queryAll();
                if (!empty($users)) {
                    $this->_sendResponse(200, $users);
                } else {
                    $this->_sendResponse(507, array());
                }
        } else {
            echo var_dump($jobmodel->getErrors());
        }
    }
    
    
     public function actionfetchvacancy() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
       
        $viewservice = [];
        $jobsmodel = Jobs::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->select = 'id, owner_id, owner_type, user_id, industry_id, company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description, additional';
        $cri->condition = 'id=' . $_GET['id'];
        $jobs = $jobsmodel->getCommandBuilder()->createFindCommand($jobsmodel->tableSchema, $cri)->queryAll();
       
        $jobskillmodel = JobSkill:: model()->findByAttributes(array('job_id' => $_GET['id']));
        $cri = new CDbCriteria();
        $cri->alias = 'js';
        $cri->select = 's.name,sc.name as Name';
         $cri->join = 'left outer join cv16_skills s on s.id = js.skill_id left outer join cv16_skill_categories sc on sc.id = s.skill_category_id';
        $cri->condition = 'js.job_id =' . $_GET['id'];        
        $jobskill = $jobskillmodel->getCommandBuilder()->createFindCommand($jobskillmodel->tableSchema, $cri)->queryAll();       
    
        foreach ($jobs as $key => $value) {
            $viewservice =  $value;
        }
        $viewservice['skills']=$jobskill ;
      
        if (!empty($jobs)) {
            $this->_sendResponse(200, $viewservice);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
    public function actionrecruitdeletevacancy() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $jobs = Jobs::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = 'id=' . $id;
        Jobs::model()->deleteAll($cri);

        $jobsskill = JobSkill::model()->findByAttributes(array('job_id' => $id));
        $cri = new CDbCriteria();
        $cri->condition = 'job_id=' . $id;
        JobSkill::model()->deleteAll($cri);
        $this->_sendResponse(200, sprintf("Deleted Successfully"));
    }
    
    
      public function actionfetchcandidatedetails() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Users::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias = 'u';
        $cri->select = "u.id as user_id,m.file_name,(SELECT count(*) FROM cv16_profile_views WHERE profile_id = p.id) as num_views,(SELECT count(*) FROM cv16_profile_favourites WHERE profile_id = p.id) as num_likes,CONCAT('http://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('http://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('http://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('http://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.stripe_active,u.current_job,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
        $cri->join = "inner join cv16_profiles p on p.owner_id = u.id
                      left outer join cv16_media m on m.id = p.photo_id
                      left outer join cv16_addresses a on a.model_id = u.id";
        $cri->condition = "u.id = " . $model->id . " and u.type like '%candidate%' and p.owner_type like '%candidate%'";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }
    
           public function actioncandidatehiredstatus() {
        if (!isset($_GET['id']) || ($_GET['id'] == 0 ))
            $this->_sendResponse(400, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }else {
            $put_vars = $_POST;
        }
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = "owner_id = ".$id." and owner_type like '%Candidate%'";
        $profilemodel = Profiles::model()->find($cri);
        $profilemodel->hired = $_POST['hired'];
        $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
        if ($profilemodel->save()) {
            $this->_sendResponse(200, sprintf("Updated status successfully"));
        } else {
            
            $this->_sendResponse(500, array());
        }
    }
    
    
    
    
    /*active or inactive*/
   public function actioncandidatechangestatus() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $usermodel = Users::model()->findByPk($id);
        if ($usermodel->status == 'active') {
            $usermodel->status = 'inactive';
        } else {
            $usermodel->status = 'active';
        }
        if ($usermodel->save()) {
            $this->_sendResponse(200, sprintf("Updated status successfully"));
        } else {
            $this->_sendResponse(500, array());
        }
    }
    
  /* public or private */
    public function actioncandidateprivatepublic() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $usermodel = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = "owner_id = " . $id . " and owner_type like '%Candidate%'";
        $profile = Profiles::model()->find($cri); 
        if ($profile->visibility == 0) {
            $profile->visibility = '1';
        } else {
            $profile->visibility = '0';
        }
        if ($profile->save()) {
            $this->_sendResponse(200, sprintf("inserted successfully"));
        } else {
            $this->_sendResponse(500, array());
        }
    }
    
    
   /*search jobs*/
    public function actionsearchjobs() {
         if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } 
        $skillcategid = $_POST['skill_category'];
        $skillarr = isset($_POST['skills']) ? $_POST['skills'] : array();
        
         if ((isset($_POST['location']))) {
            $lonlat = $this->getLatLong($_POST['location']);
        }       
        $lat = $lonlat['latitude'];
        $long = $lonlat['longitude']; 
        $jobtype = isset($_POST['jobtype']) ? $_POST['jobtype'] : "";
        $salary = isset($_POST['salary']) ? $_POST['salary'] : "";
        $radius = isset($_POST['distance']) ? $_POST['distance'] : 0;

        $skillids = implode(',', $skillarr);
       $jobmodel = Jobs::model();
        $cri = new CDbCriteria();
        $cri->alias = "j";
        $cri->join = "inner join cv16_job_skill js on js.job_id = j.id
                      left outer join cv16_skills s on s.id = js.skill_id
                      left outer join cv16_skill_categories sc on sc.id = j.skill_category_id";
        if ($skillcategid > 0)
            $cri->addCondition("j.skill_category_id =" . $skillcategid);
        if ($skillids != "")
            $cri->addCondition('js.skill_id in (' . $skillids . ')', 'AND');
        if ($location != "" && $lat != "") {
            if ($radius > 0) {
                $cri->select = "(6371 * acos( cos( radians(".$lat.") ) * cos( radians(".$lat.") ) *
                                cos(radians(".$long.") - radians(".$long.")) + sin(radians(".$lat.")) *
                                sin(radians(".$lat.")) )) as distance";   
//                 $cri->addCondition("HAVING distance < " . $radius);
                $cri->having = "distance < " . $radius;
            } else {
                $cri->addCondition("j.location like '%" . $location . "%'");
               // $cri->addCondition("j.location_lat =".$lat." and j.location_lng=".$long);
            }
        }
        if ($salary > 0) {
            $cri->addCondition("j.salary_min >= " . $salary);
        }
        if ($jobtype != "") {
            $cri->addCondition("j.type like '%" . $jobtype . "%'");
        }

        $cri->group = "j.id";        
        $jobmodel = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri)->queryAll();        
        if (!empty($jobmodel)) {
            $this->_sendResponse(200, $jobmodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    /*search jobs*/
    
     // End Vinoth API //

      // Start ilai API //


//    create language
    public function actioncreatelanguage() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();

        $model = new UserLanguages();

        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, array());
        }

        $model->user_id = $usermodel->id;
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileLanguages();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_language_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pqmodel->save()) {
                $prolan = UserLanguages::model()->findByPk($model->id);
                $this->_sendResponse(200, $prolan);
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    //    update language
    public function actionupdatelanguage() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userlanmodel = UserLanguages::model()->findByPk($id);
        if ($id != $userlanmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }


        $now = new DateTime();


        foreach ($put_vars as $var => $value) {
            if ($userlanmodel->hasAttribute($var)) {
                $userlanmodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }
        $usermodel = Users::model()->findByPk($userlanmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));


        $userlanmodel->updated_at = $now->format('Y-m-d H:i:s');

        if ($userlanmodel->save()) {

            $pemodel = ProfileLanguages::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_language_id' => $userlanmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Language Successfully updated", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        } else {
            echo var_dump($userlanmodel->errors);
        }
    }

    /// Get languages

    public function actiongetlanguage() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = UserLanguages::model();
        $cri = new CDbCriteria();
        $cri->select = "id, name, proficiency";
        $cri->condition = "user_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* language delete */

    public function actiondeletelanguage() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = UserLanguages::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        UserLanguages::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    //    create hobbies
    public function actioncreatehobbies() {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();
        $model = new UserHobbies();
        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $model->user_id = $usermodel->id;
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileHobbies();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_hobby_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pqmodel->save()) {
                $prohob = UserHobbies::model()->findByPk($model->id);
                $this->_sendResponse(200, $prohob);
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    //    update hobbies
    public function actionupdatehobbies() {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userhobmodel = UserHobbies::model()->findByPk($id);
        if ($id != $userhobmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($userhobmodel->hasAttribute($var))
                $userhobmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $usermodel = Users::model()->findByPk($userhobmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));


        $userhobmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($userhobmodel->save()) {
            $pemodel = ProfileHobbies::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_hobby_id' => $userhobmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully Hobbies updated", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    /// Get Hobbies

    public function actiongethobbies() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = UserHobbies::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "user_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* language Hobbies */

    public function actiondeletehobbies() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = UserHobbies::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        UserHobbies::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    //    create education
    public function actioncreateeducation() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $model = new UserQualifications();
        $pqmodel = new ProfileQualifications();

        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendresponse(400, array());
        }

        // $completion_date = $_POST['completion_year'] . "-" . $_POST['completion_month'] . "-01";

        $model->user_id = $usermodel->id;
        //  $model->completion_date = $completion_date;
        $model->created_at = $now->format('Y-m-d H:i:s');
        // echo var_dump($model);
        // die();
        if ($model->save()) {
            $pqmodel = new ProfileQualifications();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_qualification_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');

            if ($pqmodel->save()) {
                $proexp = UserQualifications::model()->findByPk($model->id);
                $this->_sendResponse(200, $proexp);
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    //    update education
    public function actionupdateeducation() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userqualmodel = UserQualifications::model()->findByPk($id);
        if ($id != $userqualmodel->id)
            $this->_sendResponse(404, array());

        $usermodel = Users::model()->findByPk($userqualmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($userqualmodel->hasAttribute($var))
                $userqualmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }

        // $completion_date = $_POST['completion_year'] . "-" . $_POST['completion_month'] . "-01";

        $userqualmodel->user_id = $usermodel->id;
        // $userqualmodel->completion_date = $completion_date;
        // $userqualmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($userqualmodel->save()) {
            $pemodel = ProfileQualifications::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_qualification_id' => $userqualmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Education updated Successfully", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    /// Get education

    public function actiongeteducation() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = UserQualifications::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "user_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* delete education */

    public function actiondeleteeducation() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = UserQualifications::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        UserQualifications::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    ///  /    create Professional Experience
    public function actioncreateprofessionalexprience() {
        //        if(!$this->_checkAuth())
        //	        $this->_sendResponse(405, array());



        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);

        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $model = new UserExperiences();

        $now = new DateTime();
        $iscurrent = 0;
        $start_month = null;
        $start_year = null;
        $end_month = null;
        $end_year = null;
        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else if ($var == "is_current")
                $iscurrent = $value;
            else if ($var == "start_month")
                $start_month = $value;
            else if ($var == "start_year")
                $start_year = $value;
            else if ($var == "end_month")
                $end_month = $value;
            else if ($var == "end_year")
                $end_year = $value;
            else
                $this->_sendResponse(500, array());
        }
        $model->user_id = $usermodel->id;
        $model->start_date = $start_year . "-" . $start_month . "-01";
        $model->end_date = $iscurrent == 1 ? null : ($end_year . "-" . $end_month . "-01");
        $model->created_at = $now->format('Y-m-d H:i:s');


        // Try to save the model
        if ($model->save()) {
            $pemodel = new ProfileExperiences();
            $pemodel->profile_id = $profilemodel->id;
            $pemodel->user_experience_id = $model->id;
            $pemodel->created_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

        $model = new UserExperiences();
        $cri = new CDbCriteria();
        $cri->alias = "ux";
        $cri->select = "ux.company_name,ux.location,ux.position,ux.start_date,ux.end_date,ux.description";
        $cri->join = "inner join cv16_profile_experiences px on px.user_experience_id = ux.id";
        $cri->condition = "px.profile_id =" . $id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }


//    update Professional Experience
    public function actionupdateprofessionalexprience() {
//         if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userexmodel = UserExperiences::model()->findByPk($id);
        if ($id != $userexmodel->id)
            $this->_sendResponse(404, array());

        $usermodel = Users::model()->findByPk($userexmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $iscurrent = 0;
        $start_month = null;
        $start_year = null;
        $end_month = null;
        $end_year = null;
        foreach ($_POST as $var => $value) {
            if ($userexmodel->hasAttribute($var))
                $userexmodel->$var = $value;
            else if ($var == "is_current")
                $iscurrent = $value;
            else if ($var == "start_month")
                $start_month = $value;
            else if ($var == "start_year")
                $start_year = $value;
            else if ($var == "end_month")
                $end_month = $value;
            else if ($var == "end_year")
                $end_year = $value;
            else
                $this->_sendResponse(500, array());
        }

        $userexmodel->start_date = $start_year . "-" . $start_month . "-01";
        $userexmodel->end_date = $iscurrent == 1 ? null : ($end_year . "-" . $end_month . "-01");
        $userexmodel->updated_at = $now->format('Y-m-d H:i:s');
        // Try to save the model
        if ($userexmodel->save()) {
            $pemodel = ProfileExperiences::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_experience_id' => $userexmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

       $model = new ProfileExperiences();
        $cri = new CDbCriteria();
        $cri->alias = "px";
        $cri->select = "ux.company_name,ux.location,ux.position,ux.start_date,ux.end_date,ux.description";
        $cri->join = "inner join cv16_user_experiences ux on ux.id = px.user_experience_id";
        $cri->condition = "px.user_experience_id=" . $id;
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }



    /// Get professional

    public function actiongetprofessional() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = UserExperiences::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "user_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* delete professional */

    public function actiondeleteprofessional() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = UserExperiences::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        UserExperiences::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    ///  /    create  skills
    public function actionskills() {
        //        if(!$this->_checkAuth())
        //	        $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);

        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));


        $now = new DateTime();
        $skills = $_POST['skills'];

        foreach ($skills as $skill) {
            $model = new ProfileSkills();
            $model->profile_id = intval($profilemodel->id);
            $model->skill_id = $skill;
            $model->save();
        }
        $model = new ProfileSkills();
        $cri = new CDbCriteria();

        $cri->select = "*";
        $cri->condition = "profile_id =" . $profilemodel->id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }

    public function actionupdateskills() {

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);

        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $deletedraftmsg = ProfileSkills::model();
        $cri = new CDbCriteria();
        $cri->condition = "profile_id =" . $profilemodel->id;
        ProfileSkills::model()->deleteAll($cri);
        $now = new DateTime();
        $skills = $_POST['skills'];

        foreach ($skills as $skill) {
            $model = new ProfileSkills();
            $model->profile_id = intval($profilemodel->id);
            $model->skill_id = $skill;
            $model->save();
        }
        $model = new ProfileSkills();
        $cri = new CDbCriteria();

        $cri->select = "*";
        $cri->condition = "profile_id =" . $profilemodel->id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }

    /// Get professional

    public function actiongetskillcategory() {
        $model = SkillCategories::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /// Get professional

    public function actiongetskillsubcategory() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = Skills::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "skill_category_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }



    //// create institution

    public function actioncreateinstitution() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        $instmodel = new Institutions();
        $iusermodel = new InstitutionUsers();
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($instmodel->hasAttribute($var)) {
                $instmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->status = 'pending';
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        $usermodel->type = 'institution_admin';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->save();
        //  instituion insert
        $instmodel->admin_id = $usermodel->id;
        $instmodel->email = $usermodel->email;
        $instmodel->status = 'pending';
        $instmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($instmodel->save()) {
            $iusermodel->institution_id = $instmodel->id;
            $iusermodel->user_id = $usermodel->id;
            $iusermodel->role = "admin";
            $iusermodel->created_at = $now->format('Y-m-d H:i:s');
            $iusermodel->save();
        }
        //profile insert
        $slug = str_replace(" ", "-", $instmodel->name);
        $profilemodel->owner_type = "Institution";
        $profilemodel->owner_id = $instmodel->id;
        $profilemodel->created_at = $usermodel->created_at;
        $profilemodel->type = "institution";
        $profilemodel->slug = lcfirst($slug);
        $profilemodel->save();
        //address insert
        $addressmodel->model_id = $instmodel->id;
        $addressmodel->model_type = "Institutions";
        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($addressmodel->save()) {

            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email,mobile';
            $cri->condition = "id = " . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        } else {

            // Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", "");
            $msg .= "<ul>";
            foreach ($usermodel->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, array());
        }
    }

    ///   /    get institutions
    public function actiongetinstitutions() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Institutions::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "i.id as institution_id,m.file_name,CONCAT('http://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('http://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('http://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('http://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.id as user_id,u.stripe_active,u.current_job,u.type,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
        $cri->join = "left outer join cv16_profiles p on p.owner_id = i.user_id
                      left outer join cv16_users u on u.id = i.user_id
                      left outer join cv16_media m on m.id = p.photo_id
                      left outer join cv16_addresses a on a.model_id = i.user_id";

//        if($model == null )
//            echo "sadad";
//        die();
        $cri->condition = "i.id = " . $model->id . "";

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    public function actionaddteacher() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $insmodel = Institutions::model()->findByPk($id);
        if ($id != $insmodel->id)
            $this->_sendResponse(404, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $now = new DateTime();
        $usermodel = new Users();
        $iumodel = new InstitutionUsers();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }

        }


        $usermodel->type = "teacher";
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {

            $iumodel->institution_id = $id;
            $iumodel->user_id = $usermodel->id;
            $iumodel->role = $usermodel->type;
            $iumodel->created_at = $now->format('Y-m-d H:i:s');
            if ($iumodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,forenames,surname,email,mobile';
                $cri->condition = "id = " . $usermodel->id;
                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $users);
            } else {
                $this->_sendResponse(500, array());
            }
        }
        else  $this->_sendResponse("Email already exist");
       
    }





    public function actioneditteacher() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $iumodel = InstitutionUsers::model()->findByPk($id);
        if ($id != $iumodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        

        $usermodel = Users::model()->findByPk($iumodel->user_id);
        $pwd = $usermodel->password;

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($var == 'users') {
                $_POST['users'] = $value;
            } else {
                $this->_sendResponse(400, array());
                // echo var_dump ($usermodel->errors);
            }
        }


        $now = new DateTime();

        $iumodel->updated_at = $now->format('Y-m-d H:i:s');
        $iumodel->save();

        $usermodel->attributes = $_POST['users'];
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($_POST['password'] == "")
            $usermodel->password = $pwd;
        else
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {

            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email,mobile';
            $cri->condition = "id = " . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(500, array());
        }
    }




      /* Teacher delete */

      public function actiondeleteteacher() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = InstitutionUsers::model();
        $cri = new CDbCriteria();
        $cri->condition = "user_id = " . $_GET['id'];
        InstitutionUsers::model()->deleteAll($cri);
        $deletedraftmsg = Users::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        Users::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }


    //// Add class


    public function actionsaveclass() {

          if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
      
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);

        if ($id != $usermodel->id)
            $this->_sendResponse(400, array());
        $model = Institutions::model()->findByAttributes(array('admin_id' => $usermodel->id));
        $id = $model->id;
        $now = new DateTime();
        $tgmodel = new TutorGroups();
        $tmodel = new Users();
        $iumodel = new InstitutionUsers();

        foreach ($_POST as $var => $value) {
            if ($tgmodel->hasAttribute($var)) {
                $tgmodel->$var = $value;
            } elseif ($tmodel->hasAttribute($var)) {
                $tmodel->$var = $value;
            }
            // else if ($var == 'teacher_id'){
            //     $_POST['teacher_id']=$value;
            // }
            else {
                $this->_sendResponse(400, array());
            }
        }
        //  echo var_dump($_POST['teacher_id']);
        //  die();
        if ($_POST['teacher_id'] <= 0) {
            $tmodel->type = "teacher";
            $tmodel->status = "active";
            $tmodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
            $tmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($tmodel->save()) {

                $iumodel->institution_id = $model->id;
                $iumodel->user_id = $tmodel->id;
                $iumodel->role = $tmodel->type;
                $iumodel->created_at = $now->format('Y-m-d H:i:s');
                $iumodel->save();

                $tgmodel->teacher_id = $tmodel->id;
            }
        }

        $grad_date = null;
        if ($_POST['graduation_date'] != "") {
            //   $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
        }
        $tgmodel->institution_id = $id;
        $tgmodel->graduation_date = $grad_date;
        $tgmodel->created_at = $now->format('Y-m-d H:i:s');
        // $tgmodel->account_limit = $_POST['allocated_students'];//
        if ($tgmodel->save()) {
            $forenames_arr = $_POST['forenames'];
            $surname_arr = $_POST['surname'];           
            $email_arr = $_POST['email'];
            $password_arr = $_POST['password'];
            for($i = 0; $i < count($forenames_arr); $i++) {

                $umodel = new Users();
                $umodel->forenames = $forenames_arr[$i];
                $umodel->surname = $surname_arr[$i];
                $umodel->password = $password_arr[$i];
                $umodel->email = $email_arr[$i];
                $umodel->type = "candidate";
                $umodel->status = "active";
                $umodel->created_at = $now->format('Y-m-d H:i:s');
                if ($umodel->save(false)) {
                    $pmodel = new Profiles();
                    $pmodel->owner_id = $umodel->id;
                    $pmodel->owner_type = "Candidate";
                    $pmodel->type = "candidate";
                    $pmodel->slug = $this->getValidatedSlug($umodel->forenames . "-" . $umodel->surname);
                    $pmodel->created_at = $now->format('Y-m-d H:i:s');
                    $pmodel->save(false);

                    $tgsmodel = new TutorGroupsStudents();
                    $tgsmodel->institution_id = $id;
                    $tgsmodel->tutor_group_id = $tgmodel->id;
                    $tgsmodel->student_id = $umodel->id;
                    $tgsmodel->graduation_date = $grad_date;
                    $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
                    $tgsmodel->save(false);
                }
            }
            $this->_sendResponse(200, sprintf("Successfully class added", ""));
        }
    }



    public function actioneditclass() {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $tgmodel = TutorGroups::model()->findByPk($id);
        if ($id != $tgmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $_POST = $_POST;
        }

        $model = Institutions::model()->findByPk($tgmodel->institution_id);
        $id = $model->id;
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($tgmodel->hasAttribute($var)) 
            {
                $tgmodel->$var = $value;
            }else if($var == 'TutorGroups'){

            } else {
              //  echo $var;
               $this->_sendResponse(400, array());
              //  echo var_dump($tgmodel->errors);
            }

        }
        // die();


        $grad_date = null;
        if ($_POST['graduation_date'] != "") {
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
        }
        // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");

        $tgmodel->attributes = $_POST['TutorGroups'];
        $tgmodel->graduation_date = $grad_date;
        $tgmodel->updated_at = $now->format('Y-m-d H:i:s');
      //  $tgmodel->account_limit = $_POST['allocated_students'];
        if ($tgmodel->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Successfully updated class", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }



    /* class delete */

    public function actiondeleteclass() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = TutorGroups::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        TutorGroups::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }


///  Add student

    public function actionaddstudent() {    
      
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        $now = new DateTime();

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $_POST = $_POST;
        }


        $tgsmodel = new TutorGroupsStudents();

        $umodel = new Users();

        foreach ($_POST as $var => $value) {
            if ($umodel->hasAttribute($var)) {
                $umodel->$var = $value;
            }
            else  if ($tgsmodel->hasAttribute($var)) {
                $tgsmodel->$var = $value;
            }
            else if($var == 'tutorgroup_id'){

            }
             else {
                // echo $var
                $this->_sendResponse(400, array());
            }
        }

        $insmodel = Institutions::model()->findByAttributes(array('admin_id' => $usermodel->id));
        $id = $insmodel->id;
        $tutorid = $_POST['tutorgroup_id'];
        $instid = $_POST['institution_id'];


        $umodel->type = "candidate";
        $umodel->status = "active";
        $umodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $umodel->created_at = $now->format('Y-m-d H:i:s');
        if ($umodel->save()) {
            $pmodel = new Profiles();
            $pmodel->owner_id = $umodel->id;
            $pmodel->owner_type = "Candidate";
            $pmodel->type = "candidate";
            $pmodel->slug = $this->getValidatedSlug($umodel->forenames . "-" . $umodel->surname);
            $pmodel->created_at = $now->format('Y-m-d H:i:s');
            $pmodel->save();

            $grad_date = null;
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
// 	        if($_POST['graduation_date'] != "")
            // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
            $tgsmodel = new TutorGroupsStudents();
            $tgsmodel->institution_id = $instid;
            $tgsmodel->tutor_group_id = $tutorid;
            $tgsmodel->graduation_date = $grad_date;
            $tgsmodel->student_id = $umodel->id;
            $tgsmodel->created_at = $now->format('Y-m-d H:i:s');

            if ($tgsmodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully added student", ""));
            } else {
                $this->_sendResponse(500, array());
            }
          
        }
    else   $this->_sendResponse(502, array());
     
    }



    public function actioneditstudent() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
        if ($id != $tgsmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $_POST = $_POST;
        }

        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
        $model = Institutions::model()->findByPk($tgsmodel->institution_id);
        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $pwd = $usermodel->password;

        $id = $tgsmodel->institution_id;
        $now = new DateTime();
        $tutorid = $tgsmodel->tutor_group_id;

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } 
            else  if ($tgsmodel->hasAttribute($var)) {
                $tgsmodel->$var = $value;
            }
            else if($var == 'tutorgroup_id'){

            }
            else {
                $this->_sendResponse(400, array());
            }
        }
       $tutorid = $_POST['tutorgroup_id'];
        $instid = $_POST['institution_id'];


        if (trim($_POST['password']) == "")
            $usermodel->password = $pwd;
        else
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $grad_date = null;
            if ($_POST['graduation_date'] != "") {
                $d = str_replace('/', '-', $_POST["graduation_date"]);
                $grad_date = date('Y-m-d', strtotime($d));
            }
            $tgsmodel->institution_id = $instid;
            $tgsmodel->tutor_group_id = $tutorid;
            $tgsmodel->graduation_date = $grad_date;
            $tgsmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($tgsmodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully updated student", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }



    /* Student
     delete */

    public function actiondeletestudent() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = TutorGroupsStudents::model();
        $cri = new CDbCriteria();
        $cri->condition = "student_id = " . $_GET['id'];
        TutorGroupsStudents::model()->deleteAll($cri);
        $deletedraftmsg = Users::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        Users::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }



     ///   /    get student
     public function actiongetstudent() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Users::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias ='i';
        $cri->select ="i.id,i.forenames, i.surname, t.teacher_id, t.name,t.graduation_date,t. created_at";

        $cri->join = "left outer join cv16_tutor_groups t on t.teacher_id = i.id";

        $cri->condition="t.institution_id =".$_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }


    /// Get teacher
    public function actiongeteacher() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = InstitutionUsers::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias ='i';
        $cri->select ="i.id,u.forenames,t.account_limit,u.surname, t.teacher_id, t.name,t.graduation_date,t. created_at";

        $cri->join = "left outer join cv16_users u on u.id = i.user_id

                      left outer join cv16_tutor_groups t on t.institution_id = i.id";

        $cri->condition="i.institution_id =".$_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }


     ///   /    get student
     public function actiongetclass() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = TutorGroups::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias ='i';
        $cri->select ="i.name,i.account_limit, t.forenames,i.graduation_date,i. created_at";

        $cri->join = "left outer join cv16_users t on t.id = i.teacher_id";

        $cri->condition="i.institution_id =".$_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }



      ///   /    fetch class
      public function actionfetchclass() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = TutorGroups::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
      
        $cri->select ="id,name";
        $cri->condition="institution_id =".$_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }



     ///   /    fetch student
     public function actionfetchstudent() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = TutorGroupsStudents::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
      
        $cri->alias ='i';
        $cri->select ="t.forenames,t.id";

        $cri->join = "left outer join cv16_users t on t.id = i.student_id";

        $cri->condition="i.institution_id =".$_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }



     ///   /    fetch teacher
     public function actionfetchteacher() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = TutorGroupsTeachers::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
      
        $cri->alias ='i';
        $cri->select ="t.forenames,t.id";

        $cri->join = "left outer join cv16_users t on t.id = i.teacher_id";

        $cri->condition="i.institution_id =".$_GET['id'];
       
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
     

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }



    /*Employer inbox*/
//    public function actionsendMessage() {
//        if (!isset($_GET['id']) || $_GET['id'] == 0)
//            $this->_sendResponse(400, array());
//        $id = $_GET['id'];
//
//        if (json_decode(file_get_contents("php://input"), true)) {
//            $_POST = json_decode(file_get_contents("php://input"), true);
//        }
//
//        $now = new DateTime();
//        $conmodel = new Conversations();
//        $conmodel->subject = $_POST['subject'];
//        $conmodel->created_at = $now->format('Y-m-d H:i:s');
//        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
//        if ($conmodel->save()) {
//            $usermodel = Users::model()->findByPk($id);
//            $conmsgmodel = new ConversationMessages();
//            $conmsgmodel->conversation_id = $conmodel->id;
//            $conmsgmodel->user_id = $usermodel->id;
//            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
//            $conmsgmodel->message = $_POST['message'];
//            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
//            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
//            if ($conmsgmodel->save()) {
//                $conmemmodel_sender = new ConversationMembers();
//                $conmemmodel_sender->conversation_id = $conmodel->id;
//                $conmemmodel_sender->user_id = $usermodel->id;
//                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
//                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
//                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
//                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
//                if ($conmemmodel_sender->save()) {
//                    $usermodel1 = Users::model()->findByPk($_POST['user_id']);
//                    $conmemmodel_rec = new ConversationMembers();
//                    $conmemmodel_rec->conversation_id = $conmodel->id;
//                    $conmemmodel_rec->user_id = $_POST['user_id'];
//                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
//                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
//                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
//                    if ($conmemmodel_rec->save()) {
//                        $actmodel_sender = new Activities();
//                        $actmodel_sender->user_id = $usermodel->id;
//                        $actmodel_sender->type = "message";
//                        $actmodel_sender->model_id = $conmsgmodel->id;
//                        $actmodel_sender->model_type = "ConversationMessage";
//                        $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
//                        $actmodel_sender->message = $_POST['message'];
//                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
//                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
//                        if ($actmodel_sender->save()) {
//                            $actmodel_rec = new Activities();
//                            $actmodel_rec->user_id = $_POST['user_id'];
//                            $actmodel_rec->type = "message";
//                            $actmodel_rec->model_id = $conmsgmodel->id;
//                            $actmodel_rec->model_type = "ConversationMessage";
//                            $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
//                            $actmodel_rec->message = $_POST['message'];
//                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
//                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
//                            if ($actmodel_rec->save()) {
//                                $this->_sendResponse(200, array());
//                            } else {
//                                $this->_sendResponse(507, array());
//                            }
//                        }
//                    }
//                }
//            }
//        } else {
//            $this->_sendResponse(507, array());
//        }
//    }
     /*Employer inbox*/


//  ///   /    get message
//  public function actiongetmessage() {
//    if (!isset($_GET['id']) || $_GET['id'] == 0)
//        $this->_sendResponse(404, array());
//
//    $model = ConversationMessages::model()->findByAttributes(array('user_id' =>  $_GET['id']));
//   $cri = new CDbCriteria();
//   // $cri->alias ='i';
//        $cri->select ="*";
//
//      //  $cri->join = "left outer join cv16_users t on t.id = i.teacher_id";
//
//     
//        $cri->condition = "user_id =".$_GET['id'];
//
//   
//    $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
//
//    if (empty($users)) {
//        $this->_sendResponse(404, array());
//    } else {
//        $this->_sendResponse(200, $users);
//    }
//}



 ///   /    get counts
 public function actiongetcounts() {
    if (!isset($_GET['id']) || $_GET['id'] == 0)
        $this->_sendResponse(404, array());
        $id = $_GET['id'];
   $model =  Institutions::model()->findByPk($id);
   $cri = new CDbCriteria();
        $cri->alias ='c';
        $cri->select ="c.num_pupils,(SELECT count(*) FROM cv16_tutor_groups_students WHERE institution_id = c.id) as num_count";
        $cri->condition = "c.id =".$_GET['id'];
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

    if (empty($users)) {
        $this->_sendResponse(404, array());
    } else {
        $this->_sendResponse(200, $users);
    }
//     $modelt =  TutorGroupsStudents::model();
//     $cri->select ="institution_id";
//     $cri->condition = "institution_id =".$_GET['id'];
//     $users = $modelt->getCommandBuilder()->createFindCommand($modelt->tableSchema, $cri)->queryAll();
//    count($modelt);
}

///update school
public function actionupdateschool() {
    $id = isset($_GET['id']) ? $_GET['id'] : null;
    $userhobmodel = Institutions::model()->findByPk($id);
    if ($id != $userhobmodel->id)
        $this->_sendResponse(404, array());

    if (json_decode(file_get_contents("php://input"), true)) {
        $put_vars = json_decode(file_get_contents("php://input"), true);
    } else {
        $put_vars = $_POST;
    }

    $now = new DateTime();

    foreach ($put_vars as $var => $value) {
        if ($userhobmodel->hasAttribute($var))
            $userhobmodel->$var = $value;
        else
            $this->_sendResponse(500, array());
    }
    $userhobmodel->updated_at = $now->format('Y-m-d H:i:s');
    if ($userhobmodel->save()) {
     
            $this->_sendResponsewithMessage(200, sprintf("Successfully  updated", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    
}


public function actioncreaterecruitment() {
    if (json_decode(file_get_contents("php://input"), true)) {
        $_POST = json_decode(file_get_contents("php://input"), true);
    }
    $usermodel = new Users();
    $profilemodel = new Profiles();
    $addressmodel = new Addresses();
    $agencymodel = new Agency();
    $now = new DateTime();

    foreach ($_POST as $var => $value) {
        if ($usermodel->hasAttribute($var)) {
            if ($var == "email") {
                $mmodel = Users::model()->findByAttributes(array('email' => $value));
                if ($mmodel != null) {
                    $this->_sendResponse(502, array());
                    die();
                }
            }
            $usermodel->$var = $value;
        } else if ($profilemodel->hasAttribute($var)) {
            $profilemodel->$var = $value;
        } else if ($addressmodel->hasAttribute($var)) {
            $addressmodel->$var = $value;
        }
        else if ($agencymodel->hasAttribute($var)) {
            $agencymodel->$var = $value;
        }
         else if ($var == 'ispremium') {
            // $this->_sendResponse(400, array());
        } else {
            $this->_sendResponse(500, array());
        }
    }

    $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
    if (isset($_POST['latitude']) && $_POST['latitude'])
        $usermodel->location_lat = $_POST['latitude'];
    if (isset($_POST['longitude']) && $_POST['longitude'])
        $usermodel->location_lng = $_POST['longitude'];
    // Check if using free trial then set membership trial date
    if (isset($_POST['ispremium']) && $_POST['ispremium'])
        $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
    $usermodel->type = 'Agency';
    $usermodel->created_at = $now->format('Y-m-d H:i:s');
    $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
    if ($usermodel->save()) 
    {
        $profilemodel->owner_type = "Agency";
        $profilemodel->owner_id = $usermodel->id;
        $profilemodel->created_at = $usermodel->created_at;
        $profilemodel->type = $usermodel->type;
        if ($profilemodel->save()) {
            $addressmodel->model_id = $usermodel->id;
            $addressmodel->model_type = "Agency";
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($addressmodel->save()) {
                $agencymodel->user_id = $usermodel->id; 
                $agencymodel->email =  $_POST['email'];            
                $agencymodel->created_at = $now->format('Y-m-d H:i:s');
             
                    if ($agencymodel->save()) {
                        $cri = new CDbCriteria();
                        $cri->select = 'id,forenames,surname,email';
                        $cri->condition = "id = " . $usermodel->id;
                        $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                        $this->_sendResponse(200, $users);
                       
                    }
                    else {
                        echo var_dump($agencymodel->getErrors());
                    } 
                }
                else {
                    echo var_dump($addressmodel->getErrors());
                } 
                    
                }
                else {
                    echo var_dump($profilemodel->getErrors());
                } 
             }
       
    else {
        $this->_sendResponse(500, array());
    }

}



public function actioneditrecruitment() {
    //           if (!$this->_checkAuth())
    //            $this->_sendResponse(405, array());
            $id = isset($_GET['id']) ? $_GET['id'] : null;
            $insmodel = Agency::model()->findByPk($id);
            if ($id != $insmodel->id)
                $this->_sendResponse(404, array());
    
            if (json_decode(file_get_contents("php://input"), true)) {
                $put_vars = json_decode(file_get_contents("php://input"), true);
            } else {
                $put_vars = $_POST;
            }
    
          //  $pwd = $usermodel->password;
    
            $now = new DateTime();
    
            $usermodel = Users::model()->findByPk($insmodel->user_id);
            $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $insmodel->user_id));
            $profilemodel = Profiles::model()->findByPk($pmodel->id);
            $addressmodel = Addresses::model()->findByAttributes(array('model_id' => $insmodel->user_id));
    
            foreach ($put_vars as $var => $value) {
                if ($usermodel->hasAttribute($var)) {
                    if ($var == "password" && $value == "") {
                        $usermodel->password = password_hash($value, 1, ['cost' => 10]);
                    } else if ($var == "password" && $value != "") {
                        
                    } else {
                        $usermodel->$var = $value;
                    }
                } else if ($insmodel->hasAttribute($var)) {
                    $insmodel->$var = $value;
                } else if ($profilemodel->hasAttribute($var)) {
                    $profilemodel->$var = $value;
                }
                
                else if ($insmodel->hasAttribute($var)) {
                    $insmodel->$var = $value;
                }
                else if ($addressmodel->hasAttribute($var)) {
                    $addressmodel->$var = $value;
                }
                else {

                    $this->_sendResponse(400, array());
                }
            }
    
            $insmodel->updated_at = $now->format('Y-m-d H:i:s');
            $insmodel->save();
            $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
            $addressmodel->save();
    
            $usermodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($usermodel->save()) {
                $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
                if ($profilemodel->save()) {
                    $this->_sendResponsewithMessage(200, sprintf("Successfully updated account", ""));
                } else {
                    $this->_sendResponse(500, array());
                }
            }
        }



 ///   /    get recruitmentagency
 public function actiongetrecruitmentagency() {
    if (!isset($_GET['id']) || $_GET['id'] == 0)
        $this->_sendResponse(404, array());

    $model = Agency::model()->findByAttributes(array('user_id' =>$_GET['id']));
    $cri = new CDbCriteria();
    $cri->alias = 'i';
    $cri->select = "i.id as agency_id, i.name ,m.file_name,CONCAT('http://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('http://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('http://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('http://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.id as user_id,u.stripe_active,u.current_job,u.type,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
    $cri->join = "left outer join cv16_profiles p on p.owner_id = i.user_id
                  left outer join cv16_users u on u.id = i.user_id
                  left outer join cv16_media m on m.id = p.photo_id
                  left outer join cv16_addresses a on a.model_id = i.user_id";


    $cri->condition = "i.user_id = " .$_GET['id'] . "";

    $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

    if (empty($users)) {
        $this->_sendResponse(404, array());
    } else {
        $this->_sendResponse(200, $users);
    }
}



/// create recruitment candidate

public function actioncreatereccandidate() {
    if (json_decode(file_get_contents("php://input"), true)) {
        $_POST = json_decode(file_get_contents("php://input"), true);
    }
    $usermodel = new Users();
    $profilemodel = new Profiles();
    $addressmodel = new Addresses();
    $agencymodel = new AgencyCandidates();
    $now = new DateTime();
    foreach ($_POST as $var => $value) {
        if ($usermodel->hasAttribute($var)) {
            if ($var == "email") {
                $mmodel = Users::model()->findByAttributes(array('email' => $value));
                if ($mmodel != null) {
                    $this->_sendResponse(502, array());
                    die();
                }
            }
            $usermodel->$var = $value;
        } else if ($addressmodel->hasAttribute($var)) {
            $addressmodel->$var = $value;
        } else if ($profilemodel->hasAttribute($var)) {
            $profilemodel->$var = $value;
        }else if ($agencymodel->hasAttribute($var)) {
            $agencymodel->$var = $value;
        } else if ($var == 'ispremium') {
            //  $this->_sendResponse(400, array());
        } else {
            $this->_sendResponse(500, array());
        }
    }
    $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
    if (isset($_POST['latitude']) && $_POST['latitude'])
        $usermodel->location_lat = $_POST['latitude'];
    if (isset($_POST['longitude']) && $_POST['longitude'])
        $usermodel->location_lng = $_POST['longitude'];
    // Check if using free trial then set membership trial date
    if (isset($_POST['ispremium']) && $_POST['ispremium'])
        $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
        $usermodel->type = 'candidate';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
    if ($usermodel->save()) {
            $addressmodel->model_id = $usermodel->id;
            $addressmodel->model_type = "Candidate";
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($addressmodel->save()) {
                $profilemodel->owner_type = "Candidate";
                $profilemodel->owner_id = $usermodel->id;
                $profilemodel->slug = $usermodel->forenames.''.$usermodel->surname;
                $profilemodel->created_at = $usermodel->created_at;
                $profilemodel->type = $usermodel->type;
            if ($profilemodel->save()) {
               // $mmodel = AgencyCandidates::model()->findByAttributes(array('user_id' => $value));
                $agencymodel->user_id = $usermodel->id;
              //  $agencymodel->agency_id = $usermodel->id;
                $agencymodel->created_at = $now->format('Y-m-d H:i:s');
             
                 if ($agencymodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,forenames,surname,email,mobile';
                $cri->condition = "id = " . $usermodel->id;
                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $users);
            }
            else {
                echo var_dump( $agencymodel->getErrors());
            }
        }
        else {
            $this->_sendResponsewithMessage(200, sprintf("choose different name", ""));
        }
    }
    else {
        echo $addressmodel->getErrors();
    }
 }

 else {
          echo var_dump($usermodel->getErrors());
        $this->_sendResponse(500, array());
    }
}




///   /    get recruitment candidate
public function actiongetreccandidate() {
    if (!isset($_GET['id']) || $_GET['id'] == 0)
        $this->_sendResponse(404, array());

    $model = Agency::model()->findByPk($_GET['id']);
    $cri = new CDbCriteria();
    $cri->alias ='i';
    $cri->select ="c.id  as candidate_id,u.id,u.forenames, u.surname, u.email,u.current_job";

    $cri->join = "left outer join cv16_agency_candidates c on c.agency_id = i.id
                  left outer join cv16_users u on u.id = c.user_id";

    $cri->condition="i.id =".$_GET['id'];
    // echo var_dump($cri);
    // die();

    $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

    if (empty($users)) {
        $this->_sendResponse(404, array());
    } else {
        $this->_sendResponse(200, $users);
    }
}


 /* delete rec candidate */

 public function actiondeletereccandidate() {
    if (!isset($_GET['id']) || $_GET['id'] == 0)
        $this->_sendResponse(404, array());
    $id = $_GET['id'];
    $deletedraftmsg = AgencyCandidates::model();
    $cri = new CDbCriteria();
    $cri->condition = "user_id = " . $_GET['id'];
    AgencyCandidates::model()->deleteAll($cri);
    $deletedraftmsg = Users::model();
    $cri = new CDbCriteria();
    $cri->condition = "id = " . $_GET['id'];
    Users::model()->deleteAll($cri);
    $this->_sendResponse(200, "Deleted Successfully");
}


///   /    fetch  recruitment staff
public function actionfetchrecstaff() {
    if (!isset($_GET['id']) || $_GET['id'] == 0)
        $this->_sendResponse(404, array());

    $model = AgencyStaff::model()->findByAttributes(array('agency_id' => $_GET['id']));
    $cri = new CDbCriteria();
    $cri->alias ='i';
    $cri->select ="i.user_id,u.forenames, u.surname, u.email,u.current_job";

    $cri->join = "left outer join cv16_users u on u.id = i.user_id";

    $cri->condition="i.agency_id =".$_GET['id'];
    // echo var_dump($cri);
    // die();

    $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

    if (empty($users)) {
        $this->_sendResponse(404, array());
    } else {
        $this->_sendResponse(200, $users);
    }
}




/// create assign staff 

public function actionassignstaff() {


    $id = isset($_GET['id']) ? $_GET['id'] : null;
    $candimodel = AgencyCandidates::model()->findByPk($id);
    if ($id != $candimodel->id)
        $this->_sendResponse(404, array());

    if (json_decode(file_get_contents("php://input"), true)) {
        $_POST = json_decode(file_get_contents("php://input"), true);
    }
    
    $staffcanmodel =AgencyStaffCandidates::model()->findByAttributes(array('agency_candidate_id'=>$_GET['id']));
    
    if($staffcanmodel==null){
        $staffcanmodel = new AgencyStaffCandidates();
       // $staffcanmodel->userid = $_GET['id'];   
       }

    $now = new DateTime();
    foreach ($_POST as $var => $value) {
        if ($staffcanmodel->hasAttribute($var)) {
            // if ($var == "email") {
            //     $mmodel = Users::model()->findByAttributes(array('email' => $value));
            //     if ($mmodel != null) {
            //         $this->_sendResponse(502, array());
            //         die();
            //     }
            // }
            $staffcanmodel->$var = $value;
        } else {
            $this->_sendResponse(500, array());
        }
    }

            $staffcanmodel->agency_id = $candimodel->agency_id;
            $staffcanmodel->agency_candidate_id = $candimodel->id;
            $staffcanmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($staffcanmodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,staff_id';
                $cri->condition = "agency_candidate_id = " . $candimodel->id;
                $users = $staffcanmodel->getCommandBuilder()->createFindCommand($staffcanmodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $users);
            }
          

 else {
         // echo var_dump($usermodel->getErrors());
        $this->_sendResponse(500, array());
    }
}





///   /    get recruitment staff
public function actiongetstaff() {
    if (!isset($_GET['id']) || $_GET['id'] == 0)
        $this->_sendResponse(404, array());

    $model = Agency::model()->findByPk($_GET['id']);
    $cri = new CDbCriteria();
    $cri->alias ='i';
    $cri->select ="s.id  as staff_id,u.id,u.forenames, u.surname, u.email,
    (SELECT count(*) FROM cv16_agency_staff_candidates WHERE staff_id = s.user_id) as num_count";

    $cri->join = "left outer join cv16_agency_staff s on s.agency_id = i.id
                  left outer join cv16_users u on u.id = s.user_id";

    $cri->condition="i.id =".$_GET['id'];
    // echo var_dump($cri);
    // die();

    $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

    if (empty($users)) {
        $this->_sendResponse(404, array());
    } else {
        $this->_sendResponse(200, $users);
    }
}



/// create recruitment staff

public function actioncreaterecstaff() {
    if (json_decode(file_get_contents("php://input"), true)) {
        $_POST = json_decode(file_get_contents("php://input"), true);
    }
    $usermodel = new Users();
    $profilemodel = new Profiles();
    $addressmodel = new Addresses();
    $agencymodel = new AgencyStaff();
    $now = new DateTime();
    foreach ($_POST as $var => $value) {
        if ($usermodel->hasAttribute($var)) {
            if ($var == "email") {
                $mmodel = Users::model()->findByAttributes(array('email' => $value));
                if ($mmodel != null) {
                    $this->_sendResponse(502, array());
                    die();
                }
            }
            $usermodel->$var = $value;
        } else if ($addressmodel->hasAttribute($var)) {
            $addressmodel->$var = $value;
        } else if ($profilemodel->hasAttribute($var)) {
            $profilemodel->$var = $value;
        }else if ($agencymodel->hasAttribute($var)) {
            $agencymodel->$var = $value;
        } else if ($var == 'ispremium') {
            //  $this->_sendResponse(400, array());
        } else {
            $this->_sendResponse(500, array());
        }
    }
    $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
    if (isset($_POST['latitude']) && $_POST['latitude'])
        $usermodel->location_lat = $_POST['latitude'];
    if (isset($_POST['longitude']) && $_POST['longitude'])
        $usermodel->location_lng = $_POST['longitude'];
    // Check if using free trial then set membership trial date
    if (isset($_POST['ispremium']) && $_POST['ispremium'])
        $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
        $usermodel->type = 'candidate';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
    if ($usermodel->save()) {
            $addressmodel->model_id = $usermodel->id;
            $addressmodel->model_type = "Candidate";
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($addressmodel->save()) {
                $profilemodel->owner_type = "Candidate";
                $profilemodel->owner_id = $usermodel->id;
                $profilemodel->slug = $usermodel->forenames.''.$usermodel->surname;
                $profilemodel->created_at = $usermodel->created_at;
                $profilemodel->type = $usermodel->type;
            if ($profilemodel->save()) {
               // $mmodel = AgencyCandidates::model()->findByAttributes(array('user_id' => $value));
                $agencymodel->user_id = $usermodel->id;
              //  $agencymodel->agency_id = $usermodel->id;
                $agencymodel->created_at = $now->format('Y-m-d H:i:s');
             
                 if ($agencymodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,forenames,surname,email,mobile';
                $cri->condition = "id = " . $usermodel->id;
                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $users);
            }
            else {
                echo var_dump( $agencymodel->getErrors());
            }
        }
        else {
            $this->_sendResponsewithMessage(200, sprintf("choose different name", ""));
        }
    }
    else {
        echo $addressmodel->getErrors();
    }
 }

 else {
          echo var_dump($usermodel->getErrors());
        $this->_sendResponse(500, array());
    }
}








    // end ilai API //


    
  
    



}

?>