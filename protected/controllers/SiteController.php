<?php
class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public $layout='//layouts/user';
    public $pagename ='';
    public $is_premiun = 0;
    
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
	    $this->actionHome();
		//$this->actionLogin();
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;
		$this->layout = "user";
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
			{
			    if(Yii::app()->user->getState('role') == "admin")
			       $this->redirect($this->createUrl('users/dashboard'));
			    else if(Yii::app()->user->getState('role') == "candidate")			        
			        $this->redirect($this->createUrl('users/edit',array('id'=>Yii::app()->user->getState('userid'))));
			    else if( Yii::app()->user->getState('role') == "employer")
			    {
			        $empmodel = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
			        $this->redirect($this->createUrl('employers/employer',array('id'=>$empmodel->id)));
			    }
			    else if(Yii::app()->user->getState('role') == "institution_admin")
			        $this->redirect($this->createUrl('institutions/edit',array('id'=>Yii::app()->user->getState('userid'))));
			}
				//$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		$this->layout = "user";
		Yii::app()->user->logout();
		$this->redirect(yii::app()->createurl("site/index"));
		//$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionHome()
	{	    
	    $model = Slideshows::model()->findByAttributes(array('slug'=>'home'));
	    $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id'=>$model->id,'deleted_at'=>null));
	    $this->render('index',array('model'=>$model,'slidemodel'=>$slidemodel));
	}
	public function actionabout()
	{
	    $model = Slideshows::model()->findByAttributes(array('slug'=>'about-us'));
	    $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id'=>$model->id,'deleted_at'=>null));
	    $this->render('about',array('model'=>$model,'slidemodel'=>$slidemodel));
	}
	public function actiontoptips()
	{
	    $model = Slideshows::model()->findByAttributes(array('slug'=>'top-tips'));
	    $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id'=>$model->id,'deleted_at'=>null));
	    $this->render('toptips',array('model'=>$model,'slidemodel'=>$slidemodel));
	}
	public function actionhowto()
	{
	    $model = VideoCategories::model()->findByAttributes(array('slug'=>'how-to'));
	    $videomodel = Videos::model()->findAllByAttributes(array('category_id'=>$model->id,'deleted_at'=>null));
	    $this->render('howto',array('model'=>$model,'videomodel'=>$videomodel));
	}
	public function actionworrylist()
	{
	    $model = Slideshows::model()->findByAttributes(array('slug'=>'worry-list'));
	    $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id'=>$model->id,'deleted_at'=>null));
	    $this->render('worrylist',array('model'=>$model,'slidemodel'=>$slidemodel));
	}
	public function actionvideoshowcase()
	{
	    $model = VideoCategories::model()->findByAttributes(array('slug'=>'success-stories'));
	    $videomodel = Videos::model()->findAllByAttributes(array('category_id'=>$model->id,'deleted_at'=>null));
	    $this->render('videoshowcase',array('model'=>$model,'videomodel'=>$videomodel));
	}
	public function actionjobsearch()
	{
	    $model = Jobs::model();
	    $this->render('jobsearch',array('model'=>$model));
	}
	public function actiongetSkills()
	{
	    $skillmodel = Skills::model()->findAllByAttributes(array('skill_category_id'=>$_POST['id']));
	    foreach ($skillmodel as $skill) {
	        echo "<option  value='$skill[id]'>$skill[name]</option>";
	    }
	}
	
	public function actionsearchJobs()
	{
	    $skillcategid = $_GET['skill_category'];
	    $skillarr = isset($_GET['skills']) ? $_GET['skills'] : array();
	    $location = $_GET['location'];
	    $lat = $_GET['latitude'];
	    $long = $_GET['longitude'];
	    $radius = $_GET['distance'];
	    
	    $skillids = implode(',', $skillarr);
	    
	    $cri = new CDbCriteria();
	    $cri->alias = "j";
	    $cri->join = "inner join cv16_job_skill js on js.job_id = j.id
                      inner join cv16_skills s on s.id = js.skill_id
                      inner join cv16_skill_categories sc on sc.id = j.skill_category_id";

	    $cri->addCondition("j.skill_category_id =".$skillcategid);
	    if($skillids != "")
	        $cri->addCondition('js.skill_id in ('.$skillids.')','AND');
	    if($lat != "")
	    {
	        if($radius > 0)
	        {
	           $cri->select = "(6371 * acos( cos( radians(".$lat.") ) * cos( radians(".$lat.") ) * 
                                cos( radians(lng) - radians(".$long.")) + sin(radians(".$lat.")) *
                                sin(radians(lat)) )) as distance";    
	           $cri->addCondition("HAVING distance < ".$radius);
	        }
	        else 
	        {
	            $cri->addCondition("j.location = ".$location);
	        }	        
	     }
	     $cri->group = "j.id";
	    $jobmodel = Jobs::model()->findAll($cri);
	    
	    $this->render('jobsearchresults',array('model'=>$jobmodel));
	}
	    
	public function actioncontact()
	{
	    $this->render('contact');
	}
	public function actioncandidatesearch()
	{
	    $model = Jobs::model();
	    $this->render('candidatesearch',array('model'=>$model));
	}
	public function actionsearchCandidates()
	{
	    $skillcategid = $_GET['skill_category'];
	    $skillarr = isset($_GET['skills']) ? $_GET['skills'] : array();
	    $location = $_GET['location'];
	    $lat = $_GET['latitude'];
	    $long = $_GET['longitude'];
	    
	    
	    $skillids = implode(',', $skillarr);
	    
	    $cri = new CDbCriteria();
	    $cri->alias = "p";
	    $cri->distinct = true;
	    $cri->join = "inner join cv16_users u on u.id = p.owner_id
                      inner join cv16_profile_skills ps on ps.profile_id = p.id
                      inner join cv16_skills s on s.id = ps.skill_id
                      inner join cv16_skill_categories sc on sc.id = s.skill_category_id";
	    
	    $cri->addCondition("s.skill_category_id =".$skillcategid);
	    if($skillids != "")
	       $cri->addCondition('ps.skill_id in ('.$skillids.')','AND');
	    if($lat != "")
	    {
	        if($radius > 0)
	        {
	            $cri->select = "(6371 * acos( cos( radians(".$lat.") ) * cos( radians(".$lat.") ) *
                                cos( radians(lng) - radians(".$long.")) + sin(radians(".$lat.")) *
                                sin(radians(lat)) )) as distance";
	            $cri->addCondition("HAVING distance < ".$radius);
	        }
	        else
	        {
	            $cri->addCondition("u.location = '".$location."'");
	        }	        
	    }
	    $cri->group = "p.id";
	    $pmodel = Profiles::model()->findAll($cri);
	    
	    $this->render('candidatesearchresults',array('model'=>$pmodel));
	}
	
	public function actionaddfavourite()
	{
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>Yii::app()->user->getState('userid')));
	    $pfmodel = ProfileFavourites::model()->findByAttributes(array('profile_id'=>$pmodel->id,'favourite_id'=>$_POST['fid']));
	    if($pfmodel == null)
	    {
	        $pfmodel = new ProfileFavourites();
	        $pfmodel->profile_id = $pmodel->id;
	        $pfmodel->favourite_id = $_POST['fid'];
	        $pfmodel->save();
	    }
	}
	
	public function actionsendMessage()
	{
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $_POST['subject'];
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if($conmodel->save())
	    {
	        $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
	        $conmsgmodel = new ConversationMessages();
	        $conmsgmodel->conversation_id = $conmodel->id;
	        $conmsgmodel->user_id = Yii::app()->user->getState('userid');
	        $conmsgmodel->name = $usermodel->forenames." ".$usermodel->surname;
	        $conmsgmodel->message = $_POST['message'];
	        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        if($conmsgmodel->save())
	        {
	            $conmemmodel_sender = new ConversationMembers();
	            $conmemmodel_sender->conversation_id = $conmodel->id;
	            $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
	            $conmemmodel_sender->name = $usermodel->forenames." ".$usermodel->surname;
	            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	            if($conmemmodel_sender->save())
	            {
	                $conmemmodel_rec = new ConversationMembers();
	                $conmemmodel_rec->conversation_id = $conmodel->id;
	                $conmemmodel_rec->user_id = $_POST['user_id'];
	                $conmemmodel_rec->name = $usermodel->forenames." ".$usermodel->surname;
	                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                if($conmemmodel_rec->save())
	                {
	                    $actmodel_sender = new Activities();
	                    $actmodel_sender->user_id = Yii::app()->user->getState('userid');
	                    $actmodel_sender->type = "message";
	                    $actmodel_sender->model_id = $conmsgmodel->id;
	                    $actmodel_sender->model_type = "ConversationMessage";
	                    $actmodel_sender->subject = "You sent a message in conversation: ".$_POST['subject'];
	                    $actmodel_sender->message = $_POST['message'];
	                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	                    if($actmodel_sender->save())
	                    {
	                        $actmodel_rec = new Activities();
	                        $actmodel_rec->user_id = $_POST['user_id'];
	                        $actmodel_rec->type = "message";
	                        $actmodel_rec->model_id = $conmsgmodel->id;
	                        $actmodel_rec->model_type = "ConversationMessage";
	                        $actmodel_rec->subject = "You received a message in conversation: ".$_POST['subject'];
	                        $actmodel_rec->message = $_POST['message'];
	                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->save();
	                    }
	                }
	                
	            }
	            
	        }
	    }
	}
	
	//        /ezhil coding
	public function actioncandidatememberships() {
	    $model = new Users();
	    $this->render('candidatememberships', array('model' => $model));
	}
	
	public function actionemployermemberships() {
	    $model = new Employers();
	    $this->render('employermemberships', array('model' => $model));
	}
	
	public function actionbasicregisteremployer() {
	    $model = new Employers();
	    $this->is_premiun = 0;
	    $this->render('registeremployerbasic', array('model' => $model));
	}
	
	public function actionpremiumregisteremployer() {
	    $model = new Employers();
	    $this->is_premiun = 1;
	    $this->render('registeremployerpremium', array('model' => $model));
	}
	
	public function actionbasicregistercandidate() {
	    $model = new Users();
	    $this->is_premiun = 0;
	    $this->render('registercandidatebasic', array('model' => $model));
	}
	
	public function actionpremiumregistercandidate() {
	    $model = new Users();
	    $this->is_premiun = 1;
	    $this->render('registercandidatepremium', array('model' => $model));
	}
	
	public function actionregisterinstitution() {
	    $model = new Institutions();
	    $this->render('registerinstitution', array('model' => $model));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioncreateEmployerDetails() {
	    
	    $model = new Employers;
	    $empusermodel = new EmployerUsers;
	    $usermodel = new Users;
	    $profilemodel = new Profiles;
	    $addressmodel = new Addresses;
	    
	    $now = new DateTime();
	    
	    if (isset($_POST['Employers'])) {
	        
	        //                    insert user details
	        $usermodel->attributes = $_POST["Users"];
	        $usermodel->created_at = $now->format('Y-m-d H:i:s');
	        $usermodel->type = 'employer';
	        $usermodel->location_lat = $_POST['Addresses']['latitude'];
	        $usermodel->location_lng = $_POST['Addresses']['longitude'];
	        $usermodel->is_premium = $_POST["is_premium"];
	        $usermodel->tel = $_POST['Employers']['tel'];
	        
	        if ($usermodel->save()) {
	            //                    insert employer details
	            $model->attributes = $_POST['Employers'];
	            $model->email = $usermodel->email;
	            $model->created_at = $now->format('Y-m-d H:i:s');
	            $model->user_id = $usermodel->id;
	            $model->body = $_POST['body'];
	            if ($model->save()) {
	                //employer user update
	                $empusermodel->employer_id = $model->id;
	                $empusermodel->user_id = $usermodel->id;
	                $empusermodel->role = Yii::app()->user->getState('role');
	                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
	                $empusermodel->save();
	                //address update
	                $addressmodel->attributes = $_POST["Addresses"];
	                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                $addressmodel->model_type = 'employer';
	                $addressmodel->model_id = $model->id;
	                if ($addressmodel->save()) {
	                    $profilemodel->attributes = $_POST["Profiles"];
	                    $profilemodel->owner_id = $model->id;
	                    $profilemodel->owner_type = 'employer';
	                    $profilemodel->type = 'employer';
	                    $profilemodel->slug = $_POST["Profiles"]['slug'];
	                    $profilemodel->save();
	                    
	                    $umodel = Users::model()->findByPk($model->user_id);
	                    
	                    $logmodel = new LoginForm;
	                    
	                    $logmodel->username = $umodel->email;
	                    $logmodel->password = $umodel->password;
	                    
	                    if ($logmodel->validate() && $logmodel->login()) {
	                        $this->redirect($this->createUrl('employers/edit', array('id' => $model->id)));
	                    }
	                }
	            }
	        }
	    }
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioncreateCandidateDetails() {
	    
	    $model = new Users();
	    $profilemodel = new Profiles();
	    $addressmodel = new Addresses();
	    
	    // Uncomment the following line if AJAX validation is needed
	    // $this->performAjaxValidation($model);
	    
	    if (isset($_POST['Users'])) {
	        $now = new DateTime();
	        $model->attributes = $_POST['Users'];
	        $dobformat = date_format(new DateTime($_POST["dob"]), "Y-m-d");
	        $model->dob = $dobformat;
	        $model->is_premium = $_POST["is_premium"];
	        $model->location_lat = $_POST['Addresses']['latitude'];
	        $model->location_lng = $_POST['Addresses']['longitude'];
	        $model->type = 'candidate';
	        $model->created_at = $now->format('Y-m-d H:i:s');
	        
	        if ($model->save()) {
	            $profilemodel->attributes = $_POST['Profiles'];
	            $profilemodel->owner_type = "Candidate";
	            $profilemodel->owner_id = $model->id;
	            // $profilemodel->photo_id = $_POST['photo_id'];
	            $profilemodel->created_at = $model->created_at;
	            $profilemodel->type = $model->type;
	            //$profilemodel->slug = lcfirst($model->forenames);
	            if ($profilemodel->save()) {
	                if ($_POST['Addresses']['address'] != "" || $_POST['Addresses']['postcode'] != "") {
	                    $addressmodel->attributes = $_POST['Addresses'];
	                    $addressmodel->model_id = $model->id;
	                    $addressmodel->model_type = "Candidate";
	                    $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $addressmodel->save();
	                }
	                
	                $umodel = Users::model()->findByPk($model->id);
	                
	                $logmodel = new LoginForm;
	                
	                $logmodel->username = $umodel->email;
	                $logmodel->password = $umodel->password;
	                
	                if ($logmodel->validate() && $logmodel->login()) {
	                    $this->redirect($this->createUrl('users/edit', array('id' => $model->id)));
	                }
	                
	                // $this->redirect($this->createUrl('users/admin', array('trial' => $trial, 'paid' => $paid, 'basic' => $basic)));
	            }
	        }
	    }
	}
	
	public function actioncreateInstitutions() {
	    
	    $now = new DateTime();
	    $usermodel = new Users();
	    $usermodel->attributes = $_POST['Users'];
	    $usermodel->password = '123456';
	    $usermodel->status = 'pending';
	    $usermodel->location_lat = $_POST['Addresses']['latitude'];
	    $usermodel->location_lng = $_POST['Addresses']['longitude'];
	    $usermodel->type = 'institution_admin';
	    $usermodel->created_at = $now->format('Y-m-d H:i:s');
	    if ($usermodel->save()) {
	        $model = new Institutions();
	        $model->attributes = $_POST['Institutions'];
	        $model->admin_id = $usermodel->id;
	        $model->email = $usermodel->email;
	        $model->status = 'pending';
	        $model->created_at = $now->format('Y-m-d H:i:s');
	        if ($model->save()) {
	            $iusermodel = new InstitutionUsers();
	            $iusermodel->institution_id = $model->id;
	            $iusermodel->user_id = $usermodel->id;
	            $iusermodel->role = "admin";
	            $iusermodel->created_at = $now->format('Y-m-d H:i:s');
	            if ($iusermodel->save()) {
	                $addressmodel = new Addresses();
	                $addressmodel->attributes = $_POST['Addresses'];
	                $addressmodel->model_id = $model->id;
	                $addressmodel->model_type = "Institutions";
	                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                $addressmodel->save();
	                
	                $this->redirect($this->createUrl('site/login'));
	            }
	        }
	    }
	    //        else{
	    //            echo var_dump($usermodel->errors);
	    //        }
	}
	
	public function actionloadVideo() {	    
	    $videoid = $_POST['videoid'];
	    echo '<iframe src="//player.vimeo.com/video/' . $videoid . '" width="100%" height="500px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
	}
}