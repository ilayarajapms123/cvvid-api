<?php
use Stripe\Charge;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Subscription;

header("Access-Control-Allow-Origin: *");

//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}"); 
class ApiController extends Controller {

// Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */
    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';

    /**
     * @return array action filters
     */
    public function filters() {
        return array();
    }

//        Login api




    public function actionlogin() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $email = $_POST['email'];
        $password = $_POST['password'];

        $cri = new CDbCriteria();
        $cri->condition = "email = '" . $email . "'";

        $model = Users::model();
        
        $usrarr = $model->find($cri);

         
        $cri1 = new CDbCriteria();
        $cri1->alias = 'i';
        $cri1->select = "i.name,i.id as institution_id,u.id as user_id,u.forenames,u.type,u.surname,u.email,u.stripe_active,u.current_job,u.location";
        $cri1->join = "left outer join cv16_users u on u.id = i.admin_id";
        $cri1->condition = "i.admin_id = '" . $usrarr['id'] . "'";
                
        if($usrarr['type'] == 'institution_admin')
            $result = Institutions::model()->getCommandBuilder()->createFindCommand(Institutions::model()->tableSchema, $cri1)->queryAll();
        else if($usrarr['type'] == 'candidate')
            $result = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (count($result) <= 0) {
            $this->_sendResponse(503, array());
        } else if (!password_verify($password, $usrarr['password'])) {
            $this->_sendResponse(504, array());
        } else
            $this->_sendResponse(200, $result);
    }

//    

    public function actiongetallcategory() {
//		if(!$this->_checkAuth())
//			$this->_sendResponse(405, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $cri = new CDbCriteria();
        $cri->order = 'ordering ASC';
//        $cri->condition = "deleted_at is null";

        $model = SkillCategories::model();

        $Industries = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (count($Industries) <= 0) {
            $this->_sendResponse(503, array());
        } else
            $this->_sendResponse(200, $Industries);
    }

    public function actiongetallskills() {
//		if(!$this->_checkAuth())
//			$this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());


        $cri = new CDbCriteria();
        $cri->condition = "skill_category_id=" . $_GET['id'];

        $model = Skills::model();

        $Industries = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (count($Industries) <= 0) {
            $this->_sendResponse(503, array());
        } else
            $this->_sendResponse(200, $Industries);
    }

//    get candidate
    public function actiongetcandidate() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Users::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias = 'u';
        $cri->select = "u.id as user_id,m.file_name,(SELECT count(*) FROM cv16_profile_views WHERE profile_id = p.id) as num_views,(SELECT count(*) FROM cv16_profile_favourites WHERE profile_id = p.id) as num_likes,CONCAT('http://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('http://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('http://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('http://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.stripe_active,u.current_job,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
        $cri->join = "inner join cv16_profiles p on p.owner_id = u.id
                      left outer join cv16_media m on m.id = p.photo_id
                      left outer join cv16_addresses a on a.model_id = u.id";
        $cri->condition = "u.id = " . $model->id . " and u.type like '%candidate%' and p.owner_type like '%candidate%'";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }
    
    
//    get candidate
    public function actiongetinstitutions() {
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Institutions::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "i.id as institution_id,m.file_name,CONCAT('http://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('http://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('http://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('http://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.id as user_id,u.stripe_active,u.current_job,u.type,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
        $cri->join = "inner join cv16_profiles p on p.owner_id = i.id
                      left outer join cv16_users u on u.id = i.admin_id
                      left outer join cv16_media m on m.id = p.photo_id
                      left outer join cv16_addresses a on a.model_id = i.id";
        
//        if($model == null )
//            echo "sadad";
//        die();
        $cri->condition = "i.id = " . $model->id . " and u.type like '%institution_admin%'";
        
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }
    

//    get employer
    public function actiongetemployer() {
        if (!isset($_GET['id']))
            $this->_sendResponse(500, array());

        $empuser = EmployerUsers::model()->findByAttributes(array('user_id' => $_GET['id']));

        $empcri = new CDbCriteria();
        $empcri->condition = "id = '" . $empuser->employer_id . "'";

        $usrcri = new CDbCriteria();
        $usrcri->condition = "id = '" . $empuser->user_id . "'";

        $prcri = new CDbCriteria();
        $prcri->condition = "owner_id = '" . $empuser->employer_id . "' and type='employer'";

        $addrcri = new CDbCriteria();
        $addrcri->condition = "model_id = '" . $empuser->user_id . "'";

        $empmodel = Employers::model();
        $usermodel = Users::model();
        $profilemodel = Profiles::model();
        $addressmodel = Addresses::model();

        $employers = $empmodel->getCommandBuilder()->createFindCommand($empmodel->tableSchema, $empcri)->queryAll();
        $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $usrcri)->queryAll();
        $profiles = $profilemodel->getCommandBuilder()->createFindCommand($profilemodel->tableSchema, $prcri)->queryAll();
        $address = $addressmodel->getCommandBuilder()->createFindCommand($addressmodel->tableSchema, $addrcri)->queryAll();

        $result = array();

        $result['employers'] = $employers;
        $result['users'] = $users;
        $result['profiles'] = $profiles;
        $result['address'] = $address;

        if (count($result) > 0)
            $this->_sendResponse(200, $result);
        else
            $this->_sendResponse(500, array());
    }

//        create candidate
    public function actioncreatecandidate() {
//        if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $now = new DateTime();
        // echo var_dump($_POST);die();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($var == 'ispremium') {
                //  $this->_sendResponse(400, array());
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        // Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            $usermodel->type = 'candidate';
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
                $profilemodel->owner_type = "Candidate";
                $profilemodel->owner_id = $usermodel->id;
                $profilemodel->created_at = $usermodel->created_at;
                $profilemodel->type = $usermodel->type;
            if ($profilemodel->save()) {
                $addressmodel->model_id = $usermodel->id;
                $addressmodel->model_type = "Candidate";
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($addressmodel->save()) {
                    $cri = new CDbCriteria();
                    $cri->select = 'id,forenames,surname,email,mobile';
                    $cri->condition = "id = " . $usermodel->id;
                    $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                    $this->_sendResponse(200, $users);
                }
            }
        } else {

// Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", "");
            $msg .= "<ul>";
            foreach ($usermodel->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, array());
        }
    }

//        create candidate
    public function actioncreateemployer() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $empmodel = new Employers;
        $empusermodel = new EmployerUsers;
        $usermodel = new Users;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($empmodel->hasAttribute($var)) {
                $empmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($var == 'ispremium') {
                
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->type = 'employer';
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        $usermodel->tel = $_POST['tel'];
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->save();

// insert employer details
        $empmodel->email = $usermodel->email;
        $empmodel->created_at = $now->format('Y-m-d H:i:s');
// Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $empmodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
        $empmodel->user_id = $usermodel->id;
        if ($empmodel->save()) {
//employer user update
            $empusermodel->employer_id = $empmodel->id;
            $empusermodel->user_id = $usermodel->id;
            $empusermodel->role = Yii::app()->user->getState('role');
            $empusermodel->created_at = $now->format('Y-m-d H:i:s');
            $empusermodel->save();
        }

        $profilemodel->owner_id = $empmodel->id;
        $profilemodel->owner_type = 'employer';
        if ($_POST['industry_id'] > 0)
            $profilemodel->industry_id = $_POST['industry_id'];
        $profilemodel->type = 'employer';
        $profilemodel->slug = $_POST['slug'];
        $profilemodel->save();
//address update
        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
        $addressmodel->model_type = 'employer';
        $addressmodel->model_id = $usermodel->id;
        if ($addressmodel->save()) {

            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email,mobile';
            $cri->condition = "id = " . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        } else {

// Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", "");
            $msg .= "<ul>";
            foreach ($usermodel->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, array());
        }
    }

    public function actioncreateinstitution() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        $instmodel = new Institutions();
        $iusermodel = new InstitutionUsers();
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($instmodel->hasAttribute($var)) {
                $instmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->status = 'pending';
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        $usermodel->type = 'institution_admin';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->save();
//            instituion insert
        $instmodel->admin_id = $usermodel->id;
        $instmodel->email = $usermodel->email;
        $instmodel->status = 'pending';
        $instmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($instmodel->save()) {
            $iusermodel->institution_id = $instmodel->id;
            $iusermodel->user_id = $usermodel->id;
            $iusermodel->role = "admin";
            $iusermodel->created_at = $now->format('Y-m-d H:i:s');
            $iusermodel->save();
        }
//profile insert
        $slug = str_replace(" ", "-", $instmodel->name);
        $profilemodel->owner_type = "Institution";
        $profilemodel->owner_id = $instmodel->id;
        $profilemodel->created_at = $usermodel->created_at;
        $profilemodel->type = "institution";
        $profilemodel->slug = lcfirst($slug);
        $profilemodel->save();
//address insert
        $addressmodel->model_id = $instmodel->id;
        $addressmodel->model_type = "Institutions";
        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($addressmodel->save()) {

            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email,mobile';
            $cri->condition = "id = " . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        } else {

// Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", "");
            $msg .= "<ul>";
            foreach ($usermodel->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, array());
        }
    }

    public function actionmedialibrary() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        if (!isset($_GET['id']) && !isset($_GET['type']))
            $this->_sendResponse(500, array());


        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        if ($_GET['type'] == 'candidate') {
            $mediamodel = Media::model()->findAll(array(
                'condition' => 'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                'params' => array(':model_id' => $_GET['id'], ':model_type' => 'App\\Users\\Candidate', ':collection_name' => 'photos'),
            ));
        } else if ($_GET['type'] == 'employer') {
            $mediamodel = Media::model()->findAll(array(
                'condition' => 'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                'params' => array(':model_id' => $_GET['id'], ':model_type' => 'App\Employer', ':collection_name' => 'photos'),
            ));
        } else if ($_GET['type'] == 'institution') {
            $mediamodel = Media::model()->findAll(array(
                'condition' => 'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                'params' => array(':model_id' => $_GET['id'], ':model_type' => 'App\Education\Institution', ':collection_name' => 'photos'),
            ));
        }

        $samplearr = array();
        $photosarr = array();
        $mediaarr1 = array();
        $mediaarr2 = array();
        $result = array();

        if (count($mediamodel) > 0) {

            foreach ($mediamodel as $media) {
                $mediaarr1['id'] = $media['id'];
                $mediaarr1['url'] = Yii::app()->baseUrl . '/images/media/' . $media['id'] . '/conversions/search.jpg';
                $photosarr[] = $mediaarr1;
            }
        }
        $result['photos'] = $photosarr;

        $mediamodel1 = Media::model()->findAll(array(
            'condition' => 'model_id=:model_id AND collection_name=:collection_name',
            'params' => array(':model_id' => 1, ':collection_name' => 'sample'),
        ));

        if (count($mediamodel1) > 0) {

            foreach ($mediamodel1 as $media) {
                $mediaarr2['id'] = $media['id'];
                $mediaarr2['url'] = Yii::app()->baseUrl . '/images/media/' . $media['id'] . '/conversions/search.jpg';
                $samplearr[] = $mediaarr2;
            }
        }
        $result['sample'] = $samplearr;

        if (count($result) > 0)
            $this->_sendResponse(200, $result);
        else
            $this->_sendResponse(500, array());
    }

//    cover photo update
    public function actionupdatecover() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        if (!isset($_POST['mediaid']) || !isset($_POST['profileid']))
            $this->_sendResponse(500, array());


        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $mediaid = $_POST['mediaid'];
        $profileid = $_POST['profileid'];

        $model = Profiles::model()->findByPk($profileid);
        $model->cover_id = $mediaid;

        if ($model->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Cover photo updated successfully", ""));
        } else {
            $this->_sendResponse(400, array());
        }
    }

//    update profile picture
    public function actionupdatepicture() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        if (!isset($_POST['mediaid']) || !isset($_POST['profileid']))
            $this->_sendResponse(500, array());


        $mediaid = $_POST['mediaid'];
        $profileid = $_POST['profileid'];

        $model = Profiles::model()->findByPk($profileid);
        $model->photo_id = $mediaid;

        if ($model->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Profile picture updated successfully", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }

//    update visibility
    public function actionupdatevisibility() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        if (!isset($_POST['visibility']) || !isset($_POST['profileid']))
            $this->_sendResponse(500, array());

        $visibility = $_POST['visibility'];
        $profileid = $_POST['profileid'];

        $model = Profiles::model()->findByPk($profileid);
        $model->visibility = $visibility;

        if ($model->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Profile updated successfully", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }

//    update status
    public function actionupdatestatus() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        if (!isset($_POST['status']) || !isset($_POST['profileid']))
            $this->_sendResponse(500, array());




        $status = $_POST['status'];
        $profileid = $_POST['profileid'];

        $model = Profiles::model()->findByPk($profileid);
        $model->published = ($status == "public" ? 0 : 1);

        if ($model->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Profile updated successfully", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }

//    update candidate account
    public function actionupdatecandidateaccount() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $pwd = $usermodel->password;

        $now = new DateTime();
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id, 'type' => $usermodel->type));

        foreach ($put_vars as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "password" && $value == "") {
                    $usermodel->password = password_hash($value, 1, ['cost' => 10]);
                } else if ($var == "password" && $value != "") {
                    
                } else {
                    $usermodel->$var = $value;
                }
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($profilemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully updated user", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    public function actiondoccategory() {
        $category = array("Never Married", "Divorced", "Widowed", "Awaiting Divorce", "Annulled");
        $this->_sendResponse(200, $category);
    }

//  create  documentation 
    public function actioncreateuserdocumentation() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();

        $userid = $id;
        $filename = $_FILES['doc_file']['name'];
        $size = $_FILES['doc_file']['size'];
        $name = pathinfo($_FILES['doc_file']['name'], PATHINFO_FILENAME);
        $docname = $_POST['doc_name'];
        $category_id = $_POST['category_id'];
        $published = $_POST['published'];

        $now = new DateTime();

        $mediamodel = new Media();
        $mediamodel->model_type = "Candidate";
        $mediamodel->model_id = $userid;
        $mediamodel->collection_name = "documents";
        $mediamodel->name = $name;
        $mediamodel->file_name = $filename;
        $mediamodel->size = $size;
        $mediamodel->disk = "documents";
        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
        if ($mediamodel->save()) {
            $model = new UserDocuments();
            $model->user_id = $userid;
            $model->media_id = $mediamodel->id;
            $model->name = $docname;
            $model->category_id = $category_id;
            $model->published = $published;
            $model->created_at = $now->format('Y-m-d H:i:s');
            if ($model->save()) {
                $pdmodel = new ProfileDocuments();
                $pdmodel->profile_id = $profilemodel->id;
                $pdmodel->user_document_id = $model->id;
                $pdmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($pdmodel->save()) {
                    $userdocu = UserDocuments::model()->findByPk($model->id);
                    $this->_sendResponse(200, $userdocu);
                } else {
                    $this->_sendResponse(500, array());
                }
            }
        }
    }

//    update  documentation 
    public function actionupdateuserdocumentation() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $docid = isset($_GET['docid']) ? $_GET['docid'] : null;
        $docmodel = UserDocuments::model()->findByPk($id);
        if ($docid != $docmodel->id)
            $this->_sendResponse(404, array());

        $usermodel = Users::model()->findByPk($docmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();

        $userid = $usermodel->id;
        $filename = $_FILES['doc_file']['name'];
        $size = $_FILES['doc_file']['size'];
        $name = pathinfo($_FILES['doc_file']['name'], PATHINFO_FILENAME);
        $docname = $_POST['doc_name'];
        $category_id = $_POST['category_id'];
        $published = $_POST['published'];

        $now = new DateTime();

        $mediamodel = new Media();
        $mediamodel->model_type = "Candidate";
        $mediamodel->model_id = $userid;
        $mediamodel->collection_name = "documents";
        $mediamodel->name = $name;
        $mediamodel->file_name = $filename;
        $mediamodel->size = $size;
        $mediamodel->disk = "documents";
        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
        if ($mediamodel->save()) {
            $model = new UserDocuments();
            $model->user_id = $userid;
            $model->media_id = $mediamodel->id;
            $model->name = $docname;
            $model->category_id = $category_id;
            $model->published = $published;
            $model->created_at = $now->format('Y-m-d H:i:s');
            if ($model->save()) {
                $pdmodel = new ProfileDocuments();
                $pdmodel->profile_id = $profilemodel->id;
                $pdmodel->user_document_id = $model->id;
                $pdmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($pdmodel->save()) {
                    $this->_sendResponsewithMessage(200, sprintf("Successfully Documentation updated", ""));
                } else {
                    $this->_sendResponse(500, array());
                }
            }
        }
    }

//    get profile documentaions
    public function actiongetprofiledocumentation() {
        $docid = isset($_GET['docid']) ? $_GET['docid'] : null;
        $docmodel = UserDocuments::model()->findByPk($docid);
        if ($docid != $docmodel->id)
            $this->_sendResponse(404, array());
        $id = $_GET['docid'];
        $model = new UserDocuments();
        $cri = new CDbCriteria();
        $cri->alias = "ud";
        $cri->select = "ud.name,ud.id,m.file_name as filename,mc.name as category";
        $cri->join = "inner join cv16_profile_documents pd on pd.user_document_id = ud.id inner join cv16_media m on m.id = ud.media_id  inner join cv16_media_categories mc on mc.id = ud.category_id";
        $cri->condition = "pd.user_document_id =" . $id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }

//    get all profile documentaions
    public function actiongetallprofiledocumentation() {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

// $id = $_GET['docid'];

        $model = new UserDocuments();
        $cri = new CDbCriteria();
        $cri->alias = "ud";
        $cri->select = "ud.name as name,ud.id,mc.name as category";
        $cri->join = "inner join cv16_media m on m.id = ud.media_id
                          inner join cv16_media_categories mc on mc.id = ud.category_id";
        $cri->condition = "ud.user_id =" . $id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }

//    delete profile documentations
    public function actiondeleteprofiledocumentation() {
//        if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array());

        $docid = isset($_GET['docid']) ? $_GET['docid'] : null;
        $docmodel = UserDocuments::model()->findByPk($docid);
        if ($docid != $docmodel->id)
            $this->_sendResponse(404, array());

        UserDocuments::model()->deleteAll(array(
            'condition' => "`id` = ':id'",
            'params' => array(':id' => $id),
        ));
        ProfileDocuments::model()->deleteAll(array(
            'condition' => "`user_document_id` = ':id'",
            'params' => array(':id' => $id),
        ));

        $this->_sendResponse(200, "Delete Successfull");
    }

//    create Professional Experience
    public function actioncreateprofessionalexprience() {
//        if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $profilemodel = Profiles::model()->findByPk($id);
        if ($id != $profilemodel->id)
            $this->_sendResponse(404, array());

        $model = new UserExperiences();
        $profilemodel = Profiles::model()->findByPk($id);
        $now = new DateTime();
        $iscurrent = 0;
        $start_month = null;
        $start_year = null;
        $end_month = null;
        $end_year = null;
        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else if ($var == "is_current")
                $iscurrent = $value;
            else if ($var == "start_month")
                $start_month = $value;
            else if ($var == "start_year")
                $start_year = $value;
            else if ($var == "end_month")
                $end_month = $value;
            else if ($var == "end_year")
                $end_year = $value;
            else
                $this->_sendResponse(500, array());
        }
        $model->user_id = $profilemodel->owner_id;
        $model->start_date = $start_year . "-" . $start_month . "-01";
        $model->end_date = $iscurrent == 1 ? null : ($end_year . "-" . $end_month . "-01");
        $model->created_at = $now->format('Y-m-d H:i:s');


// Try to save the model
        if ($model->save()) {
            $pemodel = new ProfileExperiences();
            $pemodel->profile_id = $id;
            $pemodel->user_experience_id = $model->id;
            $pemodel->created_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

        $model = new UserExperiences();
        $cri = new CDbCriteria();
        $cri->alias = "ux";
        $cri->select = "ux.company_name,ux.location,ux.position,ux.start_date,ux.end_date,ux.description";
        $cri->join = "inner join cv16_profile_experiences px on px.user_experience_id = ux.id";
        $cri->condition = "px.profile_id =" . $id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }

//    update Professional Experience
    public function actionupdateprofessionalexprience() {
//         if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userexmodel = UserExperiences::model()->findByPk($id);
        if ($id != $userexmodel->id)
            $this->_sendResponse(404, array());

        $usermodel = Users::model()->findByPk($userexmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $iscurrent = 0;
        $start_month = null;
        $start_year = null;
        $end_month = null;
        $end_year = null;
        foreach ($_POST as $var => $value) {
            if ($userexmodel->hasAttribute($var))
                $userexmodel->$var = $value;
            else if ($var == "is_current")
                $iscurrent = $value;
            else if ($var == "start_month")
                $start_month = $value;
            else if ($var == "start_year")
                $start_year = $value;
            else if ($var == "end_month")
                $end_month = $value;
            else if ($var == "end_year")
                $end_year = $value;
            else
                $this->_sendResponse(500, array());
        }

        $userexmodel->start_date = $start_year . "-" . $start_month . "-01";
        $userexmodel->end_date = $iscurrent == 1 ? null : ($end_year . "-" . $end_month . "-01");
        $userexmodel->updated_at = $now->format('Y-m-d H:i:s');

// Try to save the model
        if ($userexmodel->save()) {
            $pemodel = ProfileExperiences::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_experience_id' => $userexmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

        $model = new ProfileExperiences();
        $cri = new CDbCriteria();
        $cri->alias = "px";
        $cri->select = "ux.company_name,ux.location,ux.position,ux.start_date,ux.end_date,ux.description";
        $cri->join = "inner join cv16_user_experiences ux on ux.id = px.user_experience_id";
        $cri->condition = "px.user_experience_id=" . $id;
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

//    get all profile documentaions
    public function actiongetallprofessionalexperience() {
//          if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

// $id = $_GET['docid'];

        $model = new UserExperiences();
        $cri = new CDbCriteria();
        $cri->alias = "ux";
        $cri->select = "ux.id,ux.company_name,ux.location,ux.position,ux.start_date,ux.end_date,ux.description";
        $cri->join = "inner join cv16_profile_experiences px on px.user_experience_id = ux.id";
        $cri->condition = "ux.user_id =" . $id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }

//    get profile documentaions
    public function actiongetprofessionalexperience() {
//          if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        $profexpid = isset($_GET['profexpid']) ? $_GET['profexpid'] : null;
        $profexp = UserExperiences::model()->findByPk($profexpid);
        if ($profexpid != $profexp->id)
            $this->_sendResponse(404, array());

        $id = $_GET['profexpid'];

        $model = new UserExperiences();
        $cri = new CDbCriteria();
        $cri->alias = "ux";
        $cri->select = "ux.id,ux.company_name,ux.location,ux.position,ux.start_date,ux.end_date,ux.description";
        $cri->join = "inner join cv16_profile_experiences px on px.user_experience_id = ux.id";
        $cri->condition = "ux.id =" . $id;
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

//    create education
    public function actioncreateusereducation() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $model = new UserQualifications();

        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, array());
        }

        $completion_date = $_POST['completion_year'] . "-" . $_POST['completion_month'] . "-01";

        $model->user_id = $usermodel->id;
        $model->completion_date = $completion_date;
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileQualifications();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_qualification_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pdmodel->save()) {
                $proexp = UserQualifications::model()->findByPk($model->id);
                $this->_sendResponse(200, $proexp);
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

//    update education
    public function actionupdateusereducation() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userqualmodel = UserQualifications::model()->findByPk($id);
        if ($id != $userqualmodel->id)
            $this->_sendResponse(404, array());

        $usermodel = Users::model()->findByPk($userqualmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($userqualmodel->hasAttribute($var))
                $userqualmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }

        $completion_date = $_POST['completion_year'] . "-" . $_POST['completion_month'] . "-01";

        $userqualmodel->user_id = $usermodel->id;
        $userqualmodel->completion_date = $completion_date;
        $userqualmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($userqualmodel->save()) {
            $pemodel = ProfileQualifications::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_qualification_id' => $model->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully Education updated", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

//    get all profile documentaions
    public function actiongetallusereducation() {
//          if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

// $id = $_GET['docid'];

        $model = new UserQualifications();
        $cri = new CDbCriteria();
        $cri->alias = "uq";
        $cri->select = "uq.id,uq.institution,uq.location,uq.field,uq.level,uq.completion_date,uq.grade,uq.description";
        $cri->join = "inner join cv16_profile_qualifications pq on pq.user_qualification_id = uq.id";
        $cri->condition = "uq.user_id =" . $id;
        $userqual = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($userqual)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $userqual);
        }
    }

//    get profile documentaions
    public function actiongetusereducation() {
//          if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        $usreduid = isset($_GET['usreduid']) ? $_GET['usreduid'] : null;
        $usredu = UserExperiences::model()->findByPk($usreduid);
        if ($usreduid != $usredu->id)
            $this->_sendResponse(404, array());

        $id = $_GET['usreduid'];

        $model = new UserQualifications();
        $cri = new CDbCriteria();
        $cri->alias = "uq";
        $cri->select = "uq.id,uq.institution,uq.location,uq.field,uq.level,uq.completion_date,uq.grade,uq.description";
        $cri->join = "inner join cv16_profile_qualifications pq on pq.user_qualification_id = uq.id";
        $cri->condition = "uq.id =" . $id;
        $userqual = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($userqual)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $userqual);
        }
    }

//    create hobbies
    public function actioncreatehobbies() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();

        $model = new UserHobbies();

        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, array());
        }

        $model->user_id = $usermodel->id;
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileHobbies();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_hobby_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pqmodel->save()) {
                $prohob = UserHobbies::model()->findByPk($model->id);
                $this->_sendResponse(200, $prohob);
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

//    update hobbies
    public function actionupdatehobbies() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userhobmodel = UserHobbies::model()->findByPk($id);
        if ($id != $userhobmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($userhobmodel->hasAttribute($var))
                $userhobmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $usermodel = Users::model()->findByPk($userhobmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));


        $userhobmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($userhobmodel->save()) {
            $pemodel = ProfileHobbies::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_hobby_id' => $userhobmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully Hobbies updated", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

//    get all profile documentaions
    public function actiongetalluserhobbies() {
//          if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

// $id = $_GET['docid'];

        $model = new UserHobbies();
        $cri = new CDbCriteria();
        $cri->alias = "uh";
        $cri->select = "uh.id,uh.activity,uh.description";
        $cri->join = "inner join cv16_profile_hobbies ph on ph.user_hobby_id = uh.id";
        $cri->condition = "uh.user_id =" . $id;
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

//    get profile documentaions
    public function actiongetuserhobbies() {
//          if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        $usrhobid = isset($_GET['usrhobid']) ? $_GET['usrhobid'] : null;
        $usrhob = UserHobbies::model()->findByPk($usrhobid);
        if ($usrhobid != $usrhob->id)
            $this->_sendResponse(404, array());

        $id = $_GET['usrhobid'];

        $model = new UserHobbies();
        $cri = new CDbCriteria();
        $cri->alias = "uh";
        $cri->select = "uh.activity,uh.description";
        $cri->join = "inner join cv16_profile_hobbies ph on ph.user_hobby_id = uh.id";
        $cri->condition = "uh.id =" . $id;
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

//    create language

    public function actioncreatelanguage() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();

        $model = new UserLanguages();

        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, array());
        }

        $model->user_id = $usermodel->id;
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileLanguages();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_language_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pqmodel->save()) {
                $prolan = UserLanguages::model()->findByPk($model->id);
                $this->_sendResponse(200, $prolan);
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

//    update hobbies
    public function actionupdatelanguage() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userlanmodel = UserLanguages::model()->findByPk($id);
        if ($id != $userlanmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($userlanmodel->hasAttribute($var))
                $userlanmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $usermodel = Users::model()->findByPk($userlanmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));


        $userlanmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($userlanmodel->save()) {
            $pemodel = ProfileLanguages::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_language_id' => $userlanmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully Hobbies updated", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

//    get all profile langauage
    public function actiongetalluserlanguage() {
//          if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

// $id = $_GET['docid'];

        $model = new UserLanguages();
        $cri = new CDbCriteria();
        $cri->alias = "ul";
        $cri->select = "ul.id,ul.name,ul.proficiency";
        $cri->join = "inner join cv16_profile_languages pl on pl.user_language_id = ul.id";
        $cri->condition = "ul.user_id =" . $id;
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

//    get profile documentaions
    public function actiongetuserlanguage() {
//          if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        $usrlanid = isset($_GET['usrlanid']) ? $_GET['usrlanid'] : null;
        $usrlan = UserLanguages::model()->findByPk($usrhobid);
        if ($usrlanid != $usrlan->id)
            $this->_sendResponse(404, array());

        $id = $_GET['usrlanid'];

        $model = new UserLanguages();
        $cri = new CDbCriteria();
        $cri->alias = "ul";
        $cri->select = "ul.name,ul.proficiency";
        $cri->join = "inner join cv16_profile_languages pl on pl.user_language_id = ul.id";
        $cri->condition = "ul.id =" . $id;
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

//    get profile Completion
    public function actiongetprofilecompletion() {
//         if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        if (!isset($_GET['profile_id']) || $_GET['profile_id'] == 0)
            $this->_sendResponse(404, array());

        $percentage = 0;
        $profiledocs = ProfileDocuments::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profiledocs) > 0)
            $percentage += '15';

        $profileexp = ProfileExperiences::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profileexp) > 0)
            $percentage += '15';

        $profilehob = ProfileHobbies::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profilehob) > 0)
            $percentage += '15';

        $profilelang = ProfileLanguages::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profilelang) > 0)
            $percentage += '15';

        $profilequal = ProfileQualifications::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profilequal) > 0)
            $percentage += '15';

        $profileskill = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profileskill) > 0)
            $percentage += '15';

        $profilevid = ProfileVideos::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profilevid) > 0)
            $percentage += '10';

        $pcentage = array();

        $pcentage['percentage'] = $percentage;

        if (empty($percentage)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, array($pcentage));
        }
    }
    
    
//    get all profile langauage
    public function actiongetbussinesscard() {
//          if(!$this->_checkAuth())
//	        $this->_sendResponse(405, array() );

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

// $id = $_GET['docid'];

        $model = new Users();
        $cri = new CDbCriteria();
        $cri->alias = "u";
        $cri->select = "CONCAT(u.forenames,' ',u.surname) as name,u.tel,u.email";
        $cri->join = "inner join cv16_profile_languages pl on pl.user_language_id = ul.id";
        $cri->condition = "u.id =" . $id;
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }
    
    public function actiongetallvideos()
    {
        
             $id = isset($_GET['profile_id']) ? $_GET['profile_id'] : null;
        $profilemodel = Profiles::model()->findByPk($id);
        if ($id != $profilemodel->id)
            $this->_sendResponse(404, array());

// $id = $_GET['docid'];

        $model = new ProfileVideos();
        $cri = new CDbCriteria();
        $cri->alias = "pv";
        $cri->select = "v.name as name,v.id,v.duration,pv.is_default,pv.state,CONCAT('http://cvvid.com/api/images/media/',m.id,'/',m.file_name) as photo,CONCAT('http://cvvid.com/api/',v.video_id) as video";
        $cri->join = "inner join cv16_media m on m.model_id = pv.video_id
                      inner join cv16_videos v on v.id = pv.video_id";
        $cri->condition = "pv.profile_id =" . $id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
         
         
         
    }

    public function actionvideoupload() {

        if (!isset($_GET['profile_id']) || $_GET['profile_id'] == 0)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        // $target_path = Yii::app()->getBaseUrl(true); // Target path where file is to be stored
        // $target_path = $target_path . basename($_FILES['video']['name']);

        try {
//            //throw exception if can't move the file
//            if (!move_uploaded_file($_FILES['video']['tmp_name'], $target_path)) {
//               // throw new Exception('Could not move file');
//            }

            $now = new DateTime();
            $userid = $_GET['profile_id'];
            $name = $_POST['video_name'];
            $size = $_FILES['video']['size'];
            $image_type = $_FILES['video']['type'];
            $filename = $_FILES['video']['name'];

            $usermodel = Users::model()->findByPk($userid);
            $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

            $vname = pathinfo($name, PATHINFO_FILENAME);

            $mediamodel = new Media();
            $mediamodel->disk = "media";
            $mediamodel->model_type = $usermodel->type;
            $mediamodel->collection_name = "video";
            $mediamodel->name = $vname;
            $mediamodel->file_name = $vname . ".jpg";
            $mediamodel->created_at = $now->format('Y-m-d H:i:s');
            $mediamodel->size = 0; //(int)$duration;
            if ($mediamodel->save()) {
                $mediafolder = "images/media/" . $mediamodel->id;

                if (!file_exists($mediafolder)) {
                    mkdir($mediafolder, 0777, true);
                }

                $fullpath = "images/media/" . $mediamodel->id . "/" . $name;

                if (move_uploaded_file($_FILES['video']['tmp_name'], $fullpath)) {
                    $videomodel = new Videos();
                    $videomodel->model_id = $userid;
                    $videomodel->video_id = $fullpath;
                    $videomodel->name = $vname;
                    $videomodel->duration = 0;
                    $videomodel->model_type = $usermodel->type;
                    $videomodel->category_id = 1;
                    $videomodel->is_processed = 1;
                    $videomodel->state = 1;
                    $videomodel->status = "active";
                    $videomodel->created_at = $now->format('Y-m-d H:i:s');
                    if ($videomodel->save()) {

                        $pvideomodel = new ProfileVideos();
                        $pvideomodel->profile_id = $pmodel->id;
                        $pvideomodel->video_id = $videomodel->id;
                        $pvideomodel->created_at = $now->format('Y-m-d H:i:s');
                        $pvideomodel->state = 1;
                        $pvideomodel->save();
                        
                        $sec = 0;

                        $cmd = "ffmpeg -i $fullpath 2>&1";
                        $ffmpeg = shell_exec($cmd);

                        $search = "/Duration: (.*?)\./";
                        preg_match($search, $ffmpeg, $matches);

                        if (isset($matches[1])) {
                            $data['duration'] = $matches[1];
                            $time_sec = explode(':', $data['duration']);
                            $sec = ($time_sec['0'] * 3600) + ($time_sec['1'] * 60) + $time_sec['2'];
                        }

                        Videos::model()->updateByPk($videomodel->id, array('duration' => $sec));
                        Media::model()->updateByPk($mediamodel->id, array('model_id' => $videomodel->id, 'size' => $sec));

                        $iname = $vname . ".jpg";
                        $imgfullpath = "images/media/" . $mediamodel->id . "/" . $iname;
                        $imgprefix = "images/media/" . $mediamodel->id;


                        $process = exec("ffmpeg -i $fullpath -ss 00:00:2 -vframes 1 $imgfullpath");

                        $jlwidth = 350;
                        $jlheight = 190;
                        $jldestpath = $imgprefix . "/conversions/profile.jpg";

                        //create search
                        $searchwidth = 167;
                        $searchheight = 167;
                        $searchdestpath = $imgprefix . "/conversions/search.jpg";

                        //create thumb
                        $thumbwidth = 126;
                        $thumbheight = 126;
                        $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
                        $image_type == "image/jpg";

                        $image = imagecreatefromjpeg($imgfullpath);

                        $this->resize_image($image, $image_type, $imgfullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                        $this->resize_image($image, $image_type, $imgfullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                        $this->resize_image($image, $image_type, $imgfullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                    
                         echo "The video has been uploaded";
                        
                        }
                }
            }
        } catch (Exception $e) {
            die('File did not upload: ' . $e->getMessage());
        }
    }
    
    public function actionupdateVideoName() {
        
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $vmodel = Videos::model()->findByPk($id);
        if ($id != $vmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $now = new DateTime();

        $videoname = $_POST['name'];

        $vmodel->name = $videoname;
        $vmodel->updated_at = $now->format('Y-m-d H:i:s');
        if($vmodel->save())
        {
            $this->_sendResponsewithMessage(200, sprintf("Successfully updated", ""));
        } else {
            $this->_sendResponse(509, array());
        }

    }
    
    public function actionprofileVideoDelete() {
        
               
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
        $id = $_POST['id'];
        
        $vmodel = Videos::model()->findByPk($id);
        $videoid = $vmodel->id;
        
        $pvmodel = ProfileVideos::model()->findByAttributes(array('video_id' => $videoid));
        
        if($pvmodel != null){
             $mediamodel = Media::model()->findByAttributes(array('model_id' => $pvmodel['video_id'],'collection_name'=>'video'));
             if($mediamodel != null)
             {
               if (is_dir('images/media/'.$mediamodel->id))
                     $this->rrmdir('images/media/'.$mediamodel->id.'/');

                 $mediamodel = Media::model()->deleteAllByAttributes(array('model_id' => $pvmodel['video_id'],'collection_name'=>'video'));
                 $vmodel = Videos::model()->deleteByPk($videoid);
                 ProfileVideos::model()->deleteAllByAttributes(array('video_id' => $videoid));
             }
         }
         
          $this->_sendResponsewithMessage(200, sprintf("Successfully deleted", ""));

       
    }
    
    public function actionprofileVideoDefault() {
        
                 
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
        $videoid = $_POST['id'];

        $vmodel = Videos::model()->findByPk($videoid);
        $userid = $vmodel->model_id;
        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

        ProfileVideos::model()->updateAll(array('is_default' => 0), 'profile_id =:profile_id', array(':profile_id' => $pmodel->id));

        ProfileVideos::model()->updateAll(array('is_default' => 1), 'video_id =:video_id', array(':video_id' => $videoid));

         $this->_sendResponsewithMessage(200, sprintf("Successfully maked default", ""));
    }
    
    public function actiongetPlan()
    {
//        $id = isset($_GET['id']) ? $_GET['id'] : null;
//        $usermodel = Users::model()->findByPk($id);
//        if ($id != $usermodel->id)
//            $this->_sendResponse(404, array());
        
        $now = new DateTime();
        
         $monplans[] = Membership::plan('Monthly Subscription', 'premium_monthly')
                        ->price(25)
                        ->price_suffix('per month')
                        ->description('Billed Monthly')
                        ->monthly()
                        ->permissions([
                            'send_messages' => 'Send messages',
                            'apply_jobs' => 'Apply for jobs',
                            'view_favourites' => 'View those who have favourited you',
                            'view_views' => 'View those who have viewed your profile'
                        ]);
         
          $annplans[] =  Membership::plan('Annual Subscription', 'premium_annual')
                        ->price(252)
                        ->price_suffix('per year')
                        ->description('Save 10%')
                        ->permissions([
                            'send_messages'     => 'Send messages',
                            'apply_jobs'        => 'Apply for jobs',
                            'view_favourites'   => 'View those who have favourited you',
                            'view_views'        => 'View those who have viewed your profile'
                        ]);
        
        try{
            //$products  = Plan::all();
            $results['response'] = "Success";
            $results['plans'] = array_merge($monplans, $annplans);

        }catch (Exception $e){
            $results['response'] = "Error";
            $results['plans'] = $e->getMessage();
        }
        echo json_encode($results);
        
    }
    
     public function actionupgradestore()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $model = Users::model()->findByPk($id);
        if ($id != $model->id)
            $this->_sendResponse(404, array());
        
         if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
        $now = new DateTime();
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
	Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                
        $plan = $_POST['plan_id'];
        $token = $_POST['token'];
        $trial_ends = Carbon::now()->addYear()->toDateTimeString();
        
        $planarr = Plan::retrieve($plan);
        
        $amount = $planarr->amount;
        $planname = $planarr->name;
        $currency = $planarr->currency;
     
     //        create subscriber
        $customer = Customer::create(array(
            'source'   => $token,
            'email'    => $model->email,
            'plan'     => $plan,
            'description' => $model->forenames." ".$model->surname
        ));
         
         
       $charge = Charge::create(array(
            "amount" => $amount,
            "currency" => $currency,
            "customer" => $customer->id,
            "description" => $planname
        ));


////         create subscription
        $subscription = Subscription::create(array(
            "customer" => $customer->id,
            "items" => array(
                array(
                    "plan" => $plan,
                ),
            )
        ));
        
        $model->stripe_active = 1;
        $model->stripe_id = $customer->id;
        $model->stripe_subscription = $subscription->id;
        $model->stripe_plan = $plan;
        $model->last_four = $_POST['cvc'];
        $model->trial_ends_at = $trial_ends;
        $model->updated_at = $now->format('Y-m-d H:i:s');
        if($model->save(false))
        {
             $this->_sendResponsewithMessage(200, sprintf("Successfully updated your membership", ""));
        }
        else
        {
              $this->_sendResponse(500, array());
        }
        
    }
    
    function combineWithKeys($array1, $array2)
    {
        foreach($array1 as $key=>$value) $array2[$key] = $value;
        asort($array2);
        return $array2;
    } 
    
    
    function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir")
                        rrmdir($dir."/".$object);
                        else unlink   ($dir."/".$object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
    
     public function actionvideoDelete() {

        $videoid = $_POST['id'];
        $vmodel = Videos::model()->findByPk($videoid);
        $userid = $vmodel->model_id;

        $vmodel = Videos::model()->deleteByPk($videoid);

        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

        ProfileVideos::model()->deleteAllByAttributes(array('video_id' => $videoid));

        echo $this->renderPartial('load_videolist', array('model' => $usermodel, 'profilemodel' => $pmodel));
    }


//    search job
    public function actionsearchjob() {

        $skillcategid = isset($_GET['skill_category']) ? $_GET['skill_category'] : 0;
//        $skillarr = isset($_GET['skills']) ? $_GET['skills'] : array();
//        $location = $_GET['location'];
//        $lat = $_GET['latitude'];
//        $long = $_GET['longitude'];
//        $jobtype = isset($_GET['jobtype']) ? $_GET['jobtype'] : "";
//        $salary = isset($_GET['salary']) ? $_GET['salary'] : "";
//        $radius = isset($_GET['distance']) ? $_GET['distance'] : 0;
        // $skillids = implode(',', $skillarr);

        $cri = new CDbCriteria();
        $cri->alias = "j";
        $cri->join = "inner join cv16_job_skill js on js.job_id = j.id
                      inner join cv16_skills s on s.id = js.skill_id
                      inner join cv16_skill_categories sc on sc.id = j.skill_category_id";
        if ($skillcategid > 0)
            $cri->addCondition("j.skill_category_id =" . $skillcategid);
//        if ($skillids != "")
//            $cri->addCondition('js.skill_id in (' . $skillids . ')', 'AND');
//        if ($location != "" && $lat != "") {
//            if ($radius > 0) {
//                $cri->select = "(6371 * acos( cos( radians(" . $lat . ") ) * cos( radians(" . $lat . ") ) *
//                                cos(radians(" . $long . ") - radians(" . $long . ")) + sin(radians(" . $lat . ")) *
//                                sin(radians(" . $lat . ")) )) as distance";
////                 $cri->addCondition("HAVING distance < " . $radius);
//                $cri->having = "distance < " . $radius;
//            } else {
////$cri->addCondition("j.location like '%" . $location . "%'");
//                $cri->addCondition("j.location_lat =" . $lat . " and j.location_lng=" . $long);
//            }
//        }
//        if ($salary > 0) {
//            $cri->addCondition("j.salary_min >= " . $salary);
//        }
//        if ($jobtype != "") {
//            $cri->addCondition("j.type like '%" . $jobtype . "%'");
//        }

        $model = Jobs::model();

        $cri->group = "j.id";
        $jobmodel = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        //$jobmodel = Jobs::model()->findAll($cri);
        if (!isset($jobmodel[0]['id']))
            $jobmodel = array();

        if (empty($jobmodel)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $jobmodel);
        }
    }

//    add Favorite
    public function actionaddfavourite() {

        $userid = isset($_GET['userid']) ? $_GET['userid'] : null;
        $usermodel = Users::model()->findByPk($userid);
        if ($userid != $usermodel->id)
            $this->_sendResponse(404, array());

        if (!isset($_POST['favourite_id']) || $_POST['favourite_id'] == 0)
            $this->_sendResponse(404, array());

        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id));
        $pfmodel = ProfileFavourites::model()->findByAttributes(array('profile_id' => $pmodel->id, 'favourite_id' => $_POST['favourite_id']));
        if ($pfmodel == null) {
            $pfmodel = new ProfileFavourites();
            $pfmodel->profile_id = $pmodel->id;
            $pfmodel->favourite_id = $_POST['favourite_id'];
            if ($pfmodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully Added", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

//    employer post job
    public function actioncreatejob() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $empmodel = Employers::model()->findByPk($id);
        if ($id != $empmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $jobmodel = new Jobs;
        $industrymodel = Industries::model();
        $skillcategmodel = SkillCategories::model();
        $skillsmodel = Skills::model();
        $jobskillmodel = JobSkill::model();

        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($jobmodel->hasAttribute($var))
                $jobmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $jobmodel->owner_id = $id;
        $jobmodel->owner_type = 'employer';
        $jobmodel->user_id = $_POST["user_id"];
        $d = str_replace('/', '-', $_POST["end_date"]);
        $endformat = date('Y-m-d', strtotime($d));
        $jobmodel->end_date = $endformat;
        $jobmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($jobmodel->save()) {
            if (isset($_POST['skills'])) {
                $skills = $_POST['skills'];
                foreach ($skills as $skills_val) {
                    $jobskillmodel = new JobSkill;
                    $jobskillmodel->job_id = $jobmodel->id;
                    $jobskillmodel->skill_id = $skills_val;
                    $jobskillmodel->save();
                }
            }
            $prolan = Jobs::model()->findByPk($jobmodel->id);
            if ($prolan != null)
                $this->_sendResponse(200, $prolan);
            else
                $this->_sendResponse(500, array());
        }
    }

//    employer update job
    public function actionupdatejob() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $jobmodel = Jobs::model()->findByPk($id);
        if ($id != $jobmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $industrymodel = Industries::model();
        $skillcategmodel = SkillCategories::model();
        $skillsmodel = Skills::model();
        $jobskillmodel = JobSkill::model();

        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($jobmodel->hasAttribute($var))
                $jobmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $d = str_replace('/', '-', $_POST["end_date"]);
        $endformat = date('Y-m-d', strtotime($d));
        $jobmodel->end_date = $endformat;
        $jobmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($jobmodel->save()) {
            if (isset($_POST['skills'])) {
                $skills = $_POST['skills'];

                $jkillmodel = JobSkill::model()->deleteAllByAttributes(array('job_id' => $jobmodel->id));

                foreach ($skills as $skills_val) {
                    $jobskillmodel = new JobSkill;
                    $jobskillmodel->job_id = $jobmodel->id;
                    $jobskillmodel->skill_id = $skills_val;
                    $jobskillmodel->save();
                }
            }
            $prolan = Jobs::model()->findByPk($jobmodel->id);
            if ($prolan != null)
                $this->_sendResponse(200, sprintf("Successfully updated Job", ""));
            else
                $this->_sendResponse(500, array());
        }
    }

//    favorite list
    public function actioncandidatefavoritelist() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $profilemodel = Profiles::model()->findByPk($id);
        if ($id != $profilemodel->id)
            $this->_sendResponse(404, array());

        $favourites = ProfileFavourites::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));

        $fav[] = array();
        foreach ($favourites as $cand) {
            $empmodel = Employers::model()->findByPk($profilemodel->owner_id);
            $candidate = Users::model()->findByPk($empmodel->user_id);
            if ($profilemodel->photo_id > 0) {
                $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                $src = Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/search.jpg';
            } else {
                $src = Yii::app()->baseUrl . '/images/defaultprofile.jpg';
            }

            $fav['url'] = $src;
            $fav['candidate_id'] = $candidate->id;
            $fav['candidate_name'] = $candidate->forenames . " " . $candidate->surname;
            $fav['current_job'] = $candidate->current_job;
            $fav['location'] = $candidate->location;

            $psmodel = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));

            $fav['skills'] = $psmodel;

            if (empty($fav)) {
                $this->_sendResponse(404, array());
            } else {
                $this->_sendResponse(200, $fav);
            }
        }
    }

    public function actionviewapplicants() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $empmodel = Employers::model()->findByPk($id);
        if ($id != $empmodel->id)
            $this->_sendResponse(404, array());

        $job = Jobs::model()->searchByEmployer($empmodel->id);


        if (empty($job)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $job);
        }
    }

    public function actionupdateapplicationstatus() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['application_id']) ? $_GET['application_id'] : null;
        $jobappmodel = JobApplications::model()->findByPk($id);
        if ($id != $jobappmodel->id)
            $this->_sendResponse(404, array());

        $value = $_POST['value'];

        $status = 0;
        if ($value == 'pending') {
            $status = 0;
        } else if ($value == 'shortlist') {
            $status = 1;
        } else if ($value == 'invite') {
            $status = 2;
        } else if ($value == 'interview') {
            $status = 3;
        } else if ($value == 'reject') {
            $status = -1;
        }

        $jobappmodel->status = $status;
        if ($jobappmodel->save())
            $this->_sendResponse(200, sprintf("Successfully updated", ""));
        else
            $this->_sendResponse(500, array());
    }

    public function actionupdateapplicationreject() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['application_id']) ? $_GET['application_id'] : null;
        $jobappmodel = JobApplications::model()->findByPk($id);
        if ($id != $jobappmodel->id)
            $this->_sendResponse(404, array());
        $msg = $_POST['msg'];

        $status = -1;

        $jobappmodel->status = $status;
        $jobappmodel->message = $msg;
        if ($jobappmodel->save())
            $this->_sendResponse(200, sprintf("Successfully rejected", ""));
        else
            $this->_sendResponse(500, array());
    }

    public function actionupdatejobinvite() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['application_id']) ? $_GET['application_id'] : null;
        $jobappmodel = JobApplications::model()->findByPk($id);
        if ($id != $jobappmodel->id)
            $this->_sendResponse(404, array());

        $interview_date = $_POST['interview_date'];
        $msg = $_POST['msg'];

        $status = 2;

        $jobappmodel->interview_date = $interview_date;
        $jobappmodel->status = $status;
        $jobappmodel->message = $msg;

        if ($jobappmodel->save())
            $this->_sendResponse(200, sprintf("Successfully sent interview invites", ""));
        else
            $this->_sendResponse(500, array());
    }

    public function actionupdateapplicationaccept() {
        $now = new DateTime();
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['application_id']) ? $_GET['application_id'] : null;
        $jobappmodel = JobApplications::model()->findByPk($id);
        if ($id != $jobappmodel->id)
            $this->_sendResponse(404, array());

        $msg = $_POST['msg'];

        $status = 4;

        $jobappmodel->status = $status;
        $jobappmodel->message = $msg;
        $jobappmodel->accepted_date = $now->format('Y-m-d H:i:s');
        if ($jobappmodel->save())
            $this->_sendResponse(200, sprintf("Successfully sent interview invites", ""));
        else
            $this->_sendResponse(500, array());
    }

    public function actionsearchcandidates() {

        $skillcategid = $_GET['skill_category'];
        $skillarr = isset($_GET['skills']) ? $_GET['skills'] : array();
        $location = $_GET['location'];
        $lat = $_GET['latitude'];
        $long = $_GET['longitude'];
        $radius = isset($_GET['radius']) ? $_GET['radius'] : 0;


        $skillids = implode(',', $skillarr);

        $cri = new CDbCriteria();
        $cri->alias = "p";
        $cri->distinct = true;
        $cri->join = "inner join cv16_users u on u.id = p.owner_id
                      inner join cv16_profile_skills ps on ps.profile_id = p.id
                      inner join cv16_skills s on s.id = ps.skill_id
                      inner join cv16_skill_categories sc on sc.id = s.skill_category_id";
        if ($skillcategid > 0)
            $cri->addCondition("s.skill_category_id =" . $skillcategid);
        if ($skillids != "")
            $cri->addCondition('ps.skill_id in (' . $skillids . ')', 'AND');
        if ($lat != "") {
            if ($radius > 0) {
                $cri->select = "(6371 * acos( cos( radians(" . $lat . ") ) * cos( radians(" . $lat . ") ) *
                                cos(radians(" . $long . ") - radians(" . $long . ")) + sin(radians(" . $lat . ")) *
                                sin(radians(" . $lat . ")) )) as distance";
// $cri->addCondition("HAVING distance < " . $radius);
                $cri->having = "distance < " . $radius;
            } else {
                $cri->addCondition("u.location = '" . $location . "'");
            }
        }
        $cri->group = "p.id";
        $pmodel = Profiles::model()->findAll($cri);
        if (!isset($pmodel[0]['id']))
            $pmodel = array();
        if (empty($pmodel)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $pmodel);
        }
    }

    public function actionemployeradduser() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['employer_id']) ? $_GET['employer_id'] : null;

        $empusermodel = new EmployerUsers;
        $usermodel = new Users;
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var))
                $usermodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }

        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->type = 'employer';
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->save();

//employer user update
        $empusermodel->employer_id = $id;
        $empusermodel->user_id = $usermodel->id;
        $empusermodel->role = 'employer';
        $empusermodel->created_at = $now->format('Y-m-d H:i:s');
        if ($empusermodel->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email,mobile';
            $cri->condition = "id = " . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(500, array());
        }
    }

    public function actionemployeredituser() {

//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['user_id']) ? $_GET['user_id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        $empusermodel = new EmployerUsers;

        $now = new DateTime();


//      insert user details
        $pwd = $usermodel->password;

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var))
                $usermodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }


        if ($_POST["password"] == "")
            $usermodel->password = $pwd;
        else
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email,mobile';
            $cri->condition = "id = " . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(500, array());
        }
    }

//    save message
    public function actionsavemessage() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        $fromuser = $_POST['user_id'];
        $touser = $_POST['msg_to'];
        $msg = $_POST['message'];
        $subject = $_POST['subject'];
        $now = new DateTime();

        $conmodel = new Conversations();
        $conmodel->subject = $subject;
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk($fromuser);
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = $fromuser;
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $msg;
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = $fromuser;
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($touser);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $touser;
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = $fromuser;
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                        $actmodel_sender->message = $msg;
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $touser;
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                            $actmodel_rec->message = $msg;
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            if ($actmodel_rec->save()) {
                                $this->_sendResponse(200, sprintf("Successfully sent Message", ""));
                            } else
                                $this->_sendResponse(500, array());
                        }
                    }
                }
            }
        }
    }

    public function actioninstitutioneditaccountinfo() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $insmodel = Institutions::model()->findByPk($id);
        if ($id != $insmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $pwd = $usermodel->password;

        $now = new DateTime();

        $usermodel = Users::model()->findByPk($insmodel->admin_id);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $id));
        $profilemodel = Profiles::model()->findByPk($pmodel->id);

        foreach ($put_vars as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "password" && $value == "") {
                    $usermodel->password = password_hash($value, 1, ['cost' => 10]);
                } else if ($var == "password" && $value != "") {
                    
                } else {
                    $usermodel->$var = $value;
                }
            } else if ($insmodel->hasAttribute($var)) {
                $insmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $insmodel->updated_at = $now->format('Y-m-d H:i:s');
        $insmodel->save();

        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($profilemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully updated account", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    public function actionaddteacher() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $insmodel = Institutions::model()->findByPk($id);
        if ($id != $insmodel->id)
            $this->_sendResponse(404, array());


        $now = new DateTime();
        $usermodel = new Users();
        $iumodel = new InstitutionUsers();

        foreach ($put_vars as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $usermodel->type = "teacher";
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
            $iumodel->institution_id = $id;
            $iumodel->user_id = $usermodel->id;
            $iumodel->role = $usermodel->type;
            $iumodel->created_at = $now->format('Y-m-d H:i:s');
            if ($iumodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,forenames,surname,email,mobile';
                $cri->condition = "id = " . $usermodel->id;
                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $users);
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    public function actioneditteacher() {
//           if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $iumodel = InstitutionUsers::model()->findByPk($id);
        if ($id != $iumodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $usermodel = Users::model()->findByPk($iumodel->user_id);
        $pwd = $usermodel->password;

        foreach ($put_vars as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }



        $iumodel->updated_at = $now->format('Y-m-d H:i:s');
        $iumodel->save();

        $usermodel->attributes = $_POST['Users'];
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($_POST['password'] == "")
            $usermodel->password = $pwd;
        else
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {

            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email,mobile';
            $cri->condition = "id = " . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(500, array());
        }
    }

    public function actionsaveClass() {

        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $model = Institutions::model()->findByAttributes(array('admin_id' => $usermodel->id));
        $id = $model->id;

        $now = new DateTime();
        $tgmodel = new TutorGroups();
        $tmodel = new Users();
        $iumodel = new InstitutionUsers();

        foreach ($put_vars as $var => $value) {
            if ($tgmodel->hasAttribute($var)) {
                $tgmodel->$var = $value;
            } else if ($tmodel->hasAttribute($var)) {
                $tmodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }


        if ($_POST['teacher_id'] <= 0) {
            $tmodel->type = "teacher";
            $tmodel->status = "active";
            $tmodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
            $tmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($tmodel->save()) {

                $iumodel->institution_id = $model->id;
                $iumodel->user_id = $tmodel->id;
                $iumodel->role = $tmodel->type;
                $iumodel->created_at = $now->format('Y-m-d H:i:s');
                $iumodel->save();

                $tgmodel->teacher_id = $tmodel->id;
            }
        }

        $grad_date = null;
        if ($_POST['graduation_date'] != "") {
// 	        $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
        }
        $tgmodel->institution_id = $id;
        $tgmodel->graduation_date = $grad_date;
        $tgmodel->created_at = $now->format('Y-m-d H:i:s');
        $tgmodel->account_limit = $_POST['allocated_students'];
        if ($tgmodel->save()) {
            $forenames_arr = $_POST['forenames'];
            array_pop($forenames_arr);
            $surname_arr = $_POST['surname'];
            array_pop($surname_arr);
            $email_arr = $_POST['email'];
            array_pop($email_arr);
            $password_arr = $_POST['password'];
            array_pop($password_arr);
            // $confirmpwd_arr = $_POST['confirmpassword'];
            // for($i = 0; $i < $_POST['allocated_students']; $i++)
            for ($i = 0; $i < count($forenames_arr); $i++) {
                $umodel = new Users();
                $umodel->forenames = $forenames_arr[$i];
                $umodel->surname = $surname_arr[$i];
                $umodel->password = $password_arr[$i];
                $umodel->email = $email_arr[$i];
                $umodel->type = "candidate";
                $umodel->status = "active";
                $umodel->created_at = $now->format('Y-m-d H:i:s');
                if ($umodel->save(false)) {
                    $pmodel = new Profiles();
                    $pmodel->owner_id = $umodel->id;
                    $pmodel->owner_type = "Candidate";
                    $pmodel->type = "candidate";
                    $pmodel->slug = $this->getValidatedSlug($umodel->forenames . "-" . $umodel->surname);
                    $pmodel->created_at = $now->format('Y-m-d H:i:s');
                    $pmodel->save(false);

                    $tgsmodel = new TutorGroupsStudents();
                    $tgsmodel->institution_id = $id;
                    $tgsmodel->tutor_group_id = $tgmodel->id;
                    $tgsmodel->student_id = $umodel->id;
                    $tgsmodel->graduation_date = $grad_date;
                    $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
                    $tgsmodel->save(false);
                }
            }
            $this->_sendResponse(200, sprintf("Successfully updated account", ""));
        }
    }

    public function actioneditclass() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $tgmodel = TutorGroups::model()->findByPk($id);
        if ($id != $tgmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $model = Institutions::model()->findByPk($tgmodel->institution_id);
        $id = $model->id;
        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($tgmodel->hasAttribute($var)) {
                $tgmodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }


        $grad_date = null;
        if ($_POST['graduation_date'] != "") {
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
        }
        // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");

        $tgmodel->attributes = $_POST['TutorGroups'];
        $tgmodel->graduation_date = $grad_date;
        $tgmodel->updated_at = $now->format('Y-m-d H:i:s');
        $tgmodel->account_limit = $_POST['allocated_students'];
        if ($tgmodel->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Successfully updated class", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }

    public function actionaddstudent() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['userid']) ? $_GET['userid'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        $now = new DateTime();

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }


        $tgsmodel = new TutorGroupsStudents();

        $umodel = new Users();

        foreach ($put_vars as $var => $value) {
            if ($umodel->hasAttribute($var)) {
                $umodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $insmodel = Institutions::model()->findByAttributes(array('admin_id' => $usermodel->id));
        $id = $insmodel->id;
        $tutorid = $_POST['tutorgroup_id'];

        $umodel->type = "candidate";
        $umodel->status = "active";
        $umodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $umodel->created_at = $now->format('Y-m-d H:i:s');
        if ($umodel->save()) {
            $pmodel = new Profiles();
            $pmodel->owner_id = $umodel->id;
            $pmodel->owner_type = "Candidate";
            $pmodel->type = "candidate";
            $pmodel->slug = $this->getValidatedSlug($umodel->forenames . "-" . $umodel->surname);
            $pmodel->created_at = $now->format('Y-m-d H:i:s');
            $pmodel->save();

            $grad_date = null;
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
// 	        if($_POST['graduation_date'] != "")
            // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");

            $tgsmodel->institution_id = $id;
            $tgsmodel->tutor_group_id = $tutorid;
            $tgsmodel->graduation_date = $grad_date;
            $tgsmodel->student_id = $umodel->id;
            $tgsmodel->created_at = $now->format('Y-m-d H:i:s');

            if ($tgmodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully added student", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    public function actioneditstudent() {
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['tutorgrpstudentid']) ? $_GET['tutorgrpstudentid'] : null;
        $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
        if ($id != $tgsmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
        $model = Institutions::model()->findByPk($tgsmodel->institution_id);
        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $pwd = $usermodel->password;

        $id = $tgsmodel->institution_id;
        $now = new DateTime();
        $tutorid = $tgsmodel->tutor_group_id;

        foreach ($put_vars as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }


        if (trim($_POST['password']) == "")
            $usermodel->password = $pwd;
        else
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $grad_date = null;
            if ($_POST['graduation_date'] != "") {
                $d = str_replace('/', '-', $_POST["graduation_date"]);
                $grad_date = date('Y-m-d', strtotime($d));
            }
            $tgsmodel->tutor_group_id = $tutorid;
            $tgsmodel->graduation_date = $grad_date;
            $tgsmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($tgsmodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully updated student", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    function resize_image($image, $image_type, $fullpath, $destpath, $width, $height) {
        $size_arr = getimagesize($fullpath);

        list($width_orig, $height_orig, $img_type) = $size_arr;
        $ratio_orig = $width_orig / $height_orig;

        $tempimg = imagecreatetruecolor($width, $height);
        imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        if ($image_type == "image/jpg" || $image_type == "image/jpeg")
            imagejpeg($tempimg, $destpath);
        else if ($image_type == "image/png")
            imagepng($tempimg, $destpath);
        else if ($image_type == "image/gif")
            imagegif($tempimg, $destpath);
    }

    private function _sendResponse($status = 200, $body = array(), $content_type = 'text/html') {
// set the status
// and the content type
        header('Content-type: ' . $content_type);

// this should be templated in a real-world solution
        $arr = array();
        $arr['status'] = $status;
        $arr['message'] = $this->_getStatusCodeMessage($status);
        $arr['data'] = $body;
        echo json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        Yii::app()->end();
    }

    private function _sendResponsewithMessage($status = 200, $message = '', $content_type = 'text/html') {
// set the status
// and the content type
        header('Content-type: ' . $content_type);

// this should be templated in a real-world solution
        $arr = array();
        $arr['status'] = $status;
        $arr['message'] = $message;
        echo json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status) {
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Invalid Authorization Credentials',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Email already Exists',
            503 => 'Invalid Email Error',
            504 => 'Invalid Password Error',
            505 => 'Passenger Details Missing',
            506 => 'Car Model Not Found',
            507 => 'No Results Found',
            508 => 'Upload failed',
            509 => 'Updated failed'
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    private function _checkAuth() {
// 		list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':' , base64_decode(substr($_SERVER['REDIRECT_REDIRECT_HTTP_AUTHORIZATION'], 6)));
// 			if (!isset($_SERVER['PHP_AUTH_USER']) && !isset($_SERVER['PHP_AUTH_PW'])) {
// 				return false;
// 			} 
// 			else {
        if ($_SERVER['PHP_AUTH_USER'] == 'cvvid' && $_SERVER['PHP_AUTH_PW'] == 'cvvid') {
            return true;
        } else
            return false;
//}    
// Check if we have the USERNAME and PASSWORD HTTP headers set?
// if(!(isset($_SERVER['HTTP_X_USERNAME']) and isset($_SERVER['HTTP_X_PASSWORD']))) {
// 	// Error: Unauthorized
// 	$this->_sendResponse(401);
// }
// $username = $_SERVER['HTTP_X_USERNAME'];
// $password = $_SERVER['HTTP_X_PASSWORD'];
// // Find the user
// $user = Member::model()->find('LOWER(username)=?',array(strtolower($username)));
// if($user===null) {
// 	// Error: Unauthorized
// 	$this->_sendResponse(401, 'Error: User Name is invalid');
// } else if(!$user->validatePassword($password)) {
// 	// Error: Unauthorized
// 	$this->_sendResponse(401, 'Error: User Password is invalid');
// }
    }

    public function getValidatedSlug($slug) {
        $cri = new CDbCriteria();
        $cri->condition = "slug ='" . $slug . "'";
        $pmodel = Profiles::model()->findAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "slug like '" . $slug . "-%'";
        $pmodel1 = Profiles::model()->findAll($cri1);

        if (count($pmodel1) == 0 && count($pmodel) == 1) {
            return strtolower($slug) . "-1";
        } else if (count($pmodel1) > 0) {
            return (strtolower($slug) . "-" . (count($pmodel1) + 1));
        } else if (count($pmodel) == 0)
            return strtolower($slug);
        else
            return (strtolower($slug) . "-" . rand());
    }

    /**
     * Returns the json or xml encoded array
     *
     * @param mixed $model
     * @param mixed $array Data to be encoded
     * @access private
     * @return void
     */
    private function _getObjectEncoded($model, $array) {
        if (isset($_GET['format']))
            $this->format = $_GET['format'];

        if ($this->format == 'json') {
            return
            ;
        } elseif ($this->format == 'xml') {
            $result = '<?xml version="1.0">';
            $result .= "\n<$model>\n";
            foreach ($array as $key => $value)
                $result .= "    <$key>" . utf8_encode($value) . "</$key>\n";
            $result .= '</' . $model . '>';
            return $result;
        } else {
            return;
        }
    }

// }}}
    
    
    
    
    
public function actiontestcreatecandidate() {
//        if (!$this->_checkAuth())
//            $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $now = new DateTime();


       // echo var_dump($_POST);die();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            }
          
            else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;  
            }
            else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;  
            }
//            else if ($var == 'ispremium') {
//                //  $this->_sendResponse(400, array());
//            } 
            else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');                               
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        // Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            $usermodel->type = 'candidate';
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
            $addressmodel->model_id = $usermodel->id;
            $addressmodel->model_type = "Candidate";
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
              if ($addressmodel->save()) {
                    $profilemodel->owner_type = "Candidate";
                    $profilemodel->owner_id = $usermodel->id;
                    $profilemodel->created_at = $usermodel->created_at;
                    $profilemodel->type = $usermodel->type;                  
                     if ($profilemodel->save()) {
                            $cri = new CDbCriteria();
                            $cri->select = 'id,forenames,surname,email,mobile';
                            $cri->condition = "id = " . $usermodel->id;
                            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                            $this->_sendResponse(200, $users);
                     }else{
                         echo 'profile';
                     }
              } else {
//                  echo $addressmodel->getErrors();
                  echo 'address';
              }
        }else {
            echo $usermodel->getErrors();
        }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
?>