<?php

class EmployerUsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
		);
	}
        
        /**
	 * Manages all models.
	 */
	public function actionsearchAdmin()
	{
	    $model=new EmployerUsers('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['employer']))
	    {
	        $model->employer_id = $_GET['employer'];	        
	    } 
	   
        $this->render('admin',array(
            'model'=>$model,'issearch'=>1));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
        public function actionEdit($id)
	{
            $usermodel= Users::model()->findByPk($id);
            $this->render('edit',array(
                    'usermodel'=>$usermodel,
            ));
	}
        
        public function actioneditUsers($id) {

           $empusermodel = new EmployerUsers;
           $usermodel = Users::model()->findByPk($id);

           $now = new DateTime();

    //      insert user details
           $usermodel->attributes = $_POST["Users"];
           $usermodel->updated_at = $now->format('Y-m-d H:i:s');

           $usermodel->save();

           $empusermodel->unsetAttributes();  // clear any default values
	   $this->render('admin',array(
		'model'=>$empusermodel,
           ));
            
        }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EmployerUsers;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmployerUsers']))
		{
			$model->attributes=$_POST['EmployerUsers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmployerUsers']))
		{
			$model->attributes=$_POST['EmployerUsers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioncreateEmployerUsers() {

            $empusermodel = new EmployerUsers;
            $usermodel = new Users;

            $now = new DateTime();

    //      insert user details
            $usermodel->attributes = $_POST["Users"];
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->type = 'employer';

            if ($usermodel->save()) {
                //employer user update
                $empusermodel->employer_id = $_POST['employer_id'];
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = Yii::app()->user->getState('role');
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                $empusermodel->save();
            }
           $empusermodel->unsetAttributes();  // clear any default values
	   $this->render('admin',array(
		'model'=>$empusermodel,
           ));
            
        }
        
        public function actionexportCSV()
	{
	    Yii::import('ext.ECSVExport');
	    $criteria = new CDbCriteria();
            $criteria->alias = "u";
	    $criteria->select = "CONCAT_WS(' ', u.forenames, u.surname) as name,u.email,u.created_at";
            $criteria->join = "inner join cv16_employer_users as eu on eu.user_id = u.id";
	    
	    // CActiveDataProvider
	    $dataProvider = new CActiveDataProvider('Users', array(
	        'criteria'=>$criteria,
	    ));
	    
	    $csv = new ECSVExport($dataProvider);
	    $content = $csv->toCSV();
	    Yii::app()->getRequest()->sendFile('Employer Users.csv', $content, "text/csv", false);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('EmployerUsers');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
//	public function actionAdmin()
//	{
//		$model=new EmployerUsers('search');
//		$model->unsetAttributes();  // clear any default values
//		if(isset($_GET['EmployerUsers']))
//			$model->attributes=$_GET['EmployerUsers'];
//
//		$this->render('admin',array(
//			'model'=>$model,
//		));
//	}
//        
         public function actionAdmin()
        {
            $model=new EmployerUsers('search');
            $empmodel = new Employers();
            $model->unsetAttributes();  // clear any default values
            $namesearch = 0;
            $fullname = '';

            if(isset($_GET['EmployerUsers']))
            {
                $model->attributes = $_GET['EmployerUsers'];
            }
            if(isset($_GET['Employers']))
            {
                $model->employer_id = $_GET['Employers']['id'];
                //echo var_dump($_GET['Employers']['id']);                die();
                $empmodel->id = $model->employer_id;
            }
            if(isset($_GET['fullname']) && $_GET['fullname']!='')
            {
               // die();
                $namesearch = 1;
                $fullname = $_GET['fullname'];
                $cri = new CDbCriteria();
                $cri->alias = "eu";
                $cri->join = "inner join cv16_users u on u.id = eu.user_id";
                $cri->condition = "concat_ws(' ',u.forenames,u.surname) like '%".$fullname."%'";
                $searchusers = EmployerUsers::model()->findAll($cri);  
                $userarr = array();
                foreach ($searchusers as $users)
                   $userarr[] = $users['user_id'];        

            }

               $this->render('admin',array(
                   'model'=>$model,'empmodel'=>$empmodel,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
               ));
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return EmployerUsers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=EmployerUsers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param EmployerUsers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='employer-users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
}
