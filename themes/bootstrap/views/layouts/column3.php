<?php $this->beginContent('//layouts/main'); ?>
<!-- <div class="container"> -->

			<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
			
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.easy-ticker.js"></script>
	<script>
		$(function(){
			$('.demo').easyTicker({
				direction: 'up', // up or down
				easing: 'swing', // easing options
				speed: 'slow', // animation speed
				interval: 2000,
				//height: 'auto',
				visible: 0, // change the number of default visible elements.
				mousePause: 1, // pause on hover
				controls: {
					up: '',
					down: '',
					toggle: '',
					playText: 'Play',
					stopText: 'Stop'
				}
			});

			
		
		});

		
</script>
	<div class="appcontent">

		<div class="row">
			<div class="span10">
				<div id="content">
				<?php echo $content; ?>
				</div>
			</div>
		 <div class="span3" style="margin-top: 30px;border: 1px solid lightgrey;margin-left:20px; max-height: 420px;height: auto;overflow: auto">
			<h3 style="background-color: lightgrey;padding: 5px">News and Events</h3>
			<div class="demo" >
				<ul style="list-style: none;">
				<?php 
				$news = News::model()->findAll();	
				
				  foreach ($news as $n){
				?>
				<li>
					<img src="<?php echo Yii::app()->request->baseUrl."/images/news/".$n['image'] ?>" alt="<?php echo $n['title']?>" style="width: 50px;height: 50px"/>
					<a href="#" style="text-decoration: none;"><?php echo $n['title']?></a>
					<p><?php echo $n['description']?></p>
				</li>
					<?php } ?>				
				</ul>
				</div>
			
		
				
			
			</div>
		</div>
	</div>
<!-- </div> --><!-- /container -->
<?php $this->endContent(); ?>