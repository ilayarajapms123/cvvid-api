<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	

	<!-- Le styles -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/application.min.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-responsive.css" rel="stylesheet">
 <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">

   <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.js"></script>
    <!-- Add custom CSS here -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/simple-sidebar.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet">


	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo Yii::app()->request->baseUrl; ?>/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::app()->request->baseUrl; ?>/images/apple-touch-icon-114x114.png">
 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	
	<!--  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.js"></script> -->
	
	
		<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />
<style type="text/css">

#mainmenu {
  margin: 0;
  padding: 0;
  list-style-type: none;
}
#mainmenu li {
  clear: left;
}
#mainmenu a {
  //display: block;
  overflow: hidden;
  //float: left;
}
#mainmenu a:hover,
#mainmenu li.active a {
  background-position: 0 0;
}
.submenu {
  list-style-type: none; 
  float: left;
  display: none;
}
#mainmenu li a:hover+.submenu, .submenu:hover {
  display: block;
} 
.submenu li {
  display: inline; 
  clear: none !important;
}
	</style>
</head>
<script type="text/javascript">


</script>
<body>
 <div id="header">
 <a href="<?php echo $this->createAbsoluteUrl('//'); ?>"><img src ="<?php echo Yii::app()->request->baseUrl; ?>/images/vdca_logo.jpg" style="float: left;"></img></a>
      <div class="navbar">
		<div class="navbar-inner">
			<div class="container" style="margin-left: 15%">
			<div align="center">
			<a class="brand" style="text-decoration: none;margin-left: 20%"><?php echo CHtml::encode(Yii::app()->name); ?></a>
			</div>
			<div class="content-header">
          <h1>
            <a id="menu-toggle" href="#" class="btn btn-default"><i class="icon-reorder"></i></a>
           
          </h1>
        </div>
				
				<ul class="nav"  id= 'sub-parent' style="margin-left:20px" >
					
				</ul>
				<?php $this->widget('zii.widgets.CMenu',array(
					'items'=>array(
						array('label'=>'<img src="'.Yii::app()->request->baseUrl.'/images/admin.png" /> '.Yii::app()->user->name, 'url'=>array('site/profile'),'encodeLabel'=>false, 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>'<img src="'.Yii::app()->request->baseUrl.'/images/logout.png" /> Logout', 'url'=>array('/site/logout'),'encodeLabel'=>false, 'visible'=>!Yii::app()->user->isGuest, 'htmlOptions'=>array('class'=>'btn'))
					),
					'htmlOptions'=>array(
						'class'=>'nav pull-right'
					),
				));?>
			</div>
		</div>
	</div> 
	
      </div>
     
    <div id="wrapper">
 
    <!-- Sidebar -->
      <div id="sidebar-wrapper">
      <nav class="navigation">
        <ul class="sidebar-nav" id = "mainmenu">
          <!-- <li class="sidebar-brand" style="margin-bottom: 10%;"><a href="#"><img src ="<?php echo Yii::app()->request->baseUrl; ?>/images/vdca_logo.jpg"></img></a></li> -->
          <li><a href="<?php echo $this->createUrl('/site/home')?>">Home</a></li>
          <li style="display: <?php echo (Yii::app()->user->getState('userid') ? "" : "none") ?>" ><a href="<?php echo $this->createUrl('/page/admin')?>">Pages</a><ul></ul></li>
          <li style="display: <?php echo (Yii::app()->user->getState('userid') ? "" : "none") ?>"><a href="<?php echo $this->createUrl('/association/admin')?>">Association</a></li>
          <li><a href="<?php echo $this->createUrl('/season/admin')?>">Seasons</a></li>
          <li ><a href="<?php echo (Yii::app()->user->getState('userid') || Yii::app()->user->getState('association_id') || Yii::app()->user->getState('team_id') ? $this->createUrl('/member/admin') : $this->createUrl('/member/index'))?>">Members</a></li>
          <li style="display: <?php echo (Yii::app()->user->getState('team_id') ? "none" : "") ?>"><a href="<?php echo $this->createUrl('/team/admin')?>">Teams</a></li>
          <li><a href="<?php echo $this->createUrl('/league/admin')?>">Leagues</a></li>
          <li><a href="<?php echo $this->createUrl('/fixture/admin')?>">Fixtures</a></li>
          <li style="display: <?php echo (Yii::app()->user->getState('userid')|| Yii::app()->user->getState('association_id') || Yii::app()->user->getState('team_id') ? "" : "none") ?>"><a href="<?php echo $this->createUrl('/site/matches')?>">Matches</a></li>
          <li><a href="<?php echo $this->createUrl('/results/admin')?>">Results</a></li>
          <li><a href="<?php echo $this->createUrl('/pointsTable/admin')?>">PointsTable</a></li>
          <li style="display: <?php echo (!Yii::app()->user->getState('userid') && !Yii::app()->user->getState('team_id') ? "" : "none") ?>"><a href="<?php echo $this->createUrl('/site/contact')?>">ContactUs</a></li>
          <li style="display: <?php echo (Yii::app()->user->getState('userid') ? "" : "none") ?>" ><a href="<?php echo $this->createUrl('/site/banners')?>">Banner</a></li>
          <li style="" ><a href="<?php echo $this->createUrl('/site/gallery')?>">Gallery</a></li>
          <li style="display: <?php echo (Yii::app()->user->getState('userid') ? "" : "none") ?>" ><a href="<?php echo $this->createUrl('/news/admin')?>">News & Events</a></li>
          
          <li style="display: <?php echo Yii::app()->user->getState('association_id') ? "" : "none" ?>" ><a href="<?php echo $this->createUrl('/fixture/manageCommentary')?>">Manage Commentary</a></li>
          <li style="display: <?php echo Yii::app()->user->getState('association_id') ? "" : "none" ?>" ><a href="<?php echo $this->createUrl('/fixture/manageVideo')?>">Manage Video</a></li>
          
        </ul>
        </nav>
      </div>
     
	
	
	<!-- <div class="container">
	<?php /*if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			'separator'=>' / ',
		)); ?><!-- breadcrumbs -->
	<?php endif*/ ?>
	</div> -->
	<div id="content">
	<?php echo $content; ?>
	</div>
	<footer class="footer">
		<div class="container">
			<p>Copyright &copy; <?php echo date('Y'); ?> by Villupuram District Cricket Association.<br/>
			All Rights Reserved.<br/>
			<?php //echo Yii::powered(); ?></p>
		</div>
	</footer> 
	</div>
	
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
    });
    $(document).ready(function(){
    	//alert('in');
    	  $.ajax
    	    ({ 
    	        type: 'POST',
    	        url: '<?php echo $this->createUrl("page/getPages")?>',
    	        //data: {},
    	        success: function(result)
    	        { 
    	          $('#sub-parent').append(result);
    	        },
    	        error: function(result)
    	        {
    				alert('error !!!');
    	        }
    	    });
    	
    });
    </script>
</body>
</html>