<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:100,300,400,500,700,800,900" rel="stylesheet">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/media.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>
<body>
   
	<header>
		<div class="">
			<a href="<?php echo $this->createAbsoluteUrl('//'); ?>" class="logo"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="CVVID"></a>
			<span class="menu-trigger"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/menu.png" alt=""></span>
			<nav class="fR">
				<span class="close-trigger"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/close.png" alt=""></span>
				
				<ul class="clearfix">
				
				 		 <li style="display:<?php echo Yii::app()->user->isGuest ? 'none' : '' ?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Candidates<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li><a href="<?php echo Yii::app()->createUrl('users/admin',array('trial'=>1)) ?>">Free Trial Users</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('users/admin',array('paid'=>1)) ?>">Paid Members</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('users/admin',array('basic'=>1)) ?>">Basic Members</a></li>
                            </ul>
                        </li>
                        
                         <li style="display:<?php echo Yii::app()->user->isGuest ? 'none' : '' ?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Institutions<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li><a href="<?php echo Yii::app()->createUrl('institutions/overview') ?>">Overview</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('institutions/admin') ?>">Admins</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('institutions/teachers') ?>">Teachers</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('institutions/tutorgroups') ?>">Tutor Groups</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('institutions/students') ?>">Students</a></li>
                            </ul>
                        </li>
                        
                         <li style="display:<?php echo Yii::app()->user->isGuest ? 'none' : '' ?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Site Admin<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                 <li><a href="<?php echo Yii::app()->createUrl('users/manageUsers') ?>">Manage Users</a></li>
                                 <li><a href="<?php echo Yii::app()->createUrl('pages/admin') ?>">Pages</a></li>
                                 <li><a href="<?php echo Yii::app()->createUrl('videos/admin') ?>">Videos</a></li>
                                 <li><a href="<?php echo Yii::app()->createUrl('slideshows/admin') ?>">Slideshows</a></li>
                            </ul>
                        </li>
                        
						<!-- <li style="display:<?php //echo Yii::app()->user->isGuest ? 'none' : '' ?>"><a href="<?php //echo Yii::app()->createUrl('users/admin')?>">Candidates</a></li> -->
				        <li style="display:<?php echo Yii::app()->user->isGuest ? 'none' : '' ?>"><a href="#">Welcome <?php echo Yii::app()->user->getState('username')?></a></li>
				        <li style="display:<?php echo Yii::app()->user->isGuest ? '' : 'none' ?>"><a href="#">Register</a></li>
                        <li style="display:<?php echo Yii::app()->user->isGuest ? '' : 'none' ?>"><a href="#">Login</a></li>
                         <?php if(!Yii::app()->user->isGuest){  ?>
                                <ul class="nav pull-right" id="yw0">
									<li><a href="<?php echo $this->createUrl('/site/logout')?>">Logout</a></li>
							    </ul>
						 <?php } ?>
                </ul>
			</nav>
		</div>
	</header>
	
    
      
	<?php echo $content; ?>
        
        <footer id="footer">
		<div class="container">
			Copyright © 2016 by CVVID All Rights Reserved.
		</div>
	</footer>
    
 
        
	
	
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
    });
    $(document).ready(function(){
   

    	$('#cover-sec').on('click','#cover-photo-upload',function(e){
    		 e.preventDefault();
    		 $(".cover-upload").trigger('click');
    	});
    	$('#colcover').on('change','.cover-upload',function(e){
        		var id = $(this).attr('id');
    		  
    		        var data = new FormData();
    		        jQuery.each(jQuery(this)[0].files, function(i, file) {
    		            data.append('cover', file);
    		        });
    		        data.append('profileid',id);
    		        $.ajax({
    		            url: '<?php echo $this->createUrl('users/saveCoverPhoto')?>',
    		            type: 'POST',
    		            data: data,
    		            cache: false,
    		            contentType: false,
    		            processData: false,    			    
    		            success: function(data) {
        		            if(data != '')
    		            		$('#img-cover').css('background-image','url('+data+')');
        		            else
            		            alert('upload failed');
    		            },
    				    error: function(data) {		
    				        alert('err');
    				    }
    		        });

    		     
   	    });

    	   $('a.dropdown-toggle').on('click', function(e) {
               var $el = $(this);
               var $parent = $(this).offsetParent(".dropdown-menu");
               $(this).parent("li").toggleClass('open');

//               if(!$parent.parent().hasClass('nav')) {
//                   $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
//               }

               $('li.open').not($(this).parents("li")).removeClass("open");

               return false;
           });
           
    });
    </script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->
    
   <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/build/global.min.js"></script>
      <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
   
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    
</body>
</html>
