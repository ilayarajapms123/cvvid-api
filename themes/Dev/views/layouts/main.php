<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:100,300,400,500,700,800,900" rel="stylesheet">

        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/media.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/successstories.scss">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/slideshow.scss">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/showcase.scss">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    </head>
    <body>

        <?php echo $content ?>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("active");
            });
            $(document).ready(function () {

                $('#cover-sec').on('click', '#cover-photo-upload', function (e) {
                    e.preventDefault();
                    $(".cover-upload").trigger('click');
                });
                $('#colcover').on('change', '.cover-upload', function (e) {
                    var id = $(this).attr('id');

                    var data = new FormData();
                    jQuery.each(jQuery(this)[0].files, function (i, file) {
                        data.append('cover', file);
                    });
                    data.append('profileid', id);
                    $.ajax({
                        url: '<?php echo $this->createUrl('users/saveCoverPhoto') ?>',
                        type: 'POST',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data != '')
                                $('#img-cover').css('background-image', 'url(' + data + ')');
                            else
                                alert('upload failed');
                        },
                        error: function (data) {
                            alert('err');
                        }
                    });


                });
                $('a.dropdown-toggle').on('click', function (e) {
                    var $el = $(this);
                    var $parent = $(this).offsetParent(".dropdown-menu");
                    $(this).parent("li").toggleClass('open');

                    //             if(!$parent.parent().hasClass('nav')) {
                    //                 $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
                    //             }

                    $('li.open').not($(this).parents("li")).toggleClass("open");

                    return false;
                });


            });
            $(function () {
                $('.data-toggle').click(function () {                	
                    
                    if($(this).closest('li').find('.dropdown-lv').hasClass('in'))
                   		 $(this).closest('li').find('.dropdown-lv').removeClass('in');
                    else
                    {
                    	$('.dropdown-lv').removeClass('in');
                    	$(this).closest('li').find('.dropdown-lv').addClass('in');
                    }
                });
                $('.navbar-toggle').click(function () {
                    $('.navbar-nav').toggleClass('slide-in');
                    $('.side-body').toggleClass('body-slide-in');
                    //$('#search').removeClass('in').addClass('collapse').slideUp(200);
                });
                $('.menu-trigger').click(function () {
                    $('.navbar-nav').toggle();
                });

            });

            $(document).ready(function () {

                $('.photo-section').on('click', '#remove-photo-btn', function (evt) {
                    $('#profile-pic').attr('src', '');
                    $('#photo_id').val('');
                    $(this).hide();
                });

                $('.modal-dialog').on('click', '.media_item', function (evt) {
                    $('div.selected').removeClass('selected');
                    $(this).addClass('selected');
                });

                $('.photo-section').on('click', '.add-media', function (evt) {
                    $('#mediaadmin_modal').addClass('in');
                    $('body').addClass('modal-open');
                    $('#mediaadmin_modal').show();
                });


                $('.modal-dialog').on('click', '#insert-media', function (evt) {

                    var link = $(".mediaadmin_item").find(".selected").find('img').attr('src');
                    var mediaid = $(".mediaadmin_item").find(".selected").find('.media_item_delete').attr('id');
                    $('#profile-pic').attr('src', link);
                    $('#photo_id').val(mediaid);
                    $('#remove-photo-btn').show();
                    closepopup();
                });

                $("#load").click(function () {

                    var total_row = document.getElementById("total-row").value;
                    var result_no = document.getElementById("result_no").value;
                    if (total_row < result_no) {
                        loadmore();
                    } else {
                        alert('No More Data..');
                        $('#load').hide();
                    }
                });


                $('.modal-dialog').on('click', '.medaiadmin_add', function (e) {
                    e.preventDefault();
                    $(".media-upload").trigger('click');
                });

                $('.modal-dialog').on('change', '.media-upload', function (e) {

                    var data = new FormData();
                    jQuery.each(jQuery(this)[0].files, function (i, file) {
                        data.append('media', file);
                    });
                    data.append('collection_name', 'photos');
                    // alert(JSON.stringify(data));
                    $.ajax({
                        url: '<?php echo $this->createUrl('media/insertMedia') ?>',
                        type: 'POST',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            //	                alert(data);
                            $("#result_para").load(location.href + " #result_para>*", "");
                            //$('#img-media').attr("src", data);
                            //	                $('#img-media').src('background-image', 'url(' + data + ')');
                        },
                        error: function (data) {
                            alert('err');
                        }
                    });

                    return false;
                });

                //  media_item_delete  
                $('.modal-dialog').on('click', '.media_item_delete', function (e) {

                    if (confirm('Are you sure you want to delete this item?')) {

                        var id = $(this).attr('id');

                        $.ajax({
                            url: '<?php echo $this->createUrl('media/deleteMedia'); ?>',
                            type: 'POST',
                            data: {id: id},
                            success: function (data) {
                                //alert(data);
                                $("#result_para").load(location.href + " #result_para>*", "");
                            },
                            error: function (data) {
                                alert('err');
                            }
                        });
                    }

                    return false;

                });

                $(".MediaAdmin__search").keyup(function() {
        
                    var val = $(this).val();

                    $.ajax({
                             url: '<?php echo $this->createUrl('media/getData'); ?>',
                             type: 'POST',
                             data: {val: val},
                             success: function (data) {
                                $('#result_para').html(data);
                             },
                             error: function (data) {
                                 alert('err');
                             }
                         });

                  });
                
                
            });

            function closepopup()
            {
                $('.modal').removeClass('in');
                $('body').removeClass('modal-open');
                $('.modal').hide();
            }


            function loadmore()
            {
                var val = document.getElementById("result_no").value;
                $.ajax({
                    type: 'post',
                    url: '<?php echo $this->createUrl('media/getLoadData') ?>',
                    data: {
                        getresult: val
                    },
                    success: function (response) {
                        var content = document.getElementById("result_para");
                        content.innerHTML = content.innerHTML + response;
                        // We increase the value by 20 because we limit the results by 20
                        document.getElementById("result_no").value = Number(val) + 20;
                    }
                });
            }

        </script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->

        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/build/slideshow.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/build/global.min.js"></script>
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

        <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAd4jLN8LdoltMtZJfGS3C8ZTgPL7LDtdw&sensor=false&libraries=places"></script>
        <script type="text/javascript">
            google.maps.event.addDomListener(window, 'load', function () {

                var input = document.getElementsByClassName('location-search');

                for (i = 0; i < input.length; i++) {
                    var places = new google.maps.places.Autocomplete(input[i]);
                }
                google.maps.event.addListener(places, 'place_changed', function () {
                    $('#Addresses_country').val('');
                    $('#Addresses_postcode').val('');
                    $('#Addresses_county').val('');
                    $('#Addresses_town').val('');
                    console.log(places.getPlace());
                    var place = places.getPlace();
                    var latitude = place.geometry.location.lat();
                    var longitude = place.geometry.location.lng();
                    for (var i = 0; i < place.address_components.length; i += 1) {
                        var addressObj = place.address_components[i];
                        for (var j = 0; j < addressObj.types.length; j += 1) {
                            console.log(addressObj);
                            if (addressObj.types[j] === 'country') {
                                $('#Addresses_country').val(addressObj.long_name); // confirm that this is the country name
                            }
                            if (addressObj.types[j] === "postal_code") {
                                $('#Addresses_postcode').val(addressObj.long_name); // confirm that this is the country name
                            }
                            if (addressObj.types[j] === "administrative_area_level_1") {
                                $('#Addresses_county').val(addressObj.long_name); // confirm that this is the country name
                            }
                            if (addressObj.types[j] === "locality") {
                                $('#Addresses_town').val(addressObj.long_name); // confirm that this is the country name
                            }
                        }
                    }
                    $("#location_lat").val(latitude);
                    $("#location_lng").val(longitude);
                });
            });
        </script>
    </body>



    <?php
    $this->beginWidget(
            'booster.widgets.TbModal', array('id' => 'mediaadmin_modal')
    );
    ?>

    <div class="modal-body">
        <form action="">
            <div class="mediaadmin_filters">
                <div class="form-inline">
                    <input type="text" name="search" class="form-control MediaAdmin__search" placeholder="Search...">
                    <div class="pull-right">
                        <input class="media-upload" type="file" style="display:none;"/>
                        <button type="button" class="medaiadmin_add btn btn-primary btn-sm dz-clickable"><i class="fa fa-plus"></i> Add Media</button>
                    </div>
                </div>
            </div>
            <div id="result_para" class="mediaadmin_item clearfix">
                <?php
                $cri = new CDbCriteria();
                $cri->order = 'order_column DESC';
                $cri->limit = 20;
                $cri->offset = 0;
                $model = Media::model()->findAll($cri);
                foreach ($model as $media) {
                    ?>
                    <div class="media_item">
                        <div class="media_item_inner">
                            <img id="img-media" src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/thumb.jpg" alt="<?php echo $media['name'] ?>" class="img-responsive img-thumbnail">
                            <button class="media_item_delete" id="<?php echo $media['id'] ?>" type="button"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="mediaadmin_actions clearfix">
                <input type="hidden" id="result_no" value="20">
                <input type="hidden" id="total-row" value="<?php echo Media::model()->count(); ?>">
                <button type="button" id="load" class="btn btn-primary btn-sm pull-right">Load More</button>
            </div>
        </form>
    </div>

    <div class="modal-footer">
        <?php
        $this->widget(
                'booster.widgets.TbButton', array(
            'label' => 'Close',
            'url' => '#',
            'htmlOptions' => array('data-dismiss' => 'modal', 'onclick' => 'closepopup()'),
                )
        );
        ?>
        <?php
        $this->widget(
                'booster.widgets.TbButton', array(
            'context' => 'primary',
            'label' => 'Insert Media',
            'url' => '#',
            'htmlOptions' => array('data-dismiss' => 'modal', 'id' => 'insert-media'),
                )
        );
        ?>

    </div>

    <?php $this->endWidget(); ?>

</html>
