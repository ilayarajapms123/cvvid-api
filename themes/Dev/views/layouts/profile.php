<?php /* @var $this Controller */ ?>
<header style="background-color: white" id="site-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-3 header_logo">
                <a href="<?php echo $this->createAbsoluteUrl('//'); ?>" class="logo" style="margin: 0"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png"  alt="CVVID"></a>
<!--                <span class="menu-trigger"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/menu.png" alt=""></span>
                <nav class="fR" style="padding-right: 50px">
                    <span class="close-trigger"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/close.png" alt=""></span>


                </nav>-->
            </div>
            <div class="text-right col-xs-9">
                <div class="header_button">
                    <?php
                    if (Yii::app()->user->getState('userid') > 0) {
                        $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
                        ?>
                    <div class="dropdown">
                            <a class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div><?php echo $usermodel->forenames . " " . $usermodel->surname ?> <span class="caret"></span></div>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                 <?php 
                                    if(Yii::app()->user->getState('role') == "candidate")
                                        $url = $this->createUrl('users/edit',array('id'=>Yii::app()->user->getState('userid')));
                                    else if( Yii::app()->user->getState('role') == "employer")
                                    {
                                        $empmodel = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
                                        $url = $this->createUrl('employers/employer',array('id'=>$empmodel->id));
                                    }
                                    else if(Yii::app()->user->getState('role') == "institution")
                                        $url = $this->createUrl('institutions/edit',array('id'=>Yii::app()->user->getState('userid')));
                                    else if(Yii::app()->user->getState('role') == "admin")
                                        $url = Yii::app()->createUrl('users/dashboard');
                                ?>
                                <li><a href="<?php echo $url ?>">Edit Profile</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('site/logout', array('basic' => 1)) ?>">Logout</a></li>
                            </ul>
                        </div>
                        
                    <?php } else { ?>   
                        <div class="dropdown">
                            <a class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div>Register <span class="caret"></span></div>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li><a href="<?php echo Yii::app()->createUrl('site/candidatememberships') ?>">Candidate</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('site/employermemberships') ?>">Employer</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('site/registerinstitution') ?>">Institution/School</a></li>
                            </ul>
                        </div>
                        <a class="btn btn-default" href="<?php echo Yii::app()->createUrl('site/login') ?>">Login</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    
    
    
<!--    <div class="">
        
        
    </div>-->
</header> 
<div style="background-color: #EBEBEB;min-height: 62px;">
    <div class="container">
        <div class="row">
            <span class="menu-trigger" ><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/menu.png" alt=""></span>
            <nav>
                <ul class="navbar-nav clearfix">
                    <li><a href="<?php echo Yii::app()->createUrl('site/home') ?>">Home</a></li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Job Seekers<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-level">
                            <li><a href="<?php echo Yii::app()->createUrl('site/about') ?>">Why CVVID?</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('site/toptips') ?>">Top Tips</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('site/howto') ?>">How To</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('site/worrylist') ?>">Worry List</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('site/videoshowcase') ?>">CV Video Showcase</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Employers<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-level">
                            <li><a href="<?php echo Yii::app()->createUrl('employers/about') ?>">Why CVVID?</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('employers/faqs') ?>">FAQs</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('site/videoshowcase') ?>">CV Video Showcase</a></li>
                        </ul>
                    </li>
                    
                    <li style="display:<?php echo Yii::app()->user->isGuest ? '' : 'none' ?>"><a href="<?php echo Yii::app()->createUrl('site/jobsearch') ?>">Search Jobs</a></li>
                    <?php if(Yii::app()->user->getState('role') == "employer") {?>
                    <li><a href="<?php echo Yii::app()->createUrl('site/candidatesearch') ?>">Search Candidates</a></li>
                    <?php } ?>
                    <li><a href="<?php echo Yii::app()->createUrl('site/contact') ?>">Contact Us</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<?php $this->beginContent('//layouts/main'); ?>

<div id="">
    <div id="colcover" style="display: <?php echo (isset($this->dashboard) && $this->dashboard > 0) ? "none" : "" ?>">
        <?php
        $employerid = isset($this->employerid) ? $this->employerid : 0;
        $userid = isset($this->userid) ? $this->userid : 0;
        $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
        $edit = isset($this->edit) ? $this->edit : 0;
        $this->renderPartial('//profiles/Cover', array('userid'=>$userid,'employerid'=>$employerid,'institutionid'=>$institutionid,'edit' => $edit));
        ?>
    </div>
    <div class="banner-title">
    </div>

    <section id="content-section">
        <div class="content-row"> 
            <div class="pull-top">

                <?php echo $content; ?>
            </div>
        </div>
    </section>
</div>  
<footer class="clearfix" id="footer">
    <ul style="padding-left: 0;list-style: none;margin-left: -5px;">
        <li style="display: inline-block;padding-left: 5px;padding-right: 5px;"><a href="#" style="color: white;">Terms and Conditions</a></li>
        <li style="display: inline-block;padding-left: 5px;padding-right: 5px;"><a href="#" style="color: white;">Privacy Policy</a></li>
        <li style="display: inline-block;padding-left: 5px;padding-right: 5px;"><a href="#" style="color: white;">Cookie Policy</a></li>
    </ul>

</div>
<?php $this->endContent(); ?>
