function screen(){
	var winH = $(window).height(),
		winW = $(window).width(),
		fH = $('footer').innerHeight();

	if(winW <= 1024){
		$('#banner .bg-cover').css({height:winH - fH});
	}else{
		$('#banner .bg-cover').css({height:winH});
	}

	if(winW <= 768){
		var imgUrl = $('#hero-banner').find('img').attr('src');
		$('#hero-banner').css('background-image', 'url(' + imgUrl + ')');
	}else{
		$('#hero-banner').css('background-image', 'inherit');
	}
}

$(function(){
	$('#banner').slick({
		autoplay: true,
    	autoplaySpeed: 5000,
		dots: false,
		infinite: true,
		draggable: false,
		swipe: true
	});

	$('.news-list li').matchHeight();
	$('.list-col li').matchHeight();

	$('.menu-trigger').on('click', function(){
		$('nav').addClass('active');
	});

	$('.close-trigger').on('click', function(){
		$('nav').removeClass('active');
	});

	$(window).scroll(function() {
    	var scroll = $(window).scrollTop();
		if(scroll > 200){
			$('header').addClass('fixed');
		}else{
			$('header').removeClass('fixed');
		}
    });

		//datepicker
		$(".datepicker").datepicker();

// Table
	$('table td').each(function(index, element) {
			var th = $(this).closest('table').find('tr:first-child th').eq($(this).index()).text();
	    $(this).attr('data-title', th);
	});
	$('.live-score-landing:first').show();
	$('.live-tab li').on('click', function(){
		if($(this).hasClass('active')==false){
				$('.live-tab li').removeClass('active');
				var id = $(this).data('id');
				$('.live-score-landing').hide();
				$(this).addClass('active');
				$('#'+id).fadeIn();
		}
	});

	screen();
});

if (!Modernizr.touch) {
	$(window).resize(function() {
	  screen();
	});
}
