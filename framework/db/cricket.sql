-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2016 at 06:50 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cricket`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`, `userid`) VALUES
('admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `association`
--

CREATE TABLE IF NOT EXISTS `association` (
  `association_id` bigint(20) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `mobile` int(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `association`
--

INSERT INTO `association` (`association_id`, `name`, `mobile`, `email`, `description`, `username`, `password`) VALUES
(1, 'VDCA', NULL, NULL, NULL, 'vdca', 'vdca'),
(2, 'TCA', NULL, NULL, NULL, 'tca', 'tca');

-- --------------------------------------------------------

--
-- Table structure for table `balls`
--

CREATE TABLE IF NOT EXISTS `balls` (
  `balls_id` bigint(20) NOT NULL,
  `results_id` bigint(20) NOT NULL,
  `team_id` bigint(20) NOT NULL,
  `batsman_id` bigint(20) NOT NULL,
  `bowler_id` bigint(20) NOT NULL,
  `over` varchar(50) DEFAULT NULL,
  `time` time DEFAULT NULL,
  `runs` int(20) NOT NULL,
  `noball` tinyint(1) NOT NULL DEFAULT '0',
  `wide` tinyint(1) NOT NULL DEFAULT '0',
  `others` int(1) NOT NULL,
  `commentary` varchar(2000) DEFAULT NULL,
  `video_url` varchar(1000) DEFAULT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `pitch_area` varchar(200) DEFAULT NULL,
  `additional_info` varchar(1000) DEFAULT NULL,
  `wicket` tinyint(1) NOT NULL DEFAULT '0',
  `howout` varchar(200) DEFAULT NULL,
  `fielder_id` bigint(20) NOT NULL,
  `extras` int(20) NOT NULL,
  `commentary_header` varchar(1000) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balls`
--

INSERT INTO `balls` (`balls_id`, `results_id`, `team_id`, `batsman_id`, `bowler_id`, `over`, `time`, `runs`, `noball`, `wide`, `others`, `commentary`, `video_url`, `image_url`, `pitch_area`, `additional_info`, `wicket`, `howout`, `fielder_id`, `extras`, `commentary_header`, `date_time`) VALUES
(28, 4, 0, 21, 41, '0.1', '02:45:36', 1, 0, 0, 0, '<p>etwtwtwtwtwt</p>\n', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to sahid afridi', NULL),
(29, 4, 0, 26, 41, '0.2', '02:45:49', 0, 0, 0, 0, NULL, 'http://www.youtube.com/watch?v=qD2olIdUGd8', NULL, NULL, NULL, 1, 'Bowled', 0, 0, 'srinath srinath to anwar anwar', NULL),
(30, 4, 0, 21, 41, '0.3', '03:49:24', 6, 0, 0, 0, '<p>test reqrqrqr</p>\n', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to sahid afridi', NULL),
(31, 4, 0, 24, 41, '0.4', '05:49:06', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to waq younis', NULL),
(32, 4, 0, 24, 41, '0.5', '05:49:14', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'LBW', 0, 0, 'srinath srinath to waq younis', NULL),
(33, 4, 0, 21, 41, '1.0', '05:53:05', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'LBW', 0, 0, 'srinath srinath to sahid afridi', NULL),
(34, 4, 0, 27, 13, '1.1', '05:59:07', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'virat kohli to malik malik', NULL),
(35, 4, 0, 27, 13, '1.2', '05:59:11', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'virat kohli to malik malik', NULL),
(36, 4, 0, 24, 13, '1.3', '05:59:17', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'Bowled', 0, 0, 'virat kohli to waq younis', NULL),
(37, 4, 0, 28, 13, '1.4', '06:11:33', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'virat kohli to akmal akmal', NULL),
(38, 4, 0, 28, 13, '1.5', '06:11:40', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'Bowled', 0, 0, 'virat kohli to akmal akmal', NULL),
(39, 4, 0, 29, 13, '2.0', '06:13:27', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'virat kohli to rizwan rizwan', NULL),
(40, 4, 0, 29, 37, '2.1', '06:13:36', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'vijay vijay to rizwan rizwan', NULL),
(41, 4, 0, 27, 37, '2.2', '06:13:41', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'Bowled', 0, 0, 'vijay vijay to malik malik', NULL),
(42, 4, 0, 25, 37, '2.3', '06:14:15', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'vijay vijay to sho ahktar', NULL),
(43, 4, 0, 29, 37, '2.4', '06:14:20', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'vijay vijay to rizwan rizwan', NULL),
(44, 4, 0, 25, 37, '2.5', '06:14:23', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'LBW', 0, 0, 'vijay vijay to sho ahktar', NULL),
(45, 4, 0, 30, 37, '3.0', '06:16:51', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'Caught', 0, 0, 'vijay vijay to rashid rashid', NULL),
(46, 5, 0, 11, 30, '0.1', '10:31:01', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'rashid rashid to sachin tendulkar', NULL),
(47, 5, 0, 11, 30, '0.2', '10:31:05', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'rashid rashid to sachin tendulkar', NULL),
(48, 5, 0, 12, 30, '0.3', '10:31:10', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'rashid rashid to sourav ganguly', NULL),
(49, 5, 0, 11, 30, '0.4', '10:31:14', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'rashid rashid to sachin tendulkar', NULL),
(50, 5, 0, 12, 30, '0.5', '10:31:20', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'LBW', 0, 0, 'rashid rashid to sourav ganguly', NULL),
(51, 5, 0, 13, 30, '1.0', '10:31:36', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'rashid rashid to virat kohli', NULL),
(52, 5, 0, 13, 21, '1.1', '10:35:46', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(53, 5, 0, 13, 21, '1.2', '10:35:48', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(54, 5, 0, 13, 21, '1.2', '10:35:49', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(55, 5, 0, 13, 21, '1.4', '10:35:54', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(56, 5, 0, 13, 21, '1.5', '10:36:33', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(57, 5, 0, 13, 21, '2.0', '11:21:50', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(58, 5, 0, 13, 22, '2.1', '11:22:02', 3, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(59, 5, 0, 11, 22, '2.2', '11:22:34', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(60, 5, 0, 11, 22, '2.3', '11:22:38', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(61, 5, 0, 11, 22, '2.4', '11:22:41', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(62, 5, 0, 11, 22, '2.5', '11:22:44', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(63, 5, 0, 11, 22, '3.0', '11:22:47', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(64, 5, 0, 11, 21, '3.1', '12:28:56', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to sachin tendulkar', NULL),
(65, 5, 0, 11, 21, '3.2', '12:30:06', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to sachin tendulkar', NULL),
(66, 5, 0, 11, 21, '3.3', '12:30:09', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to sachin tendulkar', NULL),
(67, 5, 0, 13, 21, '3.4', '12:30:11', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(68, 5, 0, 13, 21, '3.5', '12:30:12', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(69, 5, 0, 13, 21, '4.0', '12:30:16', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(70, 5, 0, 11, 22, '4.1', '12:31:12', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(71, 5, 0, 11, 22, '4.2', '12:31:15', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(72, 5, 0, 11, 22, '4.3', '12:31:19', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(73, 5, 0, 13, 22, '4.4', '12:31:20', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(74, 5, 0, 13, 22, '4.5', '12:31:21', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(75, 5, 0, 13, 22, '5.0', '12:31:23', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(76, 5, 0, 13, 21, '5.1', '12:31:31', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(77, 5, 0, 13, 21, '5.2', '12:40:06', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(78, 5, 0, 13, 21, '5.3', '12:40:07', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(79, 5, 0, 13, 21, '5.4', '12:40:08', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(80, 5, 0, 13, 21, '5.5', '12:40:09', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(81, 5, 0, 13, 21, '6.0', '12:40:11', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(82, 5, 0, 13, 22, '6.1', '12:50:35', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(83, 5, 0, 13, 22, '6.2', '12:50:37', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(84, 5, 0, 13, 22, '6.3', '12:50:38', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(85, 5, 0, 13, 22, '6.4', '12:50:39', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(86, 5, 0, 13, 22, '6.5', '12:50:40', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(87, 5, 0, 13, 22, '7.0', '12:50:41', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to virat kohli', NULL),
(88, 5, 0, 13, 21, '7.1', '12:51:16', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(89, 5, 0, 13, 21, '7.2', '12:51:17', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(90, 5, 0, 13, 21, '7.3', '12:51:18', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(91, 5, 0, 11, 21, '7.4', '12:51:19', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to sachin tendulkar', NULL),
(92, 5, 0, 11, 21, '7.5', '12:51:22', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to sachin tendulkar', NULL),
(93, 5, 0, 11, 21, '8.0', '12:51:25', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to sachin tendulkar', NULL),
(94, 5, 0, 13, 22, '8.1', '12:51:42', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'LBW', 0, 0, 'saq mustaq to virat kohli', NULL),
(95, 5, 0, 38, 22, '8.2', '04:33:00', 4, 0, 0, 0, '<p>over 8.2 comment test</p>\n', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to rahane rahane', NULL),
(96, 5, 0, 38, 22, '8.3', '04:34:37', 6, 0, 0, 0, '<p>asfag</p>\n', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to rahane rahane', NULL),
(97, 5, 0, 38, 22, '8.4', '04:35:00', 1, 0, 0, 0, '<p>agagag</p>\n', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to rahane rahane', NULL),
(98, 5, 0, 11, 22, '8.5', '04:37:21', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'Bowled', 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(99, 5, 0, 21, 41, '9.0', '12:47:31', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to sahid afridi', NULL),
(100, 5, 0, 21, 41, '9.1', '02:22:40', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to sahid afridi', NULL),
(101, 6, 0, 52, 11, '0.1', '05:25:24', 3, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sachin tendulkar to Klusner Klusner', NULL),
(102, 6, 0, 53, 11, '0.2', '05:25:26', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sachin tendulkar to Rhodes Rhodes', NULL),
(103, 6, 0, 52, 11, '0.3', '05:25:27', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sachin tendulkar to Klusner Klusner', NULL),
(104, 6, 0, 52, 11, '0.4', '05:25:29', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sachin tendulkar to Klusner Klusner', NULL),
(105, 6, 0, 53, 11, '0.5', '05:25:30', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sachin tendulkar to Rhodes Rhodes', NULL),
(106, 6, 0, 53, 11, '1.0', '05:25:32', 3, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sachin tendulkar to Rhodes Rhodes', NULL),
(107, 7, 0, 14, 41, '0.1', '05:28:41', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to Kallis Kallis', NULL),
(108, 7, 0, 14, 41, '0.2', '05:28:43', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to Kallis Kallis', NULL),
(109, 7, 0, 18, 41, '0.3', '05:28:44', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to AB D', NULL),
(110, 7, 0, 18, 41, '0.4', '05:28:45', 3, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to AB D', NULL),
(111, 7, 0, 14, 41, '0.5', '05:28:47', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to Kallis Kallis', NULL),
(112, 7, 0, 14, 41, '1.0', '05:28:50', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'srinath srinath to Kallis Kallis', NULL),
(113, 7, 0, 11, 58, '1.1', '05:31:53', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'pollock pollock to sachin tendulkar', NULL),
(114, 7, 0, 14, 58, '1.2', '05:31:55', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'pollock pollock to Kallis Kallis', NULL),
(115, 7, 0, 14, 58, '1.3', '05:31:57', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'pollock pollock to Kallis Kallis', NULL),
(116, 8, 0, 14, 40, '0.1', '05:34:25', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to Kallis Kallis', NULL),
(117, 8, 0, 14, 40, '0.2', '05:34:25', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to Kallis Kallis', NULL),
(118, 8, 0, 18, 40, '0.3', '05:34:26', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to AB D', NULL),
(119, 8, 0, 18, 40, '0.4', '05:34:27', 3, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to AB D', NULL),
(120, 8, 0, 14, 40, '0.5', '05:34:27', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to Kallis Kallis', NULL),
(121, 8, 0, 14, 40, '1.0', '05:34:28', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to Kallis Kallis', NULL),
(122, 9, 0, 14, 40, '0.1', '05:39:24', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to Kallis Kallis', NULL),
(123, 9, 0, 14, 40, '0.2', '05:39:25', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to Kallis Kallis', NULL),
(124, 9, 0, 14, 40, '0.3', '05:39:26', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to Kallis Kallis', NULL),
(125, 9, 0, 18, 40, '0.4', '05:39:27', 3, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to AB D', NULL),
(126, 9, 0, 14, 40, '0.5', '05:39:28', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to Kallis Kallis', NULL),
(127, 9, 0, 14, 40, '1.0', '05:39:30', 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'zaheer zaheer to Kallis Kallis', NULL),
(128, 9, 0, 11, 56, '1.1', '05:39:58', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'styen styen to sachin tendulkar', NULL),
(129, 9, 0, 11, 56, '1.2', '05:40:04', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'styen styen to sachin tendulkar', NULL),
(130, 9, 0, 11, 56, '1.3', '05:40:07', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'styen styen to sachin tendulkar', NULL),
(131, 9, 0, 11, 56, '1.4', '05:59:49', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'styen styen to sachin tendulkar', NULL),
(132, 10, 0, 11, 21, '0.1', '11:55:04', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to sachin tendulkar', NULL),
(133, 10, 0, 11, 21, '0.2', '11:55:13', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to sachin tendulkar', NULL),
(134, 10, 0, 11, 21, '0.3', '11:55:15', 3, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to sachin tendulkar', NULL),
(135, 10, 0, 12, 21, '0.4', '11:55:17', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 4, 'sahid afridi to sourav ganguly', NULL),
(136, 10, 0, 12, 21, '0.5', '11:55:23', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'Bowled', 0, 0, 'sahid afridi to sourav ganguly', NULL),
(137, 10, 0, 13, 21, '1.0', '11:55:33', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(138, 10, 0, 13, 21, '1.1', '11:55:34', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'sahid afridi to virat kohli', NULL),
(139, 10, 0, 11, 22, '1.2', '11:55:42', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(140, 10, 0, 11, 22, '1.3', '11:55:55', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to sachin tendulkar', NULL),
(141, 10, 0, 13, 22, '1.4', '11:55:59', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'Caught', 0, 0, 'saq mustaq to virat kohli', NULL),
(142, 10, 0, 37, 22, '1.5', '11:56:09', 6, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to vijay vijay', NULL),
(143, 10, 0, 37, 22, '2.0', '11:56:11', 4, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'saq mustaq to vijay vijay', NULL),
(144, 10, 0, 37, 22, '2.1', '11:56:14', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'LBW', 0, 0, 'saq mustaq to vijay vijay', NULL),
(145, 10, 0, 38, 22, '2.2', '11:56:23', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, 'Stumpings', 0, 0, 'saq mustaq to rahane rahane', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `banner_id` bigint(20) NOT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `name`) VALUES
(1, 'banner-6.jpg'),
(2, 'banner-5.jpg'),
(3, 'banner-6.jpg'),
(4, 'banner-5.jpg'),
(5, 'banner-6.jpg'),
(6, 'banner-5.jpg'),
(7, 'banner-6.jpg'),
(8, 'banner-5.jpg'),
(9, 'banner-6.jpg'),
(10, 'banner-5.jpg'),
(11, 'banner-6.jpg'),
(12, 'banner-5.jpg'),
(13, 'banner-6.jpg'),
(14, 'banner-5.jpg'),
(15, 'banner-6.jpg'),
(16, 'banner-5.jpg'),
(17, 'banner-6.jpg'),
(18, 'banner-5.jpg'),
(19, 'banner-6.jpg'),
(20, 'banner-5.jpg'),
(21, 'banner-6.jpg'),
(22, 'banner-5.jpg'),
(23, 'banner-6.jpg'),
(24, 'banner-5.jpg'),
(25, 'banner-6.jpg'),
(26, 'banner-5.jpg'),
(27, 'banner-6.jpg'),
(28, 'banner-5.jpg'),
(29, 'banner-6.jpg'),
(30, 'banner-5.jpg'),
(31, 'banner-6.jpg'),
(32, 'banner-5.jpg'),
(33, 'banner-6.jpg'),
(34, 'banner-5.jpg'),
(35, 'banner-6.jpg'),
(36, 'banner-5.jpg'),
(37, 'banner-6.jpg'),
(38, 'banner-5.jpg'),
(39, 'banner-6.jpg'),
(40, 'banner-5.jpg'),
(41, 'banner-6.jpg'),
(42, 'banner-5.jpg'),
(43, 'banner-6.jpg'),
(44, 'banner-5.jpg'),
(45, 'banner-6.jpg'),
(46, 'banner-5.jpg'),
(47, 'banner-6.jpg'),
(48, 'banner-5.jpg'),
(49, 'banner-1.jpg'),
(50, 'banner-1.jpg'),
(51, 'banner-1.jpg'),
(52, 'banner-1.jpg'),
(53, 'banner-1.jpg'),
(54, 'banner-1.jpg'),
(55, 'banner-1.jpg'),
(56, 'banner-1.jpg'),
(57, 'banner-1.jpg'),
(58, 'banner-1.jpg'),
(59, 'banner-1.jpg'),
(60, 'banner-1.jpg'),
(61, 'banner-1.jpg'),
(62, 'banner-1.jpg'),
(63, 'banner-1.jpg'),
(64, 'banner-1.jpg'),
(65, 'banner-1.jpg'),
(66, 'banner-1.jpg'),
(67, 'banner-1.jpg'),
(68, 'banner-1.jpg'),
(69, 'banner-1.jpg'),
(70, 'banner-1.jpg'),
(71, 'banner-1.jpg'),
(72, 'banner-1.jpg'),
(73, 'banner-1.jpg'),
(74, 'banner-1.jpg'),
(75, 'banner-1.jpg'),
(76, 'banner-1.jpg'),
(77, 'banner-1.jpg'),
(78, 'banner-1.jpg'),
(79, 'banner-1.jpg'),
(80, 'banner-1.jpg'),
(81, 'banner-1.jpg'),
(82, 'banner-1.jpg'),
(83, 'banner-1.jpg'),
(84, 'banner-1.jpg'),
(85, 'banner-1.jpg_15832'),
(86, 'banner-1.jpg_14026'),
(87, 'banner-1.jpg_4952'),
(88, 'banner-1.jpg_15524'),
(89, 'banner-1.jpg_7146'),
(90, '30179_banner-1.jpg'),
(91, '30102_banner-1.jpg'),
(92, '21409_banner-1.jpg'),
(93, '7618_banner-1.jpg'),
(94, '32528_banner-3.jpg'),
(95, '3332_banner-4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `batting_performance`
--

CREATE TABLE IF NOT EXISTS `batting_performance` (
  `batting_performance_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `results_id` bigint(20) NOT NULL,
  `bowledid` bigint(20) NOT NULL,
  `dismiss_type` varchar(2000) DEFAULT NULL,
  `dismiss_over` float(10,1) NOT NULL,
  `runs` int(20) NOT NULL,
  `balls` int(20) NOT NULL,
  `fours` int(20) NOT NULL,
  `sixes` int(20) NOT NULL,
  `fow` int(20) NOT NULL,
  `catchid` int(20) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `striker` tinyint(1) NOT NULL,
  `team_id` int(20) NOT NULL,
  `scores` varchar(2000) DEFAULT '0',
  `batting_order` int(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batting_performance`
--

INSERT INTO `batting_performance` (`batting_performance_id`, `member_id`, `results_id`, `bowledid`, `dismiss_type`, `dismiss_over`, `runs`, `balls`, `fours`, `sixes`, `fow`, `catchid`, `active`, `striker`, `team_id`, `scores`, `batting_order`) VALUES
(89, 11, 5, 22, 'Bowled', 8.4, 62, 18, 6, 5, 0, 0, 0, 0, 1, '0,4,1,3,4,4,6,4,6,6,4,1,6,4,1,2,2,6,1', 1),
(90, 12, 5, 30, 'LBW', 0.4, 1, 2, 0, 0, 0, 0, 0, 0, 1, '0,1,1', 2),
(91, 13, 5, 22, 'LBW', 8.0, 111, 30, 12, 9, 0, 0, 0, 0, 1, '0,4,4,0,0,0,6,4,1,2,4,1,2,6,6,6,4,4,4,4,6,4,6,4,6,4,6,4,6', 2),
(92, 20, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', NULL),
(93, 37, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', NULL),
(94, 38, 5, 0, NULL, 0.0, 11, 3, 1, 1, 0, 0, 1, 0, 1, '0,4,6', 2),
(95, 39, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', NULL),
(96, 40, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', NULL),
(97, 41, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', NULL),
(98, 42, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', NULL),
(99, 43, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', NULL),
(100, 21, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 1, 0, 2, NULL, 2),
(101, 22, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(102, 23, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 1, 0, 2, '0', 3),
(103, 24, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(104, 25, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(105, 26, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(106, 27, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(107, 28, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(108, 29, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(109, 30, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(110, 31, 5, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(129, 14, 9, 0, NULL, 0.0, 13, 5, 2, 0, 0, 0, 1, 0, 3, '0,4,2,3,4,2', 1),
(130, 18, 9, 0, NULL, 0.0, 3, 1, 0, 0, 0, 0, 1, 0, 3, '0,1', 2),
(131, 56, 9, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 3, '0', NULL),
(132, 11, 9, 0, NULL, 0.0, 22, 4, 1, 3, 0, 0, 1, 0, 1, '0,6,4,6,6', 0),
(133, 13, 9, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 1, 0, 1, '0', 1),
(134, 40, 9, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', NULL),
(135, 21, 10, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(136, 22, 10, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(137, 24, 10, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(138, 25, 10, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(139, 26, 10, 0, NULL, 0.0, 0, 0, 0, 0, 0, 0, 0, 0, 2, '0', NULL),
(140, 11, 10, 0, NULL, 0.0, 20, 5, 1, 2, 0, 21, 1, 0, 1, '0,6,4,6', 1),
(141, 12, 10, 21, 'Bowled', 0.3, 0, 1, 0, 0, 0, 0, 0, 0, 1, '0,3', 2),
(142, 13, 10, 22, 'Caught', 1.2, 10, 3, 1, 1, 0, 0, 0, 0, 1, '0,4,6,1', 2),
(143, 37, 10, 22, 'LBW', 1.4, 10, 3, 1, 1, 0, 0, 0, 0, 1, '0,6,4', 2),
(144, 38, 10, 22, 'Stumpings', 1.5, 0, 1, 0, 0, 0, 0, 0, 0, 1, '0', 2);

-- --------------------------------------------------------

--
-- Table structure for table `bowling_performance`
--

CREATE TABLE IF NOT EXISTS `bowling_performance` (
  `bowling_performance_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `overs` float(10,1) NOT NULL,
  `maidens` int(20) NOT NULL,
  `runs` int(20) NOT NULL,
  `wickets` int(20) NOT NULL,
  `results_id` bigint(20) NOT NULL,
  `extras` int(20) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `bowler` int(20) NOT NULL,
  `selected` tinyint(1) NOT NULL,
  `team_id` int(20) NOT NULL,
  `bowling_order` int(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bowling_performance`
--

INSERT INTO `bowling_performance` (`bowling_performance_id`, `member_id`, `overs`, `maidens`, `runs`, `wickets`, `results_id`, `extras`, `active`, `bowler`, `selected`, `team_id`, `bowling_order`) VALUES
(89, 11, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(90, 12, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(91, 13, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(92, 20, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(93, 37, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(94, 38, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(95, 39, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(96, 40, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(97, 41, 0.0, 0, 0, 0, 5, 0, 1, 0, 0, 1, 1),
(98, 42, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(99, 43, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 1, NULL),
(100, 21, 4.0, 0, 81, 0, 5, 0, 1, 0, 0, 2, NULL),
(101, 22, 3.5, 0, 93, 2, 5, 0, 1, 0, 0, 2, NULL),
(102, 23, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 2, NULL),
(103, 24, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 2, NULL),
(104, 25, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 2, NULL),
(105, 26, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 2, NULL),
(106, 27, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 2, NULL),
(107, 28, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 2, NULL),
(108, 29, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 2, NULL),
(109, 30, 1.0, 0, 11, 1, 5, 0, 1, 0, 0, 2, 1),
(110, 31, 0.0, 0, 0, 0, 5, 0, 0, 0, 0, 2, NULL),
(129, 14, 0.0, 0, 0, 0, 9, 0, 0, 0, 0, 3, NULL),
(130, 18, 0.0, 0, 0, 0, 9, 0, 0, 0, 0, 3, NULL),
(131, 56, 0.4, 0, 22, 0, 9, 0, 1, 0, 0, 3, 1),
(132, 11, 0.0, 0, 0, 0, 9, 0, 0, 0, 0, 1, NULL),
(133, 13, 0.0, 0, 0, 0, 9, 0, 0, 0, 0, 1, NULL),
(134, 40, 1.0, 0, 16, 0, 9, 0, 1, 0, 0, 1, 1),
(135, 21, 1.0, 0, 26, 1, 10, 3, 1, 0, 0, 2, 1),
(136, 22, 0.5, 0, 17, 1, 10, 0, 1, 1, 0, 2, NULL),
(137, 24, 0.0, 0, 0, 0, 10, 0, 0, 0, 0, 2, NULL),
(138, 25, 0.0, 0, 0, 0, 10, 0, 0, 0, 0, 2, NULL),
(139, 26, 0.0, 0, 0, 0, 10, 0, 0, 0, 0, 2, NULL),
(140, 11, 0.0, 0, 0, 0, 10, 0, 0, 0, 0, 1, NULL),
(141, 12, 0.0, 0, 0, 0, 10, 0, 0, 0, 0, 1, NULL),
(142, 13, 0.0, 0, 0, 0, 10, 0, 0, 0, 0, 1, NULL),
(143, 37, 0.0, 0, 0, 0, 10, 0, 0, 0, 0, 1, NULL),
(144, 38, 0.0, 0, 0, 0, 10, 0, 0, 0, 0, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fixture`
--

CREATE TABLE IF NOT EXISTS `fixture` (
  `fixture_id` bigint(20) NOT NULL,
  `league_id` bigint(20) NOT NULL,
  `team1` bigint(20) DEFAULT NULL,
  `team2` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `fixture_type` smallint(10) DEFAULT NULL,
  `venue` varchar(2000) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `inprogress` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fixture`
--

INSERT INTO `fixture` (`fixture_id`, `league_id`, `team1`, `team2`, `date`, `fixture_type`, `venue`, `description`, `inprogress`) VALUES
(5, 14, 1, 2, '0000-00-00', NULL, '', '', 1),
(9, 14, 3, 1, '0000-00-00', NULL, '', '', 1),
(10, 16, 2, 1, '0000-00-00', NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `gallery_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `team_id` int(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`gallery_id`, `name`, `team_id`) VALUES
(2, '26248_tca-small.png', 7),
(4, '20196_logo-admin.png', 8);

-- --------------------------------------------------------

--
-- Table structure for table `league`
--

CREATE TABLE IF NOT EXISTS `league` (
  `league_id` bigint(20) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `no_teams` int(20) DEFAULT NULL,
  `overs` int(20) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `teams` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `league`
--

INSERT INTO `league` (`league_id`, `name`, `description`, `no_teams`, `overs`, `start_date`, `end_date`, `teams`) VALUES
(14, 'League A', '', 3, 1, NULL, NULL, '1,2,3'),
(15, 'League B', '', 2, 10, NULL, NULL, '5,6'),
(16, 'Test League', '', 3, 5, NULL, NULL, '1,2,3');

-- --------------------------------------------------------

--
-- Table structure for table `league_team`
--

CREATE TABLE IF NOT EXISTS `league_team` (
  `league_team_id` bigint(20) NOT NULL,
  `league_id` bigint(20) NOT NULL,
  `team_id` bigint(20) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `member_id` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1201 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `league_team`
--

INSERT INTO `league_team` (`league_team_id`, `league_id`, `team_id`, `description`, `member_id`) VALUES
(1156, 14, 2, NULL, 21),
(1157, 14, 2, NULL, 22),
(1158, 14, 2, NULL, 23),
(1159, 14, 2, NULL, 24),
(1160, 14, 2, NULL, 25),
(1161, 14, 2, NULL, 26),
(1162, 14, 2, NULL, 27),
(1163, 14, 2, NULL, 28),
(1164, 14, 2, NULL, 29),
(1165, 14, 2, NULL, 30),
(1166, 14, 2, NULL, 31),
(1185, 14, 3, NULL, 14),
(1186, 14, 3, NULL, 18),
(1187, 14, 3, NULL, 56),
(1188, 14, 1, NULL, 11),
(1189, 14, 1, NULL, 13),
(1190, 14, 1, NULL, 40),
(1191, 16, 2, NULL, 21),
(1192, 16, 2, NULL, 22),
(1193, 16, 2, NULL, 24),
(1194, 16, 2, NULL, 25),
(1195, 16, 2, NULL, 26),
(1196, 16, 1, NULL, 11),
(1197, 16, 1, NULL, 12),
(1198, 16, 1, NULL, 13),
(1199, 16, 1, NULL, 37),
(1200, 16, 1, NULL, 38);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `member_id` bigint(20) NOT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `total_wickets` int(20) DEFAULT NULL,
  `team_id` bigint(20) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `home_phone` int(20) DEFAULT NULL,
  `work_phone` int(20) DEFAULT NULL,
  `mobile` int(20) DEFAULT NULL,
  `address` varchar(2000) DEFAULT NULL,
  `player_mode` varchar(200) DEFAULT NULL,
  `role` varchar(200) DEFAULT NULL,
  `top_batsman` int(20) DEFAULT NULL,
  `top_bowler` int(20) NOT NULL,
  `centuries` int(20) DEFAULT NULL,
  `half_centuries` int(20) DEFAULT NULL,
  `five_wickets` int(20) DEFAULT NULL,
  `season_score_cards` int(20) DEFAULT NULL,
  `past_season` int(20) DEFAULT NULL,
  `photo` varchar(2000) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `runs` int(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `first_name`, `last_name`, `dob`, `username`, `total_wickets`, `team_id`, `password`, `email`, `home_phone`, `work_phone`, `mobile`, `address`, `player_mode`, `role`, `top_batsman`, `top_bowler`, `centuries`, `half_centuries`, `five_wickets`, `season_score_cards`, `past_season`, `photo`, `active`, `runs`) VALUES
(11, 'sachin', 'tendulkar', '0000-00-00', 'sachin', NULL, 1, 'sachin', '', NULL, NULL, NULL, '', '', '1', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Koala.jpg', 1, NULL),
(12, 'sourav', 'ganguly', '0000-00-00', 'sourav', NULL, 1, 'sourav', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'logo_black_copy_1024-100x100.png', 1, NULL),
(13, 'virat', 'kohli', '2015-12-01', 'virat', NULL, 1, 'virat', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(14, 'Kallis', 'Kallis', '2015-01-06', 'Kallis', NULL, 3, 'Kallis', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(15, 'test1', 'test1', NULL, 'test1', NULL, 2, 'test1', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'plus.png', 1, NULL),
(16, 'test2', 'test2', NULL, 'test2', NULL, 2, 'test2', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'images.png', 1, NULL),
(17, 'test3', 'test3', NULL, 'test3', NULL, 2, 'test3', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'calendar.png', 1, NULL),
(18, 'AB', 'D', NULL, 'ABD', NULL, 3, 'ABD', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(19, 'surendar', 'kalyanam', '0000-00-00', 'surendar ', NULL, 3, 'password', 'dreamchaser6930@gmail.com', 2147483647, 2147483647, 2147483647, 'no: 3a, usman nagar,\r\nvikravandi', 'All-rounder', '0', 1, 1, 100, 100, 50, NULL, NULL, '', 1, NULL),
(20, 'Divya', 'Bharathi', NULL, 'admin', NULL, 1, 'admin', 'divyapmscreative@gmail.com', 2147483647, 2147483647, 2147483647, '1/7 Police station street,\r\nvikravandi,\r\nvillupuram.', 'badsman', 'badman', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(21, 'sahid', 'afridi', NULL, 'Afridi', NULL, 2, 'Afridi', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(22, 'saq', 'mustaq', NULL, 'saqlin', NULL, 2, 'saqlin', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(23, 'yousuf', 'you', NULL, 'you', NULL, 2, 'you', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(24, 'waq', 'younis', NULL, 'waqar', NULL, 2, 'waqar', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(25, 'sho', 'ahktar', NULL, 'ahktar', NULL, 2, 'ahktar', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(26, 'anwar', 'anwar', NULL, 'anwar', NULL, 2, 'anwar', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(27, 'malik', 'malik', NULL, 'malik', NULL, 2, 'malik', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(28, 'akmal', 'akmal', NULL, 'akmal', NULL, 2, 'akmal', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(29, 'rizwan', 'rizwan', NULL, 'rizwan', NULL, 2, 'rizwan', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(30, 'rashid', 'rashid', NULL, 'rashid', NULL, 2, 'rashid', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(31, 'umar', 'umar', NULL, 'umar', NULL, 2, 'umar', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(32, 'sarfraz', 'sarfraz', NULL, 'sarfraz', NULL, 2, 'sarfraz', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(33, 'imran', 'imran', NULL, 'imran', NULL, 2, 'imran', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(34, 'razaq', 'razaq', NULL, 'razaq', NULL, 2, 'razaq', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(35, 'farook', 'farook', NULL, 'farook', NULL, 2, 'farook', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(36, 'latif', 'latif', NULL, 'latif', NULL, 2, 'latif', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(37, 'vijay', 'vijay', NULL, 'vijay', NULL, 1, 'vijay', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(38, 'rahane', 'rahane', NULL, 'rahane', NULL, 1, 'rahane', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(39, 'rohit', 'rohit', NULL, 'rohit', NULL, 1, 'rohit', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(40, 'zaheer', 'zaheer', NULL, 'zaheer', NULL, 1, 'zaheer', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(41, 'srinath', 'srinath', NULL, 'srinath', NULL, 1, 'srinath', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(42, 'azhar', 'azhar', NULL, 'azhar', NULL, 1, 'azhar', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(43, 'ashwin', 'ashwin', NULL, 'ashwin', NULL, 1, 'ashwin', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(44, 'chopra', 'chopra', NULL, 'chopra', NULL, 1, 'chopra', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(45, 'kambli', 'kambli', NULL, 'kambli', NULL, 1, 'kambli', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(46, 'sunil', 'sunil', NULL, 'sunil', NULL, 1, 'sunil', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(47, 'vvs', 'vvs', NULL, 'vvs', NULL, 1, 'vvs', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(48, 'shiva', 'shiva', NULL, 'shiva', NULL, 1, 'shiva', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(49, 'yuvi', 'yuvi', NULL, 'yuvi', NULL, 1, 'yuvi', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(50, 'sehwag', 'sehwag', NULL, 'sehwag', NULL, 1, 'sehwag', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(51, 'Duminy', 'Duminy', NULL, 'Duminy', NULL, 3, 'Duminy', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(52, 'Klusner', 'Klusner', NULL, 'Klusner', NULL, 3, 'Klusner', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(53, 'Rhodes', 'Rhodes', NULL, 'Rhodes', NULL, 3, 'Rhodes', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(54, 'Smith', 'Smith', NULL, 'Smith', NULL, 3, 'Smith', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(55, 'Morkel', 'Morkel', NULL, 'Morkel', NULL, 3, 'Morkel', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(56, 'styen', 'styen', NULL, 'styen', NULL, 3, 'styen', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(57, 'donald', 'donald', NULL, 'donald', NULL, 3, 'donald', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(58, 'pollock', 'pollock', NULL, 'pollock', NULL, 3, 'pollock', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(59, 'philander', 'philander', NULL, 'philander', NULL, 3, 'philander', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(60, 'tahir', 'tahir', NULL, 'tahir', NULL, 3, 'tahir', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(61, 'amla', 'amla', NULL, 'amla', NULL, 3, 'amla', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(62, 'rahane', 'rahane', NULL, 'rahane', NULL, 1, 'rahane', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Penguins.jpg', 1, NULL),
(63, 'Ponting', 'Ponting', NULL, 'Ponting', NULL, 5, 'Ponting', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(64, 'sachin2', 'tendulkar2', '0000-00-00', 'sachin2', NULL, 2, 'sachin', '', NULL, NULL, NULL, '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'CLASS-100x100.png', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` bigint(20) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `title`, `description`, `image`) VALUES
(1, 'League A', 'India top the rank.', 'Penguins.jpg'),
(2, 'League B', 'test desc.', 'Koala.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `page_id` bigint(20) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` mediumtext
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `title`, `content`) VALUES
(1, 'TEST', '<p><img alt="" src="/yii/kcfinder/upload/images/author.png" style="height:256px; width:256px" />TEST</p>\r\n'),
(2, 'TEST 1', '<p><img alt="" src="/yii/kcfinder/upload/images/avatar%282%29.png" style="height:300px; width:300px" />TEST 1</p>\r\n'),
(3, 'TEST 3', '<p><img alt="" src="/yii/kcfinder/upload/images/download.jpg" style="height:100px; width:100px" />test 3</p>\r\n'),
(4, 'test 4', '<p><img alt="" src="/yii/kcfinder/upload/images/download1.jpg" style="height:100px; width:100px" />test 444</p>\r\n'),
(5, 'TEST WEB', '<table style="width:100%">\r\n  <tr>\r\n    <td>Jill</td>\r\n    <td>Smith</td> \r\n    <td>50</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Eve</td>\r\n    <td>Jackson</td> \r\n    <td>94</td>\r\n  </tr>\r\n</table>'),
(6, 'test tca', '<p>Texas Cricket Academy :: Information<!-- lightbox start --><!-- lightbox end --><!--CSS Styles--><!--Java Script--><!-- header Begin --><!-- banner Begin --></p>\r\n\r\n<p><a href="index.php"><img alt="Texas Cricket Academy" src="images/logo.png" /></a></p>\r\n\r\n<h1>Call us Now<br />\r\n04146 233424</h1>\r\n<!-- banner End --><!-- menu Begin -->\r\n\r\n<h1><a href="index.php">Home</a> | <a href="information.php?id=1">Shipping &amp; Returns</a> | <a href="information.php?id=2">Terms &amp; Conditions</a> | <a href="information.php?id=3">Fragrance Guide</a> | <a href="information.php?id=4">Sizing Chart</a> | <a href="contact-us.php">Contact Us</a> | <a href="sign-up.php">Sign Up</a></h1>\r\n<!-- menu End --><!-- shopping cart Begin -->\r\n\r\n<h1>Shopping Cart: 0 items</h1>\r\n<!-- shopping cart End --><!-- search End --><!-- header End --><!-- content Begin --><!-- fstCln Begin --><!-- category Begin -->\r\n\r\n<h1>Categories <img alt="" src="default/btn-minus.gif" /></h1>\r\n\r\n<h1><a href="products.php?categoryid=1&amp;xsquare=BUSINESS &amp; FINANCE">BUSINESS &amp; FINANCE (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=2&amp;xsquare=EDUCATION">EDUCATION (1)</a></h1>\r\n\r\n<p><a href="products.php?categoryid=13&amp;xsquare=EDUCATION School">School</a></p>\r\n\r\n<h1><a href="products.php?categoryid=3&amp;xsquare=BUY &amp; SELL">BUY &amp; SELL (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=4&amp;xsquare=JOBS">JOBS (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=5&amp;xsquare=ELECTRONICS">ELECTRONICS (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=6&amp;xsquare=FOOD &amp; DRINKS">FOOD &amp; DRINKS (2)</a></h1>\r\n\r\n<p><a href="products.php?categoryid=15&amp;xsquare=FOOD &amp; DRINKS cashew">cashew</a></p>\r\n\r\n<h1><a href="products.php?categoryid=7&amp;xsquare=MOTORS">MOTORS (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=8&amp;xsquare=PROPERTY">PROPERTY (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=9&amp;xsquare=SPORTS &amp; RECREATION">SPORTS &amp; RECREATION (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=10&amp;xsquare=SOFTWARE">SOFTWARE (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=11&amp;xsquare=TRAVEL">TRAVEL (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=12&amp;xsquare=SHOPPING">SHOPPING (0)</a></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><a href="products.php?categoryid=16&amp;xsquare=cricket bat">cricket bat (1)</a></h1>\r\n\r\n<p><a href="products.php?categoryid=17&amp;xsquare=cricket bat BDM Cricket Bat">BDM Cricket Bat</a></p>\r\n\r\n<h1><a href="products.php?categoryid=18&amp;xsquare=Tamil">Tamil (2)</a></h1>\r\n\r\n<p><a href="products.php?categoryid=19&amp;xsquare=Tamil Comic">Comic</a>, <a href="products.php?categoryid=20&amp;xsquare=Tamil Thriller">Thriller</a></p>\r\n<!-- category End --><!-- information Begin -->\r\n\r\n<h1>Information <img alt="" src="default/btn-minus.gif" /></h1>\r\n\r\n<ul><!--\r\n<li><a href="#">Shipping & Returns</a></li>\r\n<li><a href="#">Terms & Conditions</a></li>\r\n<li><a href="#">Fragrance Guide</a></li>\r\n<li><a href="#">Sizing Chart</a></li>\r\n<li><a href="#">Contact Us</a></li>\r\n-->\r\n	<li><a href="information.php?id=1">Shipping &amp; Returns</a></li>\r\n	<li><a href="information.php?id=2">Terms &amp; Conditions</a></li>\r\n	<li><a href="information.php?id=3">Fragrance Guide</a></li>\r\n	<li><a href="information.php?id=4">Sizing Chart</a></li>\r\n</ul>\r\n<!-- information End --><!-- advice Begin -->\r\n\r\n<p><a href="advice-for-safe-shopping.php"><img alt="Advice for Safe Shopping" src="images/advice-safe-shopping.gif" /></a></p>\r\n<!-- advice End --><!-- fstCln End --><!-- sndCln Begin --><!-- nav Begin --><!-- <h1>Navigation: <a href="#">Top</a> ? <a href="#">Catalog</a> ? <a href="#">Gift Sets</a></h1> -->\r\n\r\n<h1>Navigation: <a href="index.php">Home</a> ? Information</h1>\r\n<!-- nav End -->\r\n\r\n<h1>Shipping &amp; Returns</h1>\r\n\r\n<p><strong>DELIVERY INFORMATION</strong></p>\r\n\r\n<p>We aim to dispatch all orders within 24 hours of receiving the order, but please allow 4-5 working days for delivery (UK ONLY). If items are out of stock, you shall be notified by phone or e-mail. We use a reliable delivery service with full tracking details available. A signature will be required on delivery. We will only deliver to the address where the card used for payment is registered.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Shipping will be ?4.99 flat for any number of items. Option for next day delivery will also be there for ?15.</p>\r\n\r\n<p><br />\r\n<strong>POSTAGE UK:</strong></p>\r\n\r\n<p>A maximum of ?5.00 with in UK surcharge to allow for the cost of delivery. This is the same no matter how large the order is.</p>\r\n\r\n<p>Note: If you do not receive the order within 5 days please inform us.</p>\r\n\r\n<p>Orders from any of these above mentioned countries has to be emailed to us on info@xsquare.co.uk or give us a call and will reserve your order with us and will email or give the order number.</p>\r\n\r\n<p><br />\r\n<strong>Note: If you do not receive the order with in 6 - 8 days please inform us.<br />\r\n** For European order there is no &quot; Refund or Exchange &quot;.<br />\r\n** We only accept money in pounds (?) whatever the total amount comes including postal charges.</strong><br />\r\n<br />\r\n<strong>RETURNS POLICY</strong></p>\r\n\r\n<p>Customers have cancellation rights of 7 working days starting from day after the goods are delivered.If an item is unsuitable, Please ensure the goods have been returned unworn/unwashed for a refund within 7 days of receipt. For an exchange goods should be returned within 28 days of receipts, unworn/unwashed. Jewellery items can be returned for an exchange/refund. Please ensure the jewellery item have been returned unworn for a refund within 7 days of receipt. For an exchange jewellery item should be returned within 28 days of receipts, unworn. If order in payable by cheque, then refund will be made by cheque within 28 days.</p>\r\n\r\n<p>Return address: <strong>Xsquare UK Limited, PO Box 8966, Leicester, LE41 9ET</strong>.</p>\r\n<!-- sndCln End --><!-- trdCln Begin --><!-- members login Begin -->\r\n\r\n<h1>Members Login</h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="sign-up.php">New to Villupuram Properties</a> | <a href="forgot-password.php">Forgot Password</a></p>\r\n<!-- members login End --><!-- review Begin -->\r\n\r\n<h1>Shopping Cart <a href="view-cart.php"><img alt="Cart Details" src="images/arrow-right.gif" /></a></h1>\r\n\r\n<p>0 items</p>\r\n<!-- review End --><!-- cards Begin -->\r\n\r\n<h1>Cards Accepted</h1>\r\n\r\n<p><img alt="Cards Accepted" src="images/cards.jpg" /></p>\r\n<!-- cards End --><!-- trdCln End --><!-- content End --><!-- footer Begin -->\r\n\r\n<p>Copyright 2015 &copy; Villupuram Properties. All Rights Reserved.<br />\r\n<a href="index.php">Home</a> | <a href="information.php?id=1">Shipping &amp; Returns</a> | <a href="information.php?id=2">Terms &amp; Conditions</a> | <a href="information.php?id=3">Fragrance Guide</a> | <a href="information.php?id=4">Sizing Chart</a> | <a href="contact-us.php">Contact Us</a></p>\r\n<!-- footer End -->');

-- --------------------------------------------------------

--
-- Table structure for table `points_table`
--

CREATE TABLE IF NOT EXISTS `points_table` (
  `points_table_id` bigint(20) NOT NULL,
  `league_id` bigint(20) NOT NULL,
  `team_id` bigint(20) NOT NULL,
  `played` int(20) NOT NULL,
  `won` int(20) NOT NULL,
  `loss` int(20) NOT NULL,
  `draw` int(20) NOT NULL,
  `points` int(20) NOT NULL,
  `noresults` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE IF NOT EXISTS `results` (
  `results_id` bigint(20) NOT NULL,
  `fixture_id` bigint(20) NOT NULL,
  `team1_score` int(20) NOT NULL,
  `team2_score` int(20) NOT NULL,
  `toss` int(20) NOT NULL,
  `match_result` varchar(2000) DEFAULT NULL,
  `toss_decision` varchar(30) NOT NULL,
  `team_won` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`results_id`, `fixture_id`, `team1_score`, `team2_score`, `toss`, `match_result`, `toss_decision`, `team_won`) VALUES
(5, 5, 0, 0, 1, NULL, '1', NULL),
(9, 9, 0, 0, 1, NULL, '1', NULL),
(10, 10, 0, 0, 1, NULL, '2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `role_id` bigint(20) NOT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `name`) VALUES
(1, 'Admin'),
(2, 'Association'),
(3, 'Team');

-- --------------------------------------------------------

--
-- Table structure for table `season`
--

CREATE TABLE IF NOT EXISTS `season` (
  `season_id` bigint(20) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `current` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `season`
--

INSERT INTO `season` (`season_id`, `name`, `current`) VALUES
(1, '2015', 0),
(2, '2015-2016', 1);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `team_id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `season_id` bigint(20) DEFAULT NULL,
  `description` int(11) DEFAULT NULL,
  `email` int(11) DEFAULT NULL,
  `mobile` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `no_players` int(20) DEFAULT NULL,
  `team_logo` varchar(2000) DEFAULT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`team_id`, `name`, `season_id`, `description`, `email`, `mobile`, `active`, `no_players`, `team_logo`, `username`, `password`) VALUES
(1, 'India', 2, NULL, NULL, NULL, 1, NULL, NULL, 'india', 'india'),
(2, 'Pakistan', 2, NULL, NULL, NULL, 1, NULL, NULL, 'pakistan', 'pakistan'),
(3, 'SouthAfrica', 2, NULL, NULL, NULL, 1, NULL, NULL, 'southafrica', 'southafrica'),
(5, 'Australia', 1, NULL, NULL, NULL, 1, NULL, NULL, 'australia', 'australia'),
(6, 'England', 2, NULL, NULL, NULL, 1, NULL, 'tn_logo.gif', 'england', 'england');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `association`
--
ALTER TABLE `association`
  ADD PRIMARY KEY (`association_id`);

--
-- Indexes for table `balls`
--
ALTER TABLE `balls`
  ADD PRIMARY KEY (`balls_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `batting_performance`
--
ALTER TABLE `batting_performance`
  ADD PRIMARY KEY (`batting_performance_id`),
  ADD KEY `member_id` (`member_id`) USING BTREE,
  ADD KEY `results_id` (`results_id`);

--
-- Indexes for table `bowling_performance`
--
ALTER TABLE `bowling_performance`
  ADD PRIMARY KEY (`bowling_performance_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `results_id` (`results_id`);

--
-- Indexes for table `fixture`
--
ALTER TABLE `fixture`
  ADD PRIMARY KEY (`fixture_id`),
  ADD KEY `league_id` (`league_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `league`
--
ALTER TABLE `league`
  ADD PRIMARY KEY (`league_id`);

--
-- Indexes for table `league_team`
--
ALTER TABLE `league_team`
  ADD PRIMARY KEY (`league_team_id`),
  ADD KEY `league_id` (`league_id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `points_table`
--
ALTER TABLE `points_table`
  ADD PRIMARY KEY (`points_table_id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`results_id`),
  ADD UNIQUE KEY `fixture_id` (`fixture_id`) USING BTREE;

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`season_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`team_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `association`
--
ALTER TABLE `association`
  MODIFY `association_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `balls`
--
ALTER TABLE `balls`
  MODIFY `balls_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=146;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `batting_performance`
--
ALTER TABLE `batting_performance`
  MODIFY `batting_performance_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `bowling_performance`
--
ALTER TABLE `bowling_performance`
  MODIFY `bowling_performance_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `fixture`
--
ALTER TABLE `fixture`
  MODIFY `fixture_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `league`
--
ALTER TABLE `league`
  MODIFY `league_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `league_team`
--
ALTER TABLE `league_team`
  MODIFY `league_team_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1201;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `points_table`
--
ALTER TABLE `points_table`
  MODIFY `points_table_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `results_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `season`
--
ALTER TABLE `season`
  MODIFY `season_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `team_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
